﻿using Dapper;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using Context = System.Runtime.Remoting.Contexts.Context;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Policy;
using System.Web.Mvc;
using PagedList;
using Remotion.FunctionalProgramming;
using SchoolRecognitionSystem.Controllers;
using UrlHelper = System.Web.Http.Routing.UrlHelper;
using System.Threading.Tasks;

namespace SchoolRecognitionSystem.Classes
{
    public class clsContent
    {
        public IDbConnection otCon;
        //To Handle connection related activities
        private static IDbConnection OpenConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["myDapperConnection"].ToString();
            return new SqlConnection(constr);
        }

        public static List<SchoolSubject> AddNewSchoolsubject2(List<SubjectModel> schoolsubject, long schoolids, bool IsActive, int AdequateEquipment, int AdequateReagent, int AdequateLaboratory, int QualifiedStaff, int ExaminationMalpractice)
        {
            var retvalue = new List<SchoolSubject>();
            var schsubobject = new SchoolSubject();

            try
            {

                using (IDbConnection _db = OpenConnection())
                {
                    foreach (var s in schoolsubject)
                    {
                        Subject xc = _db.Query<Subject>("dbo.GetCouncilSubjects", commandType: CommandType.StoredProcedure).FirstOrDefault(k => k.ID == s.ID);
                        schsubobject = _db.Query<SchoolSubject>("dbo.NewSchoolSubjects", new { SubjectID = xc.ID, SchoolID = schoolids, IsActive = IsActive, AdequateEquipment = AdequateEquipment, AdequateReagent = AdequateReagent, AdequateLaboratory = AdequateLaboratory, QualifiedStaff = QualifiedStaff, ExaminationMalpractice = ExaminationMalpractice }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        retvalue.Add(schsubobject);
                        //sch.Subjects.Add(Subject);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }
        public static List<SchoolSubject> AddASchoolsubject2(SubjectModel schoolsubject, long schoolids, bool IsActive, int AdequateEquipment, int AdequateReagent, int AdequateLaboratory, int QualifiedStaff, int ExaminationMalpractice)
        {
            var retvalue = new List<SchoolSubject>();
            var schsubobject = new SchoolSubject();

            try
            {

                using (IDbConnection _db = OpenConnection())
                {
                    Subject xc = _db.Query<Subject>("dbo.GetCouncilSubjects", commandType: CommandType.StoredProcedure).FirstOrDefault(k => k.ID == schoolsubject.ID);
                    schsubobject = _db.Query<SchoolSubject>("dbo.NewSchoolSubjects", new { SubjectID = xc.ID, SchoolID = schoolids, IsActive = IsActive, AdequateEquipment = AdequateEquipment, AdequateReagent = AdequateReagent, AdequateLaboratory = AdequateLaboratory, QualifiedStaff = QualifiedStaff, ExaminationMalpractice = ExaminationMalpractice }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    retvalue.Add(schsubobject);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }

        public static List<SchoolStaffModel> GetStaffNamebyschool(long schoolid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffNamebyschool",
                        new
                        {
                            SCHOOLID = schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static vmSchoolStaffQualification GetStaffByAStaffID2(int staffID)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<vmSchoolStaffQualification>("dbo.GetStaffByStaffID",
                        new
                        {
                            staffid = staffID

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        public static SchoolStaffModelEdit GetStaffByAStaffID(int staffID)
        {
            var result = new SchoolStaffModelEdit();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModelEdit>("dbo.GetStaffByStaffID",
                        new
                        {
                            staffid = staffID

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        public static SchoolStaffModel GetStaffByStaff(int staffID)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffByStaffID",
                        new
                        {
                            staffid = staffID

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        //GetSchoolStaffForEdit
        public static vmSchoolStaffForEdit GetSchoolStaffForEdit(int staffID, long schoolid)
        {
            var result = new vmSchoolStaffForEdit();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<vmSchoolStaffForEdit>("dbo.GetSchoolStaffForEdit",
                        new
                        {
                            SchoolStaffID = staffID,
                            SchoolID = schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        public static vmSchoolStaffQualification GetStaffByStaffId(long staffID)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<vmSchoolStaffQualification>("dbo.GetStaffByStaffID",
                        new
                        {
                            staffid = staffID

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetSubjectByStaffSchoolID(long? schoolid, int? staffid)
        {

            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetSubjectByStaffSchoolID",
                        new
                        {
                            StaffID = staffid,
                            SchoolID = schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        //GetSubjectByStaffSchoolID
        public static List<SchoolStaffModel> GetStaffBySchoolId(long schoolid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffBySchoolId",
                        new
                        {
                            SCHOOLID = schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static List<SchoolStaffModel> GetStaffOtherDegreeBySchoolID(long schoolId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffOtherDegreeBySchoolID",
                        new
                        {
                            SchoolID = schoolId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        //GetStaffSubjectClassTaughtByStaffID
        public static List<SchoolStaffModel> GetStaffSubjectClassTaughtByStaffID(long schoolid, int staffid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectClassTaughtByStaffID",
                        new
                        {
                            StaffID = staffid,
                            SchoolID = schoolid
                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static List<SchoolStaffModel> GetStaffOtherDegreeByStaffId(int staffid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffOtherDegreeByStaffId",
                        new
                        {
                            StaffID = staffid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        //
        public static SchoolStaffModel DeleteStaffCredentials(int credentialsID)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.DeleteStaffCredentials",
                        new
                        {
                            imageId = credentialsID

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        public static int? DeleteStaffCredentialsByStaffID(int staffID)
        {
            int? result;
             using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<int>("dbo.DeleteStaffCredentialsByStaffID",
                            new
                            {
                                StaffID = staffID

                            }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }

        public static SchoolStaffModel GetAStaffCredential(int imageid)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetAStaffCredential",
                        new
                        {
                            imageid = imageid

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }

        public static List<SubjectModel> SearchSchoolSubjectByName(string longName,string shortname,long schoolid)
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SubjectModel>("dbo.SearchSchoolSubjectByName",
                        new
                        {
                            LongName = longName,
                            ShortName=shortname,
                            SchoolID=schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetStaffCredentialss(int staffid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffCredentials",
                        new
                        {
                            staffId = staffid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        } //Get
        //DeleteStaffCredentials
        public static List<SchoolStaffModel> GetStaffCredentials(long staffid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffCredentials",
                        new
                        {
                            staffId = staffid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        } //GetStaffCredentials
        public static List<SchoolStaffModel> GetStaffClassesByStaffId(int staffid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffClassesByStaffId",
                        new
                        {
                            StaffID = staffid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetStaffClassesBySchoolId(long schoolid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffClassesBySchoolId", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetStaffOwnedDegreeBySTaffId(int staffId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffOwnedDegreeByStaffID",
                        new
                        {
                            StaffID = staffId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        //GetStaffOwnedDegreeBySTaffId
        public static List<SchoolStaffModel> GetStaffOwnedDegreeBySchoolId(long schoolId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffOwnedDegreeBySchoolId",
                        new
                        {
                            SchoolID = schoolId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetStaffSubjectTaughtByStaffIDEdit(int staffId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectTaughtByStaffIDEdit",
                        new
                        {
                            StaffID = staffId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<StaffSubject> GetAStaffSubjects(int id)
        {
            var result = new List<StaffSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<StaffSubject>("dbo.GetAStaffSubjects",
                        new
                        {
                            SchoolStaffID = id

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static List<SchoolStaffModel> GetStaffSubjectTaughtByStaffIdAss(int staffId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectTaughtByStaffIDAss",
                        new
                        {
                            StaffID = staffId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static List<SchoolStaffModel> GetStaffSubjectTaughtByStaffId(int staffId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectTaughtByStaffID",
                        new
                        {
                            StaffID = staffId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static List<SchoolStaffModel> GetStaffSubjectTaughtByStaffIdEdit(int staffId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectTaughtByStaffIDEdit",
                        new
                        {
                            StaffID = staffId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        //GetSideStaffSubjectBySchoolId
        public static List<SchoolStaffModel> GetSideStaffSubjectBySchoolId(long schoolId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetSideStaffSubjectBySchoolId",
                        new
                        {
                            SchoolID = schoolId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        public static List<SchoolStaffModel> GetStaffSubjectTaughtBySchoolId(long schoolId)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectTaughtBySchoolId",
                        new
                        {
                            SchoolID = schoolId

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }
        //GetStaffOwnedDegreeBySchoolId
        //GetStaffBySchoolId
        //GetStaffSubjectTaughtBySchoolId
        public static List<ClassAllocationvModel> GetStudentClassRecord(long? schoolid)
        {
            var result = new List<ClassAllocationvModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<ClassAllocationvModel>("dbo.GetStudentClassRecord",
                        new
                        {
                            schoolid = schoolid

                        }, commandType: CommandType.StoredProcedure)
                        .ToList();

            }
            return result;
        }

        public static UserViewModel GetRoleNameById(string RoleId)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<UserViewModel>("dbo.GetRoleNameByID",
                        new
                        {
                            roleid = RoleId

                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }

        public static Role AddRole(string rolename, bool isAdmin, bool isSuperAdmin)
        {
            var result = new Role();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<Role>("dbo.AddRole", new { rolename = rolename, isAdmin = isAdmin, isSuperAdmin = isSuperAdmin }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }
        //AddRole
        // GetRoleNameByID
        public static UserViewModel UpdateUser(int userId, bool isActive)
        {

            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.UpdateUserActive",
                                      new
                                      {
                                          ID = userId,
                                          isActive = isActive

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        public static UserViewModel UpdateUserIdRoleIdToUserRole(int userRoleID, int userId, int RoleId)
        {
            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.UpdateUserIdRoleIDToUserRole",
                                      new
                                      {
                                          ReturnedUserID = userId,
                                          ReturnedRoleID = RoleId,
                                          userRoleID = userRoleID

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        public static UserViewModel GetUserById(int userId)
        {

            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.GetUserByID",
                                      new
                                      {
                                          @UserId = userId

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }
        //GetUserByID
        public static UserViewModel GetUserRole(int userId, int RoleId)
        {

            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.GetUserRole",
                                      new
                                      {
                                          ReturnedUserID = userId,
                                          ReturnedRoleID = RoleId

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }
        public static UserViewModel UpdateUserRole(int userId, int RoleId)
        {

            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.UpdateUserRole",
                                      new
                                      {
                                          ID = userId,
                                          RolId = RoleId

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }


        public static UserViewModel UpdateUser(int userId, string EmailAddress, int RoleId, bool isActive)
        {

            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.UpdateUser",
                                      new
                                      {
                                          ID = userId,
                                          RolId = RoleId,
                                          EmailAddress = EmailAddress,
                                          isActive = isActive

                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }




        public static bool DeleteAUser(int userId)
        {
            bool check = false;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteAUser",
                      new
                      {
                          userid = userId,


                      }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    check = true;
                }

            }
            return check;
        }
        public static bool DeleteASchoolSubjects(long schoolid)
        {
            bool check = false;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteSchoolSubjects",
                      new
                      {
                          SchoolID = schoolid,


                      }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    check = true;
                }

            }
            return check;
        }

        public static bool DeleteSchoolSubjectsDependency(long schoolid,int? subjectid)
        {
            bool check = false;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                _db.Execute("dbo.DeleteSchoolSubjectsDependency",
                      new
                      {
                          SchoolID = schoolid,
                          SubjectID= subjectid
                      }, commandType: CommandType.StoredProcedure);


            }
            return true;
        }
        public static bool DeleteSchoolSubjects(long schoolid)
        {
            bool check = false;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                _db.Execute("dbo.DeleteSchoolSubjects",
                      new
                      {
                          SchoolID = schoolid
                      }, commandType: CommandType.StoredProcedure);
                

            }
            return true;
        }
        //DeleteSchoolSubjects
        //DeleteAUser
        public static bool DeleteUserState(int userId)
        {
            bool check = false;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteUserState",
                      new
                      {
                          userid = userId,


                      }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    check = true;
                }

            }
            return check;
        }



        public static List<UserViewModel> UpdateUserState(int userId, string stateid)
        {
            List<UserViewModel> resullist;
            var resultview = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                //DeleteUserState
                _db.Query<UserViewModel>("dbo.UpdateUserState",
                    new
                    {
                        userid = userId,
                        stateid = stateid

                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                resullist = _db.Query<UserViewModel>("dbo.GetUserStates",
                    new
                    {
                        userid = userId,


                    }, commandType: CommandType.StoredProcedure).ToList();

                //GetUserStates
            }
            resultview.AddRange(resullist);
            return resultview;
        }
        //ChangePassword
        public static UserViewModel ChangePassword(string emailAddress, byte[] password, byte[] newPassword)
        {
            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.ChangePassword",
                                      new
                                      {
                                          EmailAddress = emailAddress,
                                          OldPassword = password,
                                          NewPassword = newPassword
                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;

        }

        public static UserViewModel UpdateUserPasswordChanged(string emailAddress, byte[] NonexistingProfilePassword)
        {
            // var result = new UserViewModel();
            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.UpdateUserPasswordChanged",
                                      new
                                      {
                                          EmailAddress = emailAddress,
                                          Password = NonexistingProfilePassword


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        //UpdateUserPasswordChanged

        public static UserViewModel GetAUserEmailAndPassword(string emailAddress, byte[] existingProfilePassword)
        {
            // var result = new UserViewModel();
            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.GetAUserEmailAndPassword",
                                      new
                                      {
                                          EmailAddress = emailAddress,
                                          Password = existingProfilePassword


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        //InsertSchoolCentre
        public static SchoolStaffModel InsertSchoolCentre(string centreName, string centreNumber)
        {
            // var result = new UserViewModel();
            var resultview = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<SchoolStaffModel>("dbo.InsertSchoolCentre",
                                      new
                                      {
                                          CentreName = centreName,
                                          CentreNo = centreNumber


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        public static SchoolStaffModel InsertAStaffSubjectClass(int staffId, int classId)
        {
            // var result = new UserViewModel();
            var resultview = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<SchoolStaffModel>("dbo.InsertAStaffSubjectClass",
                                      new
                                      {
                                          StaffSubjectID = staffId,
                                          ClassID = classId


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }
        public static SchoolStaffModel GetStaffSubjectRowId(int staffId, int subjectid)
        {
            // var result = new UserViewModel();
            var resultview = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectRowId",
                                      new
                                      {
                                          SchoolStaffID = staffId,
                                          SubjectID = subjectid


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }
        //

        public static UserViewModel GetAUserEmail(string emailAddress)
        {
            // var result = new UserViewModel();
            var resultview = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {

                resultview = _db.Query<UserViewModel>("dbo.GetAUserEmail",
                                      new
                                      {
                                          EmailAddress = emailAddress,


                                      }, commandType: CommandType.StoredProcedure)
                                      .FirstOrDefault();

            }
            return resultview;
        }

        //UpdateUserget
        public static List<Role> GetRole()
        {
            var roleList = new List<Role>();
            using (IDbConnection _db = OpenConnection())
            {

                roleList = _db.Query<Role>("dbo.GetRole", commandType: CommandType.StoredProcedure).ToList();
            }
            return roleList;
        } // AddNewSchoolsubject
        public static List<SchoolSubject> AddNewSchoolsubject(SchoolSubjectViewModel schoolsubject, string schoolids)
        {
            var retvalue = new List<SchoolSubject>();
            var schsubobject = new SchoolSubject();

            try
            {

                using (IDbConnection _db = OpenConnection())
                {
                    foreach (var s in schoolsubject.AvailableSelected)
                    {
                        Subject xc = _db.Query<Subject>("dbo.GetCouncilSubjects", commandType: CommandType.StoredProcedure).FirstOrDefault(k => k.ID == s);
                        schsubobject = _db.Query<SchoolSubject>("dbo.NewSchoolSubjects", new { SubjectID = xc.ID, SchoolID = schoolids, }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        retvalue.Add(schsubobject);
                        //sch.Subjects.Add(Subject);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }
        // AddExaminallHallInfo
        public static SchoolStaff AddExaminallHallInfo(ExaminationHall exam, int schoolid)
        {
            var retvalue = new SchoolStaff();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue =
                        _db.Query<SchoolStaff>("dbo.AddExaminationHall",
                            new
                            {
                                CapacityofEachExaminationsHall = exam.CapacityofEachExaminationsHall,
                                NumberofDesksAndChairsAvailableInExamHalls = exam.NumberofDesksAndChairsAvailableInExamHalls,
                                NumberofExaminationHall = exam.NumberofExaminationHall,
                                SchoolID = schoolid
                            }, commandType: CommandType.StoredProcedure
                            ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;

        }

        public static SchoolLabConfig UpdateSchoolLaboByID(int id, string catee, int sub, int facilitycategoryid)
        {
            var result = new SchoolLabConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<SchoolLabConfig>("dbo.UpdateSchoolLaboByID",
                        new
                        {
                            FacID = id,
                            itemDescription = catee,

                            SubjectID = sub,

                            FacilityCategoryID = facilitycategoryid
                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;
        }

        public static ReagentModelAdmin UpdateSchoolReagentByID(int id, string catee, double Quant, int sub, string Spec,
            int facilitycategoryid)
        {

            var result = new ReagentModelAdmin();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<ReagentModelAdmin>("dbo.UpdateSchoolReagentByID",
                        new
                        {
                            FacID = id,
                            itemDescription = catee,
                            QuantityRequired = Quant,
                            SubjectID = sub,
                            Specification = Spec,
                            FacilityCategoryID = facilitycategoryid
                        }, commandType: CommandType.StoredProcedure)
                        .FirstOrDefault();

            }
            return result;

        }


        public static SchoolLabConfig UpdateSchoolLabByID(int id, string catee, int sub, int facilitycategoryid)
        {

            var result = new SchoolLabConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolLabConfig>("dbo.UpdateSchoolLaboByID", new { FacID = id, itemDescription = catee, SubjectID = sub, FacilityCategoryID = facilitycategoryid }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            return result;
        }

        //UpdateStaffClass
        public static SchoolEquipConfig UpdateSchoolEquipmentByID(int id, string catee, double Quant, int sub, string Spec, int facilitycategoryid)
        {

            var result = new SchoolEquipConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfig>("dbo.UpdateSchoolEquipmentByID", new { FacID = id, itemDescription = catee, QuantityRequired = Quant, SubjectID = sub, Specification = Spec, FacilityCategoryID = facilitycategoryid }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            return result;
        }
        public static UserRole IsUserInRole(int userid, int? roleid)

        {
            var result = new UserRole();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserRole>("dbo.IsUserInRole", new { UserId = userid, RoleId = roleid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //IsUserinroleModel

        //public static UserRoleModel IsUserInRole(int userid, int? roleid)

        //{
        //    var result = new UserRoleModel();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<UserRoleModel>("dbo.IsUserInRole", new { UserId = userid, RoleId = roleid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}
        //for multiple roles
        //public static List<RoleModel> GetRoleByName(string rolename,int userid)

        //{
        //    var result = new List<RoleModel>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<RoleModel>("dbo.GetRoleByName", new { rolename = rolename,userid=userid }, commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;
        //}


        //public static List<RoleModel> GetRoleByName(string rolename)

        //{
        //    var result = new List<RoleModel>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<RoleModel>("dbo.GetRoleByName", new { rolename = rolename }, commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;
        //}


        public static Role GetRoleByName(string rolename)

        {
            var result = new Role();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Role>("dbo.GetRoleByName", new { rolename = rolename }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //public static RoleModel GetRoleByName(string rolename)

        //{
        //    var result = new RoleModel();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<RoleModel>("dbo.GetRoleByName", new { rolename = rolename }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}

        public static SchoolLabConfig AddLabItem(string catee, string SU, int facilitycategoryid)
        {
            var result = new SchoolLabConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolLabConfig>("dbo.AddLabItem", new { FacilityCategoryID = facilitycategoryid, ItemDescription = catee, SubjectId = SU }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //SaveEquipmentSummary
        public static FacilityApprovalModel SaveEquipmentSummary(int totalEquipmentItems, int subjectid, int totalSum, int facilityCategoryId, string percent, long sddc)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityApprovalModel>("dbo.SaveEquipmentSummary", new { TotalItems = totalEquipmentItems, SubjectID = subjectid, TotalSchoolRecords = totalSum, FacilityTypeID = facilityCategoryId, Percentage = percent, SchoolID = sddc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityApprovalModel SaveLaboratorySummary(int totalItems, int subjectid, int facilityCategoryId, long schoolid, int decision)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityApprovalModel>("dbo.SaveLaboaratorySummary", new { TotalItems = totalItems, SubjectID = subjectid, TotalSchoolRecords = 8, FacilityTypeID = facilityCategoryId, SchoolId = schoolid, Percentage = decision }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityApprovalModel SaveReagentSummary(int totalEquipmentItems, int subjectid, int totalSum, int facilityCategoryId, string percent, long schoo)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityApprovalModel>("dbo.SaveReagentSummary", new { TotalItems = totalEquipmentItems, SubectID = subjectid, TotalSchoolRecords = totalSum, FacilityTypeID = facilityCategoryId, Percentage = percent, SchoolID = schoo }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //(totalLaboratoryItems, subjectid, facilityCategoryId,schoolid,decision);
        //public static LaboratoryApprovalModel SaveLaboratorySummary(int totalItems, int subjectid,int facilityCategoryId, long schoolid,int decision)
        //{
        //    var result = new LaboratoryApprovalModel();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<LaboratoryApprovalModel>("dbo.SaveLaboaratorySummary", new { TotalItem = totalItems, Subjectid = subjectid, FacilityCategoryId = facilityCategoryId, SchoolId = schoolid, GeneralAssessment = decision }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}

        public static SchoolEquipConfig AddEquipmentItem(string catee, int SU, float Quant, string specif, int facilitycategoryid)
        {
            var result = new SchoolEquipConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfig>("dbo.AddEquipmentItem", new { FacilityCategoryID = facilitycategoryid, ItemDescription = catee, QuantityRequired = Quant, SubjectId = SU, Specification = specif }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static List<LibraryViewModel> GetSchoolLibraryConfigAdmin(int? facilitycategoryid, int? recognitionid)
        {
            var result = new List<LibraryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.GetSchoolLibraryConfigAdmin", new { FacilityCategoryID = facilitycategoryid, SchoolRecognitionTypeID = recognitionid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }


        public static bool DeleteAssignRole(int id)
        {
            var result = false;
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteSchoolEquipmentItem", new { ID = id }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }

            return result;
        }
        //DeleteAssignRole

        public static bool DeleteRecordEquip(int id)
        {
            var result = false;
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteSchoolEquipmentItem", new { ID = id }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }

            return result;
        }
        //DeleteRecordEquip
        public static List<LaboratoryViewModel> GetSchoolLabConfigAdmin(int? facilitycategoryid, int? recognitionid)
        {
            var result = new List<LaboratoryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.GetSchoolLabConfigAdmin", new { FacilityCategoryID = facilitycategoryid, SchoolRecognitionTypeID = recognitionid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetSchoolLabConfigAdmin
        //GetSchoolEquipConfigAdmin
        public static List<SchoolEquipConfigViewModel> GetSchoolEquipConfigAdmin(int? facilitycategoryid, int? recognitionid)
        {
            var result = new List<SchoolEquipConfigViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolEquipConfigAdmin", new { FacilityCategoryID = facilitycategoryid, SchoolRecognitionTypeID = recognitionid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<vmFacilityCategory> GetFacilityCategories(int? id)
        {
            var result = new List<vmFacilityCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmFacilityCategory>("dbo.GetFacilityCategories", new { RecognitionID = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetExamType
        public static List<ExamTypeModel> GetExamType()
        {
            var retvalue = new List<ExamTypeModel>();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<ExamTypeModel>("dbo.GetExamType", commandType: CommandType.StoredProcedure
                        ).ToList();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;

        }

        public static SchStaffCredential AddStaffCredentials(int staffId, string image)
        {
            var retvalue = new SchStaffCredential();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<SchStaffCredential>("dbo.AddSchStaffCredentials", new { StaffID = staffId, Image = image }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;

        }

        //addschoolstaff
        public static vmSchoolStaffQualification AddSchoolStaff(vmSchoolStaffQualification schoolstaff)
        {
            var retvalue = new vmSchoolStaffQualification();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<vmSchoolStaffQualification>("dbo.AddSchoolStaff", new { TitleID = schoolstaff.TitleID, DateEmployed = schoolstaff.DateEmployed, FirstName = schoolstaff.FirstName, LastName = schoolstaff.LastName, ContactAddress = schoolstaff.ContactAddress, SchoolID = schoolstaff.SchoolID, CategoryID = schoolstaff.CategoryID, isTRCN = schoolstaff.isTRCN, TRCNTex = schoolstaff.TRCNText }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();

                    //_db.Query<vwSchStafDegCourse>("dbo.AddSchStafDegCourse", new { ID = retvalue.ID, DegreeID = schoolstaff.DegreeID, CourseID = schoolstaff.CourseID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;

        }

        public static List<StaffSubject> UpdateClassSubject(int schoolStaffId, int classid)
        {
            StaffSubject listStaffSub; List<StaffSubject> listStaffSubs = new List<StaffSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                listStaffSub = _db.Query<StaffSubject>("dbo.UpdateClassSubject", new { schoolstaffID = schoolStaffId, ClassID = classid }, commandType: CommandType.StoredProcedure
                    ).FirstOrDefault();


            }
            listStaffSubs.Add(listStaffSub);
            return listStaffSubs;
        }

        public static List<StaffSubject> AddStaffSubject(int stid, int? sid, int classid)
        {
            StaffSubject listStaffSub;
            List<StaffSubject> listStaffSubs;
            listStaffSubs = new List<StaffSubject>();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listStaffSub = _db.Query<StaffSubject>("dbo.AddStaffSubject", new { schoolstaffID = stid, subID = sid }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            listStaffSubs.Add(listStaffSub);
            return listStaffSubs;

        }

        public static List<StaffForClass> AddStaffClass(int schoolStaffid, int classId)
        {
            var listAde = new StaffForClass();
            var list = new List<StaffForClass>();
            //var retvalue = new StaffSubject();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listAde = _db.Query<StaffForClass>("dbo.AddStaffClass", new { schoolstaffID = schoolStaffid, ClassID = classId }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
                //Dictionary<"",">()

                //Session["test"] = myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            list.Add(listAde);
            return list;
        }

        //AddSchoolStaffSubject
        public static List<StaffSubject> AddStaffSubject2(int stid, int SchoolSubjectRowID)
        {
            var listAde = new StaffSubject();
            var list = new List<StaffSubject>();
            //var retvalue = new StaffSubject();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listAde = _db.Query<StaffSubject>("dbo.AddSchoolStaffSubject", new { schoolstaffID = stid, SchoolSubjectRowID = SchoolSubjectRowID }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
                //Dictionary<"",">()

                //Session["test"] = myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            list.Add(listAde);
            return list;

        }

        public static List<StaffSubject> AddStaffSubject(int stid, int? sid, bool Barred)
        {
            var listAde = new StaffSubject();
            var list = new List<StaffSubject>();
            //var retvalue = new StaffSubject();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listAde = _db.Query<StaffSubject>("dbo.AddStaffSubject", new { schoolstaffID = stid, subID = sid, isBarred = Barred }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
                //Dictionary<"",">()

                //Session["test"] = myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            list.Add(listAde);
            return list;

        }

        public static List<QuestionUploadData> Insert_SchoolProfile(IQueryable<QuestionUploadData> data)
        {
            var xxx = new List<QuestionUploadData>();
            var yyy = new QuestionUploadData();
            foreach (var vx in data)
            {
                using (IDbConnection _db = OpenConnection())
                {
                    yyy =
                        _db.Query<QuestionUploadData>("dbo.Insert_SchoolProfile",
                            new { CentreName = vx.SchoolName, CentreNo = vx.CenterNo },
                            commandType: CommandType.StoredProcedure).FirstOrDefault();

                }

                xxx.Add(yyy);
            }

            return xxx;

        }

        public static List<StaffDegree> AddStaffDegree(int stid, int degreeId)
        {
            var listAdded = new StaffDegree();
            var list = new List<StaffDegree>();
            //var retvalue = new StaffSubject();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listAdded = _db.Query<StaffDegree>("dbo.AddStaffDegree", new { schoolstaffID = stid, degID = degreeId }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
                //Dictionary<"",">()

                //Session["test"] = myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            list.Add(listAdded);
            return list;

        }//AddSecondDegree

        public static List<StaffDegree> AddStaffDegree(int stid, int degreeId, int courseid)
        {
            var listAdded = new StaffDegree();
            var list = new List<StaffDegree>();
            //var retvalue = new StaffSubject();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    listAdded = _db.Query<StaffDegree>("dbo.AddStaffDegree", new { schoolstaffID = stid, degID = degreeId, CourseID = courseid }, commandType: CommandType.StoredProcedure
                        ).FirstOrDefault();


                }
                //Dictionary<"",">()

                //Session["test"] = myList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            list.Add(listAdded);
            return list;

        }//AddSecondDegree
         //DeleteSecondDegree
        public static List<SecondDegree> DeleteSecondDegree(int staffId)
        {
            var retvalue = new List<SecondDegree>();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<SecondDegree>("dbo.DeleteSecondDegree", new { staffID = staffId }, commandType: CommandType.StoredProcedure
                   ).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }

        public static SchStaffCourse GetCourseID(int course)
        {
            var retvalue = new SchStaffCourse();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<SchStaffCourse>("dbo.CourseTitle", new { courseID = course }, commandType: CommandType.StoredProcedure
                   ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }

        public static SecondDegree AddSecondDegree(int staffid, string otherDegree)
        {
            var retvalue = new SecondDegree();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<SecondDegree>("dbo.AddOtherDegree", new { StaffID = staffid, OtherDegree = otherDegree }, commandType: CommandType.StoredProcedure
                   ).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }
        //AddNewSchoolSubjectEdit
        public static SchoolSubjectViewModel AddNewSchoolSubjectEdit(int subjectid, long schoolid, bool IsActive)
        {
            var retvalue = new SchoolSubjectViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                retvalue = _db.Query<SchoolSubjectViewModel>("dbo.AddNewSchoolSubjectEdit", new { SchoolID = schoolid, SubjectID = subjectid, IsActive = IsActive }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return retvalue;
        }
        public static SchoolProfileViewModel AddNewSchool(SchoolProfileViewModel schoolprofile)
        {
            var retvalue = new SchoolProfileViewModel();
            try
            {
                using (IDbConnection _db = OpenConnection())
                {
                    retvalue = _db.Query<SchoolProfileViewModel>("dbo.NewSchoolProfile", schoolprofile, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }

        // [GetSchoolSubjectByID]
        public static bool SetIsTextBookAttached(SchoolSubjectViewModel[] checkeditems, SchoolSubjectViewModel[] textbooks, long schoolid)
        {
            var result = new SchoolSubjectViewModel();
            var obj = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                foreach (var t1 in checkeditems)
                {
                    foreach (var t in textbooks)
                    {
                        if (t1.ID.Equals(t.ID))
                        {
                            obj.ID = t1.ID;
                            if (t.Textboxvalues != null)
                            {
                                obj.AvailableTextbooks = t.Textboxvalues;
                                obj.isTextBookAttached = true;
                            }
                            if (t.Textboxvalues == null)
                            {
                                obj.AvailableTextbooks = t.Textboxvalues;
                                obj.isTextBookAttached = false;
                            }

                            result = _db.Query<SchoolSubjectViewModel>("dbo.SetIsTextBookAttached", new { ID = obj.ID, ifTextBookAttached = obj.isTextBookAttached, textbook = obj.AvailableTextbooks, schoolid = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        }

                    }
                }
            }
            return result != null && result.ID == obj.ID;
        }

        //GetSchoolLaboratory
        public static List<vwSchoolLaboratoryModel> GetSchoolLaboratory2(int schoolid, int subjectid)
        {
            var result = new List<vwSchoolLaboratoryModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolLaboratoryModel>("dbo.GetSchoolLaboratory22", new { SchoolID = schoolid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static SchoolStaffModel CountDegreeHolder(long schoolid)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.CountDegreeHolder", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolStaffModel CountNonDegreeHolder(long schoolid)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.CountNonDegreeHolder", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }//
        public static List<SecondDegree> GetSecondDegreeForStaff(int staffId)
        {
            var result = new List<SecondDegree>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SecondDegree>("dbo.GetSecondDegreeForStaff", new { StaffID = staffId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static SchoolStaffModelEdit UpdateDegreeForStaffDegree(int stafftableid, int degreeid, int courseid)
        {
            var result = new SchoolStaffModelEdit();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModelEdit>("dbo.UpdateDegreeForStaffDegree", new { ID = stafftableid, DegreeID = degreeid, CourseID = courseid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        //UpdateCourseForStaffID
        public static SchoolStaffModelEdit UpdateCourseForStaffID(int staffId, int courseid)
        {
            var result = new SchoolStaffModelEdit();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModelEdit>("dbo.UpdateCourseForStaffID", new { ID = staffId, CourseID = courseid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolStaffModelEdit GetStaffDegreeRow(int staffId)
        {
            var result = new SchoolStaffModelEdit();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModelEdit>("dbo.GetStaffDegreeRow", new { StaffID = staffId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolStaffModel GetSchoolNameAndPrincipal(long schoolid)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetSchoolNameAndPrincipal", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<ClassAllocationvModel> GetSchoolClassBySchoolId(long schoolid)
        {
            var result = new List<ClassAllocationvModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationvModel>("dbo.GetSchoolClassBySchoolId", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetSchoolClassBySchoolId

        public static List<vmSchoolStaffQualification> GetSchoolSummary(long schoolid)
        {
            var result = new List<vmSchoolStaffQualification>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchoolSummary", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        //GetAnyOtherSubject
        public static List<SubjectModel> GetAnyOtherSubject()
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetAnyOtherSubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetCompulsorySchoolSubjects()
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetCompulsorySubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }


        public static SchoolProfileViewModel UpdateNewSchoolProfile2(SchoolProfileViewModel model)
        {
            SchoolProfileViewModel result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel>("dbo.UpdateNewSchoolProfile", new
                {
                    ID = model.ID,
                    CategoryID = model.CategoryID,
                    SchoolName = model.SchoolName,
                    SchoolLocation = model.SchoolLocation,
                    YearEstablished = model.YearEstablished,
                    StateID = model.StateID,
                    LgaID = model.LgaID,
                    IsJointProprietorship = model.IsJointProprietorship,
                    AproximateLandArea = model.AproximateLandArea,
                    IsLandAreaAdequate = model.IsLandAreaAdequate,
                    IsSchoolFencedRound = model.IsSchoolFencedRound,
                    IsAdmissionRegisterAvailable = model.IsAdmissionRegisterAvailable,
                    isDiaryofWorksAvaialble = model.isDiaryofWorksAvaialble,
                    isClassRegistersAvaialable = model.isClassRegistersAvaialable,
                    isCurricullumAvailable = model.isCurricullumAvailable,
                    IsScheemOfWorkAvailable = model.IsScheemOfWorkAvailable,
                    OtherSpecify = model.OtherSpecify
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }

            return result;
        }
        //UpdateSchoolProfileCentreNo
        public static SchoolProfileViewModel1 UpdateSchoolProfileCentreNo(SchoolProfileViewModel1 model)
        {
            SchoolProfileViewModel1 result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.UpdateSchoolProfileCentreNo", new
                {
                    ID = model.ID,
                    CentreNo = model.CenterNo
                    
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }

            return result;
        }

        public static SchoolProfileViewModel1 UpdateNewSchoolProfile(SchoolProfileViewModel1 model)
        {
            SchoolProfileViewModel1 result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.UpdateNewSchoolProfile", new
                {
                    ID = model.ID,
                    CategoryID = model.CategoryID,
                    SchoolName = model.SchoolName,
                    SchoolLocation = model.SchoolLocation,
                    YearEstablished = model.YearEstablished,
                    StateID = model.StateID,
                    LgaID = model.LgaID,
                    IsJointProprietorship = model.IsJointProprietorship,
                    AproximateLandArea = model.AproximateLandArea,
                    IsLandAreaAdequate = model.IsLandAreaAdequate,
                    IsSchoolFencedRound = model.IsSchoolFencedRound,
                    IsAdmissionRegisterAvailable = model.IsAdmissionRegisterAvailable,
                    isDiaryofWorksAvaialble = model.isDiaryofWorksAvaialble,
                    isClassRegistersAvaialable = model.isClassRegistersAvaialable,
                    isCurricullumAvailable = model.isCurricullumAvailable,
                    IsScheemOfWorkAvailable = model.IsScheemOfWorkAvailable,
                    OtherSpecify = model.OtherSpecify
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }

            return result;
        }

        public static List<SchoolProfile> GetSchoolsForAutoCentre()
        {
            var result = new List<SchoolProfile>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfile>("dbo.GetSchoolsForAutoCentre", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<SchoolProfile> GetSchoolsForAutoCentre(int page, int pageSize = 20, int userId = 0)
        {
            var result = new List<SchoolProfile>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfile>("dbo.GetSchoolsForAutoCentre", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //UpdateNewSchoolProfile
        //GetSchoolsApproved
        public static List<SchoolProfileViewModel1> GetSchoolsApproved()
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                // totalRows = _db.Query<int>("dbo.GetTotalSchoolsRecognised", commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsForAutoCentre2", commandType: CommandType.StoredProcedure).ToList();

            }

            return result;

        }




        public static IPagedList<SchoolProfileViewModel1> GetSchoolsApproved(int page, int pageSize = 20, int userId = 0)
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetTotalSchoolsRecognised", commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsForAutoCentre", new { PageNumber = page, RowsPerPage = pageSize }, commandType: CommandType.StoredProcedure).ToList();

            }
            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return pagedList;

        }
        public static List<SchoolProfileViewModel1> GetSchoolsAdmin(string stateCode = "")
        {
            List<SchoolProfileViewModel1> result; var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                // totalRows = _db.Query<int>("dbo.GetTotalSchoolsAdmin", new { UserID = userid, StateCode = stateCode }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsForAutoCentre2", new { StateCode = stateCode }, commandType: CommandType.StoredProcedure).ToList();

            }

            //var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return result;

        }
        public static IPagedList<SchoolProfileViewModel1> GetSchoolsAdminB(int page, int pageSize = 20, int userid = 0, string stateCode = "")
        {
            List<SchoolProfileViewModel1> result; var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetTotalSchoolsAdminB", new { UserID = userid, StateCode = stateCode }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsAdminB", new { UserID = userid, StateCode = stateCode, RowsPerPage = pageSize, PageNumber = page }, commandType: CommandType.StoredProcedure).ToList();

            }

            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return pagedList;

        }
        public static IPagedList<SchoolProfileViewModel1> GetSchoolsAdmin(int page, int pageSize = 20, int userid = 0, string stateCode = "")
        {
            List<SchoolProfileViewModel1> result; var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetTotalSchoolsAdmin", new { UserID = userid, StateCode = stateCode }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsAdmin", new { UserID = userid, StateCode = stateCode, RowsPerPage = pageSize, PageNumber = page }, commandType: CommandType.StoredProcedure).ToList();

            }

            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return pagedList;

        }


        //public static IPagedList<SchoolProfileViewModel1> GetSchoolsAdmin(int page, int pageSize = 20, int userId = 0)
        //{
        //    List<SchoolProfileViewModel1> result;
        //    var totalRows = 0;
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        totalRows = _db.Query<int>("dbo.GetSchoolsTotalByAdminID", new { UserID = userId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //        result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchools", new { PageNumber = page, RowsPerPage = pageSize, UserID = userId }, commandType: CommandType.StoredProcedure).ToList();

        //    }
        //    var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
        //    return pagedList;

        //}

        public static IPagedList<SchoolProfileViewModel1> GetSchools(int page, int pageSize = 20, string stateid = "")
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetSchoolsTotalByUserId", commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchools", new { PageNumber = page, RowsPerPage = pageSize, UserState = stateid }, commandType: CommandType.StoredProcedure).ToList();

            }
            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return pagedList;

        }

        public static IPagedList<SchoolProfileViewModel1> GetCentres(int page, int pageSize = 500, string stateid = "")
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetSchoolCentresTotalByUserId", commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetCentres", commandType: CommandType.StoredProcedure).ToList();

            }
            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
            return pagedList;
        }



        //public static List<SchoolProfileViewModel1> GetCentresX(int page, int pageSize = 500, string stateid = "")
        //{
        //    List<SchoolProfileViewModel1> result;
        //    var totalRows = 0;
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        totalRows = _db.Query<int>("dbo.GetSchoolCentresTotalByUserId", commandType: CommandType.StoredProcedure).FirstOrDefault();
        //        result = _db.Query<SchoolProfileViewModel1>("dbo.GetCentres", new { PageNumber = page, RowsPerPage = pageSize, UserState = stateid }, commandType: CommandType.StoredProcedure).ToList();

        //    }
        //    var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, page, pageSize, totalRows);
        //    return result;

        //}


        public static List<StateModel> GetStatesByUserId(int userID)
        {
            var result = new List<StateModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StateModel>("dbo.GetStatesByUserId", new { userID = userID }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static StateModel GetStatesById(string stateid)
        {
            var result = new StateModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StateModel>("dbo.GetStatesById", new { stateid = stateid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static SchoolSubjectViewModel UpdateQualifiedStaffStatus1(long schid, int subjectid, int finalRating)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateQualifiedStaffStatus1", new { SchoolID = schid, SubjectID = subjectid, FinalRating = finalRating }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolSubjectViewModel UpdateItemsPlusReagentDerec(long schid, int subjectid, int equipmentFacility, int laboratoryFacility, int reagentfacility)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateItemsPlusReagentDerec", new { SchoolID = schid, SubjectID = subjectid, EquipmentFacility = equipmentFacility, LaboratoryFacility = laboratoryFacility, ReagentFacility = reagentfacility }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateSchoolSubjecLabFacilityDerec
        public static SchoolSubjectViewModel UpdateSchoolSubjecLabFacilityDerec(long schid, int subjectid, int adequateLaboratory)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjecLabFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, AdequateLaboratory = adequateLaboratory }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel UpdateSchoolSubjecReagentFacilityDerec(long schid, int subjectid, int adequateReagent)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjecReagentFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, ReagentFacility = adequateReagent }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        //UpdateSchoolSubjecLabFacilityDerec

        public static SchoolSubjectViewModel UpdateSchoolSubjecEquipmentFacilityDerec(long schid, int subjectid, int adequateEquipment)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjecEquipmentFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, AdequateEquipment = adequateEquipment }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateLaboratoryFacilityDerec

        public static SchoolSubjectViewModel UpdateLaboratoryFacilityDerec(long schid, int subjectid, int? laboratoryFacility)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateLaboratoryFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, LaboratoryFacility = laboratoryFacility }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static ItemSectionAssessmentModel UpdateReagentFacilityDerec(long schid, int subjectid, int? reagentFacility)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateReagentFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, ReagentFacility = reagentFacility }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateReagentFacilityDerec1
        public static ItemSectionAssessmentModel UpdateEquipmentFacilityDerec1(long schid, int subjectid, int? equipmentFacility)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateEquipmentFacilityDerec1", new { SchoolID = schid, SubjectID = subjectid, EquipmentFacility = equipmentFacility }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel UpdateEquipmentFacilityDerec(long schid, int subjectid, int? equipmentFacility)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateEquipmentFacilityDerec", new { SchoolID = schid, SubjectID = subjectid, EquipmentFacility = equipmentFacility }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateSchoolSubjectExamMalPracticeDerec
        public static SchoolSubjectViewModel UpdateSchoolSubjectExamMalPracticeDerec(long schid, int subjectid, bool isApproved, int examinationMalpractice)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectExamMalPracticeDerec", new { SchoolID = schid, SubjectID = subjectid, IsApproved = isApproved, ExaminationMalpractice = examinationMalpractice }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel UpdateSchoolSubject4QualifiedStaff(long schid, int subjectid, bool isApproved, int qualifiedStaff)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubject4QualifiedStaff", new { SchoolID = schid, SubjectID = subjectid, IsApproved = isApproved, QualifiedStaff = qualifiedStaff }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel ProcStaffSummaryDerec(long schid, int subjectid)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.procStaffSummaryDerec", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //ProcItemSectionAssessmentEquip
        public static ItemSectionAssessmentModel ProcItemSectionAssessmentReagent(long schid, int subjectid)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.ProcItemSectionAssessmentReagent", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static ItemSectionAssessmentModel ProcItemSectionAssessmentLabora(long schid, int subjectid)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.ProcItemSectionAssessmentLabora", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static ItemSectionAssessmentModel ProcItemSectionAssessmentEquip(long schid, int subjectid)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.ProcItemSectionAssessmentEquip", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel ProcSchoolSubjectDerec(long schid, int subjectid)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.procSchoolSubjectDerecs", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolSubjectViewModel ProcFetchStaffSubClassDerecs(long schid, int subjectid)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.procFetchStaffSubClassDerecs", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static List<SubjectModel> GetCompulsorySubjects()
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetCompulsorySubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //CheckIfPrincipalExist
        public static bool CheckIfPrincipalExist(long schoolid, int categoryId)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.CheckIfPrincipalExist", new { schoolId = schoolid, categoryId = categoryId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (result != null && result.CategoryID == categoryId)
                {
                    return true;
                }
            }

            return false;
        }

        public static UserViewModel CheckIfAdminHasSameStateAssigned(string stateId, int usersId)
        {

            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.CheckIfAdminHasSameStateAssigned", new { StateCode = stateId, UserID = usersId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;
        }
        public static UserViewModel AddStateToUsers(string stateId, int usersId)
        {

            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.AddStateToUsers", new { stateid = stateId, userid = usersId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static State GetSelectedStateById(int? stateid)
        {
            var result = new State();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<State>("dbo.GetSelectedStateByID", new { SelectedStateID = stateid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SubjectModel GetSelectedSubjectById(long? subjectid)
        {
            var result = new SubjectModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSelectedSubjectByID", new { SelectedSubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        //GetSelectedStateByID
        //public static Subject GetSelectedSubjectById(long? subjectid)
        //{
        //    var result=new Subject();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<Subject>("dbo.GetSelectedSubjectByID", new { SelectedSubjectID= subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;

        //}
        //GetSelectedSubjectByID

        public static List<SchoolSubjectViewModel> GetSchoolTradeSubjects()
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetTradeSubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static List<SubjectModel> GetTradeSubjects()
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetTradeSubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static ClassAllocationModel GetSchoolSs2(long schoolid)
        {
            var result = new ClassAllocationModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationModel>("dbo.GetSchoolSS2", new { SCHOOLID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static vmSchoolStaffQualification GetSchoolStudentClass(long schoolid, int classid)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchoolStudentClass", new { SCHOOLID = schoolid, ClassID = classid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<viewSummaryGeneral> GetGeneralSummary(long schoolid)
        {
            var result = new List<viewSummaryGeneral>();
            using (IDbConnection _db = OpenConnection())
            {
                //GetSchoolSS2
                result = _db.Query<viewSummaryGeneral>("dbo.GeneralSummary", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<StudentInClass> GetStudentClass(int schid)
        {
            var result = new List<StudentInClass>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StudentInClass>("dbo.GetStudentClass", new { schoolid = schid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }


        //GetSchoolStudentClass

        public static vmSchoolStaffQualification GetSchoolStaffByID(int id, int schoolid)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchooStaffsID", new { STAFFID = id, SCHOOLID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //GetAssessmentUpdateRowSchSub
        public static ItemSectionAssessmentModel GetAssessmentUpdateRowSchSub(long SchoolId, int SubjectId, int facilityID)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.GetAssessmentUpdateRowSchSub", new { SchoolID = SchoolId, SubjectId = SubjectId, FacilityID = facilityID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel GetAssessmentUpdateRow(int updateId = 0)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.GetAssessmentUpdateRow", new { updateId = updateId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //FetchStaffSubjectToClass
        public static SchoolSubjectViewModel FetchStaffSubjectToClass(int classId, int staffId, int subjectId)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchStaffSubjectToClass", new { ClassID = classId, SchoolStaffID = staffId, SubjectID = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static SchoolSubjectViewModel UpdateStaffSubjectSubjectReport2(int staffId, int subjectId, int? subjectReport2)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateStaffSubjectSubjectReport2", new { StaffID = staffId, SubjectID = subjectId, SubjectReport2 = subjectReport2 }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static SchoolSubjectViewModel UpdateStaffSubject2Class(int classId,int realRowForUpdate, int finalRating)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateStaffSubject2Class", new {ClassID= classId, RealRowForUpdate = realRowForUpdate, FinalRating = finalRating }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static StaffSubjectClassSectionModel GetStaffSubClassAssessmentUpdateRow(int updateId = 0)
        {
            var result = new StaffSubjectClassSectionModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StaffSubjectClassSectionModel>("dbo.GetStaffSubClassAssessmentUpdateRow", new { UpdateId = updateId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessment GetItemSectionRow(int updateId = 0)
        {
            var result = new ItemSectionAssessment();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessment>("dbo.ItemSectionRow", new { updatedId = updateId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //
        public static StaffSubjectClassSectionModel UpdateStaffSubjectClassSection(int finalRating, int updateId = 0)
        {
            var result = new StaffSubjectClassSectionModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StaffSubjectClassSectionModel>("dbo.UpdateStaffSubjectClassSection", new { FinalRating = finalRating, updatedId = updateId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //UpdateNewItemSectionLa
        //public static ItemSectionAssessmentModel UpdateNewItemSectionLa(int LaboratoryFacility, long sdc,int subjectId)
        //{
        //    var result = new ItemSectionAssessmentModel();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionLa", new { LaboratoryFacility = LaboratoryFacility, SchoolID = sdc,SubjectId=subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}
        public static ItemSectionAssessmentModel UpdateNewItemSectionEq(int? EquipmentFacility, long sdc, int subjectId, int x)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionEq", new { EquipmentFacility = EquipmentFacility, SchoolID = sdc, SubjectId = subjectId, FacilityID = x }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel UpdateNewItemSectionEq(int EquipmentFacility, long sdc, int subjectId)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionEq", new { EquipmentFacility = EquipmentFacility, SchoolID = sdc, SubjectId = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel UpdateNewItemSectionLa(int? LaboratoryFacility, long sdc, int subjectId, int z)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionLa", new { LaboratoryFacility = LaboratoryFacility, SchoolID = sdc, SubjectId = subjectId, FacilityID = z }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel UpdateNewItemSectionLa(int LaboratoryFacility, long sdc, int subjectId)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionLa", new { LaboratoryFacility = LaboratoryFacility, SchoolID = sdc, SubjectId = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel UpdateNewItemSectionRe(int? ReagentFacility, long sdc, int subjectId, int y)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionRe", new { ReagentFacility = ReagentFacility, SchoolID = sdc, SubjectId = subjectId, FacilityID = y }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static ItemSectionAssessmentModel UpdateNewItemSectionRe(int ReagentFacility, long sdc, int subjectId)
        {
            var result = new ItemSectionAssessmentModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ItemSectionAssessmentModel>("dbo.UpdateNewItemSectionRe", new { ReagentFacility = ReagentFacility, SchoolID = sdc, SubjectId = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static LaboratoryViewModel UpdateLaboratoryValue(string user, int record_id = 0)
        {
            var result = new LaboratoryViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.UpdateLaboratoryValue", new { ID = record_id, LabValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static RecreationalViewModel UpdateRecreationalValue(string user, int record_id = 0)
        {
            var result = new RecreationalViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<RecreationalViewModel>("dbo.UpdateRecreationalValue", new { ID = record_id, Value = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetSchoolStateLocalDetails
        public static vwGetLocalDetailsByStateLocal GetSchoolStateLocalDetails(int statecode,int localcode)
        {

            var result = new vwGetLocalDetailsByStateLocal();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwGetLocalDetailsByStateLocal>("dbo.procvwGetLocalDetailsByStateLocal", new { StateCode = statecode,LocalCode= localcode }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;
            //JavaScriptSerializer js=new JavaScriptSerializer();
            //   js.Serialize(schoolname);
        }
        public static List<vwStateByStateCode> GetStateByStateCode(int statecode)
        {

            var result = new List<vwStateByStateCode>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwStateByStateCode>("dbo.GetStateByStateCode", new { StateCode = statecode }, commandType: CommandType.StoredProcedure).ToList();
            }

            return result;
            //JavaScriptSerializer js=new JavaScriptSerializer();
            //   js.Serialize(schoolname);
        }
        public static CentreTableModel GetCentreNameByCentreNum(string centreNum)
        {

            var result = new CentreTableModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<CentreTableModel>("dbo.GetCentreNameByCentreNum", new { CentreNo = centreNum }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;
            //JavaScriptSerializer js=new JavaScriptSerializer();
            //   js.Serialize(schoolname);
        }
        public static SchoolProfile CheckIfSchoolNameExists2(string schoolName)
        {

            var result = new SchoolProfile();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfile>("dbo.CheckIfSchoolNameExists2", new { schoolname = schoolName }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;
            //JavaScriptSerializer js=new JavaScriptSerializer();
            //   js.Serialize(schoolname);
        }
        public static SchoolProfile CheckIfSchoolNameExists(string schoolname, int local)
        {

            var result = new SchoolProfile();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfile>("dbo.CheckIfSchoolNameExists", new { schoolname = schoolname, LOcal = local }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;
            //JavaScriptSerializer js=new JavaScriptSerializer();
            //   js.Serialize(schoolname);
        }


        public static ExamHallViewModel UpdateExamHallValue(string user, int record_id = 0)
        {
            var result = new ExamHallViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ExamHallViewModel>("dbo.UpdateExamHallValue", new { ID = record_id, ExamValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static List<SchoolPreviewModel> GetSchoolPreview(int schoolid)
        {
            var generalresult = new List<SchoolPreviewModel>();
            var countTeacher = new SchoolPreviewModel();
            var countNCE = new SchoolPreviewModel();
            var countBSc = new SchoolPreviewModel();
            var countHND = new SchoolPreviewModel();
            var schoolinfo = new List<SchoolPreviewModel>();

            using (IDbConnection _db = OpenConnection())
            {
                schoolinfo = _db.Query<SchoolPreviewModel>("dbo.CountForSchool", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            generalresult.Add(schoolinfo.FirstOrDefault());
            return generalresult;
        }

        public static List<SchoolPreviewModel> GetCountNCE(int schoolid)
        {
            var result = new List<SchoolPreviewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolPreviewModel>("dbo.CountNCE", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //CountNCE
        public static List<SchoolPreviewModel> GetCountBsc(int schoolid)
        {
            var result = new List<SchoolPreviewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolPreviewModel>("dbo.CountBSc", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetSubjectByID
        public static SubjectModel GetSubjectById(int updatesubjectid)
        {
            var result = new SubjectModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSubjectByID", new { SubjectID = updatesubjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static List<SchoolPreviewModel> GetSchoolPreview1(int schoolid)
        {
            var result = new List<SchoolPreviewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolPreviewModel>("dbo.CountForSchool", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static LibraryViewModel UpdateLibraryValue(string user, int record_id = 0)
        {
            var result = new LibraryViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.UpdateLibraryValue", new { ID = record_id, LibraryValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //UpdateLaboratoryValue
        public static ReagentViewModel UpdateSchoolReagentValue(string user, int record_id = 0)
        {
            var result = new ReagentViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.UpdateSchoolReagentValue", new { ReagentValueID = record_id, ReagentValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static SchoolEquipConfigViewModel UpdateEquipFacilityValue(int record_id = 0, decimal user = 0m,int digitvalue=0)
        {
            var result = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.UpdateSchoolFacilityValue", new { ID = record_id, EquipmentValue = user, DigitValue = digitvalue }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //UpdateSchoolReagentValue
        //public static SchoolEquipmentValue UpdateEquipFacilityValue(string user, int record_id = 0)
        //{
        //    var result = new SchoolEquipmentValue();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolEquipmentValue>("dbo.UpdateSchoolFacilityValue", new { ID = record_id, EquipmentValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}

        //public static SchoolFacilityValue UpdateEquipFacilityValue(string user, int record_id = 0)
        //{
        //    var result = new SchoolFacilityValue();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolFacilityValue>("dbo.UpdateSchoolFacilityValue", new { ID = record_id, UserValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}
        public static SchoolFacilityValue GetRatedFacility(int dd, string user, int record_id = 0, long? schoolid = 0)
        {
            var result = new SchoolFacilityValue();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolFacilityValue>("select * from ", new { SchoolFacConfigID = dd, SchoolID = schoolid, UserValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static List<SchoolRecognitionType> GetSchoolRecogntion()
        {
            var result = new List<SchoolRecognitionType>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolRecognitionType>("GetSchoolRecogntion", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetSchoolRecogntion
        public static ClassAllocation AddClasses(long schoolid, ClassAllocationvModel classalloc)
        {
            var xc = new ClassAllocation();
            using (IDbConnection _db = OpenConnection())
            {
                xc = _db.Query<ClassAllocation>("dbo.AddClass", new { Schoolid = schoolid, classalloc.ClassID, classalloc.NoOfStreams, classalloc.TotalStudents }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return xc;
        }
        public static List<Class> GetClass(long schoolId)
        {
            var xc = new List<Class>();
            using (IDbConnection _db = OpenConnection())
            {
                xc = _db.Query<Class>("dbo.GetASchoolClassBySchoolId", new {SchoolID=schoolId}, commandType: CommandType.StoredProcedure).ToList();
            }
            return xc;
        }
        public static List<Class> GetClass()
        {
            var xc = new List<Class>();
            using (IDbConnection _db = OpenConnection())
            {
                xc = _db.Query<Class>("dbo.GetClass", commandType: CommandType.StoredProcedure).ToList();
            }
            return xc;
        }
        //UpdateRecreational
        public static RecreationalViewModel UpdateRecreational(int dd, string user, long sddc)
        {
            var result = new RecreationalViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<RecreationalViewModel>("dbo.UpdateSchRecreatByRecreIDSchoolID", new { SchRecreatConfigID = dd, Value = user, SchoolID = sddc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static LibraryViewModel UpdateSchoolExam(int dd, string user, long sddc)
        {
            var result = new LibraryViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.UpdateSchoolExamHallByExamIDSchoolID", new { ExamhallConfigID = dd, UserValue = user, SchoolID = sddc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static LibraryViewModel UpdateSchoolLibrary(int dd, string user, long sddc)
        {
            var result = new LibraryViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.UpdateSchoolLibraryByLibrIDSchoolID", new { SchoolLibraryFacilityID = dd, LibraryValue = user, SchoolID = sddc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //UpdateSchoolLibraryByLibrIDSchoolID
        public static LaboratoryViewModel UpdateSchoolLaboratoryValue(int dd, string user, long sddc)
        {
            var result = new LaboratoryViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.UpdateSchoolLaboratoryByLabConfigIDSchoolID", new { SchLabID = dd, LabValue = user, SchoolID = sddc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static LaboratoryViewModel AddLaboratoryValue(int dd, string user, int record_id = 0, long? schoolid = 0)
        {
            var result = new LaboratoryViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.AddLaboratoryValue", new { SchLabID = dd, LabValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static ExamHallViewModel AddExaminationHall(int dd, string user, long? schoolid = 0)
        {
            var result = new ExamHallViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ExamHallViewModel>("dbo.AddEamHallValue", new { ExamhallConfigID = dd, UserValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //AddExaminationHall
        public static LibraryViewModel AddLibraryValue(int dd, string user, long? schoolid = 0)
        {
            var result = new LibraryViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.AddLibraryValue", new { SchoolLibraryFacilityID = dd, LibraryValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }//AddClassroomApproval   UpdateSchoolEquipConfigForAssessment
        public static FacilityApprovalModel UpdateSchoolEquipConfigForAssessment(FacilityApprovalModel[] items)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())
            {
                foreach (var vx in items)
                {
                    if (vx.Value == true)
                    {
                        result =
                            _db.Query<FacilityApprovalModel>("dbo.UpdateSchoolEquipConfigForAssessment",
                                new { ID = vx.ID, value = vx.Value, }, commandType: CommandType.StoredProcedure)
                                .FirstOrDefault();
                    }

                }

            }

            return result;
        }

        public static ClassAllocationvModel AddClassroomToRecord(long schoolid, ClassAllocationvModel item)
        {
            var result = new ClassAllocationvModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationvModel>("dbo.AddClassroomToRecord", new { Schoolid = schoolid, Classid = item.ClassID, NoOfStreams = item.NoOfStreams, totalStudent = item.totalstudent, NoOfStreams2 = item.NoOfStreams2, classid2 = item.classid2, totalstudent2 = item.totalstudent2 }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }


            return result;
        }
        //AddClassroomToRecord
        public static ClassAllocationvModel AddClassroomApproval(long schoolid, ClassAllocationvModel[] items)
        {
            var result = new ClassAllocationvModel();
            foreach (var vx in items)
            {
                using (IDbConnection _db = OpenConnection())
                {
                    result = _db.Query<ClassAllocationvModel>("dbo.AddClassroomApproval", new { schoolid = schoolid, SchoolID = schoolid, Classid = vx.ClassID, NoOfStreams = vx.NoOfStreams, totalstudent = vx.totalstudent, classid2 = vx.classid2, NoOfStreams2 = vx.NoOfStreams2 }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }

            return result;
        }

        public static ClassAllocationvModel AddClassroomApproval(long schoolid, bool isapproved, ClassAllocationvModel[] items)
        {
            var result = new ClassAllocationvModel();
            foreach (var vx in items)
            {
                using (IDbConnection _db = OpenConnection())
                {
                    result = _db.Query<ClassAllocationvModel>("dbo.AddClassroomApproval", new { schoolid = schoolid, isApproved = isapproved, SchoolID = schoolid, Classid = vx.ClassID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }

            return result;
        }
        public static RecreationalViewModel AddRecreational(int dd, string user, long? schoolid = 0)
        {
            var result = new RecreationalViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<RecreationalViewModel>("dbo.AddRecreational", new { SchRecreatConfigID = dd, Value = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //GetClassForProportion

        public static ClassAllocationModel GetClassForProportion(long schoolid = 0)
        {
            var result = new ClassAllocationModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationModel>("dbo.GetClassForProportion", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //AddLaboratoryValue
        public static ReagentViewModel AddReagentValue(int dd, string user, long? schoolid = 0)
        {
            var result = new ReagentViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.AddReagentValue", new { SchReagentID = dd, ReagentValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }





        public static FacilityGrade GetgradeGood(string Good)
        {
            var result = new FacilityGrade();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGrade>("dbo.GetgradeGood", new { Good = Good }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityGrade GetgradePoor(string Poor)
        {
            var result = new FacilityGrade();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGrade>("dbo.GetgradePoor", new { Poor = Poor }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityGrade GetgradeVPoor(string Very_Poor)
        {
            var result = new FacilityGrade();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGrade>("dbo.GetgradeVPoor", new { Very_Poor = Very_Poor }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static AssessmentSummary AddAssessmentSummary(int gradeid, int itemid, int schoolid)
        {
            var result = new AssessmentSummary();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<AssessmentSummary>("dbo.AddAssessment", new { FacilityTypeSettingID = gradeid, RatingID = itemid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }



        //============================================================


        //===========================================================

        //AddNewSchoolSubject

        public static List<SchoolSubject> UpdateSchoolSjubject2(List<SubjectModel> schoolsubject, long schoolids)
        {
            var retvalue = new List<SchoolSubject>();
            var schsubobject = new SchoolSubject();

            try
            {

                using (IDbConnection _db = OpenConnection())
                {
                    foreach (var s in schoolsubject)
                    {
                        schsubobject = _db.Query<SchoolSubject>("dbo.UpdateSchoolSubjects", new { SubjectID = s.ID, SchoolID = schoolids, }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                        retvalue.Add(schsubobject);
                        //sch.Subjects.Add(Subject);
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retvalue;
        }

        public static List<SchoolSubject> UpdateSchoolSjubject(int schoolid, int? subjectid)
        {
            List<SchoolSubject> resullist;
            var resultview = new List<SchoolSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                //DeleteUserState
                _db.Query<SchoolSubject>("dbo.UpdateSchoolSubjects",
                    new
                    {
                        SchoolID = schoolid,
                        SubjectID = subjectid

                    }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                resullist = _db.Query<SchoolSubject>("dbo.GetSchoolSubjects",
                    new
                    {
                        SchoolID = schoolid,


                    }, commandType: CommandType.StoredProcedure).ToList();

                //GetUserStates
            }
            resultview.AddRange(resullist);
            return resultview;
        }

        //GetStaffDegreeCourse
        public static List<SchoolStaffModel> GetStaffDegreeCourse(int staffid)
        {
            List<SchoolStaffModel> result;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffDegreeCourse", new { StaffID = staffid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<vmSchoolStaffQualification> GetSchoolStaffBySchoolId2(long schoolid)
        {
            List<vmSchoolStaffQualification> result;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchoolStaffBySchoolID", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<SchoolStaffModel> GetSchoolStaffBySchoolId(long schoolid)
        {
            List<SchoolStaffModel> result;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolStaffModel>("dbo.GetSchoolStaffBySchoolID", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        //public static SchoolProfile GetMaximumCentreNumber(long? schoolid)
        //{
        //    //GetMaximumCentreNumber(CentreTableAddNewWithLGAWithAudit2);
        //}

        //UpdateAStaffSubjectClass
        //GetClassIfInserted
        public static ClassAllocationModel GetClassIfInserted(long schoolid)
        {
           var result = new ClassAllocationModel();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<ClassAllocationModel>("dbo.GetClassIfInserted", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }//Delet

        //GetSchoolSubjectNotApproved
        public static List<SchoolSubjectFacilityModel> GetSchoolSubjectEquipment(long schoolid)
        {
            var result = new List<SchoolSubjectFacilityModel>();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolSubjectFacilityModel>("dbo.GetSubjectsWithEquipment", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<SchoolSubjectFacilityModel> GetSchoolSubjectReagent(long schoolid)
        {
            var result = new List<SchoolSubjectFacilityModel>();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolSubjectFacilityModel>("dbo.GetSubjectsWithReagent", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<SchoolSubjectFacilityModel> GetSchoolSubjectLaboratory(long schoolid)
        {
            var result = new List<SchoolSubjectFacilityModel>();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolSubjectFacilityModel>("dbo.GetSubjectsWithLaboratory", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<vwSchoolsubjectForAssessment> GetSchoolSubjectForAssessment(long schoolid)
        {
            var result = new List<vwSchoolsubjectForAssessment>();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<vwSchoolsubjectForAssessment>("dbo.GetvwSchoolsubjectForAssessment", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static SchoolProfileViewModel GetSchoolById(long? schoolid)
        {
            if (schoolid == null)
            {
                return new SchoolProfileViewModel();
            }
            var result = new SchoolProfileViewModel();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SchoolProfileViewModel>("dbo.GetSchoolByID", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }//DeleteStaffDegree
        public static int DeleteStaffDegree(int staffID)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffDegree", new { staffID = staffID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //DeleteStaffSubjectToClassDependency
        public static int DeleteStaffSubjectToClassDependency(long schoolid, int? subjectid)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffSubjectToClassDependency", new { SchooliD = schoolid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteStaffSubjectBack(long schoolid,int? subjectid)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffSubjectBack", new { SchooliD = schoolid,SubjectID=subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteStaffSubject(int staffID)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffSubject", new { staffID = staffID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteStaffSubject(int staffID, int sub)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffSubject", new { staffID = staffID, sub = sub }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteStaff(int staffid, long schoolid)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaff", new { staffID = staffid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteAClass(int classid,long schoolid)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteAClass", new { ClassID = classid,SchoolID= schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static int DeleteStaffClass(int staffID)
        {
            int result = 0;

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<int>("dbo.DeleteStaffClass", new { staffID = staffID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //DeleteStaffClass  UpdateCousreForStaffID
        //GetSchoolByID
        public static vmSchoolStaffQualification UpdateSchoolStaffByID(int id, int courseid, long schoolid, int TitleID, string FirstName, string LastName, string ContactAddress, DateTime? DateEmployed, int CategoryID)
        {
            var result = new vmSchoolStaffQualification();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<vmSchoolStaffQualification>("dbo.UpdateSchoolStaffByID", new { SchoolStaffID = id, SchoolID = schoolid, TitleID = TitleID, FirstName = FirstName, LastName = LastName, ContactAddress = ContactAddress, DateEmployed = DateEmployed, CategoryID = CategoryID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static vmSchoolStaffQualification UpdateSchoolStaffDegreeByID(int id, int degreeid, int courseid)
        {
            var result = new vmSchoolStaffQualification();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<vmSchoolStaffQualification>("dbo.UpdateSchoolStaffDegreeByID", new { SchoolStaffID = id, DegreeID = degreeid, CourseID = courseid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //UpdateStaffOtherDegree
        public static SecondDegree UpdateStaffOtherDegree(int id, string seconddegree)
        {
            var result = new SecondDegree();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<SecondDegree>("dbo.UpdateStaffOtherDegree", new { staffID = id, OtherDegree = seconddegree }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static vmSchoolStaffQualification UpdateSchoolStaffByID(int id, long schoolid, int TitleIDx, string FirstNamex, 
            string LastNamex, DateTime? DateEmployedx, int CategoryIDx, string TRCNText)
        {
            var result = new vmSchoolStaffQualification();

            using (IDbConnection _db = OpenConnection())
            {
                //schoolStaff.TitleID, schoolStaff.FirstName, schoolStaff.LastName, schoolStaff.ContactAddress, schoolStaff.DateEmployed, schoolStaff.CategoryID
                result = _db.Query<vmSchoolStaffQualification>("dbo.UpdateSchoolStaffByID", new { SchoolStaffID = id, SchoolID = schoolid, TitleID = TitleIDx, FirstName = FirstNamex, LastName = LastNamex, DateEmployed = DateEmployedx, CategoryID = CategoryIDx, TRCNText = TRCNText }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        /*   public static SchoolEquipConfigViewModel UpdateFacility(int subjectIds,int id,int facid,int digitvalue,long schoolid)
        {

            var result = new SchoolEquipConfigViewModel();
            var resultcheck= new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result= _db.Query<SchoolEquipConfigViewModel>("dbo.UpdateFacility", new {FacilityGradeID = facid,DigitValue=digitvalue }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                resultcheck = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectIds, schoolid = schoolid },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            result.DigitValue = resultcheck.DigitValue;
            return result;
        }
      */
        //subjectIds, uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue,schoolid
        public static ReagentViewModel UpdateReagentFacilityValue(int SchoolReagentValueID, int FacilityID, int DigitValue, long schoolid)
        {
            var result = new ReagentViewModel();
            var resultcheck = new ReagentViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.UpdateReagentFacilityValue", new { ID = SchoolReagentValueID, FacilityGradeID = FacilityID, DigitValue = DigitValue }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            return result;
        }


        public static SchoolEquipConfigViewModel UpdateFacility(int subjectIds, int id, int facid, int digitvalue, long schoolid)
        {

            var result = new SchoolEquipConfigViewModel();
            var resultcheck = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.UpdateFacility", new { FacilityGradeID = facid, DigitValue = digitvalue }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                resultcheck = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectIds, schoolid = schoolid },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            result.DigitValue = resultcheck == null ? 0 : resultcheck.DigitValue;
            return result;
        }
        //UpdateFacility
        public static FacilityGrade GetgradeFair(string Fair)
        {
            var result = new FacilityGrade();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGrade>("dbo.GetgradeFair", new { Fair = Fair }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetgradeFair
        public static FacilityGradeModel GetgradeVGood(string Very_Good)
        {
            var result = new FacilityGradeModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGradeModel>("dbo.GetgradeVGood", new { Very_Good = Very_Good }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetgradeVGood
        public static FacilityGradeModel GetGradeExcell(string Excellent)
        {
            var result = new FacilityGradeModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityGradeModel>("dbo.GetGradeExcell", new { Excellent = Excellent }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static vwSchoolEquipmentValueModel GetSchoolEquipmentIDByRecordID(int schoolFacilityValueID, int schoolid)
        {
            var result = new vwSchoolEquipmentValueModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolEquipmentValueModel>("dbo.GetSchoolEquipmentIDByRecordID", new { schoolFacilityValueID = schoolFacilityValueID, schoolid = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetSchoolFacilityValuebyRecordID
        public static vwSchoolFacilityValueModel GetSchoolFacilityValuebyRecordID(int recordid)
        {
            var result = new vwSchoolFacilityValueModel();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolFacilityValueModel>("dbo.GetSchoolFacilityValuebyRecordID", new { record_id = recordid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //AddNewSchoolSubject
        public static SchoolEquipConfigViewModel AddEQuipmentValue(int subjectIds, int dd, string user, long? schoolid = 0)
        {
            SchoolEquipConfigViewModel result;
            SchoolEquipConfigViewModel resultchecking;

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.AddEQuipmentValue", new { SchoolFacilityValueID = dd, EquipmentValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                resultchecking = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectIds, schoolid = schoolid },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            if (result != null)
            {
                if (resultchecking != null) result.DigitValue = resultchecking.DigitValue;

            }
            return result;
        }

        //public static FacilityAssessment GetFacilityAssessment(int schoolid,int subjectid,int facilityid, int totalDbRecords = 0, int totalComputed = 0)
        //{

        //}

        //AddReagentValue
        //public static SchoolEquipmentValue AddEQuipmentValue(int dd, string user, int record_id = 0, long? schoolid = 0)
        //{
        //    var result = new SchoolEquipmentValue();

        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolEquipmentValue>("dbo.AddEQuipmentValue", new { SchoolFacilityValueID = dd, EquipmentValue = user, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}

        //public static SchoolFacilityValue AddEQuipmentValue(int dd, string user, int record_id = 0, long? schoolid = 0)
        //{
        //    var result = new SchoolFacilityValue();

        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolFacilityValue>("dbo.AddEQuipmentValue", new { SchoolFacilityValueID = dd, EquipmentValue = user, SchoolID = schoolid}, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}

        public static SchoolFacilityValue AddEquipFacilityValue(int dd, string user, int record_id = 0, long? schoolid = 0)
        {
            var result = new SchoolFacilityValue();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolFacilityValue>("dbo.AddFacilityValue", new { SchoolFacConfigID = dd, SchoolID = schoolid, UserValue = user }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        //public static List<Subject> GetSchoolSubjectItems(int? SubjectID, long schoolid)
        //{
        //    var result = new List<Subject>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<Subject>("dbo.GetCouncilSubjectsWithItems2", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;

        //}
        public static List<SchoolSubjectViewModel> GetSchoolSubjectItems(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetCouncilSubjectsWithItems2", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetSubjectItemsGetSchoolSubjectItems
        public static List<Subject> GetSubjectItems()
        {
            var result = new List<Subject>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Subject>("dbo.GetSubjectItems", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetSchoolSubject
        public static List<Subject> GetCouncilSubjectsItems()
        {
            var result = new List<Subject>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Subject>("dbo.GetCouncilSubjectsWithItems", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }


        public static List<vmSchoolStaffQualification> GetSchoolStaffs()
        {
            var result = new List<vmSchoolStaffQualification>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchooStaffs", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static vmSchoolStaffQualification GetSchoolPrincipalTeacher(int schoolid, string category)
        {
            var result = new vmSchoolStaffQualification();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchoolPrincipalTeacher", new { SCHOOLID = schoolid, SchoolStaffCategory = category }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //GetSchoolPrincipalTeacher
        public static List<vmSchoolStaffQualification> GetSchoolStaffs(long schoolid)
        {
            var result = new List<vmSchoolStaffQualification>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchooStaffs", new { SCHOOLID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //public static List<vmSchoolStaffQualification> GetSchoolStaffsByID(int schoolid)
        //{
        //    var result = new List<vmSchoolStaffQualification>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<vmSchoolStaffQualification>("dbo.GetSchooStaffsID", new { SCHOOLID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;

        //}
        //[GetSchoolEquipment]


        public static List<SchoolEquipConfigViewModel> GetSchoolFacilitiesBySchoolID(int? schoolid, int? subjectid)
        {
            var result = new List<SchoolEquipConfigViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolFacilitiesBySchoolID", new { SchoolID = schoolid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }


        //public static List<SchoolEquipConfigViewModel> GetSchoolEquipment(int? subjectid,long schoolid)
        //{
        //    var result = new List<SchoolEquipConfigViewModel>();
        //    var resultchecking = new SchoolEquipConfigViewModel();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolEquipConfig", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
        //        resultchecking = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectid, schoolid = schoolid },
        //               commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;

        //}


        public static SchoolEquipConfigViewModel GetExistingSchoolEquipValueById(int id)
        {
            var result = new SchoolEquipConfigViewModel();
            //var resultchecking = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetExistingSchoolEquipValueById", new { id = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();

            }


            return result;

        }


        public static List<SchoolEquipConfigViewModel> GetExistingSchoolEquipValue(int? subjectid, long schoolid, int facid)
        {
            var result = new List<SchoolEquipConfigViewModel>();
            var resultchecking = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetExistingSchoolEquipValue", new { sub = subjectid, SchoolID = schoolid, FacilityCategoryID = facid }, commandType: CommandType.StoredProcedure).ToList();

            }


            return result;

        }

        public static ReagentViewModel FetchExistingReagentRecord(long schoolid, int dd)
        {
            var result = new ReagentViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<ReagentViewModel>("dbo.FetchExistingReagentRecord", new { Schoolid = schoolid, SchReagentID = dd },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();

            }


            return result;

        }
        //FetchExistingReagentRecord

        public static List<SchoolExamHallConfigModel> GetExaminationFacilityAssess(long schoolid)
        {
            var result = new List<SchoolExamHallConfigModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolExamHallConfigModel>("dbo.GetSchoolExamHallForAssess", new { Schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            return result;

        }
        //GetExaminationFacilityAssess

        public static List<SchoolRecreatConfigModel> GetSchoolReacreationalForAssess(long schoolid)
        {
            var result = new List<SchoolRecreatConfigModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolRecreatConfigModel>("dbo.GetSchoolRecreational", new { school = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            return result;

        }
        //GetSchoolReacreationalForAssess

        public static List<SchoolLibraryConfigModel> GetSchoolLibraryForAssess(long schoolid)
        {
            var result = new List<SchoolLibraryConfigModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolLibraryConfigModel>("dbo.GetSchoolLibraryForAssess", new { Schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            return result;

        }


        //AddClassroomApproval
        //UpdateForDerecognition_X  GetSchoolReagentConfigForAssessment
        public static List<ReagentViewModel> GetSchoolReagentConfigForAssessment(int? subjectid, long schoolid)
        {
            var result = new List<ReagentViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.GetSchoolEquipConfigForAssessment", new { SubjectID = subjectid, schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            return result;

        }
        public static List<SchoolEquipConfigViewModel> GetSchoolEquipConfigForAssessment(int? subjectid, long schoolid)
        {
            var result = new List<SchoolEquipConfigViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolEquipConfigForAssessment", new { SubjectID = subjectid, schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();


            }

            return result;

        }
        public static List<ReagentViewModel> GetSchoolupdateReagentDigitValue(long? schoolid)
        {
            List<ReagentViewModel> result;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.GetSchoolupdateReagentDigitValue", new { Schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetSchoolupdateEquipmentDigitValue
        public static List<SchoolEquipConfigViewModel> GetSchoolupdateEquipmentDigitValue(long? schoolid)
        {
            List<SchoolEquipConfigViewModel> result;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolupdateEquipmentDigitValue", new { Schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<SchoolEquipConfigViewModel> GetSchoolupdateEquipmentDigitValue(long? schoolid, long? subjectid)
        {
            List<SchoolEquipConfigViewModel> result;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolEquipmentBySubjectID2", new { Schoolid = schoolid, subjectid = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<SchoolReagentConfig> GetSchoolReagentConfig(int? subjectid)
        {
            var result = new List<SchoolReagentConfig>();
            var resultchecking = new SchoolReagentConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolReagentConfig>("dbo.GetSchoolReagentConfig", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
                //resultchecking = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectid, schoolid = schoolid },
                //        commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            //if (resultchecking != null)
            //{
            //    foreach (var item in result)
            //    {
            //        item.DigitValue = resultchecking.DigitValue;
            //    }
            //    //  //  result = result.Select(item => item.DigitValue = resultchecking.DigitValue).ToList();
            //}

            return result;

        }
        //GetSchoolReagentConfig
        public static List<SchoolEquipConfigViewModel> GetSchoolEquipment(int? subjectid)
        {
            var result = new List<SchoolEquipConfigViewModel>();
            var resultchecking = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.GetSchoolEquipConfig", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
                //resultchecking = _db.Query<SchoolEquipConfigViewModel>("dbo.ComputeDigitValue", new { SubjectID = subjectid, schoolid = schoolid },
                //        commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            //if (resultchecking != null)
            //{
            //    foreach (var item in result)
            //    {
            //        item.DigitValue = resultchecking.DigitValue;
            //    }
            //    //  //  result = result.Select(item => item.DigitValue = resultchecking.DigitValue).ToList();
            //}

            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectByIDGeneral(long schoolid)
        {
            var results = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                results = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectByID", new { SchoolSubjectID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return results;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectById(long? schoolid)
        {
            var results = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                results = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectByID", new { SchoolSubjectID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return results;

        }
        public static SchoolProfileViewModel1 GetSchoolByValidCentre(string centreNumber)
        {
            var result = new SchoolProfileViewModel1();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolByValidCentre", new { centreNumber = centreNumber }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetSchoolSubjectByID
        public static List<SchoolProfileViewModel1> GetSchoolByCentre(string centreNumber)
        {
            var result = new List<SchoolProfileViewModel1>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolByCentre", new { centreNumber = centreNumber }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }//GetSchoolExamHallValue
        public static List<ExamHallViewModel> GetSchoolExamHallValue(long sddc)
        {
            var result = new List<ExamHallViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ExamHallViewModel>("dbo.GetSchoolExamHallValue", new { SchoolID = sddc,count=0 }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<ExamHallViewModel> GetSchoolExamHall(long schoolid)
        {
            var result = new List<ExamHallViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ExamHallViewModel>("dbo.GetSchoolExamHall",new {SchoolID= schoolid,count=0}, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //  GetSchoolByCentre
        public static List<SchoolExamHallConfigModel> GetExamHallBySchoolId(long schoolid)
        {
            var result = new List<SchoolExamHallConfigModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolExamHallConfigModel>("dbo.GetExamHallBySchoolID", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<LibraryViewModel> GetSchoolLibraryValue(long sddc)
        {
            var result = new List<LibraryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.GetSchoolLibraryValue", new { SchoolID = sddc }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<LibraryViewModel> GetSchoolLibrary(long schoolid)
        {
            var result = new List<LibraryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LibraryViewModel>("dbo.GetSchoolLibrary",new {SchoolID=schoolid,count=0}, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetSchoolLibrary
        public static List<ReagentViewModel> GetSchoolReagent(int? subjectid)
        {
            var result = new List<ReagentViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReagentViewModel>("dbo.GetReagent", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        //GetRecreationalFacilityValue
        public static List<RecreationalViewModel> GetRecreationalFacilityValue(long sddc)
        {
            var result = new List<RecreationalViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<RecreationalViewModel>("dbo.GetRecreationalFacilityValue", new { SchoolID = sddc,count=0 }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static List<RecreationalViewModel> GetSchoolRecreational(long schoolid)
        {
            var result = new List<RecreationalViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
               result = _db.Query<RecreationalViewModel>("dbo.GetSchoolRecreational",new {SchoolID= schoolid, count=0}, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }//GetSchoolFacilityvalueID


        public static SchoolFacilityValue GetSchoolFacilityvalueID(int SchoolEquipConfigID)
        {
            var result = new SchoolFacilityValue();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolFacilityValue>("dbo.GetSchoolFacilityvalueID", new { SchoolEquipConfigID = SchoolEquipConfigID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //GetSchoolRecreational 
        public static List<LaboratoryViewModel> GetSchoolLaboratoryForAssessment(int subjectid, long schoolid)
        {
            var result = new List<LaboratoryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.GetSchoolLaboratoryForAssessment", new { SubjectID = subjectid, SchoolId = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static List<LaboratoryViewModel> GetSchoolLaboratoryValues(int? subjectid, long sddc)
        {
            var result = new List<LaboratoryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.GetSchoolLaboratoryValues", new { SubjectID = subjectid, SchoolID = sddc,count=0 }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        //GetSchoolLaboratoryValues
        public static List<LaboratoryViewModel> GetSchoolLaboratory(int? subjectid)
        {
            var result = new List<LaboratoryViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LaboratoryViewModel>("dbo.GetLaboratoryBySubject", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolProfileViewModel> GetSchoolProfile2()
        {
            var result = new List<SchoolProfileViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel>("dbo.GetAllSchoolProfile2", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolProfileIndexModel> SchoolPreviewModelIndex()
        {
            var result = new List<SchoolProfileIndexModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileIndexModel>("dbo.SchoolProfileIndex", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolProfileIndexModel> SchoolProfileIndex()
        {
            var result = new List<SchoolProfileIndexModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileIndexModel>("dbo.SchoolProfileIndex", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolStaffProfileViewModel> GetSchoolProfile()
        {
            var result = new List<SchoolStaffProfileViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffProfileViewModel>("dbo.GetAllSchoolProfile", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        //public static List<vwSchoolProfileListing> GetSchoolProfile()
        //{
        //    var result = new List<vwSchoolProfileListing>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<vwSchoolProfileListing>("dbo.GetAllSchoolProfile", commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;

        //}

        public static List<AssessmentViewModel> GetAccessmentFacility()
        {
            var result = new List<AssessmentViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<AssessmentViewModel>("dbo.GetSummaryFacility", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        //GetStaffWithSubject
        public static List<SchoolStaffProfileViewModel> GetStaffWithSubject(long schoolid)
        {
            var result = new List<SchoolStaffProfileViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffProfileViewModel>("dbo.GetStaffWithSubject", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //
        public static SchoolSubjectViewModel GetSchoolSubjectRowId(long schoolid, int subjectid)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectRowId", new { SchoolID = schoolid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //CheckIfTextBooksAdded2School
        public static SchoolSubjectViewModel CheckIfTextBooksAdded2School(int textbookToSchoolSubjectRowId, string avaiilableTextbooks)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.CheckIfTextBooksAdded2School", new { SchoolSubjectID = textbookToSchoolSubjectRowId, AvaiilableTextbooks = avaiilableTextbooks }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static bool CheckIfClassAddedForTeacher(int staffId, int classId, int subjectid)
        {
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.CheckIfClassAddedForTeacher", new { SchoolStaffID = staffId, ClassID = classId, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (result != null && result.ClassID == classId && result.StaffID == staffId && result.SubjectID == subjectid)
                {
                    return true;
                }
            }

            return false;
        }
        public static SchoolSubjectViewModel CheckIfTextBooksInTextbookBank(int subjectId, string avaiilableTextbooks)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.CheckIfTextBooksInTextbookBank", new { SchoolSubjectID = subjectId, AvaiilableTextbooks = avaiilableTextbooks }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //AddNewSchoolSubject
        public static SubjectModel AddNewSchoolSubject(int textbookToSchoolSubjectRowId, string textBook, bool istextbookAttached)
        {
            var result = new SubjectModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.AddNewSchoolSubject", new { AvaiilableTextbooks = textBook, SchoolSubjectID = textbookToSchoolSubjectRowId, IsTextBookAttached = istextbookAttached }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static SchoolSubjectViewModel AddTextbookToSchoolSubject(int textbookToSchoolSubjectRowId, string textBook, bool istextbookAttached)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.AddTextbookToSchoolSubject", new { AvaiilableTextbooks = textBook, SchoolSubjectID = textbookToSchoolSubjectRowId, IsTextBookAttached = istextbookAttached }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }


        public static List<SchoolSubjectViewModel> AddNewTextbook(string TextBook, int SubjectId)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.AddNewTextbook", new { TextBook = TextBook, SubjectId = SubjectId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //AddNewTextbook  
        public static List<SchoolSubjectViewModel> GetAllAvailableTextBookBank(int subjectId)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetAllAvailableTextBookBank", new { SubjectID = subjectId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetAllAvailableTextBook
        public static List<SchoolSubjectViewModel> GetAllAvailableTextBook()
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetAllAvailableTextBook", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static SchoolStaffModel UpdateAStaffSubjectClass(int schoolId = 0, int staffId = 0, int sub = 0, int classId = 0)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolStaffModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.UpdateAStaffSubjectClass", new { SchoolID = schoolId, SchoolStaffID = staffId, SubjectID = sub, ClassID = @classId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateAStaffSubjectClass
        public static SchoolSubjectViewModel GetSchoolSubjectsTextbookRow(long schoolid, int subjectId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateASchoolSubject", new { SchoolID = schoolid, SubjectID = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateASchoolSubject  GetSchoolSubjectsTextbookRow
        public static SchoolSubjectViewModel UpdateASchoolSubject(SchoolSubjectViewModel model, long schoolid, int subjectId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateASchoolSubject", new { Textbook = model.AvailableTextbooks, isTextBookAttached = model.isTextBookAttached, SchoolID = schoolid, SubjectID = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolSubjectViewModel GetASchoolSubject(long schoolid, int subjectId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectRecord", new { SchoolID = schoolid, SubjectId = subjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<SchoolStaffModel> GetStaffSubjectToClassFormData(int staffSubjectRowId, int classId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectToClassFormData", new { StaffSubjectID = staffSubjectRowId, ClassID = classId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetStaffSubjectClasses
        public static List<SchoolStaffModel> GetStaffSubjectClasses(long schoolId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffSubjectClasses", new { SchoolID = schoolId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetStaffInfoForEdit
        public static List<StaffModelForEdit> GetAStaffInfoForEdit(int staffid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<StaffModelForEdit>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StaffModelForEdit>("dbo.GetAStaffInfoForEdit", new { StaffID = staffid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<StaffModelForEdit> GetStaffInfoForEdit(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<StaffModelForEdit>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StaffModelForEdit>("dbo.GetStaffInfoForEdit", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetASchoolTextBooks(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetASchoolTextBooks", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectsByTextbook(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectsByTextbook", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetCoreSubjectsViewBySchoolId(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetCoreSubjectsViewBySchoolId", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<vwOnlyCoreSubject> GetCoreSubjects()
        {
            var result = new List<vwOnlyCoreSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwOnlyCoreSubject>("dbo.ProcvwOnlyCoreSubject", commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }
        public static List<vwOnlyTradeSubject> GetOnlyTradeSubjects()
        {
            var result = new List<vwOnlyTradeSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwOnlyTradeSubject>("dbo.ProcvwOnlyTradeSubject", commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }
        public static List<vwNotCoreTradeSubject> GetNotCoreTradeSubject()
        {
            var result = new List<vwNotCoreTradeSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwNotCoreTradeSubject>("dbo.ProcvwNotCoreTradeSubject", commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }
        public static List<vwSchoolInfo> GetSchoolDetails()
        {
            var result = new List<vwSchoolInfo>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwSchoolInfo>("dbo.ProcvwSchoolInfo", commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }
        //procGetStateToAdmin
        public static vwGetStateToAdmin GetStateToAdmin(int statecode)
        {
            var result = new vwGetStateToAdmin();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwGetStateToAdmin>("dbo.procGetStateToAdmin", new { StateCode = statecode }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;

        }
        public static vwSchoolCompletionReport2 GetSchoolAndStaffForEmail(long schoolid)
        {
            var result = new vwSchoolCompletionReport2();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwSchoolCompletionReport2>("dbo.procSchoolCompletionReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;

        }
        public static vwSchoolCompletionReport2 GetSchoolAndStaffInformation(long schoolid)
        {
            var result = new vwSchoolCompletionReport2();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwSchoolCompletionReport2>("dbo.procSchoolCompletionReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return result;

        }

        public static List<vwSchoolCompletionReport2> GetSchoolAndStaffReportDetails(long schoolid)
        {
            var result = new List<vwSchoolCompletionReport2>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwSchoolCompletionReport2>("dbo.procSchoolCompletionReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }

        public static List<vwCentreNumberReport> GetSchoolReportDetails(long schoolid)
        {
            var result = new List<vwCentreNumberReport>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolCentreSubInfo
                result = _db.Query<vwCentreNumberReport>("dbo.procCentreNumberReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }

            return result;

        }
        public static List<vwSchoolFooterReportMessage> GetFooterReportMessage()
        {
            var result = new List<vwSchoolFooterReportMessage>();
            using (IDbConnection _db = OpenConnection())
            {
                //ProcvwSchoolReportFooterMessage
                result = _db.Query<vwSchoolFooterReportMessage>("dbo.ProcvwSchoolReportFooterMessage", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<vwSchoolSubjectFacilityReport.vwSubjectRecogntionReport> GetSubjectForRecognition(long schoolid)
        {
            var result = new List<vwSchoolSubjectFacilityReport.vwSubjectRecogntionReport>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolSubectsGetAll
                result = _db.Query<vwSchoolSubjectFacilityReport.vwSubjectRecogntionReport>("dbo.procSubjectForRecognition", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<vwSchoolSubject> GetSubjectBySchool(long schoolid)
        {
            var result = new List<vwSchoolSubject>();
            using (IDbConnection _db = OpenConnection())
            {
                //SchoolSubectsGetAll
                result = _db.Query<vwSchoolSubject>("dbo.procSchoolSubectsGetAll", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static List<SchoolSubjectViewModel> GetOtherSubjectViewSchoolId(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetOtherSubjectViewSchoolId", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        //GetStaffBySchoolId
        public static List<SchoolStaffModel> GetStaffMembers(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetStaffBySchoolId", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //ReasonsForDerecognition
        //GetAASchoolSubjectApprovedBySchoolID
        public static SchoolSubjectViewModel GetAASchoolSubjectApprovedBySchoolID(int subjectid, long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetAASchoolSubjectApprovedBySchoolID", new { SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<ReasonsToDereSubjectcModel> ReasonsForDerecognition()
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<ReasonsToDereSubjectcModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ReasonsToDereSubjectcModel>("dbo.ReasonsForDerecognition", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectNotApproved(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectNotApproved", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectApprovedBySchoolIdB(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectApprovedBySchoolIdB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectApprovedBySchoolId(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectApprovedBySchoolId", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> UpdateSchoolSubjectsApproved(long subjectid, long schoolid, bool approve)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectsApproved", new { SubjectId = subjectid, SchoolID = schoolid, isApproved = approve }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //UpdateSchoolSubjectsApprovedPhyAgr
        public static List<SchoolSubjectViewModel> UpdateSchoolSubjectsApprovedPhyAgr(long subjectid, long schoolid, bool approve, int qualifiedStaff, int examMalpractice, int equipment, int lab)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectsApprovedPhyAgr", new { SubjectId = subjectid, SchoolID = schoolid, isApproved = approve, QualifiedStaff = qualifiedStaff, AdequateEquipment = equipment, AdequateLaboratory = lab, ExaminationMalpractice = examMalpractice }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //public static List<SchoolSubjectViewModel> UpdateSchoolSubjectsApproved(long subjectid, long schoolid, bool approve, int qualifiedStaff, int examMalpractice, int equipment, int lab, int reagent)
        //{
        //    //var retvalue = new List<SchoolSubject>();
        //    //var schsubobject = new SchoolSubject();
        //    var result = new List<SchoolSubjectViewModel>();
        //    using (IDbConnection _db = OpenConnection())
        //    {
        //        result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectsApproved", new { SubjectId = subjectid, SchoolID = schoolid, isApproved = approve}, commandType: CommandType.StoredProcedure).ToList();
        //    }
        //    return result;

        //}UpdateSchoolSubjectsApprovedStaff
        public static List<SchoolSubjectViewModel> UpdateSchoolSubjectsApprovedStaff(long subjectid, long schoolid, bool approve, int qualifiedStaff, int examMalpractice)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectsApprovedStaff", new { SubjectId = subjectid, SchoolID = schoolid, isApproved = approve, QualifiedStaff = qualifiedStaff, ExaminationMalpractice = examMalpractice }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }//UpdateSchoolSubjecToTrueIfAfter
        public static SchoolSubjectViewModel UpdateSchoolSubjecToTrueIfAfter(long schoolid, long subjectid, bool approve)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjecToTrueIfAfter", new { SubjectID = subjectid, SchoolID = schoolid, IsApproved = approve }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> UpdateSchoolSubjectsApproved(long subjectid, long schoolid, bool approve, int qualifiedStaff, int examMalpractice, int equipment, int lab, int reagent)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.UpdateSchoolSubjectsApproved", new { SubjectId = subjectid, SchoolID = schoolid, isApproved = approve, QualifiedStaff = qualifiedStaff, ExaminationMalpractice = examMalpractice, AdequateEquipment = equipment, AdequateLaboratory = lab, AdequateReagent = reagent }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetItemSectionSubjectBySchoolB(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetItemSectionSubjectBySchoolB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }//
        public static List<SchoolSubjectViewModel> GetItemSectionSubjectBySchool(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetItemSectionSubjectBySchool", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }//GetEquipmentFacilityRated
        public static FacilityRatedModel GetLaboratoryFacilityRated(long schoolid, int subjectid, int facilityid)
        {
            var result = new FacilityRatedModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityRatedModel>("dbo.GetLaboratoryFacilityRated", new { SchoolID = schoolid, SubjectID = subjectid, FacilityID = facilityid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static FacilityRatedModel GetEquipmentFacilityRated(long schoolid, int subjectid, int facilityid)
        {
            var result = new FacilityRatedModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityRatedModel>("dbo.GetEquipmentFacilityRated", new { SchoolID = schoolid, SubjectID = subjectid, FacilityID = facilityid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static FacilityRatedModel GetReagentFacilityRated(long schoolid, int subjectid, int facilityid)
        {
            var result = new FacilityRatedModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityRatedModel>("dbo.GetReagentFacilityRated", new { SchoolID = schoolid, SubjectID = subjectid, FacilityID = facilityid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetStaffSectionSubjectBySchoolB(long schoolid, int classId)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetStaffSectionSubjectBySchoolB", new { SchoolID = schoolid, ClassID = classId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetStaffSectionSubjectBySchool(long schoolid,int classid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetStaffSectionSubjectBySchool", new { SchoolID = schoolid,ClassID=classid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetTextBooksBySchool
        public static List<SubjectModel> GetTextBooksBySchool(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetTextBooksBySchool", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetSchoolSubjectsTrueFalse
        public static List<SubjectModel> GetSchoolSubjectsActive(long schoolid, bool isActive)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSchoolSubjects", new { SchoolID = schoolid, IsActive = isActive }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SubjectModel> GetSchoolSubjectsTrueFalse(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSchoolSubjectsTrueFalse", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SubjectModel> GetSubjectsSchool(long schoolid)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSchoolSubjects", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        /// <summary>
        /// Get school subjects for the activeselected school
        /// The model return the view definition vwSchoolSubjectListing
        /// </summary>
        /// <param name="schoolid"></param>
        /// <returns></returns>
        public static List<vwSchoolSubjectListing> GetSchoolSubjects(long schoolid)
        {
            var result = new List<vwSchoolSubjectListing>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolSubjectListing>("dbo.GetSchoolSubjectList", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<vwSchoolStaffsSubjects> GetStaffsSubjects(long schoolid)
        {
            var result = new List<vwSchoolStaffsSubjects>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolStaffsSubjects>("dbo.procvwGetStaffsSubjects", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<vwSchoolStaffsSubjects> GetStaffsSubjects(long schoolid,int staffid)
        {
            var result = new List<vwSchoolStaffsSubjects>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolStaffsSubjects>("dbo.procvwGetStaffsSubjects2", new { SchoolID = schoolid, SchoolStaffID= staffid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<SchoolSubjectViewModel> GetSchoolSubjects(long schoolid, bool isActive)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjects", new { SchoolID = schoolid, IsActive = isActive }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static List<SchoolSubjectViewModel> GetSchoolSubjectsTextBook(long schoolid, bool isActive)
        {
            //var retvalue = new List<SchoolSubject>();
            //var schsubobject = new SchoolSubject();
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectsTextsBooks", new { SchoolID = schoolid, IsActive = isActive }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //CentreTableUpdateForIsBarred(centrename, statecode, lga, localCode, examtype, categoryid, maxCentre)
        public static SchoolCentreIsRecognisedIsBarred CentreTableUpdateForIsBarred(long sdc, bool isBarred)
        {
            var result = new SchoolCentreIsRecognisedIsBarred();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolCentreIsRecognisedIsBarred>("dbo.CentreTableUpdateForIsBarred", new { SchoolID = sdc, IsBarred = isBarred }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolCentreIsRecognisedIsBarred CentreTableUpdateForIsRecognised(long sdc, bool IsRecognised)
        {
            var result = new SchoolCentreIsRecognisedIsBarred();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolCentreIsRecognisedIsBarred>("dbo.CentreTableUpdateForIsRecognised", new { SchoolID = sdc, IsRecognised = IsRecognised }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }


        public static Centre GenerateCentreForSchool(string centrename, string state, string lga, int examtype, string maxCentre, long SchoolID)
        {
            var result = new Centre();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Centre>("dbo.CentreTableAddNewWithLGAWithAudit", new { centrenameReal = centrename, State = state, lga = lga, examType = examtype, centreno = maxCentre, SchoolID = SchoolID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        //GenerateCentreForSchool(centrename, statecode, lga, localCode, examtype,categoryid, maxCentre);
        public static int GetLocalCode(int lgaId)
        {
            int result = 0;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<int>("dbo.GetLocalCode", new { lgaID = lgaId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //GetLocalCode
        public static Centre GenerateCentreForSchool()
        {
            var result = new Centre();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Centre>("dbo.GetCentreNumbers", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static string GetMaxCentreNumber()
        {
            var result = "";
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<string>("dbo.GetMaxCentreNumber", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<Centre> GetCentreNumbers()
        {
            var result = new List<Centre>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Centre>("dbo.GetCentreNumbers", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetCentreNumbers
        public static List<SchoolEquipConfig> GetSchoolEquipConfigPara(int? subjectid)
        {
            var result = new List<SchoolEquipConfig>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfig>("dbo.GetSchoolEquipConfigPara", new { SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }

        public static SchoolProfileViewModel1 GetSchoolByIDSum(long schoolid)
        {
            var result = new SchoolProfileViewModel1();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolByID", new { schoolId = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionEquipmentB(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionEquipmentB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //FetchReasonsFromItemSectionEquipment
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionEquipment(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionEquipment", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionReagentB(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionReagentB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionReagent(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionReagent", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //ProcDeleteSchoolFootMessage
        public static List<SchoolSubjectFootMessage> ProcDeleteSchoolFootMessage(long schoolid)
        {
            var result = new List<SchoolSubjectFootMessage>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectFootMessage>("dbo.ProcDeleteSchoolFootMessage", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectFootMessage> ProcSchoolFootMessage()
        {
            var result = new List<SchoolSubjectFootMessage>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectFootMessage>("dbo.ProcSchoolFootMessage", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static SchoolSubjectFootMessage AddSchoolSubjectMessage(long schoolid, string schoolSubjectMessage, int subjectId, int status)
        {
            var result = new SchoolSubjectFootMessage();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectFootMessage>("dbo.AddSchoolSubjectMessage", new { SchoolID = schoolid, SchoolSubjectMessage = schoolSubjectMessage, SubjectID = subjectId, Statuss = status }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }//GetSchoolFootMessageRecog
        public static List<SchoolSubjectFootMessage> GetSchoolFootMessageRecog(long schoolid)
        {
            var result = new List<SchoolSubjectFootMessage>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectFootMessage>("dbo.GetSchoolFootMessageRecog", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static UserViewModel GetUserForSchool(long schoolid)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUserForSchool", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<FacilityRatedModel> FetchReasonsFromItemSectionFacility(long schoolid, int subjectid, bool isApproved)
        {
            var result = new List<FacilityRatedModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityRatedModel>("dbo.FetchReasonsFromItemSectionFacility", new { SchoolID = schoolid, SubjectID = subjectid, IsApproved = isApproved }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<FacilityRatedModel> FetchReasonsFromItemSectionFacilityB(long schoolid, int subjectid, bool isApproved)
        {
            var result = new List<FacilityRatedModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<FacilityRatedModel>("dbo.FetchReasonsFromItemSectionFacilityB", new { SchoolID = schoolid, SubjectID = subjectid, IsApproved = isApproved }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromStaff2SubjectClassB(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromStaff2SubjectClassB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromStaff2SubjectClass(long schoolid, int classId)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromStaff2SubjectClass", new { SchoolID = schoolid, ClassID = classId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionLaboratoryB(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionLaboratoryB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> FetchReasonsFromItemSectionLaboratory(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.FetchReasonsFromItemSectionLaboratory", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectsApprovedB(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectsApprovedB", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolSubjectViewModel> GetSchoolSubjectsApproved(long schoolid)
        {
            var result = new List<SchoolSubjectViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.GetSchoolSubjectsApproved", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<CentreTableItemSectionModel> GetItemSectionAssessment(long schoolid)
        {
            var result = new List<CentreTableItemSectionModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<CentreTableItemSectionModel>("dbo.GetItemSectionAssessment", new { sdc = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //GetItemSectionAssessment
        public static SchoolProfileViewModel1 GetSchoolEditByID(long schoolid)
        {
            var result = new SchoolProfileViewModel1();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolByID", new { schoolId = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }


        public static List<SchoolStaffModel> GetSchooStaffInfo(long schoolid)
        {
            var result = new List<SchoolStaffModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffModel>("dbo.GetSchooStaffInfo", new { SCHOOLID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //AddCentreFileToSchoolProfile
        public static SchoolProfileViewModel1 AddCentreFileToSchoolProfile(string centrenum, string schoolName,int lgaid,int statecode,int officeid)
        {
            var result = new SchoolProfileViewModel1();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.AddCentreFileToSchoolProfile", new { CenterNo = centrenum, schoolname = schoolName,StateID=statecode,LgaID= lgaid,OfficeID= officeid}, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static SchoolProfileViewModel1 GetSchoolByIDEdit2(long schoolid = 0, string centerno = "")
        {
            var result = new SchoolProfileViewModel1();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolByIDEdit", new { schoolId = schoolid, centreNo = centerno }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolProfileViewModel GetSchoolByIDEdit(long? schoolid, string centernumber = "")
        {
            var result = new SchoolProfileViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel>("dbo.GetSchoolByIDEdit", new { schoolId = schoolid, centreno = centernumber }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //procSchoolSubectsReport
        public static List<vwSchoolSubjectReport> GetSchoolSubjectReport(long schoolid)
        {
            var result = new List<vwSchoolSubjectReport>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolSubjectReport>("dbo.procSchoolSubectsReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        //
        public static vwSchoolSubjectFacilityReport GetSchoolSubjectFacilityForEmail(long schoolid)
        {
            var result = new vwSchoolSubjectFacilityReport();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolSubjectFacilityReport>("dbo.procSchoolSubectsFacilityReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static List<vwSchoolSubjectFacilityReport> GetSchoolSubjectFacilityReport(long schoolid)
        {
            var result = new List<vwSchoolSubjectFacilityReport>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolSubjectFacilityReport>("dbo.procSchoolSubectsFacilityReport", new { SchoolID = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static vwSchoolCompletionReport2 GetSchoolStaffCountX(long schoolid)
        {
            var result = new vwSchoolCompletionReport2();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolCompletionReport2>("dbo.procSchoolCompletionReport", new { schoolId = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        public static vwSchoolStaffCount GetSchoolStaffCount(long schoolid)
        {
            var result = new vwSchoolStaffCount();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolStaffCount>("dbo.sp_GetSchoolStaffCount", new { schoolId = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateDateRecognisedForSchool
        public static SchoolProfileViewModel UpdateDateRecognisedForSchool(DateTime dateinspection,long schoolid)
        {
            var result = new SchoolProfileViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel>("dbo.UpdateDateRecognisedForSchool", new { SchoolID = schoolid, DateRecognition = dateinspection }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        //procGetSupervisorRoleOffice
        public static UserViewModel procGetSupervisorRoleOffice(int officeId)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.procGetSupervisorRoleOffice", new { OfficeID = officeId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //UpdateSchoolProfileStatus
        public static string UpdateSchoolProfileStatus(long schoolid,string status)
        {
            string result;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<string>("dbo.UpdateSchoolProfileStatus", new { schoolId = schoolid,Status=status }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }

        public static SchoolProfileViewModel GetSchoolByID(long? schoolid)
        {
            var result = new SchoolProfileViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolProfileViewModel>("dbo.GetSchoolByID", new { schoolId = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }
        //GetSchoolByID
        public static SchoolEquipConfig GetSchoolEquipConfigByID(int dd)
        {
            var result = new SchoolEquipConfig();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfig>("dbo.[GetSchoolEquipConfigByID]", new { SchoolEquipConfigID = dd }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;

        }


        public static List<UserViewModel> GetAdministratorsOnly()
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAdministratorsOnly", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchoolEquipmentReportModel> GetEquipmentReportBySubjectID(long schid, int subjectid)
        {
            var result = new List<SchoolEquipmentReportModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipmentReportModel>("dbo.GetSchoolEquipmentBySubjectID", new { SchoolID = schid, SubjectID = subjectid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }



        //schoolequipconfig
        public static List<SchoolEquipConfig> GetSchoolEquipConfig()
        {
            var result = new List<SchoolEquipConfig>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfig>("dbo.GetSchoolEquipConfig", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;

        }
        public static List<SchStaffCourseModel> GetCourseBySubjectId(int SubjectID)
        {
            List<SchStaffCourseModel> result;
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchStaffCourseModel>("dbo.GetCourseBySubjectID", new { SubjectID = SubjectID }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }


        public static List<LocalModel> GetLgas(string stateID)
        {
            int vv = int.Parse(stateID);
            var result = new List<LocalModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LocalModel>("dbo.GetLgas", new { StateID = vv }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }  
        public static List<LocalModel> GetLgasByStateId(int stateId)
        {
            var result = new List<LocalModel>();
            using (var _db = OpenConnection())
            {
                result = _db.Query<LocalModel>("dbo.GetLgasByStateID", new { StateID = stateId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<LocalModel> InsertOfficeLocalgovernment(int officeId, int stateCode, int lgaId)
        {
            var result = new List<LocalModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<LocalModel>("dbo.InsertOfficeLocalgovernment", new {OfficeID = officeId, StateCode = stateCode, LgaID= lgaId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        
        public static List<UserViewModel> GetUsersForRoles()
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUsersForRoles", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<UserViewModel> GetUsersForRoles(string office)
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUsersForRoles", new { offfice = office }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetUsersForRoles
        public static List<UserViewModel> GetAllUsers()
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAllUsers", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<UserViewModel> GetUsers(string office)
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUsers", new { offfice = office }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<Office> GetOffices()
        {
            var result = new List<Office>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Office>("dbo.GetOffices", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static SchoolCategory GetSchoolCategory(long schoolid)
        {
            var result = new SchoolCategory();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolCategory>("dbo.GetSchoolCategory", commandType: CommandType.StoredProcedure).FirstOrDefault();

            }
            return result;
        }

        public static List<SchoolCategory> GetSchoolCategory()
        {
            var result = new List<SchoolCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolCategory>("dbo.GetSchoolCategory", commandType: CommandType.StoredProcedure).ToList();

            }
            return result;
        }

        public static List<SchoolCategory> GetSchoolCategoryP(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            var result = new List<SchoolCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolCategory>("dbo.GetSchoolCategory", commandType: CommandType.StoredProcedure).OrderByWithDirection(x => x.Category, Dir).Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            }
            return result;
        }

        //GetSchoolsIsactive
        public static List<SchoolEquipmentValueModel> GetSchoolsIsactive()
        {
            var result = new List<SchoolEquipmentValueModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipmentValueModel>("dbo.GetSchoolsIsactive", commandType: CommandType.StoredProcedure).ToList();

            }
            return result;
        }
        public static FacilityApprovalModel UpdateFacilitySummaryBySchId(long? schoolId, int FacilityId, int SubjectId, int totalsum, string percentage)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.UpdateFacilitySummaryBySchId", new { schoolid = schoolId, facilityid = FacilityId, subjectid = SubjectId, totalSchoolRecords = totalsum, percentage = percentage }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //UpdateFacilitySummaryBySchId
        public static FacilityApprovalModel GetFacilitySummaryBySchoolId(long? schoolId, int FacilityId, int SubjectId)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.GetFacilitySummaryBySchId", new { schoolid = schoolId, facilityid = FacilityId, SubjectId = SubjectId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static List<LaboratoryApprovalModel> GetLaboratorySummary(long? schoolid)
        {
            var result = new List<LaboratoryApprovalModel>();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<LaboratoryApprovalModel>("dbo.GetLaboratorySummary", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        //(0, finalRating, subjectId,sdc
        public static StaffSubjectClassSection AddNewItemStaffSection(int StaffId, int finalRating = 0, int subjectId = 0, long sdc = 0)
        {
            var result = new StaffSubjectClassSection();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<StaffSubjectClassSection>("dbo.AddNewItemStaffSection", new { StaffId = StaffId, FinalRating = finalRating, SubjectId = subjectId, SchoolId = sdc }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityApprovalModel AddNewItemSection1(int FacilityApprovalID, long sdc, string itemSection, int subjectid, int? facilityId, int x)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.AddNewItemSection1", new { FacilityApprovalID = FacilityApprovalID, SchoolID = sdc, ItemSection = itemSection, SubjectID = subjectid, EquipmentFacility = facilityId, FacilityID = x }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static FacilityApprovalModel AddNewItemSection3(int FacilityApprovalID, long sdc, string itemSection, int subjectid, int? facilityId, int z)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.AddNewItemSection3", new { FacilityApprovalID = FacilityApprovalID, SchoolID = sdc, ItemSection = itemSection, SubjectID = subjectid, LaboratoryFacility = facilityId, FacilityID = z }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static FacilityApprovalModel AddNewItemSection2(int FacilityApprovalID, long sdc, string itemSection, int subjectid, int? facilityId, int y)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.AddNewItemSection2", new { FacilityApprovalID = FacilityApprovalID, SchoolID = sdc, ItemSection = itemSection, SubjectID = subjectid, ReagentFacility = facilityId, FacilityID = y }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //public static LaboratoryApprovalModel AddNewItemSection3(int LabApprovalID, long sdc, string itemSection, int subjectid, int facilityId)
        //{
        //    var result = new LaboratoryApprovalModel();
        //    using (IDbConnection _db = OpenConnection())

        //    {
        //        result = _db.Query<LaboratoryApprovalModel>("dbo.AddNewItemSection3", new { LabApprovalID = LabApprovalID, SchoolID = sdc, ItemSection = itemSection,  SubjectID = subjectid, LaboratoryFacility = facilityId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }
        //    return result;
        //}
        public static FacilityApprovalModel AddNewItemSection(long sdc, string itemSection, int finalRating, int subjectid, int facilityId)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.AddNewItemSection", new { SchoolID = sdc, ItemSection = itemSection, FinalRating = finalRating, SubjectID = subjectid, EquipmentFacility = facilityId, ReagentFacility = facilityId, LaboratoryFacility = facilityId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //ssdc, itemSection, finalRating
        public static FacilityApprovalModel AddNewItemSection(long sdc, string itemSection, int finalRating)
        {
            var result = new FacilityApprovalModel();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.AddNewItemSectionIndex", new { SchoolID = sdc, ItemSection = itemSection, FinalRating = finalRating }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //AddNewItemSection GetFacilitySummarySubjectNotApproved
        public static List<FacilityApprovalModel> GetFacilitySummarySubjectNotApproved(long? schoolid)
        {
            var result = new List<FacilityApprovalModel>();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.GetFacilitySummarySubjectNotApproved", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
                // result = result.DistinctBy(item => item.ID).ToList();
            }
            return result;
        }
        public static List<FacilityApprovalModel> GetFacilitySummary(long? schoolid)
        {
            var result = new List<FacilityApprovalModel>();
            using (IDbConnection _db = OpenConnection())

            {
                result = _db.Query<FacilityApprovalModel>("dbo.GetFacilitySummary", new { schoolid = schoolid }, commandType: CommandType.StoredProcedure).ToList();
                // result = result.DistinctBy(item => item.ID).ToList();
            }
            return result;
        }

        //UpdateSchoolForDerecognition_A  GetFacilitySummary
        public static bool UpdateSchoolForDerecognition_A(long schoolid)
        {
            var outcome = new SchoolProfileViewModel1();

            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                outcome = _db.Query<SchoolProfileViewModel1>("dbo.UpdateSchoolForDerecognition_A", new { schoolid = schoolid, isrecognised = false }, commandType: CommandType.StoredProcedure).FirstOrDefault();


                if (outcome != null)
                {
                    result = true;
                }
            }
            return result;
        }



        public static bool UpdateForDerecognition_X(long schoolid)
        {
            var outcome = new SchoolEquipmentValueModel();
            List<SchoolEquipmentValueModel> listSchoolForUpdate;
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {
                listSchoolForUpdate = GetSchoolsIsactive();
                foreach (var update in listSchoolForUpdate)
                {
                    outcome = _db.Query<SchoolEquipmentValueModel>("dbo.UpdateForDerecognition_X", new { schoolid = schoolid, isActive = false }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }

                if (outcome != null)
                {
                    result = true;
                }
            }
            return result;
        }

        //UpdateForDerecognition_X
        public static bool UpdateSchoolCategory(int id, string catee)
        {
            var outcome = new SchoolCategory();
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {
                outcome = _db.Query<SchoolCategory>("dbo.UpdateSchoolCategory", new { ID = id, Category = catee }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (outcome.ID > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool AddSchoolCategory(string catee)
        {
            var outcome = new SchoolCategory();
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {
                outcome = _db.Query<SchoolCategory>("dbo.AddSchoolCategory", new { Category = catee }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (outcome != null && outcome.ID > 0)
                {
                    result = true;
                }
            }
            return result;
        }
        //procUpdateEquipmentDetailSchoolSubjectIDs
        public static SchoolEquipConfigViewModel ProcUpdateEquipmentDetailSchoolSubjectIDs(bool IsActive, int subjectid, long schoolid)
        {
            var result = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.ProcUpdateEquipmentDetailSchoolSubjectIDs", new { IsActive = IsActive, SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static SchoolEquipConfigViewModel ProcEquipmentDetailSchoolSubjectIDs(int subjectid, long schoolid)
        {
            var result = new SchoolEquipConfigViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolEquipConfigViewModel>("dbo.ProcEquipmentDetailSchoolSubjectIDs", new { SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //procUpdateStaffSubjectDetailBySchoolSubjectIDs
        public static SchoolSubjectViewModel ProcUpdateStaffSubjectDetailBySchoolSubjectIDs(bool IsActive, int subjectid, long schoolid)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.procUpdateStaffSubjectDetailBySchoolSubjectIDs", new { isBarred = IsActive, SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static StaffSubjectModel ProcStaffSubjectDetailBySchoolSubjectIDs(int subjectid, long schoolid)
        {
            var result = new StaffSubjectModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StaffSubjectModel>("dbo.procStaffSubjectDetailBySchoolSubjectIDs", new { SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static SchoolSubjectViewModel MakeSchoolSubjectActive(int subjectid, long schoolid, bool IsActive)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.MakeSchoolSubjectActive", new { SubjectID = subjectid, SchoolID = schoolid, IsActive = IsActive }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static SchoolSubjectViewModel MakeSchoolSubjectNotActive(int subjectid, long schoolid, bool IsActive)
        {
            var result = new SchoolSubjectViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolSubjectViewModel>("dbo.MakeSchoolSubjectNotActive", new { SubjectID = subjectid, SchoolID = schoolid, IsActive = IsActive }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static bool DeleteSubjectTextbook(int subjectid, long schoolid)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.ProcDeleteSubjectTextbook", new { SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }
            return result;
        }
        public static bool DeleteSchoolSubjectsFromList(int subjectid, long schoolid)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.DeleteSchoolSubjectsFromList", new { SubjectID = subjectid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }
            return result;
        }
        //DeleteStaffSubjectX
        public static bool DeleteStaffSubjectX(int token)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.DeleteStaffSubjectX", new { StaffSubjectID = token }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }
            return result;
        }
        public static bool DeleteStaffSubjectClass(int token)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.DeleteStaffSubjectClass", new { ID = token }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }
            return result;
        }
        //DeleteSchoolSubjectTextBook
        public static bool DeleteSchoolSubjectTextsBooks(long schoolid,int? subjectid)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.DeleteSchoolSubjectTextsBooks", new { SchoolID = schoolid,SubjectID=subjectid }, commandType: CommandType.StoredProcedure);

            }
            return true;
        }
        public static bool DeleteTextBook(int textbookToSchoolSubjectRowId)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {

                int yy = _db.Execute("dbo.DeleteTextBook", new { ID = textbookToSchoolSubjectRowId }, commandType: CommandType.StoredProcedure);
                
            }
            return true;
        }

        //DeleteTextBook
        public static bool DeleteSchoolCategory(int id)
        {
            bool result = false;
            using (IDbConnection _db = OpenConnection())
            {
                int yy = _db.Execute("dbo.DeleteSchoolCategory", new { ID = id }, commandType: CommandType.StoredProcedure);
                if (yy != 0)
                {
                    result = true;
                }
            }
            return result;
        }


        public static int GetSchoolCategoryCount()
        {
            var x = 0;//new  List<SchoolCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                x = _db.Query<int>("dbo.GetSchoolCategory", commandType: CommandType.StoredProcedure).Count();
            }
            return x;
        }



        public static List<vwSchoolCategory> GetSchoolStaff()
        {
            var result = new List<vwSchoolCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwSchoolCategory>("dbo.GetSchoolCategory", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<Title> GetTitle()
        {
            var result = new List<Title>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Title>("dbo.GetTitle", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }


        public static List<SchoolStaffCategory> GetSchoolStaffCategory()
        {
            var result = new List<SchoolStaffCategory>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchoolStaffCategory>("dbo.GetSchoolStaffCategory", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }


        public static List<SecondDegree> GetSecondDegree()
        {
            var result = new List<SecondDegree>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SecondDegree>("dbo.GetSecondDegree", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetSchoolStaffCategory
        public static List<Degree> GetDegree()
        {
            var result = new List<Degree>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Degree>("dbo.GetDegree", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }//GetCourse
        public static List<Subject> GetSubject()
        {
            var result = new List<Subject>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Subject>("dbo.GetSubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<SubjectModel> GetSubjectEdit()
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetSubject", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }


        public static List<SchStaffCourse> NtGetCourse()
        {
            var result = new List<SchStaffCourse>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchStaffCourse>("dbo.GetCourse", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<SchStaffCourse> GetCourse()
        {
            var result = new List<SchStaffCourse>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SchStaffCourse>("dbo.GetCourse", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static ClassAllocationvModel UpdateASchoolClass(ClassAllocationvModel model)
        {
            var result = new ClassAllocationvModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationvModel>("dbo.UpdateASchoolClass", new { ID = model.ID, ClassID = model.ClassID, TotalStudent = model.TotalStudents, Noofstream = model.NoOfStreams }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static ClassAllocationvModel GetASchoolClass(int sctid)
        {
            var result = new ClassAllocationvModel();
            using (IDbConnection _db = OpenConnection())
            {
                result =
                    _db.Query<ClassAllocationvModel>("dbo.GetASchoolClass", new { schoolClassTableId = sctid },
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        //UpdateASchoolClass
        public static ClassAllocationvModel GetASchoolClass(int sctid, long schoolid)
        {
            var result = new ClassAllocationvModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<ClassAllocationvModel>("dbo.GetASchoolClass", new { schoolClassTableId = sctid, SchoolID = schoolid }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetASchoolClass
        public static UserViewModel GetAUserNoRole(int id)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAUserNoRole", new { ID = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetAUserNoRole
        //GetAUser

        public static vwUserListing2 GetUserEdit(string EmailAddress)
        {
            var result = new vwUserListing2();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing2>("dbo.ProGetUserX", new { EmailAddress = EmailAddress }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static vwUserListing GetUser(string EmailAddress)
        {
            var result = new vwUserListing();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.GetUser", new { EmailAddress = EmailAddress }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetUserStates
        public static List<StateViewModel> GetUserStates2(int id)
        {
            var result = new List<StateViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StateViewModel>("dbo.GetUserStates", new { userid = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetUsersByState
        public static List<UserViewModel> GetUsersByState(string userstate)
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUsersByState", new { userstate = userstate }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetAUserInfoForSupervisor
        public static UserViewModel GetAUserInfoForSupervisor(string userEmail)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAUserInfoForSupervisor", new { EmailAddress = userEmail }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static UserViewModel GetAUserInfoForAdmin(string userEmail)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAUserInfoForAdmin", new { EmailAddress = userEmail }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static UserViewModel GetSEDEmail()
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetSEDEmail", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static UserViewModel GetAUserInfoForSED(string userEmail)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAUserInfoForSED", new { EmailAddress = userEmail }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static List<UserViewModel> GetUserStates(int id)
        {
            var result = new List<UserViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetUserStates", new { userid = id }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static UserViewModel GetAUser(int id)
        {
            var result = new UserViewModel();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<UserViewModel>("dbo.GetAUser", new { ID = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetStatesByUserEmail
        public static StateViewModel GetStatesByUser(string emailAddreess)
        {
            var stateAddedForUser = clsContent.GetStatesByUserEmail(emailAddreess);
            var stateIds = stateAddedForUser.Select(item => item.StateCode).ToList();
            return new StateViewModel
            {
                States = clsContent.GetStates().OrderBy(b => b.StateName).ToList(),
                AvailableStateSelected = stateIds.Select(dummy => dummy).ToList()
                //RequestedSubjects = new List<Subject>()
            };
        }
        public static StateViewModel GetStateByUserId(int userid)
        {
            var stateAddedForUser = clsContent.GetStatesByUser(userid);
            var stateIds = stateAddedForUser.Select(item => item.StateCode).ToList();
            return new StateViewModel
            {
                States = clsContent.GetStates().OrderBy(b => b.StateName).ToList(),
                AvailableStateSelected = stateIds.Select(dummy => dummy).ToList()
                //RequestedSubjects = new List<Subject>()
            };
        }
        public static List<StateViewModel> GetStatesByUserEmail(string email)
        {
            var result = new List<StateViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StateViewModel>("dbo.GetStatesByUserEmail", new { emailAddress = email }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<StateViewModel> GetStatesByUser(int userid)
        {
            var result = new List<StateViewModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<StateViewModel>("dbo.GetStatesByUserId", new { userid = userid }, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<Role> GetRoles()
        {
            var result = new List<Role>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Role>("dbo.GetRoles", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        //GetStatesByUser  GetRoles
        //GetSchoolsForAutoCentre

        public static List<State> GetStates()
        {
            var result = new List<State>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<State>("dbo.GetStates", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<Rank> GetRank()
        {
            var result = new List<Rank>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<Rank>("dbo.GetRank", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }

        public static List<SubjectModel> GetCouncilSubjects()
        {
            var result = new List<SubjectModel>();
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<SubjectModel>("dbo.GetCouncilSubjects", commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static List<AssignedStatesByRoleNameModel> GetAssignedStatesByRoleName(string roleName)
        {
            var result = new List<AssignedStatesByRoleNameModel>();
            using (var _db = OpenConnection())
            {
                result = _db.Query<AssignedStatesByRoleNameModel>("dbo.GetAssignedStatesByRoleName", new {RoleName = roleName}, commandType: CommandType.StoredProcedure).ToList();
            }
            return result;
        }
        public static async Task<bool> SendEmailOnRegister(string useremail) //emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();

            mess.From = new MailAddress(useremail, "School Recognition");
            //GetSEDEmail
            var sedEmailAddress = clsContent.GetSEDEmail();
            //GetAUserInfoForSED
            var userEmailInfo = clsContent.GetAUserInfoForSED(useremail);

            // mess.To.Add("ujimadiyi@waec.org.ng");
            //mess.To.Add("paogbekhilu@waec.org.ng");
            mess.To.Add(sedEmailAddress.EmailAddress);
            //mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            mess.Body = "I " + userEmailInfo.SurName + " from" + userEmailInfo.StateName + " office have registered my details on the school recognition platform,please sir/ma,activate me to be able to login";
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential("paogbekhilu@waec.org.ng", "welcome");
                smtpClient.Send(mess);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task.FromResult(false);
        }






        public static bool SendEmailOnActive(string sed, string useremail) //emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            //var url = new UrlHelper(ControllerContext.RequestContext);
            //url.Action(an_action_name, route_values);
            mess.From = new MailAddress(sed, "Mail Testing");
            // mess.To.Add("ujimadiyi@waec.org.ng");
            mess.To.Add(useremail);
            mess.Body = "You have been activated. You can now login";
            //mess.Body = "You have been activated. You can now login using <a href=\"" + Url.Action("action") + "\"> to unlock.</a>"; 
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(useremail, "welcome");
                smtpClient.Send(mess);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }








        public static IEnumerable<yearClass> GetYears()
        {
            List<yearClass> lstYears = new List<yearClass>();
            int c = 1;
            for (int i = DateTime.Now.Year - 100; i <= DateTime.Now.Year; i++)
            {
                lstYears.Add(new yearClass { ID = c, yearName = i.ToString() });
                c++;
            }
            return lstYears.AsEnumerable(); //&& c.isApproved == true 

        }

        public static IPagedList<SchoolProfileViewModel1> GetSchoolsByOfficeIdVet(int officeId, int pageIndex, int pageSize = 20)
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
            using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetSchoolsTotalByOfficeIdVet", new { OfficeID = officeId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsByOfficeIdVet", new { OfficeID = officeId, PageIndex = pageIndex, PageSize = pageSize }, commandType: CommandType.StoredProcedure).ToList();
            }
            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, pageIndex, pageSize, totalRows);
            return pagedList;
        }
        public static IPagedList<SchoolProfileViewModel1> GetSchoolsByOfficeId(int officeId, int pageIndex, int pageSize = 20)
        {
            List<SchoolProfileViewModel1> result;
            var totalRows = 0;
                using (IDbConnection _db = OpenConnection())
            {
                totalRows = _db.Query<int>("dbo.GetSchoolsTotalByOfficeId", new { OfficeID = officeId}, commandType: CommandType.StoredProcedure).FirstOrDefault();
                result = _db.Query<SchoolProfileViewModel1>("dbo.GetSchoolsByOfficeId", new { OfficeID = officeId, PageIndex = pageIndex, PageSize = pageSize }, commandType: CommandType.StoredProcedure).ToList();
            }
            var pagedList = new StaticPagedList<SchoolProfileViewModel1>(result, pageIndex, pageSize, totalRows);
            return pagedList;
        }
    }

    public class yearClass
    {
        public int ID { get; set; }
        public string yearName { get; set; }
    }
    


}