﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Classes
{
    public static class clsSession
    {
        public static T GetDataFromSession<T>(this HttpSessionStateBase session, string key)
        {
            return (T)session[key];
        }
        public static void SetDataInSession(this HttpSessionStateBase session, string key, object value)
        {
            session[key] = value;
        }
    }
}