﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SchoolRecognitionSystem.Models;

//using OfflineDigitalPay.Common.Entities.Base;

namespace SchoolRecognitionSystem.Classes
{
    /// <summary>
    /// AuthorizeAccessLevelAttribute is used to control the access to different parts of the app
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class AuthorizeAccessAttribute:AuthorizeAttribute
    {
        private string _roles;

        /// <summary>
        /// Processes HTTP requests that fail authorization.
        /// </summary>
        /// <param name="filterContext">Encapsulates the information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute"/>. The <paramref name="filterContext"/> object contains the controller, HTTP context, request context, action result, and route data.</param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var roles = Roles;
            string returnUrl = null;
            if (filterContext.HttpContext.Request.HttpMethod.Equals("GET", System.StringComparison.CurrentCultureIgnoreCase))
                returnUrl = filterContext.HttpContext.Request.Url?.GetComponents(UriComponents.PathAndQuery, UriFormat.SafeUnescaped);

            if (string.IsNullOrEmpty(roles))
            {
                if (HttpContext.Current.Session["UserID"] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(
                           new RouteValueDictionary(
                           new
                           {
                               controller = "Home",
                               action = "Login",
                               returnUrl = returnUrl
                           }));
                    //filterContext.HttpContext.Response.RedirectPermanent("/Home/Login?returnUrl=" + returnUrl);
                }
            }
            else
            {
                if (!UserManager.IsUserInRole(Roles))
                {
                    //filterContext.HttpContext.Response.RedirectPermanent("/Home/Login");
                    //HttpContext.Current.
                    filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary(
                            new
                            {
                                controller = "Home",
                                action = "Login",
                                returnUrl = returnUrl
                            }));
                }
            }
           
            //if (filterContext.HttpContext.Request.IsAuthenticated)
            //{
            //    if(HttpContext.Current.User.IsInRole(new User().Role.Rolename))
            //        return;
            //    filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //    filterContext.HttpContext.Response.RedirectPermanent("/AccessDenied.html");
            //}
           // base.HandleUnauthorizedRequest(filterContext);
        }

    }
}