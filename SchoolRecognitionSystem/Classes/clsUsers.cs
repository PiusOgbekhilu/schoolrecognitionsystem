﻿using Dapper;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Classes
{
    public class clsUsers
    {
        public IDbConnection otCon;
        //To Handle connection related activities
        private static IDbConnection OpenConnection()
        {
            string constr = ConfigurationManager.ConnectionStrings["myDapperConnection"].ToString();
            return new SqlConnection(constr);
        }

        public static vwUserListing2 AddUpdateUser(RegisterUserViewModel _obj)
        {
            var result = new vwUserListing2();

            var userPassword = clsEncryption.EncryptPassword(_obj.Password);

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing2>("dbo.AddUpdateUser", new
                {
                    ID = _obj.ID,
                    Password = userPassword,
                    PhoneNumber = _obj.PhoneNumber,
                    EmailAddress = _obj.EmailAddress,
                    Surname = _obj.Surname,
                    OtherNames = _obj.OtherNames,
                    OfficeID = _obj.OfficeID,
                    //StateID = _obj.StateID,
                    //LGAID = _obj.LGAID,
                    RANKID = _obj.RankID,
                    LPNO = _obj.LPNO,
                    Signature = _obj.Signature
                }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //GetAUserRole
        public static vwUserListing GetAUserRole(int? roleId)
        {
            var result = new vwUserListing();


            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.GetAUserRole", new { ID = roleId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static vwUserListing GetUserLogin(string email)
        {
            var result = new vwUserListing();

            //var userPassword = clsEncryption.EncryptPassword(password);
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.GetUserListByLogin", new { EmailAddress = email }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }

        public static vwUserListing GetUserLogin22(string email, string password, string abc)
        {
            var result = new vwUserListing();

            var userPassword = clsEncryption.EncryptPassword(password);
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.GetUserListByLogin22", new { EmailAddress = email, Password = userPassword }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        //AddUserIdRoleIDToUserRole
        public static vwUserListing AddUserIdRoleIDToUserRole(int userId, int? roleId, string phoneNumber, string emailAddress)
        {
            var result = new vwUserListing();

            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.AddUserIdRoleIDToUserRole", new { EmailAddress = emailAddress, PhoneNumber = phoneNumber, ReturnedUserID = userId, ReturnedRoleID = roleId }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
        public static vwUserListing GetUserLogin(string email, string password)
        {
            var result = new vwUserListing();

            var userPassword = clsEncryption.EncryptPassword(password);
            using (IDbConnection _db = OpenConnection())
            {
                result = _db.Query<vwUserListing>("dbo.GetUserListByLogin", new { EmailAddress = email, Password = userPassword }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
            return result;
        }
    }
}