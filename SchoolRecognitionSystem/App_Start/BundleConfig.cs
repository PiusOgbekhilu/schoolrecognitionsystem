﻿using System.Web;
using System.Web.Optimization;

namespace SchoolRecognitionSystem
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            //customised js for facilityView
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //          "~/Scripts/jquery-ui-{version}.js"));


            // bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/plugins/jquery/jquery-2.1.4.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                       "~/Scripts/scriptz/angular.min.js",
                       "~/Scripts/scriptz/elif.js",
                        "~/Scripts/scriptz/schoolregistrationcontroller.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/plugins/form.validate/jquery.form.min.js",
                        "~/Content/plugins/form.validate/jquery.validation.min.js"));


            // JQuery validator. 
            bundles.Add(new ScriptBundle("~/bundles/custom-validator").Include(
                                  "~/Scripts/script-custom-validator.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/plugins/bootstrap/js/bootstrap.min.js",
                     "~/Content/js/scripts.js",
                     "~/Content/plugins/bootstrap.datepicker/js/bootstrap-datepicker.min.js",
                     "~/Content/plugins/bootstrap.dialog/js/bootstrap-dialog.min.js",
                     "~/Content/plugins/select2/js/select2.js",
                     //"~/Content/plugins/owl-carousel/owl.carousel.min.js"
                     "~/Content/plugins/smoothscroll.js"
                     ));



            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new StyleBundle("~/Content/Styles").Include(
                     "~/Content/plugins/bootstrap/css/bootstrap.min.css",
                     "~/Content/css/essentials.css",
                     "~/Content/css/header-1.css",
                     "~/Content/css/layout.css",
                     "~/Content/css/select2.min.css"));


        }
    }
}
