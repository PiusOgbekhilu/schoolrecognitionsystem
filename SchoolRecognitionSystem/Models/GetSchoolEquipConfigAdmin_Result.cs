//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class GetSchoolEquipConfigAdmin_Result
    {
        public string ItemDescription { get; set; }
        public Nullable<int> FacilityCategoryID { get; set; }
        public string Specification { get; set; }
        public Nullable<double> QuantityRequired { get; set; }
        public string TypeofRecognition { get; set; }
        public int ID { get; set; }
        public Nullable<int> SchoolRecognitionTypeID { get; set; }
        public string LongName { get; set; }
        public int SubjectID { get; set; }
    }
}
