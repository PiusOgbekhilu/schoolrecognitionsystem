﻿namespace SchoolRecognitionSystem.Models
{
    public class CurrentUserModel
    {
        public string Surname { get; set; }
        public  string  OtherNames  { get; set; }
        public  string  EmailAddress  { get; set; }
        public  string  PhoneNumber  { get; set; }
        public int? OfficeId { get; set; }
        public int?  RoleId { get; set; }
        public string  RoleName { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }

        public bool IsAuthenticated => Surname != null;
       
    }
}