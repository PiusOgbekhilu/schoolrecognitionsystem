//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class procSchoolSubectsReport_Result
    {
        public string LongName { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public Nullable<bool> HasItem { get; set; }
    }
}
