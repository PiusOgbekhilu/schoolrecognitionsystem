﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Routing.Constraints;

namespace SchoolRecognitionSystem.Models
{

    public class vwCentreNumberReportModel
    {
        public string SchoolName { get; set; }
        public string CentreNo { get; set; }
        public string CentreName { get; set; }
        public long SchoolID { get; set; }
        public string StateName { get; set; }
        public string Category { get; set; }
        public long ID { get; set; }
        public string SchoolLocation { get; set; }
        public DateTime DateIssued { get; set; }
    }

    public class vwSchoolSubjectModel
    {
        public long SchoolID { get; set; }
        public string LongName { get; set; }
        public int SubjectID { get; set; }
        public bool IsCore { get; set; }
        public bool IsTrade { get; set; }
        public string SubjectCode { get; set; }
        public string Category { get; set; }
    }

    [Serializable()]
    public class QuestionUploadData
    {
        public string CenterNo { get; set; }
        public string SchoolName { get; set; }

    }

    public class SchoolRecreatConfigModel
    {
        //totalItems
        public int ID { get; set; }
        public string ItemDescription { get; set; }
        public string Value { get; set; }
    }

    //SchoolRecreatConfig
    public class ItemSectionAssessmentModel
    {
        public int ID { get; set; }
        public string ItemSection { get; set; }
        public int? EquipmentFacility { get; set; }
        public int? ReagentFacility { get; set; }
        public int? LaboratoryFacility { get; set; }
        public int IsTextBookAttached { get; set; }
        public int FacilityID { get; set; }
        public int SubjectID { get; set; }
        public long SchoolID { get; set; }
        public int FinalRating { get; set; }
        public int Index { get; set; }
        public int FacilityApprovalID { get; set; }
        public int LabApprovalID { get; set; }
    }

    public class LaboratoryApprovalModel
    {
        //totalItems
        public int ID { get; set; }
        public int TotalItem { get; set; }
        public int LaboratoryFacility { get; set; }
        public int Schoolid { get; set; }
        public int SubjectId { get; set; }
        public string LongName { get; set; }
        public string GeneralAssessment { get; set; }
        public string TypeOfFacility { get; set; }
        public string FacilityCategory { get; set; }

        public int FacilityCategoryId { get; set; }
        public bool IsAdequate { get; set; }
    }



    public class FacilityApprovalModel
    {
        public int ID { get; set; }
        public string ItemSection { get; set; }
        public int ItemSectionAssessmentID { get; set; }
        public int? ReagentFacility { get; set; }
        public int? EquipmentFacility { get; set; }
        public int? LaboratoryFacility { get; set; }


        public int FinalRating { get; set; }

        public bool Value { get; set; }
        public int FacilityCategoryId { get; set; }
        //FacilityCategoryId
        public long SchoolID { get; set; }
        public string TypeOfFacility { get; set; }
        public string LongName { get; set; }
        //LongName

        public bool isApproved { get; set; }
        public int SubjectID { get; set; }
        public int FacilityTypeID { get; set; }
        public int TotalItems { get; set; }
        public int TotalSchoolRecords { get; set; }
        public string Percentage { get; set; }
        public int? LaboratoryDigit { get; set; }
        public int Approved { get; set; }
        public bool NotApproved { get; set; }
        public DateTime Date { get; set; }

        public FacilityCategory FacilityCategory { get; set; }
        public SchoolProfile SchoolProfile { get; set; }
        public Subject Subject { get; set; }
        
    }

    public class FacilityRatedModel
    {
        public int SubjectID { get; set; }
        public int SchoolID { get; set; }
        public int FacilityID { get; set; }
        public int? LaboratoryFacility { get; set; }
        public int? ReagentFacility { get; set; }
        public int? EquipmentFacility { get; set; }
        public bool IsApproved { get; set; }

        public List<string> Messages { get; set; }
        //IsApproved
        public SchoolSubject Subject { get; set; }
    }

    public class SchoolSubjectFootMessage
    {
        public int ID { get; set; }
        public long SchoolID { get; set; }
        public int SubjectID { get; set; }

        public string SchoolSubjectMessage { get; set; }

    }

    public class ClassAllocationvModel
    {
        public int ID { get; set; }
        public long SchoolID { get; set; }

        public int ClassID { get; set; }
        public int NoOfStreams { get; set; }
        public int TotalStudents { get; set; }

        public int totalstudent { get; set; }
        public int classid2 { get; set; }
        public string ClassName { get; set; }
        public int NoOfStreams2 { get; set; }
        public int totalstudent2 { get; set; }
        //NoOfStreams2  //totalstudent2
    }

    public class CountBSc
    {
        public int countBSc { get; set; }
        public int ID { get; set; }
    }

    public class CountTeacher
    {
        public int countTeacher { get; set; }
        public int ID { get; set; }
    }

    public class CountNCE
    {
        public int countNCE { get; set; }
        public int ID { get; set; }
    }

    public class SchoolPreviewModel
    {
        public long ID { get; set; }
        public int TitleID { get; set; }
        public string FirstName { get; set; }
        public string SchoolName { get; set; }
        public string Category { get; set; }
        public int TotalStudent { get; set; }
        public int countHND { get; set; }
        public int countNCE { get; set; }
        public int countBSc { get; set; }
        public int countTeacher { get; set; }
    }

    //SchoolPreviewModel
    public class LoginViewModel
    {
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Text)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

    }

    public class SchoolCentreIsRecognisedIsBarred
    {
        public int ID { get; set; }
        public int SchoolID { get; set; }
        public bool IsRecognisedID { get; set; }
        public bool IsBarred { get; set; }
        public int CentreNo { get; set; }
        public string CentreName { get; set; }
        public string SchoolName { get; set; }
    }

    public class UserRoleModel
    {
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int UserID { get; set; }
        public User User { get; set; }
        public Role Role { get; set; }
    }

    public class IsUserinroleModel
    {
//dbo.[User].ID, dbo.[User].Password, dbo.[User].PhoneNumber,dbo.Role.ID as RoleID, dbo.[User].EmailAddress, dbo.[User].Surname, dbo.[User].OtherNames, dbo.Role.Rolename
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string EmailAddress { get; set; }
        public string Rolename { get; set; }
        public string PhoneNumber { get; set; }

    }

    //isUserinrole
    public class RoleModel
    {
        public int ID { get; set; }
        public int RoleID { get; set; }
        public int UserID { get; set; }

        [Required(ErrorMessage = "Rolename is required")]
        [DataType(DataType.Text)]
        public string Rolename { get; set; }

        public bool isAdmin { get; set; }
        public bool isSuperAdmin { get; set; }
        public bool isDefault { get; set; }
    }

    public class UserViewModel
    {

        public int ID { get; set; }
        public int UserID { get; set; }

        //UserRoleId
        public string StateID { get; set; }
        public string StateCode { get; set; }
        public int UserRoleId { get; set; }
        public int OfficeID { get; set; }
        public string Signature { get; set; }


        public List<int?> AvailableRoleSelectedEdit { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Both Password fields must match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Email Address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        //[Remote("EmailExists", "Account", HttpMethod = "POST", ErrorMessage = "Email address already registered.")]
        public string EmailAddress { get; set; }

        //EmailAddress
        public int RoleIDD { get; set; }
        public int RoleID { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Surname")]
        public string SurName { get; set; }

        public string OfficeName { get; set; }
        public string RoleName { get; set; }
        public bool isActive { get; set; }
        public string StateName { get; set; }
        public byte[] Password { get; set; }
        public List<UserRole> UserRole { get; set; }
        public List<State> States { get; set; }
        public List<State> StatesEdit { get; set; }

        public List<State> RequestedSubjects { get; set; }

        public List<string> AvailableStateSelected { get; set; }

        public List<int?> AvailableStateSelectedEdit { get; set; }


    }

    public class RegisterUserViewModel
    {
        public int? ID { get; set; }
        public int LPNO { get; set; }
        //public string StateID { get; set; }
        //public int LGAID { get; set; }
        public int RankID { get; set; }
        //RankID

        [Required(ErrorMessage = "Surname is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Other Name is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Other Names")]
        public string OtherNames { get; set; }

        //[Required(ErrorMessage = "Valid Email Address is required")]
        //[DataType(DataType.EmailAddress)]
        //[Display(Name = "Email Address")]
        public string EmailAddress { get; set; }


        //[Required(ErrorMessage = "Enter the Password")]
        //[DataType(DataType.Password)]
        //[Display(Name = "Password")]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Confirm your password")]
        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm Password")]
        //[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The confirm password does not match.")]
        //public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Enter a valid phone number")]
        [DataType(DataType.Text)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Select an Office")]
        [DataType(DataType.Text)]
        [Display(Name = "Office")]
        public int? OfficeID { get; set; }

        public string Signature { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password",
            ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class vwSchStafDegCourse
    {
        public long ID { get; set; }
        public int DegreeID { get; set; }
        public int CourseID { get; set; }
    }

    /*
     *  public partial class SchStafDegCourse
    {
        public long ID { get; set; }
        public Nullable<int> SchStaffID { get; set; }
        public Nullable<int> DegreeID { get; set; }
        public Nullable<int> CourseID { get; set; }
    
        public virtual Cours Cours { get; set; }
        public virtual Degree Degree { get; set; }
        public virtual SchoolStaff SchoolStaff { get; set; }
    }
     * 
     * 
     */



    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get; set; }
    }

    public class FacilityApprovalViewModel
    {
        public int ID { get; set; }
        public int SchoolID { get; set; }
        public int SubjectID { get; set; }
        public int FacilityTypeID { get; set; }
        public int TotalItems { get; set; }
        public int TotalSchoolRecords { get; set; }
        public string Percentage { get; set; }
        public bool Approved { get; set; }
        public int totalstudent { get; set; }
        public int classid2 { get; set; }
        public string ClassName { get; set; }
        public int NoOfStreams2 { get; set; }
        public int totalstudent2 { get; set; }
    }

    public class SchoolProfileViewModel1
    {
        public long ID { get; set; }
        public string OfficeName { get; set; }
        public string OfficeID { get; set; }
        public string Status { get; set; }
        public string CenterNo { get; set; }
        public string LocalCode { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Category")]
        public int CategoryID { get; set; }

        public string Category { get; set; }
        public bool isBarred { get; set; }
        public string WaecStaff { get; set; }
        public string CentreName { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Centre Number")]
        public string CentreNo { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Name")]
        //[Remote("doesSchoolNameExist", "Schools", HttpMethod = "POST", ErrorMessage = "School name already exists. Please enter a different school name.")]
        public string SchoolName { get; set; }

        public string SchoolNameEdit { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Location")]
        public string SchoolLocation { get; set; }

        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Year Established")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Year Established")]
        public string YearEstablished { get; set; }

        //DateDerecognised
        public bool isRecognised { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "State of School")]
        public string StateCode { get; set; }

        public string StateID { get; set; }

        public string StateName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Local Government")]
        public int LgaID { get; set; }

        public string Name { get; set; }

        public string SchoolLocalGovName { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is School Joint Proprietorship?")]
        public bool IsJointProprietorship { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Aproximate Land Area")]
        public string AproximateLandArea { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Land Area Adequate?")]
        public bool IsLandAreaAdequate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Fenced Round?")]
        public bool IsSchoolFencedRound { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Admission Registers Available")]
        public bool IsAdmissionRegisterAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Class Registers Available")]
        public bool isClassRegistersAvaialable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Diaries of Work Available")]
        public bool isDiaryofWorksAvaialble { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Curricula Available")]
        public bool isCurricullumAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Scheme of work Available")]
        public bool IsScheemOfWorkAvailable { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Others Details (Specify)")]
        public string OtherSpecify { get; set; }

        public string EmailAddress { get; set; }





        //public string SchoolStaffID { get; set; }
    }

    public class SchoolSubjectFacilityModel
    {
        public int ID { get; set; }
        public int FacilityCategoryID { get; set; }
        public int SubjectID { get; set; }
    }

    public class SchoolProfileViewModel
    {
        public long? ID { get; set; }
        //public bool isRecognised { get; set; }
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Category")]
        public int CategoryID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Centre Number")]
        public string CenterNo { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Name")]
        //[Remote("doesSchoolNameExist", "Schools", HttpMethod = "POST", ErrorMessage = "School name already exists. Please enter a different school name.")]
        public string SchoolName { get; set; }

        public string SchoolNameEdit { get; set; }
        public string OfficeID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Location")]
        public string SchoolLocation { get; set; }

        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Year Established")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Year Established")]
        public string YearEstablished { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "State of School")]
        public string StateID { get; set; }

        //public string StateName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Local Government")]
        public int? LgaID { get; set; }

        // public string Name { get; set; }

        //public string SchoolLocalGovName { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is School Joint Proprietorship?")]
        public bool IsJointProprietorship { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Aproximate Land Area*")]
        public string AproximateLandArea { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Land Area Adequate?")]
        public bool IsLandAreaAdequate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Fenced Round?")]
        public bool IsSchoolFencedRound { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Admission Registers Available")]
        public bool IsAdmissionRegisterAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Class Registers Available")]
        public bool isClassRegistersAvaialable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Diaries of Work Available")]
        public bool isDiaryofWorksAvaialble { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Curricula Available")]
        public bool isCurricullumAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Scheme of work Available")]
        public bool IsScheemOfWorkAvailable { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Others Details (Specify)")]
        public string OtherSpecify { get; set; }

        public int CreatedByUserId { get; set; }
       // public string Status { get; set; }
        //public DateTime DateRecognition { get; set; }

    }

    public class vwSchoolsubjectForAssessment
    {
        public string LongName { get; set; }
        public long SchoolID { get; set; }
        public bool IsActive { get; set; }
        public bool HasItem { get; set; }


    }
    public class ClassAllocationModel
    {

        public int classid { get; set; }
        public string streams { get; set; }
        public string totalstudents { get; set; }
        public long ID { get; set; }
        public long SchoolID { get; set; }
        public int ClassID { get; set; }
        public int NoOfStreams { get; set; }
        public int TotalStudents { get; set; }
    }



    public class SchoolProfileIndexModel
    {
        public string Category { get; set; }
        public bool isRecognized { get; set; }
        public long? ID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Category")]
        public int CategoryID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Centre Number")]
        public string CenterNo { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Name")]
        public string SchoolName { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "School Location")]
        public string SchoolLocation { get; set; }

        //[Required(ErrorMessage = "Required")]
        //[Display(Name = "Year Established")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Year Established")]
        public string YearEstablished { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "State of School")]
        public int StateID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Local Government")]
        public int LgaID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Joint Proprietorship?")]
        public bool IsJointProprietorship { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Aproximate Land Area")]
        public string AproximateLandArea { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is Land Area Adequate?")]
        public bool IsLandAreaAdequate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Fenced Round?")]
        public bool IsSchoolFencedRound { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Admission Registers Available")]
        public bool IsAdmissionRegisterAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Class Registers Available")]
        public bool isClassRegistersAvaialable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Diaries of Work Available")]
        public bool isDiaryofWorksAvaialble { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Curricula Available")]
        public bool isCurricullumAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Scheme of work Available")]
        public bool IsScheemOfWorkAvailable { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Others Details (Specify)")]
        public string OtherSpecify { get; set; }

    }

    public class SchoolSubjectList
    {

        public List<SchoolSubjectViewModel> Subject { get; set; }
        public List<SchoolSubjectViewModel> SchoolCompulsorySubject { get; set; }
        public List<SchoolSubjectViewModel> SchoolLeastTradeSubject { get; set; }

        public List<SchoolSubjectViewModel> ReaLabo { get; set; }
        public List<SchoolSubjectViewModel> StaffSubjectO { get; set; }
        // public List<SchoolSubjectViewModel> SchoolLeastTradeSubject { get; set; }
    }

    public class StaffSubjectModel
    {
        public int ID { get; set; }
        public int SchoolStaffID { get; set; }

        public int SubjectID { get; set; }
        public bool IsActive { get; set; }
    }

    /*
            public int ID { get; set; }
            public Nullable<int> SchoolStaffID { get; set; }
            public Nullable<int> SubjectID { get; set; }
            public Nullable<bool> IsActive { get; set; }
        */

    public class StaffSubjectClass
    {
        public SchoolStaffModel Staff { get; set; }
        public List<SchoolStaffModel> SchoolStafflassList { get; set; }
        public List<SchoolStaffModel> SchoolStaffSubjectTaughtList { get; set; }
    }

    public class SchoolStaffList
    {
        public int SchoolStaffNameId { get; set; }
        public SchoolStaffModel Staff { get; set; }
        public List<SchoolStaffModel> SchoolStafflassList { get; set; }
        public List<SchoolStaffModel> Credentials { get; set; }
        public string CredentialID { get; set; }
        public string Image { get; set; }

        //schoolStaffSubjectToClassList
        public List<List<SchoolStaffModel>> SchoolStaffSubjectToClassList { get; set; }
        public List<List<SchoolStaffModel>> GetStaffListDegrees { get; set; }
        public List<SchoolStaffModel> SchoolStaffDegreeList { get; set; }
        public List<SchoolStaffModel> SchoolStaffOtherDegreeList { get; set; }
        public List<SchoolStaffModel> SchoolStaffSubjectTaughtList { get; set; }

    }

    public class SchoolStaffModelEdit
    {
        public int CountNonDegreeHolder { get; set; }
        public int NumberOfDegreeHolder { get; set; }
        public int ID { get; set; }
        public int SubId { get; set; }
        public int[] SubjectIDs { get; set; }
        public int SubjectID { get; set; }
        //combinedSecondDegree
        public string combinedSecondDegree { get; set; }
        public IEnumerable<string> SecondDegrees { get; set; }
        public List<SchoolStaffModel> Images { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAddress { get; set; }
        public DateTime DateEmployed { get; set; }
        public int CategoryID { get; set; }
        public int Degree { get; set; }
        //public bool isTRCN { get; set; }

        public int[] DegreeID { get; set; }
        public int[] CourseID { get; set; }
        public int Course { get; set; }
        public int CoursID { get; set; }
        public int[] ClassID { get; set; }
        //ClassID
        public string Category { get; set; }
        public bool isTRCN { get; set; }
        public string TRCNText { get; set; }
        public int SchoolID { get; set; }


        public DateTime YearEmployed { get; set; }
        public int[] Subject { get; set; }
        public string DegreeType { get; set; }

        public int TitleID { get; set; }

        public string ClassName { get; set; }
        public int StaffDegreeId { get; set; }

        public string CourseTitle { get; set; }
        //public string Category { get; set; }
        public string SubjectLongName { get; set; }
        public string OtherDegree { get; set; }

    }

    public class StaffSubjectClassSectionModel
    {

        public int ID { get; set; }
        public int StaffSubjectClassSectionID { get; set; }
        public int StaffId { get; set; }
        public int SubjectId { get; set; }
        public int FinalRating { get; set; }
        public long SchoolId { get; set; }
    }

    public class SchoolStaffModel
    {
        public string TRCNText;
        //StaffSubjectClassSectionID
        public int StaffSubjectToClassID { get; set; }
        public int? FinalRating { get; set; }
        public int DegID { set; get; }
        public int?[] SubjectIDss { set; get; }

        public string SchoolStaffName { get; set; }
        public int StaffSubjectRowId { get; set; }
        public int StaffSubjectToClassRowId { get; set; }
        public int ItemSectionAssessmentID { get; set; }
        //SchoolStaffID
        public int SchoolStaffID { get; set; }
        public int CountNonDegreeHolder { get; set; }
        public string Image { get; set; }

        public int NumberOfDegreeHolder { get; set; }
        public int ID { get; set; }
        public int StaffID { get; set; }
        public int CredentialID { get; set; }
        public string FullName => FirstName + " " + LastName;
        public string FirstName { get; set; }
        public string StaffName { get; set; }
        public string LastName { get; set; }
        public string Category { get; set; }
        public bool isTRCN { get; set; }
        public int SchoolID { get; set; }


        public DateTime YearEmployed { get; set; }
        public int SubjectID { get; set; }
        public string DegreeType { get; set; }
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int StaffDegreeId { get; set; }
        public int CourseID { get; set; }
        public string CourseTitle { get; set; }
        //public string Category { get; set; }
        public int staffIdEdit { get; set; }
        public string LongName { get; set; }
        public string SubjectLongName { get; set; }
        public string OtherDegree { get; set; }

        //CourseID
        //DegreeType
    }

    public class StaffModelForEdit
    {
        public int ID { get; set; }
        public string LongName { get; set; }
        public string StaffName { get; set; }
        public int SchoolID { get; set; }
        public int ClassID { get; set; }
        public int DegreeID { get; set; }
        public int SubjectID { get; set; }
        public string DegreeType { get; set; }
        public string CourseTitle { get; set; }
        public string CourseID { get; set; }
        public string ClassName { get; set; }

    }

    public class SchoolStaffProfileViewModel
    {
        public long ID { get; set; }

        public int StaffSchoolID { get; set; }
        public int StaffID { get; set; }
        public int StaffSubjectID { get; set; }
        public int SubjectID { get; set; }
        public string StaffSubjectName { get; set; }
        //StaffSubjectName
        public string SchoolName { get; set; }
        public string LongName { get; set; }

        public string SchoolLocation { get; set; }

        public string YearEstablished { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Joint Proprietorship?")]
        public bool IsJointProprietorship { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]
        [Display(Name = "Aproximate Land Area")]
        public string AproximateLandArea { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is Land Area Adequate?")]
        public bool IsLandAreaAdequate { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Is School Fenced Round?")]
        public bool IsSchoolFencedRound { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Admission Registers Available")]
        public bool IsAdmissionRegisterAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Class Registers Available")]
        public bool isClassRegistersAvaialable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Diaries of Work Available")]
        public bool isDiaryofWorksAvaialble { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Are Curricula Available")]
        public bool isCurricullumAvailable { get; set; }


        [DataType(DataType.Text)]
        [Display(Name = "Is Scheme of work Available")]
        public bool IsScheemOfWorkAvailable { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Others Details (Specify)")]
        public string OtherSpecify { get; set; }

        public string Category { get; set; }
        public string StateName { get; set; }

        public string Name { get; set; }

        //public string PrincipalFirstName { get; set; }
        //public string Name { get; set; }

        public string PrincipalFirstName { get; set; }
        public string DegreeType { get; set; }

        //public string ContactAddress { get; set; }

        //public string proprietorName { get; set; }
        //public string DegreeType { get; set; }

        public string ContactAddress { get; set; }

        public string proprietorName { get; set; }

        //public string SchoolStaffID { get; set; }
    }


    //vwGetLocalDetailsByStateLocal
    public class vwGetLocalDetailsByStateLocal
    {
        public string LocalCode { get; set; }
        public int StateCode { get; set; }
        public int LgaID { get; set; }
        public int officeid { get; set; }
        public int OfficeID { get; set; }
        public string OfficeName { get; set; }
        public string Name { get; set; }
        public string StateName { get; set; }
    }

    public class vwGetStateToAdmin
    {
        public int StateCode { get; set; }
        public string UserID { get; set; }
        public string StateName { get; set; }
        public bool isAdmin { get; set; }
        public string EmailAddress { get; set; }
    }

    public class vwSchoolCompletionReport2
    {
        public string SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string SchoolNameLocation { get; set; }
        public string StateName { get; set; }
        public string SchoolCategory { get; set; }
        public string SchoolLocation { get; set; }
        public string OfficeName { get; set; }
        public string OfficerName { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string DateRecognition { get; set; }
        public int StaffCount { get; set; }
        public string SupervisorSign { get; set; }
        public string OfficerSignature { get; set; }
    }

    public partial class vwUserListing2
    {
        public int ID { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string OfficeName { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string Rolename { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<bool> isSuperAdmin { get; set; }
        public bool isActive { get; set; }
        //public string StateID { get; set; }
        public Nullable<int> LGAID { get; set; }
        public Nullable<int> RankID { get; set; }
        public string LPNO { get; set; }
        public Nullable<int> OfficeID { get; set; }
        public string StateName { get; set; }
        public int StateID { get; set; }
        public byte[] Password { get; set; }
        public string LGAName { get; set; }
        public string StateCode { get; set; }
        public string Signature { get; set; }
    }

//vwStateByStateCode
    public class vwStateByStateCode
    {
        public int StateCode { get; set; }

        public int StateName { get; set; }
    }

    public class RoleViewModel
    {
        public int? Id { get; set; }
        public List<Role> Roles { get; set; }
        public string SurName { get; set; }
        public string OfficeName { get; set; }
    }

    public class LocalModel
    {
        public int ID { get; set; }
        public int LgaID { get; set; }
        public string Name { get; set; }
        public List<LocalModel> Names { get; set; }
        public List<int> AvailableStateSelectedX { get; set; }
        //AvailableStateSelectedX
        public int StateID { get; set; }
        public int StateCode { get; set; }
        public string OfficeID { get; set; }
        public string LocalCode { get; set; }
    }


    public class StateModel
    {
        public string StateCode { get; set; }
        public string StateID { get; set; }
        public string StateName { get; set; }
    }

    public class StateViewModel
    {
        public int? Id { get; set; }
        public string StateName { get; set; }
        public string StateCode { get; set; }
        public int OfficeID { get; set; }
        public int StateID { get; set; }
        public int UserID { get; set; }
        public List<StateViewModel> Statess { get; set; }
        public StateViewModel FilterStates { get; set; }
        public List<State> States { get; set; }
        public List<State> StatesEdit { get; set; }

        public List<State> RequestedSubjects { get; set; }
        public List<string> AvailableStateSelected { get; set; }
        public List<int> AvailableStateSelectedX { get; set; }

        //public List<string> AvailableStateSelected { get; set; }
        public List<string> AvailableStateSelectedEdit { get; set; }
    }

    public class ReasonsToDereSubjectcModel
    {
        public int ReasonId { get; set; }
        public string ReasonsForDerecognition { get; set; }

    }

    public class SchoolSubjectViewModel
    {

        public int TextbookToSchoolSubjectRowId { get; set; }
        public int AdequateEquipment { get; set; }

        public int SubId { get; set; }
        public int AdequateReagent { get; set; }
        public int AdequateLaboratory { get; set; }
        public int QualifiedStaff { get; set; }
        public int ExaminationMalpractice { get; set; }

        public int[] ReasonId { get; set; }
        public string AvaiilableTextbooks { get; set; }
        public int StaffSubjectID { get; set; }
        public int StaffSubjectIDD { get; set; }
        public int ClassID { get; set; }

        public int RealRowForUpdate { get; set; }

        public int LaboratoryFacility { get; set; }
        //LaboratoryFacility
        public int EquipmentFacility { get; set; }
        public int ReagentFacility { get; set; }
        public string ItemSection { get; set; }
        public int FinalRating { get; set; }
        public bool IsApproved { get; set; }
        public int SchoolSubjectID { get; set; }
        public string Textboxvalues { get; set; }
        public Subject Subject { get; set; }
        public string AvailableTextbook { get; set; }
        public string AvailableTextbooks { get; set; }
        public bool isBarred { get; set; }

        public long ID { get; set; }
        public int SubjectID { get; set; }

        public long SchoolID { get; set; }
        public List<SubjectModel> AvailableSubjects { get; set; }
        // public List<AssignedSchoolSubject> assAvailableSubjects { get; set; }
        public string LongName { get; set; }
        public string SubjectLongName { get; set; }


        public int[] RequestedSelected { get; set; }

        public List<SubjectModel> RequestedSubjects { get; set; }

        public List<int?> AvailableSelected { get; set; }
        public bool isTextBookAttached { get; set; }
        public bool IsTextBookAttached { get; set; }
        public bool isTeacherAvailable { get; set; }
        public int Grade { get; set; }
        public DateTime DateUpdated { get; set; }
        public int UpdatedBy { get; set; }

        public bool HasItem { get; set; }
        public DateTime BarredDate { get; set; }

    }



    public class LaboratoryViewModel
    {
        public int ID { get; set; }
        public int SchLabValueID { get; set; }

        public Nullable<int> SubjectID { get; set; }
        public Nullable<int> FacilityID { get; set; }
        public Nullable<int> Facility_ID { get; set; }
        //
        public string Specification { get; set; }
        public string QuantityRequired { get; set; }
        public string ItemDescription { get; set; }
        public string LongName { get; set; }
        public string UnitType { get; set; }
        public string UserValue { get; set; }
        public string LabValue { get; set; }


        //  public int SchoolFacilityValue_ID { get; set; }
        public int SchoolLabConfig_ID { get; set; }
        //public int SchoolEquipConfigID { get; set; }
        public int SchoolFacilityValueID { get; set; }
    }

    public class LibraryViewModel
    {
        public int ID { get; set; }
        public int SchoolLibraryConfig_ID { get; set; }
        public string ItemDescription { get; set; }
        public string LibraryValue { get; set; }
        public string UnitType { get; set; }

    }

    public class RecreationalViewModel
    {
        public int ID { get; set; }
        public int SchoolRecreatConfig_ID { get; set; }
        public string ItemDescription { get; set; }
        public string Value { get; set; }
        public string UnitType { get; set; }

    }

    public class ReagentViewModel
    {
        public int ID { get; set; }
        public int DigitValue { get; set; }
        public long SchoolID { get; set; }
        public int FacilityCategoryID { get; set; }

        public int SubjectID { get; set; }
        public int FacilityID { get; set; }
        public string Definition { get; set; }
        public string Specification { get; set; }
        public decimal? QuantityRequired { get; set; }
        public string ItemDescription { get; set; }
        public string LongName { get; set; }
        public string UnitType { get; set; }
        public decimal? ReagentValue { get; set; }
        public int SchReagentID { get; set; }
        public int SchoolReagentValueID { get; set; }
        //SchReagentID
        public int SchoolReagentConfig_ID { get; set; }
        //public int SchoolEquipConfigID { get; set; }
        public int SchoolFacilityValueID { get; set; }

        public string GetDefinitionDefaultValue()
        {
            return string.IsNullOrEmpty(Definition) ? "Not specified" : Definition;
        }
    }

    public class SchoolExamHallConfigModel
    {
        public string ItemDescription { get; set; }
        public string UserValue { get; set; }
        public int ID { get; set; }
    }

    //SchoolExamHallConfig

    public class SchoolReagentConfigModel
    {
        public string ItemDescription { get; set; }
        public string Value { get; set; }
        public long SchoolID { get; set; }
        public int ID { get; set; }

    }

    public class SchoolLibraryConfigModel
    {
        public int ID { get; set; }
        public int SchLLIBConfigID { get; set; }
        public int LibraryFacilityID { get; set; }
        public string LibraryValue { get; set; }
        public string ItemDescription { get; set; }
        public int SchoolLibraryFacilityID { get; set; }
        public int SchoolID { get; set; }



    }

    public class StudentInClass
    {
        public string ClassName { get; set; }
        public string SchoolName { get; set; }
        public int NoOfStreams { get; set; }
        public int TotalStudents { get; set; }
        public long ID { get; set; }
        public int classid { get; set; }
    }

    /*
     *  public string ClassName { get; set; }
        public string SchoolName { get; set; }
        public Nullable<int> NoOfStreams { get; set; }
        public Nullable<int> TotalStudents { get; set; }
        public long ID { get; set; }
        public int classid { get; set; }
     */

    public class vwSchoolFacilityValueModel
    {
        public int ID { get; set; }
        public long SchoolID { get; set; }
        public int SchoolFacConfigID { get; set; }
        public string UserValue { get; set; }
    }

    public class vwSchoolLaboratoryModel
    {
        public string ItemDescription { get; set; }
        public string LongName { get; set; }
        public string LabValue { get; set; }
        public int LabItemID { get; set; }
        public int schoolID { get; set; }
    }

    /*
          dbo.Facility.ItemDescription, dbo.Subject.LongName, dbo.SchLabValue.LabValue, dbo.SchoolProfile.ID AS schoolID,
             dbo.SchoolLabConfig.ID AS LabItemID


             */

    public class vwSchoolEquipmentValueModel
    {

        public int ID { get; set; }
        public Nullable<int> SchoolFacilityValueID { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public Nullable<double> EquipmentValue { get; set; }
        public Nullable<int> FacilityGradeID { get; set; }
    }

    public class SchoolEquipmentReportModel
    {
        public int SchoolEquipmentValueID { get; set; }
        public string ItemDescription { get; set; }
        public string Definition { get; set; }
        public int EquipSubjectID { get; set; }
        public string LongName { get; set; }
        public string Specification { get; set; }
        public int QuantityRequired { get; set; }
        public int Unit { get; set; }
        public string EquipmentValue { get; set; }
        public long SchoolID { get; set; }
        public string SchoolName { get; set; }





    }

    public class FacilityGradeModel
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string Definition { get; set; }
        public string Range { get; set; }
        public int DigitValue { get; set; }
    }

    public class SchoolEquipmentValueModel
    {
        public int ID { get; set; }
        public int DigitValue { get; set; }
        public long SchoolID { get; set; }
        public bool isActive { get; set; }
        public int FacilityGradeID { get; set; }
        public int EntryUpdateByUserId { get; set; }
        public int BarredByUserId { get; set; }
        public DateTime DateEntry { get; set; }
        public FacilityGrade facGrade { get; set; }
        public string Definition { get; set; }
        public decimal? EquipmentValue { get; set; }
        //  public int SchoolFacilityValue_ID { get; set; }
        public int Equipmentvalue_ID { get; set; }
        public int UserId { get; set; } //
        //DateDerecognised
        public DateTime DateUpdated { get; set; }

        public string GetDefinitionDefaultValue()
        {
            return !string.IsNullOrEmpty(Definition) && Definition.ToLower().Equals("Excellent".ToLower()) ? "1" : "0";
        }
    }


    public class SchoolEquipConfigViewModel
    {
        public int ID { get; set; }
        public string Value { get; set; } //  Unique_row
        public int Unique_row { get; set; }
        public int DigitValue { get; set; }
        public int FacilityGradeID { get; set; }
        //FacilityGradeID
        public int? SubjectID { get; set; }
        public int FacilityID { get; set; }
        public string isApproved { get; set; }
        public bool IsActive { get; set; }


        // public int SchoolEquipmentValueID { get; set; }

        public string LongName { get; set; }
        public int UserId { get; set; } //
        public int EntryUpdateByUserId { get; set; }
        public int BarredByUserId { get; set; }
        public DateTime DateEntry { get; set; }
        public DateTime DateUpdated { get; set; }
        public string Specification { get; set; } //QuantityRequired
        public decimal? QuantityRequired { get; set; }
        public string ItemDescription { get; set; }
        public string UnitType { get; set; }
        public FacilityGrade facGrade { get; set; }
        public string Definition { get; set; }
        public decimal? EquipmentValue { get; set; }
        //  public int SchoolFacilityValue_ID { get; set; }
        public int Equipmentvalue_ID { get; set; }
        //public int SchoolEquipConfigID { get; set; }
        public int SchoolFacilityValueID { get; set; }
        public int FacilityCategoryID { get; set; }

        public string GetDefinitionDefaultValue()
        {
            return !string.IsNullOrEmpty(Definition) && Definition.ToLower().Equals("Excellent".ToLower()) ? "1" : "0";
        }
    }

    public class vmFacilityCategory
    {
        public int ID { get; set; }
        public string TypeOfFacility { get; set; }
        public bool isActive { get; set; }
        public int UseOrder { get; set; }
        public bool hasSubjects { get; set; }
        public Nullable<int> RecognitionID { get; set; }

        public int FacilityCategoryID { get; set; }
    }

    public class AssessmentViewModel
    {
        public long ID { get; set; }
        public long? SchoolID { get; set; }
        public int? FacilityTypeSettingID { get; set; }
        public int? RatingID { get; set; }

        public string ItemDescription { get; set; }
        public virtual RatingSetting RatingSetting { get; set; }
        public virtual SchoolProfile SchoolProfile { get; set; }

        public string SchoolName { get; set; }


    }

    public class SelectedSchoolSubjectViewModel
    {
        public long ID { get; set; }

        public string Textboxvalues { get; set; }

        public string AvailableTextbooks { get; set; }
        //AvailableTextbooks
        //isTeacherAvailable
        public string GetDefinitionDefaultValue()
        {
            return string.IsNullOrEmpty(Definition) ? "0" : Definition;
        }

        public string Definition { get; set; }
        public bool isTeacherAvailable { get; set; }
        public string LongName { get; set; }
        public long SchoolID { get; set; }
        public int SubjectID { get; set; }
        public int SchoolSubjectID { get; set; }
        public bool isTextBookAttached { get; set; }
        public bool ifTextBookAttached { get; set; }



    }

    public class ExamHallViewModel
    {
        public long ID { get; set; }
        public int SchoolExamHallConfig_ID { get; set; }
        public long SchoolID { get; set; }
        public string ItemDescription { get; set; }
        public string ExamValue { get; set; }

    }



    public class vmSchoolSummary
    {

    }

    public class ReagentModelAdmin
    {
        public int ID { get; set; }
        public string ItemDescription { get; set; }
        public double QuantityRequired { get; set; }
        public string Specification { get; set; }
        public int SubjectID { get; set; }
        public int FacilityCategoryID { get; set; }
    }

    public class EquipmentModel
    {
        public int ID { get; set; }
        public string itemDescription { get; set; }
        public double QuantityRequired { get; set; }
        public string Specification { get; set; }
        public int SubjectID { get; set; }
    }

    public class vmSchoolStaffQualification2
    {
        public int ID { get; set; }
        public long SchoolID { get; set; }
        //SubjectIDss
        public string SchoolName { get; set; }
        public string LongName { get; set; }
        public string FullName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactAddress { get; set; }

        public bool isTRCN { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]

        public DateTime? DateEmployed { get; set; }
        public int CategoryID { get; set; }
        public string OtherDegree { get; set; }
        public string Category { get; set; }
        public int SchoolStaffID { get; set; }
        public SchoolStaff schoolStaff { get; set; }
        public SchStafDegCourse course { get; set; }
        public string CourseName { get; set; }
        public string CourseTitle { get; set; }
        public int? SecondDegreeCourseID { get; set; }
        //public int? CourseID { get; set; }
        public int[] CourseID { get; set; }

        public int[] NTCourseID { get; set; }

        public int[] NTDegreeID { get; set; }

        public int[] SubjectIDss { get; set; }
        //SubjectIDss

        public int[] SubjectID { get; set; }
        public Subject Subject { get; set; }
        //public int? DegreeID { get; set; }

        public int[] DegreeID { get; set; }
        public int? SecondDegreeID { get; set; }
        // public List<int> ClassID { get; set; }
        public int[] ClassID { get; set; }
        public string ClassName { get; set; }
        public int TotalStudents { get; set; }
        public int NoOfStreams { get; set; }
        public string DegreeType { get; set; }
        //public Degree DegreeType { get; set; }
        public string Label { get; set; }
        public int TitleID { get; set; }
        public int Deg { get; set; }

        //=========================================================


    }

    public class vmSchoolStaffForEdit
    {
        public string LongName { get; set; }
        public long SchoolID { get; set; }
        public string LastName { get; set; }
        public int TitleID { get; set; }
        public int SubjectID { get; set; }
        public int DegreeID { get; set; }
        public string TRCNText { get; set; }
        public int CategoryID { get; set; }
        public int CoursID { get; set; }

    }

    public class vmSchoolStaffQualification
    {

        public int ID { get; set; }
        public string TRCNText { get; set; }
        public long SchoolID { get; set; }
        public int SubId { get; set; }
        public string Image { get; set; }
        public string SchoolStaffName { get; set; }

        public int SchoolStaffNameId { get; set; }

        public List<SchoolStaffModel> Credentials { get; set; }
        public int CredentialID { get; set; }
        public List<long> SchoolSubjectID { get; set; }
        //SubjectIDss
        public string SchoolName { get; set; }
        public string LongName { get; set; }
        public string FullName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ContactAddress { get; set; }

        public bool isTRCN { get; set; }

        //[DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DateEmployed { get; set; }
        public int CategoryID { get; set; }
        public string OtherDegree { get; set; }
        public string Category { get; set; }
        public int DegID { get; set; }
        public int SchoolStaffID { get; set; }
        public SchoolStaff schoolStaff { get; set; }
        public SchStafDegCourse course { get; set; }
        public string CourseName { get; set; }
        public string CourseTitle { get; set; }
        public int? SecondDegreeCourseID { get; set; }
        //public int? CourseID { get; set; }
        public int CourseID { get; set; }
        public int CoursID { get; set; }

        public int[] NTCourseID { get; set; }

        public int[] NTDegreeID { get; set; }

        public int?[] SubjectIDss { get; set; }
        //SubjectIDss
        public int Subj { get; set; }
        public int[] SubjectID { get; set; }
        //public vmSchoolStaffQualification()
        //{
        //    SubjectID = new int[] { 100, 50, 250 };
        //}
        public Subject Subject { get; set; }
        //public int? DegreeID { get; set; }

        public int DegreID { get; set; }
        public int[] DegreeID { get; set; }
        public int? SecondDegreeID { get; set; }
        // public List<int> ClassID { get; set; }
        public int[] ClassID { get; set; }
        public string ClassName { get; set; }
        public int TotalStudents { get; set; }
        public int NoOfStreams { get; set; }
        public string DegreeType { get; set; }
        //public Degree DegreeType { get; set; }
        public string Label { get; set; }
        public int TitleID { get; set; }

        //=========================================================


    }

    public class SubjectModelFootMessage
    {
        public List<string> Messages { get; set; }
        public List<int> MessagesInt { get; set; }
        public int SubjectId { get; set; }
    }

    //vwGetStaffsSubjects
    public class vwSchoolStaffsSubjects
    {

        public int SchoolStaffID { get; set; }
        public int SubjectID { get; set; }
        public string LongName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StaffSubjectID { get; set; }
        public long SchoolID { get; set; }
    }

    public class SubjectModel
    {

        public int ID { get; set; }
        public int SubjectID { get; set; }
        public int SubjectLongName { get; set; }
        public long SchoolID { get; set; }
        public List<SubjectModel> AvailableSubjects { get; set; }

        public int[] RequestedSelected { get; set; }

        public List<SubjectModel> RequestedSubjects { get; set; }

        public List<int?> AvailableSelected { get; set; }
        public bool isTextBookAttached { get; set; }
        public bool isTeacherAvailable { get; set; }
        public int Grade { get; set; }
        public DateTime DateUpdated { get; set; }
        public int UpdatedBy { get; set; }
        public bool isBarred { get; set; }
        public bool HasItem { get; set; }
        public string LongName { get; set; }
        public DateTime BarredDate { get; set; }
        public Subject Subject { get; set; }
    }

    public class SchStaffCourseModel
    {
        public int ID { get; set; }
        public int CoursID { get; set; }
        public string CourseTitle { get; set; }

    }

    public class SchoolCentre
    {
        public string CentreNumber { get; set; }
        public string CentreName { get; set; }
    }

    public class vwSchoolSubjectReport
    {
        public int ID { get; set; }
        public string LongName { get; set; }
        public long SchoolID { get; set; }
        public bool HasItem { get; set; }

    }

    public class vwSchoolSubjectFacilityReport
    {
        public int ID { get; set; }
        public long SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string LongName { get; set; }
        public int Total_Equipment { get; set; }
        public int TotalSchoolEquipment { get; set; }
        public int TotalSchoolReagent { get; set; }
        public int Total_Reagent { get; set; }

        public class SchoolSubjectCompletionReportModel
        {
            public int SchoolID { get; set; }
            public string SchoolName { get; set; }
            public string LongName { get; set; }
            public int Total_Equipment { get; set; }
            public int TotalSchoolEquipment { get; set; }
            public int TotalSchoolReagent { get; set; }
            public int Total_Reagent { get; set; }
            public bool HasItem { get; set; }


        }

        //vwSubjectRecogntionReport
        public class vwSubjectRecogntionReport
        {
            public int SubjectID { get; set; }
            public bool isApproved { get; set; }
            public string LongName { get; set; }

        }
       
    }
    public class AssignedStatesByRoleNameModel
    {
        public string Rolename { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public int StateCode { get; set; }
        public string StateName { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }

    }
}

