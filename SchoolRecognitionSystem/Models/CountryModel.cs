﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Models
{
    public class CountryModel
    {
        public List<Country> CountryList { get; set; }
        public string SelectedCountryId { get; set; }
    }
    public class Country
    {
        public string CountryName { get; set; }
    }
}