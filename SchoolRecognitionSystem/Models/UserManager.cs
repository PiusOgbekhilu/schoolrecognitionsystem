﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Controllers;

namespace SchoolRecognitionSystem.Models
{
    public class UserManager
    {
        private string role;
        public User GetCurrentUser()
        {
            return new User();
        }


        //public static bool IsUserInRole(string roleName)
        //{
        //    var userRoles = new List<Role>();
        //    var roles = roleName.Split(',');
        //    foreach (var role in roles)
        //    {
        //        var roleSearch = clsContent.GetRoleByName(role);
        //        if (roleSearch != null)
        //            userRoles.Add(roleSearch);
        //    }
        //    var userid = HttpContext.Current.Session["UserID"]?.ToString();
        //    if (string.IsNullOrEmpty(userid))
        //    {
        //        return false;
        //    }
        //    var userId = int.Parse(userid);
        //    var userRolesGotten = new List<UserRoleModel>();

        //    // var userRole = clsContent.IsUserInRole(userId, userRoles.FirstOrDefault().ID);

        //    //userRolesGotten.Add(userRole);

        //    //    return userRolesGotten.Count > 0;
        //    foreach (var role in userRoles)
        //    {
        //        var userRole = clsContent.IsUserInRole(userId, role.ID);
        //        if (userRole != null)
        //            userRolesGotten.Add(userRole);
        //    }


        //    return userRolesGotten.Count > 0;


        //    // Write store to retrive the role based on the name
        //}



        #region working for multiple roles

        //public static bool IsUserInRole(string roleName)
        //{
        //    var userRoles = new List<RoleModel>();
        //    var userid = HttpContext.Current.Session["UserID"]?.ToString();
        //    var userId = int.Parse(userid);
        //    var roles = roleName.Split(',');
        //    foreach (var role in roles)
        //    {
        //        var roleSearch = clsContent.GetRoleByName(role, userId);
        //        if (roleSearch != null)
        //            userRoles.AddRange(roleSearch);
        //    }

        //    if (string.IsNullOrEmpty(userid))
        //    {
        //        return false;
        //    }

        //    var userRolesGotten = new List<UserRoleModel>();

        //    // var userRole = clsContent.IsUserInRole(userId, userRoles.FirstOrDefault().ID);

        //    //userRolesGotten.Add(userRole);

        //    //    return userRolesGotten.Count > 0;
        //    foreach (var role in userRoles)
        //    {
        //        var userRole = clsContent.IsUserInRole(userId, role.RoleID);
        //        if (userRole != null)
        //            userRolesGotten.Add(userRole);
        //    }


        //    return userRolesGotten.Count > 0;


        //    // Write store to retrive the role based on the name
        //}
        #endregion


        public static bool IsUserInRole(string roleName)
        {
            var userRoles = new List<Role>();
            var roles = roleName.Split(',');
            foreach (var role in roles)
            {
                var roleSearch = clsContent.GetRoleByName(role);
                if (roleSearch != null)
                    userRoles.Add(roleSearch);
            }
            var userId = BaseController.CurrentUser.UserId;
            var userRolesGotten = new List<UserRole>();
            foreach (var role in userRoles)
            {
                var userRole = clsContent.IsUserInRole(userId, role.ID);
                if (userRole != null)
                    userRolesGotten.Add(userRole);
            }

            return userRolesGotten.Count > 0;


            // Write store to retrive the role based on the name
        }

        public List<Role> GetUserRoles()
        {
            // Write store to retrive the role based on the name
            return new List<Role>();
        }
       

    }
}