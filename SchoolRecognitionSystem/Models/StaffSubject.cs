//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StaffSubject
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public StaffSubject()
        {
            this.StaffSubjectToClasses = new HashSet<StaffSubjectToClass>();
        }
    
        public int ID { get; set; }
        public Nullable<int> SchoolStaffID { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public Nullable<int> SubjectReport1 { get; set; }
        public Nullable<int> SubjectReport2 { get; set; }
        public Nullable<bool> isBarred { get; set; }
    
        public virtual SchoolStaff SchoolStaff { get; set; }
        public virtual Subject Subject { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffSubjectToClass> StaffSubjectToClasses { get; set; }
    }
}
