﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System.Collections;
using System.Transactions;
using System.Web;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace SchoolRecognitionSystem.Models
{
    public static class SortExtension
    {
        public static IOrderedEnumerable<TSource> OrderByWithDirection<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

        public static IOrderedQueryable<TSource> OrderByWithDirection<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

    }




    public class ModelServices : IDisposable
    {
        private readonly SchoolSubjectRecognitionContext entities = new SchoolSubjectRecognitionContext();

       // For Custom Paging
      
        //public int CountCustomer()
        //{
        //    return entities.Customers.Count();
        //}

        public void Dispose()
        {
            entities.Dispose();
        }

    }

    public class PagedCustomerModel
    {
        public int TotalRows { get; set; }
        public IEnumerable<SchoolCategory> Category { get; set; }
        public int PageSize { get; set; }
    }















    //========================================================================================================




    public class vmTypesettingsUser:IDisposable
    {
        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();

        public string SchoolItems { get; set; }

        public string Facilities { get; set; }

        public void Dispose()
        {
            ctx.Dispose();
        }
        public IEnumerable<SchoolCategory> GetCategoryPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "category")
                return ctx.SchoolCategories.OrderByWithDirection(x => x.Category, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else
                return ctx.SchoolCategories.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public IList<SelectListItem> RecNames { get; set; }
        public IList<SelectListItem> FacNames { get; set; }
        //public int CountCategory()
        //{
        //    return ctx.SchoolCategories.Count();
        //}

        //[Required(ErrorMessage = "The Item Description cannot be blank")]
        //public string ItemDescription { get; set; }
        [Required(ErrorMessage = "The Facility Type cannot be blank")]
        public int FacilityTypeID { get; set; }
        public string ItemDescription { get; set; }
        public List<InspectionType> InspectionTypes { get; set; }
        public List<FacilityCategory> FacilityCategorys { get; set; }
       // public List<Subject> _subjectList = new SchoolSubjectRecognitionContext().Subjects.ToList();
        //public List<Subject> SubjectList
        //{
        //    get { return _subjectList; }
        //    set { _subjectList = value; }
        //}

        public int ID { get; set; }
        public string TypeofRecognition { get; set; }
        public string FacilityType { get; set; }

       
        public bool isActive { get; set; }

        // public string userValue { get; set; }
        //[Required(ErrorMessage ="please select EITHER PHYSICS, CHEMISTRY, BIOLOGY, AGRIC-SCIENCE")]
        public int SubjectID { get; set; }
        [Required(ErrorMessage = "The Facility Type Settings cannot be blank")]
        public int FacilityID { get; set; }
        [Required(ErrorMessage = "The Quantity Required cannot be blank")]
        public string QuantityRequired { get; set; }
        public Subject SubjectLongName { get; set; }
        public string LongName { get; set; }
        public int schoolequipID { get; set; }
        public int TotalRows { get; set; }
        public IEnumerable<SchoolLabView> Laboratory { get; set; }
        public IEnumerable<SchoolEquipView> Equipment { get; set; }
        public int PageSize { get; set; }

        public IEnumerable<SchSumaryAssessView> GetSummaryPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchSumaryAssessViews.OrderByWithDirection(x => x.ItemDescription, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchSumaryAssessViews.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }

        public IEnumerable<SchSumAsesConfig> GetSummaryPage2(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchSumAsesConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchSumAsesConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }


        public IEnumerable<SchoolLabView> GetLabPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolLabViews.OrderByWithDirection(x => x.ItemDescription, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolLabViews.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }

        public IEnumerable<SchoolLabConfig> GetLabPage2(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolLabConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolLabConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }








        public IEnumerable<SchSumaryAssessView> GetSumPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchSumaryAssessViews.OrderByWithDirection(x => x.ItemDescription, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchSumaryAssessViews.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public IEnumerable<SchRecreatView> GetRecreationPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchRecreatViews.OrderByWithDirection(x => x.ItemDescription, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchRecreatViews.OrderByWithDirection(x => x.ItemDescription, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        
           
        public IEnumerable<SchoolRecreatConfig> GetRecreationPage2(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolRecreatConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolRecreatConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public IEnumerable<SchoolExamHallConfig> GetExamHallDiv(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolExamHallConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolExamHallConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public IEnumerable<ClassroomConfig> GetClassroomDiv(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.ClassroomConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.ClassroomConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }



        public IEnumerable<SchoolEquipView> GetEquipPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolEquipViews.OrderByWithDirection(x => x.FacilityID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolEquipViews.OrderByWithDirection(x => x.FacilityID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public IEnumerable<SchoolEquipConfig> GetEquipPage2(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolEquipConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolEquipConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }

        public IEnumerable<SchoolLibraryConfig> GetLibPage2(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.SchoolLibraryConfigs.OrderByWithDirection(x => x.ID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.SchoolLibraryConfigs.OrderByWithDirection(x => x.ID, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }




        public IEnumerable<schoollibview> GetLibPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "catee")
                return ctx.schoollibviews.OrderByWithDirection(x => x.ItemDescription, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();

            else
                return ctx.schoollibviews.OrderByWithDirection(x => x.ItemDescription, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public int CountLab()
        {
            return ctx.SchoolLabViews.Count();
        }
        public int CountEquip()
        {
            return ctx.SchoolEquipViews.Count();
        }

        public bool SaveRecordLab(string catee, string SU, string facateid)
        {
            bool dec = false;
            var catid = int.Parse(facateid);
            var subid = int.Parse(SU);

            //var subjct = ctx.Subjects.Where(su => su.ID == subid).FirstOrDefault();

            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    Facility fac = new Facility();

                    var f = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
                    if (f == null)
                    {
                        fac.ItemDescription = catee;
                        fac.FacilityCategoryID = catid;
                        ctx.Facilities.Add(fac);
                        ctx.SaveChanges();

                    }
                    SchoolLabConfig slc = new SchoolLabConfig();
                    //SchoolEquipConfig slc = new SchoolEquipConfig();
                    var labconfig = ctx.SchoolLabConfigs.Where(v => v.LabFacilityID == fac.ID).FirstOrDefault();
                    if (labconfig == null)
                    {  
                        slc.LabFacilityID = fac.ID;
                        slc.SubjectID = subid;                       
                        ctx.SchoolLabConfigs.Add(slc);
                        ctx.SaveChanges();
                    }
                    cc.Complete();
                    dec = true;
                }
            }
            catch
            {
                dec = false;
            }
            return dec;
        }

        ////public bool SaveCategoryE(string catee, string Quant)
        ////{
        ////    bool dec = false;
        ////    try
        ////    {
        ////        SchoolLabView cat = new SchoolLabView();
        ////        var fac = new Facility();

            ////        var t = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
            ////        if (t == null)
            ////        {
            ////            cat.ItemDescription = catee;
            ////            ctx.SchoolLabViews.Add(cat);
            ////            ctx.SaveChanges();
            ////            dec = true;
            ////        }
            ////    }
            ////    catch
            ////    {
            ////        dec = false;
            ////    }
            ////    return dec;
            ////}
        public bool SaveCategoryCate(string catee)
        {
            bool reTrueifsave = false;
            try
            {
               // SchoolLabView cat = new SchoolLabView();
                SchoolCategory cat= new SchoolCategory();
                var t = ctx.SchoolCategories.Where(v => v.Category == catee).FirstOrDefault();
                if (t == null)
                {
                    cat.Category = catee;
                    ctx.SchoolCategories.Add(cat);
                    ctx.SaveChanges();
                    reTrueifsave = true;
                }
            }
            catch
            {
                reTrueifsave = false;
            }
            return reTrueifsave;
        }


        public bool SaveRecordEquip(string catee, string Quant, string SU,string facateid)
        {
            bool dec = false;
            var catid = int.Parse(facateid);
            var subid = int.Parse(SU);

            //var subjct = ctx.Subjects.Where(su => su.ID == subid).FirstOrDefault();

            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    Facility fac = new Facility();

                    var f = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
                    if (f == null)
                    {
                        fac.ItemDescription = catee;
                        fac.FacilityCategoryID = catid;
                        ctx.Facilities.Add(fac);
                        ctx.SaveChanges();

                    }
                    SchoolEquipConfig slc = new SchoolEquipConfig();
                    var libconfig = ctx.SchoolEquipConfigs.Where(v => v.FacilityID == fac.ID).FirstOrDefault();
                    if (libconfig == null)
                    {
                        slc.FacilityID = fac.ID;
                        slc.SubjectID = subid;
                       // slc.QuantityRequired = Quant;
                        ctx.SchoolEquipConfigs.Add(slc);
                        ctx.SaveChanges();
                    }
                    cc.Complete();
                    dec = true;

                }
            }
            catch
            {
                dec = false;
            }
            return dec;
        }

        public bool updateSchCate(string id,string catee)
        {
            int idcat = int.Parse(id);
            try
            {
                var categ = (from tbl in ctx.SchoolCategories
                            where tbl.ID == idcat
                             select tbl).FirstOrDefault();
                categ.Category = catee;            

                ctx.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SaveRecordLib(string catee,string cateid)
        {
            bool dec = false;
            int catid = int.Parse(cateid);

            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    Facility fac = new Facility();

                    var f = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
                    if (f == null)
                    {
                        fac.ItemDescription = catee;
                        fac.FacilityCategoryID = catid;
                        ctx.Facilities.Add(fac);
                        ctx.SaveChanges();

                    }
                    SchoolLibraryConfig slc = new SchoolLibraryConfig();
                    var libconfig = ctx.SchoolLibraryConfigs.Where(v => v.LibraryFacilityID == fac.ID).FirstOrDefault();
                    if(libconfig==null)
                    {
                        slc.LibraryFacilityID = fac.ID;
                        ctx.SchoolLibraryConfigs.Add(slc);
                        ctx.SaveChanges();
                    }
                    cc.Complete();
                    dec = true;
                    
                }
            }
            catch
            {
                dec = false;
            }
            return dec;
        }
        
             public bool SaveRecordRecrea(string catee, string cateid)
        {
            bool dec = false;
            int catid = int.Parse(cateid);

            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    Facility fac = new Facility();

                    var f = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
                    if (f == null)
                    {
                        fac.ItemDescription = catee;
                        fac.FacilityCategoryID = catid;
                        ctx.Facilities.Add(fac);
                        ctx.SaveChanges();

                    }
                    SchoolRecreatConfig slc = new SchoolRecreatConfig();
                    var recreconfig = ctx.SchoolRecreatConfigs.Where(v => v.RecreationalFacilityID == fac.ID).FirstOrDefault();
                    if (recreconfig == null)
                    {
                        slc.RecreationalFacilityID = fac.ID;
                        ctx.SchoolRecreatConfigs.Add(slc);
                        ctx.SaveChanges();
                    }
                    cc.Complete();
                    dec = true;

                }
            }
            catch
            {
                dec = false;
            }
            return dec;
        }
        public bool SaveRecordSUM(string catee, string cateid)
        {
            bool dec = false;
            int catid = int.Parse(cateid);

            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    Facility fac = new Facility();

                    var f = ctx.Facilities.Where(v => v.ItemDescription == catee).FirstOrDefault();
                    if (f == null)
                    {
                        fac.ItemDescription = catee;
                        fac.FacilityCategoryID = catid;
                        ctx.Facilities.Add(fac);
                        ctx.SaveChanges();

                    }
                   SchSumAsesConfig sum = new SchSumAsesConfig();
                    var sumconfig = ctx.SchSumAsesConfigs.Where(v => v.SumAsesmentFacID == fac.ID).FirstOrDefault();
                    if (sumconfig == null)
                    {
                        sum.SumAsesmentFacID = fac.ID;
                        ctx.SchSumAsesConfigs.Add(sum);
                        ctx.SaveChanges();
                    }
                    cc.Complete();
                    dec = true;

                }
            }
            catch
            {
                dec = false;
            }
            return dec;
        }
        public bool UpdateRecordRecre(int id, string catee,string cateid)
        {
            try
            {

                using (TransactionScope cc = new TransactionScope())
                {

                    int ide = int.Parse(cateid);
                    var faceq = (from facil in ctx.Facilities where facil.ID == id select facil).FirstOrDefault();
                    faceq.ItemDescription = catee;
                    faceq.FacilityCategoryID = ide;
                    ctx.SaveChanges();

                    
                    var trcreaconfig = (from equip in ctx.SchoolRecreatConfigs where equip.RecreationalFacilityID ==id select equip).FirstOrDefault();

                    trcreaconfig.RecreationalFacilityID = faceq.ID;

                    ctx.SaveChanges();
                    cc.Complete();
                    return true;

                }


            }
            catch
            {
                return false;
            }
        }



        public bool UpdateCategoryE(int id, string catee, string qty, string sub, string equipID)
        {
            try
            {

                using (TransactionScope cc = new TransactionScope())
                {

                    var faceq = ctx.Facilities.Where(f => f.ID == id).FirstOrDefault();//(from facil in ctx.Facilities where facil.ID == id select facil).FirstOrDefault();
                    faceq.ItemDescription = catee;

                    ctx.SaveChanges();

                    int ide = int.Parse(equipID);
                    var equipconfig = (from equip in ctx.SchoolEquipConfigs where equip.ID == ide select equip).FirstOrDefault();
                   // equipconfig.QuantityRequired = qty;
                    int subj = int.Parse(sub);
                   // var subjectname = ctx.Subjects.Where(s => s.ID == subj).First().LongName;
                    // equipconfig.Subject.LongName = subjectname;
                    equipconfig.SubjectID = subj;
                    equipconfig.FacilityID = id;

                    ctx.SaveChanges();
                    cc.Complete();
                    return true;

                }


            }
            catch
            {
                return false;
            }
        }


        public bool UpdateCategoryL(int id, string catee, string sub,string labID)
        {
            try
            {
           
                using (TransactionScope cc = new TransactionScope())
                {
                    int labd = int.Parse(labID);
                    var faceq = (from facil in ctx.Facilities where facil.ID == id select facil).FirstOrDefault();
                    faceq.ItemDescription = catee;                    
                    
                    ctx.SaveChanges();

                   int ide = int.Parse(labID);
                    var labconfig = (from equip in ctx.SchoolLabConfigs where equip.ID ==ide  select equip).FirstOrDefault();
                    int subj = int.Parse(sub);                    
                    
                    labconfig.SubjectID = subj;
                    labconfig.LabFacilityID = faceq.ID ;
                    
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;

                }
             

            }
            catch
            {
                return false;
            }
        }
        public bool UpdateCategoryLib(int id, string catee)
        {
            try
            {

                using (TransactionScope cc = new TransactionScope())
                {
                   
                    var faceq = (from facil in ctx.Facilities where facil.ID == id select facil).FirstOrDefault();
                    faceq.ItemDescription = catee;

                    ctx.SaveChanges();

                    // int ide = int.Parse(equipID);
                    var libconfig = (from lib in ctx.SchoolLibraryConfigs where lib.LibraryFacilityID == id select lib).FirstOrDefault();
                    libconfig.LibraryFacilityID = faceq.ID;
                   

                    ctx.SaveChanges();
                    cc.Complete();
                    return true;

                }


            }
            catch
            {
                return false;
            }
        }

        public bool DeleteRecordRecrea(int id)
        {
            try
            {
                using (TransactionScope cc = new TransactionScope())
                {
                    var recearowcofig = (from tbl in ctx.SchoolRecreatConfigs
                                       where tbl.RecreationalFacilityID == id
                                       select tbl).FirstOrDefault();

                    ctx.SchoolRecreatConfigs.Remove(recearowcofig);
                    ctx.SaveChanges();

                    var librowfac = (from tbl in ctx.Facilities
                                     where tbl.ID == id
                                     select tbl).FirstOrDefault();

                    ctx.Facilities.Remove(librowfac);
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;
                }

            }
            catch
            {
                return false;
            }
        }


        public bool DeleteRecordLib(int id)
        {
            try
            {
                //var retlibr = ctx.SchoolLibraryConfigs.Where(libr => libr.ID == id).FirstOrDefault();
                using (TransactionScope cc = new TransactionScope())
                {
                    var librowcofig = (from tbl in ctx.SchoolLibraryConfigs
                                  where tbl.LibraryFacilityID == id
                                  select tbl).FirstOrDefault();

                    ctx.SchoolLibraryConfigs.Remove(librowcofig);
                    ctx.SaveChanges();

                    var librowfac = (from tbl in ctx.Facilities
                                  where tbl.ID == id
                                  select tbl).FirstOrDefault();

                    ctx.Facilities.Remove(librowfac);
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;
                }
             
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteRecordEquip(int id,string facid)
        {
            int faccd=int.Parse(facid);
            try
            {
                //var myvw = ctx.SchoolEquipViews.Where(ec => ec.Expr1 == id).FirstOrDefault();
                var faci = ctx.Facilities.Where(ec => ec.ID == id).FirstOrDefault();
               // var scheqipcofg = ctx.SchoolEquipConfigs.Where(d => d.ID == faci.EquipConfigID).FirstOrDefault();
                // int eqpd = int.Parse(equipid);
                using (TransactionScope cc = new TransactionScope())
                {
                    var scheq = (from tbl in ctx.SchoolEquipConfigs
                                 where tbl.ID == faccd
                                 select tbl).FirstOrDefault();

                    ctx.SchoolEquipConfigs.Remove(scheq);
                    ctx.SaveChanges();

                    var fac = (from tbl in ctx.Facilities
                               where tbl.ID ==id
                               select tbl).FirstOrDefault();

                    ctx.Facilities.Remove(fac);
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;
                }
               
            }
            catch
            {
                return false;
            }
        }
        public bool DeleteRecordL(int id)
        {
            try
            {
                var myvw = ctx.SchoolLabViews.Where(ec => ec.SchoolLabConfigID == id).FirstOrDefault();
                // int eqpd = int.Parse(equipid);
                using (TransactionScope cc = new TransactionScope())
                {
                    var scheq = (from tbl in ctx.SchoolLabConfigs
                                 where tbl.ID == myvw.SchoolLabConfigID
                                 select tbl).FirstOrDefault();

                    ctx.SchoolLabConfigs.Remove(scheq);
                    ctx.SaveChanges();

                    var fac = (from tbl in ctx.Facilities
                               where tbl.ID == scheq.LabFacilityID
                               select tbl).FirstOrDefault();

                    ctx.Facilities.Remove(fac);
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;
                }

            }
            catch
            {
                return false;
            }
        }


        public bool DeleteRecordL2(int id)
        {
            try
            {
                var retfac = ctx.Facilities.Where(ec => ec.ID == id).FirstOrDefault();
                // int eqpd = int.Parse(equipid);
                using (TransactionScope cc = new TransactionScope())
                {
                    var scheq = (from tbl in ctx.SchoolLabConfigs
                                 where tbl.LabFacilityID == retfac.ID
                                 select tbl).FirstOrDefault();

                    ctx.SchoolLabConfigs.Remove(scheq);
                    ctx.SaveChanges();

                    var fac = (from tbl in ctx.Facilities
                               where tbl.ID == retfac.ID
                               select tbl).FirstOrDefault();

                    ctx.Facilities.Remove(fac);
                    ctx.SaveChanges();
                    cc.Complete();
                    return true;
                }

            }
            catch
            {
                return false;
            }
        }

        //public List<SchoolStaff> GetStudent(string search,string sort,string sortdir,int skip,int pageSize,out int totalrecord)
        //{
        //    var v = (from u in ctx.SchoolStaffs where u.FullName.Contains(search) select u);
        //    v = v.OrderBy(sort + "" + sortdir);
        //    totalrecord = v.Count();
        //    if(pageSize>0)
        //    { v.Skip(skip).Take(PageSize); }
        //    return v.ToList();
        //}




        public bool DeleteRecordCateg(int id)
        {
            String result = String.Empty;

            var categ = (from tbl in ctx.SchoolCategories
                         where tbl.ID == id
                         select tbl).FirstOrDefault();
            if (categ != null)
            {
                ctx.SchoolCategories.Remove(categ);
                ctx.SaveChanges();
                result = "1";
                return true;
            }
            else
                result = "0";

            return false;
        }


    }
}