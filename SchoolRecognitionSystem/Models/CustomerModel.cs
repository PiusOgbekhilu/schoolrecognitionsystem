﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Text.RegularExpressions;

//Required for custom paging
using System.Linq.Expressions;


namespace SchoolRecognitionSystem.Models
{
    public static class SortExtension
    {
        public static IOrderedEnumerable<TSource> OrderByWithDirection<TSource, TKey>(this IEnumerable<TSource> source,Func<TSource, TKey> keySelector,bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

        public static IOrderedQueryable<TSource> OrderByWithDirection<TSource, TKey>(this IQueryable<TSource> source,Expression<Func<TSource, TKey>> keySelector,bool descending)
        {
            return descending ? source.OrderByDescending(keySelector)
                              : source.OrderBy(keySelector);
        }

    }

    public class ModelServices : IDisposable
    {
        private readonly SchoolSubjectRecognitionContext entities = new SchoolSubjectRecognitionContext();

        //For Custom Paging
        public IEnumerable<Customer> GetCustomerPage(int pageNumber, int pageSize, string sort, bool Dir)
        {
            if (pageNumber < 1)
                pageNumber = 1;

            if (sort == "name")
                return entities.Customers.OrderByWithDirection(x => x.Name, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else if (sort == "custid")
                return entities.Customers.OrderByWithDirection(x => x.CustID, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else if (sort == "contactno")
                return entities.Customers.OrderByWithDirection(x => x.ContactNo, Dir)
              .Skip((pageNumber - 1) * pageSize)
              .Take(pageSize)
              .ToList();
            else 
                return entities.Customers.OrderByWithDirection(x => x.Address, Dir)
             .Skip((pageNumber - 1) * pageSize)
             .Take(pageSize)
             .ToList();
        }
        public int CountCustomer()
        {
            return entities.Customers.Count();
        }

        public void Dispose()
        {
            entities.Dispose();
        }

    }

    public class PagedCustomerModel
    {
        public int TotalRows { get; set; }
        public IEnumerable<Customer> Customer { get; set; }
        public int PageSize { get; set; }
    }
}