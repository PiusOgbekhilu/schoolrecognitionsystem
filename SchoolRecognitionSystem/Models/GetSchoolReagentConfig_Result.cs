//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class GetSchoolReagentConfig_Result
    {
        public string ItemDescription { get; set; }
        public string UnitType { get; set; }
        public Nullable<int> FacilityCategoryID { get; set; }
        public string Specification { get; set; }
        public Nullable<double> QuantityRequired { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public int SchoolReagentConfig_ID { get; set; }
    }
}
