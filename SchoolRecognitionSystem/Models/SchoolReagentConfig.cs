//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SchoolReagentConfig
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SchoolReagentConfig()
        {
            this.SchoolReagentValues = new HashSet<SchoolReagentValue>();
            this.SchReagents = new HashSet<SchReagent>();
        }
    
        public int ID { get; set; }
        public Nullable<int> ReagentFacilityID { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public string Specification { get; set; }
        public Nullable<double> QuantityRequired { get; set; }
        public Nullable<int> Unit { get; set; }
    
        public virtual Facility Facility { get; set; }
        public virtual Subject Subject { get; set; }
        public virtual UNIT UNIT1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SchoolReagentValue> SchoolReagentValues { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SchReagent> SchReagents { get; set; }
    }
}
