﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Models
{
    public class vmFacilityTypeUserValues
    {
        public int ID { get; set; }
        public string ItemDescription { get; set; }
        public string QuantityRequired { get; set; }
        public string UsersValue { get; set; }
       
        private SchoolFacilityValue _fusers = new SchoolFacilityValue();
        public SchoolFacilityValue fusers { get { return _fusers; } set { _fusers = value; } }



        public List<Facility> ftss = new List<Facility>();
        public List<Facility> ftssubject = new List<Facility>();
        private Facility _fftype = new Facility();

        public Facility fftype { get { return _fftype; } set { _fftype = value; } }
    }
}