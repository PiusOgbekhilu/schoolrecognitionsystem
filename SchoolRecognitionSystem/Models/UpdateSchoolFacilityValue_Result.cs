//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class UpdateSchoolFacilityValue_Result
    {
        public Nullable<double> EquipmentValue { get; set; }
        public int Equipmentvalue_ID { get; set; }
        public Nullable<int> FacilityGradeID { get; set; }
        public string Definition { get; set; }
        public string Specification { get; set; }
        public Nullable<double> QuantityRequired { get; set; }
        public Nullable<int> Unit { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<int> DigitValue { get; set; }
    }
}
