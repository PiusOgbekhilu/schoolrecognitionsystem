//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class ProcvwSchoolReportFooterMessage_Result
    {
        public string SchoolSubjectMessage { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public int ID { get; set; }
    }
}
