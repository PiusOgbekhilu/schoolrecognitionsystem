//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassroomConfig
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ClassroomConfig()
        {
            this.ClassroomUserValues = new HashSet<ClassroomUserValue>();
        }
    
        public int ID { get; set; }
        public Nullable<int> ClassroomFacilityID { get; set; }
    
        public virtual Facility Facility { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ClassroomUserValue> ClassroomUserValues { get; set; }
    }
}
