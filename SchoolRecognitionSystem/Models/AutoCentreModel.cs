﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Models
{
    public class ExamTypeModel
    {
        public int ID { get; set; }
        public string ExamType { get; set; }
    }
    public class CentreTableItemSectionModel
    {
         public int ID { get; set; }
        public string ItemSection { get; set; }
        public long SchoolID { get; set; }
        public int FinalRating { get; set; }
      //  public List<ItemSectionAssessment> ItemSectionAssessment { get; set; }
         public int SchoolId { get; set; }
        public int SchoolName { get; set; }

    public string centrename { get; set; }
    public string ExamType { get; set; }
    public int ExamTypeID { get; set; }
    public string CenterEmailAddress { get; set; }
    public bool IsMailSent { get; set; }
    public bool IsCentreConfirmed { get; set; }
    public string WhoConfirmed { get; set; }
    public int SchoolType { get; set; }
    public string centreno { get; set; }
    public int StateId { get; set; }
    public string StateCode { get; set; }
    public string Approve { get; set; }
    public int LgaID { get; set; }
    public int CategoryID { get; set; }//centreno
}
    public class CentreTableModel
    {
        public int ID { get; set; }
       public List<ItemSectionAssessment> ItemSectionAssessment { get; set; }
        public int SchoolId { get; set; }
        public int SchoolName { get; set; }

        public string centrename { get; set; }
        public string ExamType { get; set; }
        public int ExamTypeID { get; set; }
        public string CenterEmailAddress { get; set; }
        public bool IsMailSent { get; set; }
        public bool IsCentreConfirmed { get; set; }
        public string WhoConfirmed { get; set; }
        public int SchoolType { get; set; }
        public string centreno { get; set; }
        public int StateId { get; set; }
        public string StateCode { get; set; }
        public string Approve { get; set; }
        public int LgaID { get; set; }
        public int CategoryID { get; set; }//centreno
    }
}