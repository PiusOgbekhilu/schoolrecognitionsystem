//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class GetSchoolByCentre_Result
    {
        public Nullable<long> ID { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> isRecognised { get; set; }
        public string CentreNo { get; set; }
        public string SchoolName { get; set; }
        public string CentreName { get; set; }
        public string StateName { get; set; }
        public string Name { get; set; }
        public string SchoolEmailAddress { get; set; }
        public string SchoolLocation { get; set; }
        public string SchoolWebsite { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string SchoolPhoneNumber { get; set; }
        public string Category { get; set; }
        public string WaecStaff { get; set; }
    }
}
