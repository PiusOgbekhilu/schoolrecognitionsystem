//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class GetUsersForRoles_Result
    {
        public string EmailAddress { get; set; }
        public bool isActive { get; set; }
        public int ID { get; set; }
        public string Rolename { get; set; }
        public string StateName { get; set; }
        public Nullable<int> Expr1 { get; set; }
        public int StateID { get; set; }
    }
}
