﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Models
{
    public class StudentModel
    {
        public List<Student> StudentList { get; set; }
    }
    public class Student
    {
        public string Name { get; set; }
        public string ClassOfStudent { get; set; }
        public string Section { get; set; }
        public string Address { get; set; }
    }
}