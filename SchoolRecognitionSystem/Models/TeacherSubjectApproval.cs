//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TeacherSubjectApproval
    {
        public int ID { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public string TotalSubject { get; set; }
        public string TeacherAvailable { get; set; }
        public string TextBookAvailable { get; set; }
        public Nullable<bool> IsApproved { get; set; }
    
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
