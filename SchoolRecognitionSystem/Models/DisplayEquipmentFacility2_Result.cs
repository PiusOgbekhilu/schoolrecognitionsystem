//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class DisplayEquipmentFacility2_Result
    {
        public int Unique_row { get; set; }
        public string Specification { get; set; }
        public Nullable<double> QuantityRequired { get; set; }
        public Nullable<int> Unit { get; set; }
        public string ItemDescription { get; set; }
        public string TypeofRecognition { get; set; }
        public string TypeOfFacility { get; set; }
        public string LongName { get; set; }
        public int SubjectID { get; set; }
    }
}
