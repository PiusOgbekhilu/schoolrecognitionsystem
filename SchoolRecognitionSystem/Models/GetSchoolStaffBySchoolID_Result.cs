//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class GetSchoolStaffBySchoolID_Result
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SchoolStaffName { get; set; }
        public Nullable<System.DateTime> DateEmployed { get; set; }
        public bool isTRCN { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public string Label { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string Category { get; set; }
    }
}
