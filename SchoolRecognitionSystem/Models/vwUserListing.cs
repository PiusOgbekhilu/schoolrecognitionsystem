//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwUserListing
    {
        public int ID { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string OfficeName { get; set; }
        public Nullable<int> RoleID { get; set; }
        public string Rolename { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<bool> isSuperAdmin { get; set; }
        public bool isActive { get; set; }
        public string StateID { get; set; }
        public Nullable<int> LGAID { get; set; }
        public Nullable<int> RankID { get; set; }
        public string LPNO { get; set; }
        public Nullable<int> OfficeID { get; set; }
        public string StateName { get; set; }
        public byte[] Password { get; set; }
        public string LGAName { get; set; }
        public string StateCode { get; set; }
    }
}
