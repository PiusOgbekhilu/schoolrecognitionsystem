﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolRecognitionSystem.Models
{
    public class vmSchoolSubject
    {
        public List<Subject> AvailableSubjects { get; set; }
        // public List<AssignedSchoolSubject> assAvailableSubjects { get; set; }

        public int SubjectID { get; set; }

        public int[] RequestedSelected { get; set; }

        public List<Subject> RequestedSubjects { get; set; }

        public int[] AvailableSelected { get; set; }

    }
}