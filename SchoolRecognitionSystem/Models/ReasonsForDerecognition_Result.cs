//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    
    public partial class ReasonsForDerecognition_Result
    {
        public int ReasonId { get; set; }
        public string ReasonsForDerecognition { get; set; }
    }
}
