//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassAssessment
    {
        public int ID { get; set; }
        public Nullable<long> SchoolID { get; set; }
        public Nullable<int> SS3 { get; set; }
        public Nullable<int> SS2 { get; set; }
        public Nullable<int> SS1 { get; set; }
        public Nullable<int> isApproved { get; set; }
    
        public virtual SchoolProfile SchoolProfile { get; set; }
    }
}
