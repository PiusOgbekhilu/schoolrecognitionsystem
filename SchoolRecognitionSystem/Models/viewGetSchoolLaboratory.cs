//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SchoolRecognitionSystem.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class viewGetSchoolLaboratory
    {
        public string ItemDescription { get; set; }
        public int ID { get; set; }
        public string LongName { get; set; }
        public int SubjectID { get; set; }
        public string LabValue { get; set; }
        public long SchoolID { get; set; }
    }
}
