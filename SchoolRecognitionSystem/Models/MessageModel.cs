﻿namespace SchoolRecognitionSystem.Models
{
    public class MessageModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}