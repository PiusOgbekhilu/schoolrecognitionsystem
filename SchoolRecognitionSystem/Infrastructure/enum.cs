﻿namespace SchoolRecognitionSystem.Infrastructure
{
    public enum AlertType
    {
        Default, Success, Warning, Danger
    }
}