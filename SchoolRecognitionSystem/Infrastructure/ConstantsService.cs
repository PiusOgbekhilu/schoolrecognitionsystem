﻿using System;
using System.Configuration;
using System.Web;

namespace SchoolRecognitionSystem.Infrastructure
{
    public class ConstantsService
    {
        public static int FacilityCategoryEquipment = 1;
        public static int FacilityCategoryReagent = 2;
        public static int FacilityCategoryLaboratory = 3;
        public const string SignaturePath = "/Content/images/signatures/";
        public const string CredentialPath = "/images/";

        /// <summary>
        /// Roles
        /// </summary>
        public const string RoleUser = "User";
        public const string RoleAdmin = "Admin";
        public const string RoleSuperiosor = "Supervisor";
        public const string RoleSuperAdmin = "SuperAdmin";

        //School profile status
        public const string SchoolProfilePending = "Pending";
        public const string SchoolProfileVetted = "Vetted";
        public const string SchoolProfileRejected = "Rejected";
        public const string CookieSchoolRecognitionTokenKey = "_SchoolRecognitionToken";

        public static string GetApplicationRoot()
        {
            return ConfigurationManager.AppSettings["ApplicationRoot"];   
        }

        
        public const string CookieSchoolIdKey = "SchoolID";
        public const string CookieClassIdKey = "ClassID";
        public const string CookieExaminationIdKey = "ExaminationID";
        public const string CookieLibraryIdKey = "LibraryID";
        public const string CookieRecreationalIdKey = "Rejected";
        public const string CookieFacilityManagementIdKey = "FacilityManagementID";
        public const string CookieManageStaffIdKey = "ManageStaffID";
        public const string CookieSubjectSchoolId  = "SubjectSchoolId";
        public static void CreateCookie(string key, string value, DateTime? expiration = null)
        {
            var httpCookie = new HttpCookie(key, value);
            if (expiration != null) httpCookie.Expires = expiration.Value;
            HttpContext.Current.Response.Cookies.Add(httpCookie);
        }
        public static HttpCookie GetCookie(string key)
        {
            return HttpContext.Current.Request.Cookies.Get(key);
            
        }
        public static void RemoveCookie(string key)
        {
            if (HttpContext.Current.Request.Cookies[key] == null) return;
            var httpCookie = new HttpCookie(key);
            httpCookie.Expires = DateTime.Now.AddDays(-5);
            HttpContext.Current.Response.Cookies.Add(httpCookie);
        }
    }
}