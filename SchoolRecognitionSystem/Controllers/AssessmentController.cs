﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
//using SRSTesting.Models;

using System.Data;

using System.Net;
using System.Runtime.CompilerServices;
using SchoolRecognitionSystem.Models;
using System.Data.Entity;
using System.IO;
using System.Net.Mail;
using System.Web.Script.Serialization;
using CrystalDecisions.CrystalReports.Engine;
using Newtonsoft.Json;
using Remotion.FunctionalProgramming;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Pdf;
using SchoolRecognitionSystem.Report;
using RemoveNullableClass = SchoolRecognitionSystem.Classes.RemoveNullableClass;
using System.Threading.Tasks;
using System.Text;

namespace SchoolRecognitionSystem.Controllers
{
    [SchoolRecognitionAuthorized]
    public class AssessmentController : BaseController
    {

        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();
        // GET: Assessment

        //private int ScoreProcessor(string textfile, string constring, string procedureName)
        //{
        //    string outputmessage = "";
        //    const string directory = @"C:\WAEC\";
        //    StreamReader dFile = new StreamReader(textfile);
        //    //string[] eFile = System.IO.File.ReadAllLines(textfile);
        //    int rowCount = 0;
        //    int affectedRows;
        //    dFile.Close();
        //    try
        //    {
        //        if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);
        //        System.IO.File.Delete("C:\\WAEC\\CentreFile.txt");
        //        long rowTotal = 0;
        //        String totalRecordCount;
        //        using (StreamReader r = new StreamReader(textfile))
        //        {
        //            string record;
        //            while ((record = r.ReadLine()) != null)
        //            {
        //                rowTotal++;
        //            }
        //        }
        //        using (StreamWriter sw = new StreamWriter("C:\\WAEC\\Scoresfile.txt", true))
        //        {

        //            //long rowTotal = eFile.LongLength;


        //            {
        //                var staffId = GetStationId();
        //                using (StreamReader r = new StreamReader(textfile))
        //                {
        //                    string recordAll;
        //                    while ((recordAll = r.ReadLine()) != null)
        //                    {
        //                        rowCount += 1;
        //                        if (recordAll.StartsWith("10") && recordAll.Length >= 20)
        //                        {

        //                            string RecType = recordAll.Substring(0, 2);
        //                            string PaperCode = recordAll.Substring(2, 6);
        //                            string CandidateNumber = recordAll.Substring(8, 10);
        //                            string Score = recordAll.Substring(18, 3);
        //                            //string Filler = Str.Substring(21);
        //                            // string StationID = stationID

        //                            string concat = staffId + "%" + RecType.Trim() + "%" + PaperCode.Trim() + "%" + CandidateNumber.Trim() + "%" + Score.Trim() + "%" + '~';

        //                            if (rowCount <= rowTotal)
        //                            {

        //                                sw.WriteLine(concat);

        //                            }

        //                        }
        //                    }
        //                }

        //                //  outputmessage = rowCount.ToString();


        //            }
        //            //dFile.Close();
        //            sw.Close();

        //            CallEntriesProcedure(constring, procedureName);
        //            System.IO.File.Delete("C:\\WAEC\\Scoresfile.txt");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        outputmessage = ex.Message;
        //    }
        //    return rowCount;
        //}



        public ActionResult SchoolList(string state = "")
        {
            var userId = CurrentUser.UserId;
            ViewBag.StateCodes = new SelectList(clsContent.GetStatesByUserId(userId), "StateCode", "StateName");
            int pageIndex = 1;
            var stateCode = state;
            int pageSize = 20;
            // pageIndex = page;
            IPagedList<SchoolProfileViewModel1> schools = null;
            //if (stateCode == "")
            //{
            //    schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode);
            //    //.OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);

            //}
            //else
            //{
            //    schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode).OrderBy
            //       (m => m.ID).ToPagedList(pageIndex, pageSize);
            //}

            var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;
            //var ass = ctx.SchoolAssessmentSummaries.Include(m => m.CloudSchool).ToList();
            //AssessmentModel mod = new AssessmentModel();
            //mod.vmassessmentList = ass;
            //var ass = ctx.Facilities.Where(m => m.FacilityCategoryID == 6).ToList();
            return View(allSchools);

        }

        private void GetStateSchoolsForAdmin(string state)
        {
            var userId = CurrentUser.UserId;
            //  var useridd = long.Parse(userId); 

            var schy = clsContent.GetStatesByUserId(userId).Select(s => new SelectListItem
            {
                Text = s.StateName,
                Value = s.StateCode.ToString(),
                Selected = (s.StateCode == state)
            });


            ViewBag.StateCodess = schy;


        }

        public ActionResult SIndex(string state = "", int page = 1)
        {
            var userId = CurrentUser.UserId;
            GetStateSchoolsForAdmin(state);
            // ViewBag.StateCodess = new SelectList(clsContent.GetStatesByUserId(userId), "StateCode", "StateName");
            int pageIndex = 1;
            var stateCode = state;
            int pageSize = 20;
            pageIndex = page;
            IPagedList<SchoolProfileViewModel1> schools = null;
            if (stateCode == "")
            {
                schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode);
                //.OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);

            }
            else
            {
                schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode);
                //  .OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);
            }

            // var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.StateCode = stateCode;
            return View(schools);
        }

        public ActionResult Index(string state = "", int page = 1)
        {
            var userId = CurrentUser.UserId;// int.Parse(CurrentUser.UserId.ToString());
            GetStateSchoolsForAdmin(state);
            // ViewBag.StateCodess = new SelectList(clsContent.GetStatesByUserId(userId), "StateCode", "StateName");
            int pageIndex = 1;
            var stateCode = state;
            int pageSize = 20;
            pageIndex = page;
            IPagedList<SchoolProfileViewModel1> schools = null;
            if (stateCode == "")
            {
                schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode);
                //.OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);

            }
            else
            {
                schools = clsContent.GetSchoolsAdmin(pageIndex, pageSize, userId, stateCode);
                //  .OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);
            }

            // var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.StateCode = stateCode;
            return View(schools);
        }

        public ActionResult IndexB(string state = "", int page = 1)
        {
            var userId = CurrentUser.UserId;
            GetStateSchoolsForAdmin(state);
            // ViewBag.StateCodess = new SelectList(clsContent.GetStatesByUserId(userId), "StateCode", "StateName");
            int pageIndex = 1;
            var stateCode = state;
            int pageSize = 20;
            pageIndex = page;
            IPagedList<SchoolProfileViewModel1> schools = null;
            if (stateCode == "")
            {
                schools = clsContent.GetSchoolsAdminB(pageIndex, pageSize, userId, stateCode);
                //.OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);

            }
            else
            {
                schools = clsContent.GetSchoolsAdminB(pageIndex, pageSize, userId, stateCode);
                //  .OrderBy (m => m.ID).ToPagedList(pageIndex, pageSize);
            }

            // var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.StateCode = stateCode;
            return View(schools);
        }


        private void GetClassFormData(long? schoolid)
        {

            var studentClassRecord = clsContent.GetStudentClassRecord(schoolid);
            var classes = new List<SelectListItem>();
            foreach (var stdC in clsContent.GetClass())
            {
                if (studentClassRecord == null)
                {
                    classes.Add(new SelectListItem
                    {
                        Text = stdC.ClassName,
                        Value = stdC.ID.ToString()
                    });
                }

            }
            ViewBag.ClassSchoolid = schoolid;
            ViewBag.Classes = classes;
            ViewBag.ClassesRegistered = studentClassRecord;
        }


        

        public ActionResult ApproveSchoolSubjectList(long sddc,int state, int? classid)
        {
            if (classid == null)
            {
                ShowMessage("Please review the school staff first before visiting approvals.", AlertType.Danger);
                return RedirectToAction("StaffSummary", new { sddc = sddc, state = state });
            }

            var allList = new List<SchoolSubjectViewModel>();
            var itemsectionSubject = clsContent.GetItemSectionSubjectBySchool(sddc);
            var staffsectionSubject = clsContent.GetStaffSectionSubjectBySchool(sddc, int.Parse(classid.ToString()));
            var getEquipmentFacilityRated1 = clsContent.GetEquipmentFacilityRated(sddc, 1, 1);
            var getEquipmentFacilityRated2 = clsContent.GetEquipmentFacilityRated(sddc, 2, 1);
            var getEquipmentFacilityRated3 = clsContent.GetEquipmentFacilityRated(sddc, 3, 1);
            var getEquipmentFacilityRated8 = clsContent.GetEquipmentFacilityRated(sddc, 8, 1);
            var getLaboratoryFacilityRated1 = clsContent.GetLaboratoryFacilityRated(sddc, 1, 3);
            var getLaboratoryFacilityRated2 = clsContent.GetLaboratoryFacilityRated(sddc, 2, 3);
            var getLaboratoryFacilityRated3 = clsContent.GetLaboratoryFacilityRated(sddc, 3, 3);
            var getLaboratoryFacilityRated8 = clsContent.GetLaboratoryFacilityRated(sddc, 8, 3);
            var getReagentFacilityRated1 = clsContent.GetReagentFacilityRated(sddc, 1, 2);
            var getReagentFacilityRated2 = clsContent.GetReagentFacilityRated(sddc, 2, 2);

            
            foreach (var staffSubject in staffsectionSubject)
            {
                var schSub =
                    itemsectionSubject.FirstOrDefault(
                        sch => sch.SchoolID == staffSubject.SchoolID && sch.SubjectID == staffSubject.SubjectID);
                
                    //Update approved false
                    if (staffSubject.FinalRating == 1)
                    {
                        // SchoolSubject sub = new SchoolSubject();
                        staffSubject.QualifiedStaff = 1;
                        staffSubject.ExaminationMalpractice = 1;
                        clsContent.UpdateSchoolSubjectsApprovedStaff(staffSubject.SubjectID, sddc, true, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice);
                    }
                    else
                    {

                        staffSubject.QualifiedStaff = 0;
                        staffSubject.ExaminationMalpractice = 1;
                        clsContent.UpdateSchoolSubjectsApprovedStaff(staffSubject.SubjectID, sddc, false, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice);

                    }
                
                

                    if (staffSubject.SubjectLongName.Equals("BIOLOGY"))
                    {

                        if (staffSubject.FinalRating == 1 &&
                        getEquipmentFacilityRated1 != null &&
                        getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                        getReagentFacilityRated1 != null &&
                        getReagentFacilityRated1.ReagentFacility == 1 &&
                        getLaboratoryFacilityRated1 != null &&
                            getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                        {
                        //if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated1.EquipmentFacility == 1 && getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                        //{
                            SchoolSubject sub = new SchoolSubject();
                            staffSubject.QualifiedStaff = 1;
                            staffSubject.ExaminationMalpractice = 1;
                            staffSubject.AdequateEquipment = 1;
                            staffSubject.AdequateLaboratory = 1;
                            staffSubject.AdequateReagent = 1;
                            clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, true, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory, staffSubject.AdequateReagent);


                        }
                        else
                        {
                            if (staffSubject.FinalRating == 1) { staffSubject.QualifiedStaff = 1; }
                            else { staffSubject.QualifiedStaff = 0; }

                            staffSubject.ExaminationMalpractice = 1;
                            if (getEquipmentFacilityRated1 != null && getEquipmentFacilityRated1.EquipmentFacility == 1)
                            { staffSubject.AdequateEquipment = 1; }
                            else
                            { staffSubject.AdequateEquipment = 0; }
                            if (getReagentFacilityRated1 != null && getReagentFacilityRated1.ReagentFacility == 1)
                            { staffSubject.AdequateReagent = 1; }
                            else
                            { staffSubject.AdequateReagent = 0; }
                            if (staffSubject.AdequateLaboratory == 1)
                            { staffSubject.AdequateLaboratory = 1; }
                            else
                            { staffSubject.AdequateLaboratory = 0; }

                            clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, false, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory, staffSubject.AdequateReagent);

                        }
                    }

                    if (staffSubject.SubjectLongName.Equals("CHEMISTRY"))
                    {
                        if (staffSubject.FinalRating == 1 &&
                        getEquipmentFacilityRated2 != null &&
                        getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                        getReagentFacilityRated2 != null &&
                        getReagentFacilityRated2.ReagentFacility == 1 &&
                        getLaboratoryFacilityRated2 != null &&
                        getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                        {
                            SchoolSubject sub = new SchoolSubject();
                            staffSubject.QualifiedStaff = 1;
                            staffSubject.ExaminationMalpractice = 1;
                            staffSubject.AdequateEquipment = 1;
                            staffSubject.AdequateLaboratory = 1;
                            staffSubject.AdequateReagent = 1;
                            clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, true, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory, staffSubject.AdequateReagent);


                        }
                        else
                        {
                            if (staffSubject.FinalRating == 1) { staffSubject.QualifiedStaff = 1; }
                            else { staffSubject.QualifiedStaff = 0; }

                            staffSubject.ExaminationMalpractice = 1;
                            if (getEquipmentFacilityRated2 != null && getEquipmentFacilityRated2.EquipmentFacility == 1)
                            { staffSubject.AdequateEquipment = 1; }
                            else
                            { staffSubject.AdequateEquipment = 0; }
                            if (getReagentFacilityRated2 != null && getReagentFacilityRated2.ReagentFacility == 1)
                            {
                                staffSubject.AdequateReagent = 1;
                            }
                            else
                            {
                                staffSubject.AdequateReagent = 0;
                            }
                            if (getLaboratoryFacilityRated2 != null && getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                            { staffSubject.AdequateLaboratory = 1; }
                            else
                            { staffSubject.AdequateLaboratory = 0; }
                            clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, false, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory, staffSubject.AdequateReagent);

                        }
                    }
                    else if (staffSubject.SubjectLongName.Equals("AGRIC SCIENCE"))
                    {
                        if (staffSubject.FinalRating == 1 &&
                        getEquipmentFacilityRated8 != null &&
                        getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                        getLaboratoryFacilityRated8 != null &&
                        getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                        {
                            staffSubject.QualifiedStaff = 1;
                            staffSubject.ExaminationMalpractice = 1;
                            staffSubject.AdequateEquipment = 1;
                            staffSubject.AdequateLaboratory = 1;
                            clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, true, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                            //  sub.AdequateReagent = 1;

                        }
                        else
                        {
                            if (staffSubject.FinalRating == 1)
                            {
                                staffSubject.QualifiedStaff = 1;
                            }
                            else
                            {
                                staffSubject.QualifiedStaff = 0;
                            }

                            staffSubject.ExaminationMalpractice = 1;
                            if (getEquipmentFacilityRated8 != null && getEquipmentFacilityRated8.EquipmentFacility == 1)
                            {
                                staffSubject.AdequateEquipment = 1;
                            }
                            else
                            {
                                staffSubject.AdequateEquipment = 0;
                            }

                            if (getLaboratoryFacilityRated8 != null && getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                            {
                                staffSubject.AdequateLaboratory = 1;
                            }
                            else
                            {
                                staffSubject.AdequateLaboratory = 0;
                            }
                            clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, false, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                        }
                    }

                    else if (staffSubject.SubjectLongName.Equals("PHYSICS"))
                    {
                        if (staffSubject.FinalRating == 1 &&
                         getEquipmentFacilityRated3 != null &&
                        getEquipmentFacilityRated3.EquipmentFacility == 1 &&
                        getLaboratoryFacilityRated3 != null && 
                        getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                        {
                            staffSubject.QualifiedStaff = 1;
                            staffSubject.ExaminationMalpractice = 1;
                            staffSubject.AdequateEquipment = 1;
                            staffSubject.AdequateLaboratory = 1;
                            clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, true, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                            //  sub.AdequateReagent = 1;

                        }
                        else
                        {
                            if (staffSubject.FinalRating == 1) { staffSubject.QualifiedStaff = 1; }
                            else { staffSubject.QualifiedStaff = 0; }

                            staffSubject.ExaminationMalpractice = 1;
                            if (getEquipmentFacilityRated3 != null &&  getEquipmentFacilityRated3.EquipmentFacility == 1)
                            { staffSubject.AdequateEquipment = 1; }
                            else
                            { staffSubject.AdequateEquipment = 0; }

                            if (getLaboratoryFacilityRated3 != null && getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                            { staffSubject.AdequateLaboratory = 1; }
                            else
                            { staffSubject.AdequateLaboratory = 0; }
                            clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, false, staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice, staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                        }
                    }


                }

            
            //fetch all the school subjects that are true
            var subjectApproved = clsContent.GetSchoolSubjectApprovedBySchoolId(sddc);


            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.ApproveSchoolSubjectList = "active";


            return View(subjectApproved);
        }
       
        public ActionResult ApproveSchoolSubjectListB(long sddc, int state, int? classid)
        {
            if (classid == null)
            {
                ShowMessage("Please review the school staff first before visiting approvals.", AlertType.Danger);
                return RedirectToAction("StaffSummaryB", new { sddc = sddc, state = state });
            }
            var allList = new List<SchoolSubjectViewModel>();
            var itemsectionSubject = clsContent.GetItemSectionSubjectBySchoolB(sddc);
            var staffsectionSubject = clsContent.GetStaffSectionSubjectBySchoolB(sddc, int.Parse(classid.ToString()));
            var getEquipmentFacilityRated1 = clsContent.GetEquipmentFacilityRated(sddc, 1, 1);
            var getEquipmentFacilityRated2 = clsContent.GetEquipmentFacilityRated(sddc, 2, 1);
            var getEquipmentFacilityRated3 = clsContent.GetEquipmentFacilityRated(sddc, 3, 1);
            var getEquipmentFacilityRated8 = clsContent.GetEquipmentFacilityRated(sddc, 8, 1);
            var getLaboratoryFacilityRated1 = clsContent.GetLaboratoryFacilityRated(sddc, 1, 3);
            var getLaboratoryFacilityRated2 = clsContent.GetLaboratoryFacilityRated(sddc, 2, 3);
            var getLaboratoryFacilityRated3 = clsContent.GetLaboratoryFacilityRated(sddc, 3, 3);
            var getLaboratoryFacilityRated8 = clsContent.GetLaboratoryFacilityRated(sddc, 8, 3);
            var getReagentFacilityRated1 = clsContent.GetReagentFacilityRated(sddc, 1, 2);
            var getReagentFacilityRated2 = clsContent.GetReagentFacilityRated(sddc, 2, 2);
            if (staffsectionSubject != null)
            {
                foreach (var staffSubject in staffsectionSubject)
                {
                    var schSub =
                        itemsectionSubject.FirstOrDefault(
                            sch => sch.SchoolID == staffSubject.SchoolID && sch.SubjectID == staffSubject.SubjectID);
                    if (schSub == null)
                    {
                        //Update approved false
                        if (staffSubject.FinalRating == 1)
                        {
                            // SchoolSubject sub = new SchoolSubject();
                            staffSubject.QualifiedStaff = 1;
                            staffSubject.ExaminationMalpractice = 1;
                            clsContent.UpdateSchoolSubjectsApprovedStaff(staffSubject.SubjectID, sddc, true,
                                staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice);


                        }
                        else
                        {

                            staffSubject.QualifiedStaff = 0;
                            staffSubject.ExaminationMalpractice = 1;
                            clsContent.UpdateSchoolSubjectsApprovedStaff(staffSubject.SubjectID, sddc, false,
                                staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice);

                        }
                    }
                    else
                    {

                        if (staffSubject.SubjectLongName.Equals("BIOLOGY"))
                        {

                            //if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated1.EquipmentFacility == 1 && getReagentFacilityRated1.ReagentFacility == 1 &&
                            //    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                            //{
                            if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                            {
                                SchoolSubject sub = new SchoolSubject();
                                staffSubject.QualifiedStaff = 1;
                                staffSubject.ExaminationMalpractice = 1;
                                staffSubject.AdequateEquipment = 1;
                                staffSubject.AdequateLaboratory = 1;
                                staffSubject.AdequateReagent = 1;
                                clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, true,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory,
                                    staffSubject.AdequateReagent);


                            }
                            else
                            {
                                if (staffSubject.FinalRating == 1)
                                {
                                    staffSubject.QualifiedStaff = 1;
                                }
                                else
                                {
                                    staffSubject.QualifiedStaff = 0;
                                }

                                staffSubject.ExaminationMalpractice = 1;
                                if (getEquipmentFacilityRated1.EquipmentFacility == 1)
                                {
                                    staffSubject.AdequateEquipment = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateEquipment = 0;
                                }
                                if (getReagentFacilityRated1.ReagentFacility == 1)
                                {
                                    staffSubject.AdequateReagent = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateReagent = 0;
                                }
                                if (staffSubject.AdequateLaboratory == 1)
                                {
                                    staffSubject.AdequateLaboratory = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateLaboratory = 0;
                                }

                                clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, false,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory,
                                    staffSubject.AdequateReagent);

                            }
                        }

                        if (staffSubject.SubjectLongName.Equals("CHEMISTRY"))
                        {
                            if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                            {
                                SchoolSubject sub = new SchoolSubject();
                                staffSubject.QualifiedStaff = 1;
                                staffSubject.ExaminationMalpractice = 1;
                                staffSubject.AdequateEquipment = 1;
                                staffSubject.AdequateLaboratory = 1;
                                staffSubject.AdequateReagent = 1;
                                clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, true,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory,
                                    staffSubject.AdequateReagent);


                            }
                            else
                            {
                                if (staffSubject.FinalRating == 1)
                                {
                                    staffSubject.QualifiedStaff = 1;
                                }
                                else
                                {
                                    staffSubject.QualifiedStaff = 0;
                                }

                                staffSubject.ExaminationMalpractice = 1;
                                if (getEquipmentFacilityRated2.EquipmentFacility == 1)
                                {
                                    staffSubject.AdequateEquipment = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateEquipment = 0;
                                }
                                if (getReagentFacilityRated2.ReagentFacility == 1)
                                {
                                    staffSubject.AdequateReagent = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateReagent = 0;
                                }
                                if (getReagentFacilityRated2.LaboratoryFacility == 1)
                                {
                                    staffSubject.AdequateLaboratory = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateLaboratory = 0;
                                }
                                clsContent.UpdateSchoolSubjectsApproved(staffSubject.SubjectID, sddc, false,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory,
                                    staffSubject.AdequateReagent);

                            }
                        }
                        else if (staffSubject.SubjectLongName.Equals("AGRIC SCIENCE"))
                        {
                            if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                                (getLaboratoryFacilityRated8.LaboratoryFacility == 1 ||
                                 getLaboratoryFacilityRated8.LaboratoryFacility == 0))
                            {
                                staffSubject.QualifiedStaff = 1;
                                staffSubject.ExaminationMalpractice = 1;
                                staffSubject.AdequateEquipment = 1;
                                staffSubject.AdequateLaboratory = 1;
                                clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, true,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                                //  sub.AdequateReagent = 1;

                            }
                            else
                            {
                                if (staffSubject.FinalRating == 1)
                                {
                                    staffSubject.QualifiedStaff = 1;
                                }
                                else
                                {
                                    staffSubject.QualifiedStaff = 0;
                                }

                                staffSubject.ExaminationMalpractice = 1;
                                if (getEquipmentFacilityRated8.EquipmentFacility == 1)
                                {
                                    staffSubject.AdequateEquipment = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateEquipment = 0;
                                }

                                if (getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                                {
                                    staffSubject.AdequateLaboratory = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateLaboratory = 0;
                                }
                                clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, false,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                            }
                        }

                        else if (staffSubject.SubjectLongName.Equals("PHYSICS"))
                        {
                            if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated3.EquipmentFacility == 1 &&
                                getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                            {
                                staffSubject.QualifiedStaff = 1;
                                staffSubject.ExaminationMalpractice = 1;
                                staffSubject.AdequateEquipment = 1;
                                staffSubject.AdequateLaboratory = 1;
                                clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, true,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                                //  sub.AdequateReagent = 1;

                            }
                            else
                            {
                                if (staffSubject.FinalRating == 1)
                                {
                                    staffSubject.QualifiedStaff = 1;
                                }
                                else
                                {
                                    staffSubject.QualifiedStaff = 0;
                                }

                                staffSubject.ExaminationMalpractice = 1;
                                if (getEquipmentFacilityRated3.EquipmentFacility == 1)
                                {
                                    staffSubject.AdequateEquipment = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateEquipment = 0;
                                }

                                if (getEquipmentFacilityRated3.LaboratoryFacility == 1)
                                {
                                    staffSubject.AdequateLaboratory = 1;
                                }
                                else
                                {
                                    staffSubject.AdequateLaboratory = 0;
                                }
                                clsContent.UpdateSchoolSubjectsApprovedPhyAgr(staffSubject.SubjectID, sddc, false,
                                    staffSubject.QualifiedStaff, staffSubject.ExaminationMalpractice,
                                    staffSubject.AdequateEquipment, staffSubject.AdequateLaboratory);

                            }
                        }


                    }
                }
            }

            else
            {
                if (getEquipmentFacilityRated1.EquipmentFacility == 1 && getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                {
                    SchoolSubject sub = new SchoolSubject();
                    clsContent.UpdateSchoolSubjectsApproved(1, sddc, true,1, 1,1, 1,1);

                }
                else
                {

                    clsContent.UpdateSchoolSubjectsApproved(1,sddc,false,1, 0, 0,1,1);

                }
                if (getEquipmentFacilityRated3.EquipmentFacility == 1 && getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                {
                    SchoolSubject sub = new SchoolSubject();
                    clsContent.UpdateSchoolSubjectsApproved(3, sddc, true, 1, 1, 1, 1, 1);

                }
                else
                {

                    clsContent.UpdateSchoolSubjectsApproved(3, sddc, false, 1, 0, 0, 1, 1);

                }
                if (getEquipmentFacilityRated3.EquipmentFacility == 1 && getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                {
                    SchoolSubject sub = new SchoolSubject();
                    clsContent.UpdateSchoolSubjectsApproved(8, sddc, true, 1, 1, 1, 1, 1);

                }
                else
                {

                    clsContent.UpdateSchoolSubjectsApproved(8, sddc, false, 1, 0, 0, 1, 1);

                }
                if (getEquipmentFacilityRated2.EquipmentFacility == 1 && getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                {
                    SchoolSubject sub = new SchoolSubject();
                    clsContent.UpdateSchoolSubjectsApproved(2, sddc, true, 1, 1, 1, 1, 1);

                }
                else
                {

                    clsContent.UpdateSchoolSubjectsApproved(2, sddc, false, 1, 0, 0, 1, 1);

                }
                //if (staffSubject.FinalRating == 1 && getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                //               getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                //{
            }
        
                
            //fetch all the school subjects that are true
            var subjectApproved = clsContent.GetSchoolSubjectApprovedBySchoolIdB(sddc);


            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.ApproveSchoolSubjectList = "active";



            return View(subjectApproved);
        }






        public ActionResult TextBook()
        {
            return View();
        }


        public ActionResult SubjectList(long sdc)
        {
            //public ActionResult Classroom(long sch, bool isAppoved, ClassAllocationvModel[] items)

            //var schoolids = Session.GetDataFromSession<string>("SchoolID");
            //var schoolid = long.Parse(schoolids);

            var schoolSubjectView = new List<SchoolSubjectList>();
            var school = clsContent.GetSchoolById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.Schoolid = sdc;

            var schoolsubjectList = clsContent.GetSchoolSubjects(sdc, true);

            var tradeSubjectsFromDb = clsContent.GetSchoolTradeSubjects();
            var tradeSubjectsSelected = new List<SchoolSubjectViewModel>();

            var compulsorySchoolSubjectsFromDb = clsContent.GetCompulsorySchoolSubjects();
            var compulsorySubjectsSelected = new List<SchoolSubjectViewModel>();

            var otherSubjects = new List<SchoolSubjectViewModel>();
            foreach (var sub in schoolsubjectList)
            {
                if (compulsorySchoolSubjectsFromDb.Any(comp => comp.SubjectID == sub.SubjectID))
                {
                    compulsorySubjectsSelected.Add(sub);
                }
                else if (tradeSubjectsFromDb.Any(comp => comp.SubjectID == sub.SubjectID))
                {
                    tradeSubjectsSelected.Add(sub);
                }
                else
                {
                    otherSubjects.Add(sub);
                }

            }
            schoolSubjectView.Add(new SchoolSubjectList
            {

                Subject = otherSubjects,
                SchoolCompulsorySubject = compulsorySubjectsSelected,
                SchoolLeastTradeSubject = tradeSubjectsSelected
            });

            ViewBag.Subject = "active";

            //   ViewBag.Classroom = "active";

            // clsContent.UpdateClassForApproval(isAppoved);
            return View(schoolSubjectView);
        }

       [HttpGet]
        public ActionResult Classroom(long sddc)
        {   var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Classroom = "active";
           ViewBag.Years = new DateTime((DateTime.Now - DateTime.Parse("01/01/" + school.YearEstablished)).Ticks).Year;
            var schoolClassRecord = clsContent.GetSchoolClassBySchoolId(sddc);
            if (schoolClassRecord == null)
            {
                ShowMessage("Class was not found for this school.", AlertType.Danger);
                return View();
            }
            return View(schoolClassRecord);
        }

        private IEnumerable<SelectListItem> GetSchoolSubjectItems(long schoolid, int subjectid)
        {
            // var schoo = Session.GetDataFromSession<string>("SchoolID");
            var schy = clsContent.GetSchoolSubjectItems(schoolid).Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            return schy;

        }


        public ActionResult UpdateFacilitySummary(FacilityApprovalModel[] radiobuttonvalues)
        {
            var data = clsContent.UpdateSchoolEquipConfigForAssessment(radiobuttonvalues);

            return Json(new
            {
                status = true
            },
               JsonRequestBehavior.AllowGet);

        }

        public ActionResult FetchLaboratorySummary(long sdc)
        {
            var school = clsContent.GetSchoolById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Facility = "active";

            var summarylist = clsContent.GetLaboratorySummary(sdc);
            var facilityApps = new List<LaboratoryApprovalModel>();
            var hasSubjectPassedBiology = false;
            var hasSubjectPassedChemistry = false;
            var hasSubjectPassedAgric = false;
            var hasSubjectPassedPhysics = false;
            foreach (var summary in summarylist)
            {
                if (summary.LongName.Equals("BIOLOGY"))
                {
                    if (!hasSubjectPassedBiology)
                    {
                        if (summary.LaboratoryFacility == 1)
                        {
                            hasSubjectPassedBiology = true;
                        }
                    }
                    else
                    {
                        summary.LaboratoryFacility = 1;
                        hasSubjectPassedBiology = false;
                    }

                }
                if (summary.LongName.Equals("CHEMISTRY"))
                {
                    if (!hasSubjectPassedChemistry)
                    {
                        if (summary.LaboratoryFacility == 1)
                        {
                            hasSubjectPassedChemistry = true;
                        }
                    }
                    else
                    {
                        summary.LaboratoryFacility = 1;
                        hasSubjectPassedChemistry = false;
                    }
                    if (summary.LongName.Equals("AGRIC SCIENCE"))
                    {
                        if (!hasSubjectPassedAgric)
                        {
                            if (summary.LaboratoryFacility == 1)
                            {
                                hasSubjectPassedAgric = true;
                            }
                        }
                        else
                        {
                            summary.LaboratoryFacility = 1;
                            hasSubjectPassedAgric = false;
                        }

                    }
                    if (summary.LongName.Equals("PHYSICS"))
                    {
                        if (!hasSubjectPassedPhysics)
                        {
                            if (summary.LaboratoryFacility == 1)
                            {
                                hasSubjectPassedPhysics = true;
                            }
                        }
                        else
                        {
                            summary.LaboratoryFacility = 1;
                            hasSubjectPassedPhysics = false;
                        }

                    }

                }
                facilityApps.Add(summary);
            }
            return View(facilityApps);
            // return View(summarylist);
        }



        [HttpGet]
        public ActionResult ExaminationFacilityAssess(long sdc)
        {
            var school = clsContent.GetSchoolById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var examhallModel = clsContent.GetExaminationFacilityAssess(sdc);

            //var reacreationalModel = clsContent.GetSchoolReacreationalForAssess(sdc);

            ViewBag.Facility = "active";

            return View(examhallModel);
        }

        //ExaminationFacility

        ////[HttpGet]
        ////public ActionResult SchoolReacreationalForAssess(long sdc)
        ////{
        ////    var school = clsContent.GetSchoolById(sdc);
        ////    TempData["DisplaySchoolName"] = school.SchoolName;
        ////    TempData["Message"] = CurrentUser.EmailAddress;

        ////    var reacreationalModel = clsContent.GetSchoolReacreationalForAssess(sdc);

        ////    ViewBag.Facility = "active";

        ////    return View(reacreationalModel);

        ////}
        //GetSchoolReacreationalForAssess
        [HttpGet]
        public ActionResult LibraryFacilityAssess(long sdc)
        {
            var school = clsContent.GetSchoolById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            var libraryModel = clsContent.GetSchoolLibraryForAssess(sdc);

            ViewBag.Facility = "active";

            return View(libraryModel);

        }


        //FacilitySummaryB
        //LibraryFacilityAssess/GetSchoolLibraryForAssess
        [HttpGet]
        public ActionResult FacilitySummaryB(long sddc = 0)
        {
            //  var sch = long.Parse(schoolid);
            Session.SetDataInSession("AdminSchoolID",
                                              string.Format("{0}", sddc));
            var school = clsContent.GetSchoolById(sddc);
          //  var schoolSubjectNotApproved = clsContent.GetSchoolSubjectNotApproved(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Facility = "active";

            //get facility summary for subject not approved GetFacilitySummarySubjectNotApproved

          //  var summarylist = clsContent.GetFacilitySummary(sdc);

            var summarylistB = clsContent.GetFacilitySummarySubjectNotApproved(sddc);


            return View(summarylistB);
            //return Json(Url.Action("FacilitySummary", "Assessment"),JsonRequestBehavior.AllowGet);
            // return RedirectToAction("FacilitySummary", "Assessment", sdc);
        }

        //LibraryFacilityAssess/GetSchoolLibraryForAssess
        [HttpGet]
        public ActionResult FacilitySummary(long sddc)
        {
           
           ViewBag.SubjectsWithEquipment = clsContent.GetSchoolSubjectEquipment(sddc);
           ViewBag.SubjectsWithLaboratory = clsContent.GetSchoolSubjectLaboratory(sddc);
           ViewBag.SubjectsWithReagent = clsContent.GetSchoolSubjectReagent(sddc);

            var school = clsContent.GetSchoolById(sddc);




            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Facility = "active";

            var summarylist = clsContent.GetFacilitySummary(sddc);
            //var facilityApps = new List<FacilityApprovalModel>();
            //var hasSubjectPassedBiology = false;
            //var hasSubjectPassedChemistry = false;
            //var hasSubjectPassedPhysics = false;
            //foreach (var summary in summarylist)
            //{
            //    if (summary.LongName.Equals("BIOLOGY"))
            //    {
            //        if (!hasSubjectPassedBiology)
            //        {
            //            if (summary.EquipmentFacility == 1 && summary.LaboratoryFacility == 1
            //                && summary.ReagentFacility == 1)
            //            {
            //                hasSubjectPassedBiology = true;
            //            }
            //        }
            //        else
            //        {
            //            summary.EquipmentFacility = 1;
            //            summary.LaboratoryFacility = 1;
            //            summary.ReagentFacility = 1;
            //            hasSubjectPassedBiology = false;
            //        }

            //    }
            //    if (summary.LongName.Equals("CHEMISTRY"))
            //    {
            //        if (!hasSubjectPassedChemistry)
            //        {
            //            if (summary.EquipmentFacility == 1 && summary.LaboratoryFacility == 1
            //                && summary.ReagentFacility == 1)
            //            {
            //                hasSubjectPassedChemistry = true;
            //            }
            //        }
            //        else
            //        {
            //            summary.EquipmentFacility = 1;
            //            summary.LaboratoryFacility = 1;
            //            summary.ReagentFacility = 1;
            //            hasSubjectPassedChemistry = false;
            //        }

            //    }
            //    if (summary.LongName.Equals("PHYSICS"))
            //    {
            //        if (!hasSubjectPassedPhysics)
            //        {
            //            if (summary.EquipmentFacility == 1 && summary.LaboratoryFacility == 1)
            //            {
            //                hasSubjectPassedPhysics = true;
            //            }
            //        }
            //        else
            //        {
            //            summary.EquipmentFacility = 1;
            //            summary.LaboratoryFacility = 1;
            //            hasSubjectPassedPhysics = false;
            //        }

            //    }

            //    facilityApps.Add(summary);
            //}
            return View(summarylist);
            //return Json(Url.Action("FacilitySummary", "Assessment"),JsonRequestBehavior.AllowGet);
            // return RedirectToAction("FacilitySummary", "Assessment", sdc);
        }

        //public ActionResult _ItemSectionPartial(long sdc)
        //{
        //    var fetchFinal = clsContent.GetItemSectionAssessment(sdc);
        //    return PartialView("_ItemSectionPartial", fetchFinal);
        //}


        public ActionResult PrintReport2(long sdc)
        {
            var schoolDetails = clsContent.GetSchoolDetails();

            var coreSubjects = clsContent.GetCoreSubjects();
            var tradeSubjects = clsContent.GetOnlyTradeSubjects();
            var others = clsContent.GetNotCoreTradeSubject();

            var sortedSchools = schoolDetails.Where(sc => sc.SchoolID == sdc).ToList();

            var sortedCoreSubjects = coreSubjects.Where(xc => xc.SchoolID == sdc && xc.IsCore == true).ToList();
            var sortedTradeSubjects = tradeSubjects.Where(xc => xc.SchoolID == sdc && xc.IsTrade == true).ToList();
            var otherSubjects = others.Where(xc => xc.SchoolID == sdc && xc.IsCore == false && xc.IsTrade == false).ToList();



            var reportPath = Path.Combine(Server.MapPath("~/Report"), "SchoolLetter.rpt");
            DataTable tblSchool = RemoveNullableClass.ToDataTable(sortedSchools);
            DataTable tblCoreSubject = RemoveNullableClass.ToDataTable(sortedCoreSubjects);
            DataTable tblTradeSubject = RemoveNullableClass.ToDataTable(sortedTradeSubjects);
            DataTable tblOtherSubject = RemoveNullableClass.ToDataTable(otherSubjects);

            return new CrystalReportPdfResult(reportPath, tblSchool, tblCoreSubject, tblTradeSubject, tblOtherSubject);
        }

        public ActionResult PrintReport(long sdc)
        {
            var subjectData = clsContent.GetCoreSubjectsViewBySchoolId(sdc);

            // var vwReportModel = new List<vwReportModel>();
            //Get all subjects
            var objSubjects = clsContent.GetSubjectBySchool(sdc);

            //Get core subjects
            var objSubjectListing = objSubjects.Where(c => c.IsCore == true).OrderBy(c => c.SubjectID).ToList();

            var footMessage = clsContent.GetFooterReportMessage();

            var sortedFootMessage = footMessage.Where(xc => xc.SchoolID == sdc).ToList();

            var tradeCoreSubject = objSubjects.Where(c => c.IsTrade == true).OrderBy(c => c.SubjectID).ToList();
            if (tradeCoreSubject.Count > 0)
                objSubjectListing.Add(new vwSchoolSubject()
                {
                    Category = "CORE SUBJECT(S)",
                    IsCore = true,
                    IsTrade = true,
                    SchoolID = tradeCoreSubject[0].SchoolID,
                    SubjectID = tradeCoreSubject[0].SubjectID,
                    LongName = tradeCoreSubject[0].LongName,
                    SubjectCode = tradeCoreSubject[0].SubjectCode
                });

            //get other subjects
            var objOtherSubject =
                objSubjects.Where(c => c.IsCore == false && !objSubjectListing.Exists(d => d.SubjectID == c.SubjectID))
                    .OrderBy(c => c.SubjectID)
                    .ToList();

            foreach (var subj in objOtherSubject)
            {
                objSubjectListing.Add(subj);
            }
            //sortedFootMessage
            var otherReportDetail = clsContent.GetSchoolReportDetails(sdc);
            var reportPath = Path.Combine(Server.MapPath("~/Report"), "SchoolLetter.rpt");
            DataTable tblSchool = RemoveNullableClass.ToDataTable(otherReportDetail);
            DataTable tblSchoolSubjects = RemoveNullableClass.ToDataTable(objSubjectListing);
            DataTable tblsortedFootMessage = RemoveNullableClass.ToDataTable(sortedFootMessage);
            return new CrystalReportPdfResult(reportPath, tblSchool, tblSchoolSubjects, tblsortedFootMessage);
        }

        public ActionResult PrintReportX(long sdc)
        {
            var subjectForRecognition = clsContent.GetSubjectForRecognition(sdc);
            var otherReportDetail = clsContent.GetSchoolReportDetails(sdc);
            var reportPath = Path.Combine(Server.MapPath("~/Report"), "SchoolLetterB.rpt");
            DataTable mainPageData = RemoveNullableClass.ToDataTable(otherReportDetail);
            DataTable subPageData = RemoveNullableClass.ToDataTable(subjectForRecognition);
            return new CrystalReportPdfResult(reportPath, mainPageData, subPageData);
        }


        public ActionResult Approve2(long sddc)
        {
            //var centreNumbers = clsContent.GetMaximumCentreNumber(CentreTableAddNewWithLGAWithAudit2);
            //var maxCentre = centreNumbers[centreNumbers.count - 1];


            var getSchool = clsContent.GetSchoolEditByID(sddc);
            var centrename = getSchool.SchoolName;
            var statecode = getSchool.StateID;
            var lga = getSchool.LocalCode;
            //var localCode = clsContent.GetLocalCode(lga);

            int examtype = 4;
            var categoryid = getSchool.CategoryID;
            var maxCentre = clsContent.GetMaxCentreNumber();
            //var centres = clsContent.GetCentreNumbers();  //GetMaxCentreNumber
            //var maxCentre = centres[centres.Count - 1].CentreNo;
            var generatedNumber = clsContent.GenerateCentreForSchool(centrename, statecode, lga, examtype, maxCentre, sddc);
            TempData["Message"] = CurrentUser.EmailAddress;
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.Approve = "active";
           
            if (generatedNumber.CentreNo != maxCentre)
            {
                getSchool.CenterNo = generatedNumber.CentreNo;
                getSchool.isRecognised = true;
                var returnedx=clsContent.UpdateSchoolProfileCentreNo(getSchool);
                //update school profile with centre number
                return Json(new { status = true, newcentreNumber = generatedNumber.CentreNo, schoolname = generatedNumber.CentreNo }, JsonRequestBehavior.AllowGet);

                //go set is recognised to be true
                //var isRecognisedStatus = clsContent.CentreTableUpdateForIsRecognised(sdc,true);
                //var isBarredStatus = clsContent.CentreTableUpdateForIsBarred(sdc, false);
                //if (isRecognisedStatus.IsRecognisedID ==true && isBarredStatus.IsBarred==false)
                //{
                //    return Json(new { status = true, newcentreNumber = generatedNumber.CentreNo, schoolname = generatedNumber.CentreNo }, JsonRequestBehavior.AllowGet);

                //}

            }
            //set isRecognised to be false
            return Json(new { status = false, message = "please contact the administartor" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DisplaySubjectNotApproved(long schoolid, int classId)
        {
            var fetchFinal = clsContent.GetSchoolSubjectsApproved(schoolid);
            var reasonEquip = clsContent.FetchReasonsFromItemSectionEquipment(schoolid);
            var reasonReagent = clsContent.FetchReasonsFromItemSectionReagent(schoolid);
            var reasonLabora = clsContent.FetchReasonsFromItemSectionLaboratory(schoolid);
            var reasonStaffSubject = clsContent.FetchReasonsFromStaff2SubjectClass(schoolid, classId);
            //var allFacility = clsContent.FetchReasonsFromItemSectionFacility(schoolid);
            var storageRELA = new List<SchoolSubjectViewModel>();
            var storageStaff = new List<SchoolSubjectViewModel>();
            var storageAll = new List<SchoolSubjectList>();


            //foreach (var vxx in allFacility)
            //{
            //   // storageRELA.Add(vxx);
            //}
            foreach (var vxx in reasonStaffSubject)
            {
                storageStaff.Add(vxx);
            }
            storageAll.Add(new SchoolSubjectList
            {
                ReaLabo = storageRELA,
                StaffSubjectO = storageStaff
            });


            ViewBag.StorageAll = storageAll;
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Approve = "active";
            return View(fetchFinal);
        }
        public ActionResult ApproveB(long sdc)
        {
            var fetchFinal = clsContent.GetSchoolSubjectsApprovedB(sdc);
            var reasonEquip = clsContent.FetchReasonsFromItemSectionEquipmentB(sdc);
            var reasonReagent = clsContent.FetchReasonsFromItemSectionReagentB(sdc);
            var reasonLabora = clsContent.FetchReasonsFromItemSectionLaboratoryB(sdc);
            var reasonStaffSubject = clsContent.FetchReasonsFromStaff2SubjectClassB(sdc);

            var storageRELA = new List<SchoolSubjectViewModel>();
            var storageStaff = new List<SchoolSubjectViewModel>();
            var storageAll = new List<SchoolSubjectList>();
            //if i get reasonequip for 


            var facility1 = clsContent.FetchReasonsFromItemSectionFacilityB(sdc, 1, false);
            var facility2 = clsContent.FetchReasonsFromItemSectionFacilityB(sdc, 2, false);
            var facility3 = clsContent.FetchReasonsFromItemSectionFacilityB(sdc, 3, false);
            var facility8 = clsContent.FetchReasonsFromItemSectionFacilityB(sdc, 8, false);

            var bioF =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioE =
                facility1
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioR =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var bioL =
               facility1
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);



            var chemF =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemE =
                facility2
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemR =
                facility2
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var chemL =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);

            var phyF = facility3
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var phyE = facility3
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var phyL = facility3
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);



            var agricF = facility8
                   .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == null);

            var agricE = facility8
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var agricL = facility8
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);

            string bioMessage1 = "";
            string bioMessage2 = "";
            string bioMessage3 = "";
            string chemMessage1 = "";
            string chemMessage2 = "";
            string chemMessage3 = "";
            string phyMessage1 = "";
            string phyMessage3 = "";
            string agricMessage1 = "";
            string agricMessage3 = "";
            string staffmessageSubject = "";
            var allStaffMessageSubject = new List<string>();
            var allStaffMessageSubjectItem = new List<string>();
            var messageStaffList = new List<string>();
            var messageStaffSubjectList = new List<int>();


            if (bioF == true && bioE == false)
            {
                bioMessage1 = "BIOLOGY has Inadequate Equipment";
            }
            if (bioF == true && bioR == false)
            {
                bioMessage2 = "BIOLOGY has Inadequate Reagent";
            }
            if (bioF == true && bioL == false)
            {
                bioMessage3 = "BIOLOGY has Inadequate Laboratory";
            }

            if (chemF == true && chemE == false)
            {
                chemMessage1 = "CHEMISTRY has Inadequate Equipment";
            }
            if (chemF == true && chemR == false)
            {
                chemMessage2 = "CHEMISTRY has Inadequate Reagent";
            }
            if (chemF == true && chemL == false)
            {
                chemMessage3 = "CHEMISTRY has Inadequate Laboratory";
            }

            if (phyF == true && phyE == false)
            {
                phyMessage1 = "PHYSICS has Inadequate Equipment";
            }
            if (phyF == true && phyL == false)
            {
                phyMessage3 = "PHYSICS has Inadequate Laboratory";
            }
            if (agricF == true && agricE == false)
            {
                agricMessage1 = "AGRIC SCIENCE has Inadequate Equipment";
            }
            if (agricF == true && agricL == false)
            {
                agricMessage3 = "AGRIC SCIENCE has Inadequate Laboratory";
            }

            object[] array = new object[50];
            var myList = new List<KeyValuePair<string, int>>();

            foreach (var vxx in reasonStaffSubject)
            {
                staffmessageSubject = vxx.LongName;
                string messageStaff = "";
                if (vxx.SubjectID == 1 || vxx.SubjectID == 2 || vxx.SubjectID == 3 || vxx.SubjectID == 8)
                {

                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                }
                else
                {
                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                    // messageStaffSubjectList.Add(vxx.SubjectID);
                }

                messageStaffList.Add(messageStaff);


            }

            //var reportmessage = new SubjectModelFootMessage();

            //reportmessage.Messages= messageStaffList.ToList();
            //reportmessage.MessagesInt = messageStaffSubjectList.ToList();


            List<string> messages = messageStaffList.ToList();
            List<int> messagesInt = messageStaffSubjectList.ToList();

            //reportmessage.Messages.Add(bioMessage1);
            //reportmessage.Messages.Add(bioMessage2);
            //reportmessage.Messages.Add(bioMessage3);
            //reportmessage.Messages.Add(chemMessage1);
            //reportmessage.Messages.Add(chemMessage2);
            //reportmessage.Messages.Add(chemMessage3);
            //reportmessage.Messages.Add(phyMessage1);
            //reportmessage.Messages.Add(phyMessage3);
            //reportmessage.Messages.Add(agricMessage1);
            //reportmessage.Messages.Add(agricMessage3);
            //ViewBag.ReasonMessages = messages;

            myList.Add(new KeyValuePair<string, int>(bioMessage1, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage2, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage3, 1));
            myList.Add(new KeyValuePair<string, int>(chemMessage1, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage2, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage3, 2));
            myList.Add(new KeyValuePair<string, int>(phyMessage1, 3));
            myList.Add(new KeyValuePair<string, int>(phyMessage3, 3));
            myList.Add(new KeyValuePair<string, int>(agricMessage1, 8));
            myList.Add(new KeyValuePair<string, int>(agricMessage3, 8));
            var storeViewBag = new List<string>();
            foreach (var L in myList)
            {
                storeViewBag.Add(L.Key);
            }
            ViewBag.ReasonMessages = storeViewBag;


            //messages.Add(bioMessage1);
            //messages.Add(bioMessage2);
            //messages.Add(bioMessage3);
            //messages.Add(chemMessage1);
            //messages.Add(chemMessage2);
            //messages.Add(chemMessage3);
            //messages.Add(phyMessage1);
            //messages.Add(phyMessage3);
            //messages.Add(agricMessage1);
            //messages.Add(agricMessage3);
            //ViewBag.ReasonMessages = messages;

            //log to database centretablefootter message
            //schoolid, message,subjectid
            //AddSchoolSubjectMessage
            var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
            foreach (var mes in myList)
            {
                if (mes.Key == "" && mes.Value != 0)
                {
                    continue;
                }

                if (mes.Key == "BIOLOGY has Inadequate Equipment" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "BIOLOGY has Inadequate Reagent" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "BIOLOGY has Inadequate Laboratory" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Equipment" && mes.Value == 2)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Reagent" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Laboratory" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "AGRIC SCIENCE has Inadequate Equipment" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "AGRIC SCIENCE has Inadequate Laboratory" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "PHYSICS has Inadequate Equipment" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "PHYSICS has Inadequate Laboratory" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }

                //"The teacher for " + vxx.LongName + " is not qualified to teach subject"
                else if (mes.Key == "The teacher for PHYSICS is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for CHEMISTRY is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for AGRIC SCIENCE is not qualified to teach subject" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for BIOLOGY is not qualified to teach subject" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);
                }
                else
                {

                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var fromDB = clsContent.GetSchoolFootMessageRecog(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, errorMesage, subjectId, statuss);



                }


                //foreach (var row in fromDB)
                //{
                //    if (row.SchoolSubjectMessage == mes)
                //    {

                //    }
                //}
                //if (intMessages != 1 || intMessages != 2 || intMessages != 3 || intMessages != 8)
                //{

                //    if (fromDB.Count > 0)
                //    {
                //        foreach (var row in fromDB)
                //        {
                //            if (row.SchoolSubjectMessage != mes)
                //            {
                //                var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, mes,
                //                    intMessages, 4);

                //            }

                //        }
                //    }
                //    else
                //    {
                //        var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, mes,
                //                   intMessages, 4);
                //    }


                //}

            }


            //storageAll.Add(new SchoolSubjectList
            //{
            //    ReaLabo=storageRELA,
            //    StaffSubjectO=storageStaff
            //});

            var allList = new List<SchoolSubjectViewModel>();
            var subjectApproved = clsContent.GetSchoolSubjectsApproved(sdc);
            var compulsorySubject = clsContent.GetCompulsorySubjects();

            var tradedSubject = clsContent.GetTradeSubjects();

            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in subjectApproved
                       where compulsorySubject.Any(
                           x => x.ID == lst1.SubjectID)
                       select lst1).ToList();


            var lstTraded = (from lst1 in subjectApproved
                             where tradedSubject.Any(
                                 x => x.ID == lst1.SubjectID)
                             select lst1).ToList();

            var lstotherSubject = (from lst1 in subjectApproved
                                   where anyotherSubject.Any(
                                       x => x.ID == lst1.SubjectID)
                                   select lst1).ToList();

            // TODO show error message and stop
            //lst.AddRange(lstTraded);
            //lst.AddRange(lstotherSubject);
            allList.AddRange(lst);
            allList.AddRange(lstTraded);
            allList.AddRange(lstotherSubject);
            var school = clsContent.GetSchoolById(sdc);
            if (lst.Count < 3)
            {
                ViewBag.HoldCS = "School cannot be recognised for Inadequate Compulsory Subject basically Mathematics, English-Language, Civic-Education and at least one trade subjects";
                ViewBag.Approve = "active";
                ViewBag.SchoolID = school.ID;
                TempData["DisplaySchoolName"] = school.SchoolName;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(fetchFinal);

            }
            if (lstTraded.Count < 1)
            {
                ViewBag.TradeSubject = "No Trade Subject.There must be at least a trade subject with MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION before a school can be recognised";
                // ViewBag.StorageAll = storageAll;
                ViewBag.Approve = "active";
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.SchoolID = school.ID;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(fetchFinal);
                //  return PartialView("DisplaySubjectNotApproved",new {schoolid=sdc});
            }
            if (allList.Count < 8)
            {
                ViewBag.atLeast8Subject = "Minimum of 8 subjects is required including MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION with at least a trade subject";
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.SchoolID = school.ID;
                TempData["Message"] = CurrentUser.EmailAddress;
                ViewBag.Approve = "active";
                return View(fetchFinal);
                //return Json(new { holdAtLeast8 = ViewBag.atLeast8Subject });
                // return RedirectToAction("DisplaySubjectNotApproved", new { schoolid = sdc });
            }
            //ViewBag.StorageAll = storageAll;

            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.SchoolID = school.ID;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Approve = "active";
            return View(fetchFinal);
        }

        public ActionResult Recommendation(long sddc, int state, int classid)
        {
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Recommendation = "active";
            PrepareApprovalMessages(sddc, state, classid);

            return View();
        }

        public ActionResult Approve(long sddc, int state, int? classId)
        {
            if (classId == null)
            {
                ShowMessage("Please review the school staff first before visiting recognized centre.", AlertType.Danger);
                return RedirectToAction("StaffSummary", new {sddc, state});
            }
            var fetchFinal = clsContent.GetSchoolSubjectsApproved(sddc);
            //var reasonEquip = clsContent.FetchReasonsFromItemSectionEquipment(sddc);
            //var reasonReagent = clsContent.FetchReasonsFromItemSectionReagent(sddc);
            //var reasonLabora = clsContent.FetchReasonsFromItemSectionLaboratory(sddc);
            var reasonStaffSubject = clsContent.FetchReasonsFromStaff2SubjectClass(sddc, int.Parse(classId.ToString()));

            //var storageRELA = new List<SchoolSubjectViewModel>();
            //var storageStaff = new List<SchoolSubjectViewModel>();
            //var storageAll = new List<SchoolSubjectList>();
            //if i get reasonequip for 


            var facility1 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 1, false);
            var facility2 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 2, false);
            var facility3 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 3, false);
            var facility8 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 8, false);

            var bioF =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioE =
                facility1
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioR =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var bioL =
               facility1
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);



            var chemF =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemE =
                facility2
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemR =
                facility2
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var chemL =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);

            var phyF = facility3
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var phyE = facility3
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var phyL = facility3
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);



            var agricF = facility8
                   .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == null);

            var agricE = facility8
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var agricL = facility8
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);

            string bioMessage1 = "";
            string bioMessage2 = "";
            string bioMessage3 = "";
            string chemMessage1 = "";
            string chemMessage2 = "";
            string chemMessage3 = "";
            string phyMessage1 = "";
            string phyMessage3 = "";
            string agricMessage1 = "";
            string agricMessage3 = "";
            string staffmessageSubject = "";
            var allStaffMessageSubject = new List<string>();
            var allStaffMessageSubjectItem = new List<string>();
            var messageStaffList = new List<string>();
            var messageStaffSubjectList = new List<int>();


            if (bioF == true && bioE == false)
            {
                bioMessage1 = "BIOLOGY has Inadequate Equipment";
            }
            if (bioF == true && bioR == false)
            {
                bioMessage2 = "BIOLOGY has Inadequate Reagent";
            }
            if (bioF == true && bioL == false)
            {
                bioMessage3 = "BIOLOGY has Inadequate Laboratory";
            }

            if (chemF == true && chemE == false)
            {
                chemMessage1 = "CHEMISTRY has Inadequate Equipment";
            }
            if (chemF == true && chemR == false)
            {
                chemMessage2 = "CHEMISTRY has Inadequate Reagent";
            }
            if (chemF == true && chemL == false)
            {
                chemMessage3 = "CHEMISTRY has Inadequate Laboratory";
            }

            if (phyF == true && phyE == false)
            {
                phyMessage1 = "PHYSICS has Inadequate Equipment";
            }
            if (phyF == true && phyL == false)
            {
                phyMessage3 = "PHYSICS has Inadequate Laboratory";
            }
            if (agricF == true && agricE == false)
            {
                agricMessage1 = "AGRIC SCIENCE has Inadequate Equipment";
            }
            if (agricF == true && agricL == false)
            {
                agricMessage3 = "AGRIC SCIENCE has Inadequate Laboratory";
            }

            object[] array = new object[50];
            var myList = new List<KeyValuePair<string, int>>();

            foreach (var vxx in reasonStaffSubject)
            {
                staffmessageSubject = vxx.LongName;
                string messageStaff = "";
                if (vxx.SubjectID == 1 || vxx.SubjectID == 2 || vxx.SubjectID == 3 || vxx.SubjectID == 8)
                {

                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                }
                else
                {
                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                    // messageStaffSubjectList.Add(vxx.SubjectID);
                }

                messageStaffList.Add(messageStaff);


            }

            //var reportmessage = new SubjectModelFootMessage();

            //reportmessage.Messages= messageStaffList.ToList();
            //reportmessage.MessagesInt = messageStaffSubjectList.ToList();


            //List<string> messages = messageStaffList.ToList();
            //List<int> messagesInt = messageStaffSubjectList.ToList();

            //reportmessage.Messages.Add(bioMessage1);
            //reportmessage.Messages.Add(bioMessage2);
            //reportmessage.Messages.Add(bioMessage3);
            //reportmessage.Messages.Add(chemMessage1);
            //reportmessage.Messages.Add(chemMessage2);
            //reportmessage.Messages.Add(chemMessage3);
            //reportmessage.Messages.Add(phyMessage1);
            //reportmessage.Messages.Add(phyMessage3);
            //reportmessage.Messages.Add(agricMessage1);
            //reportmessage.Messages.Add(agricMessage3);
            //ViewBag.ReasonMessages = messages;

            myList.Add(new KeyValuePair<string, int>(bioMessage1, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage2, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage3, 1));
            myList.Add(new KeyValuePair<string, int>(chemMessage1, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage2, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage3, 2));
            myList.Add(new KeyValuePair<string, int>(phyMessage1, 3));
            myList.Add(new KeyValuePair<string, int>(phyMessage3, 3));
            myList.Add(new KeyValuePair<string, int>(agricMessage1, 8));
            myList.Add(new KeyValuePair<string, int>(agricMessage3, 8));
            var storeViewBag = new List<string>();
            foreach (var L in myList)
            {
                storeViewBag.Add(L.Key);
            }
            ViewBag.ReasonMessages = storeViewBag;


            //messages.Add(bioMessage1);
            //messages.Add(bioMessage2);
            //messages.Add(bioMessage3);
            //messages.Add(chemMessage1);
            //messages.Add(chemMessage2);
            //messages.Add(chemMessage3);
            //messages.Add(phyMessage1);
            //messages.Add(phyMessage3);
            //messages.Add(agricMessage1);
            //messages.Add(agricMessage3);
            //ViewBag.ReasonMessages = messages;

            //log to database centretablefootter message
            //schoolid, message,subjectid
            //AddSchoolSubjectMessage
            var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sddc);
            foreach (var mes in myList)
            {
                if (mes.Key == "" && mes.Value != 0)
                {
                    continue;
                }

                if (mes.Key == "BIOLOGY has Inadequate Equipment" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "BIOLOGY has Inadequate Reagent" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "BIOLOGY has Inadequate Laboratory" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Equipment" && mes.Value == 2)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Reagent" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Laboratory" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "AGRIC SCIENCE has Inadequate Equipment" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "AGRIC SCIENCE has Inadequate Laboratory" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "PHYSICS has Inadequate Equipment" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "PHYSICS has Inadequate Laboratory" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                //"The teacher for " + vxx.LongName + " is not qualified to teach subject"
                else if (mes.Key == "The teacher for PHYSICS is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for CHEMISTRY is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for AGRIC SCIENCE is not qualified to teach subject" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for BIOLOGY is not qualified to teach subject" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else
                {

                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var fromDB = clsContent.GetSchoolFootMessageRecog(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);



                }


                //foreach (var row in fromDB)
                //{
                //    if (row.SchoolSubjectMessage == mes)
                //    {

                //    }
                //}
                //if (intMessages != 1 || intMessages != 2 || intMessages != 3 || intMessages != 8)
                //{

                //    if (fromDB.Count > 0)
                //    {
                //        foreach (var row in fromDB)
                //        {
                //            if (row.SchoolSubjectMessage != mes)
                //            {
                //                var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, mes,
                //                    intMessages, 4);

                //            }

                //        }
                //    }
                //    else
                //    {
                //        var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sdc, mes,
                //                   intMessages, 4);
                //    }


                //}

            }


            //storageAll.Add(new SchoolSubjectList
            //{
            //    ReaLabo=storageRELA,
            //    StaffSubjectO=storageStaff
            //});

            var allList = new List<SchoolSubjectViewModel>();
            var subjectApproved = clsContent.GetSchoolSubjectsApproved(sddc);
            var compulsorySubject = clsContent.GetCompulsorySubjects();

            var tradedSubject = clsContent.GetTradeSubjects();

            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in subjectApproved
                       where compulsorySubject.Any(
                           x => x.ID == lst1.SubjectID)
                       select lst1).ToList();


            var lstTraded = (from lst1 in subjectApproved
                             where tradedSubject.Any(
                                 x => x.ID == lst1.SubjectID)
                             select lst1).ToList();

            var lstotherSubject = (from lst1 in subjectApproved
                                   where anyotherSubject.Any(
                                       x => x.ID == lst1.SubjectID)
                                   select lst1).ToList();

            // TODO show error message and stop
            //lst.AddRange(lstTraded);
            //lst.AddRange(lstotherSubject);
            allList.AddRange(lst);
            allList.AddRange(lstTraded);
            allList.AddRange(lstotherSubject);
            var school = clsContent.GetSchoolById(sddc);
            if (lst.Count < 3)
            {
                ViewBag.HoldCS = "School cannot be recognised for Inadequate Compulsory Subject basically Mathematics, English-Language, Civic-Education and at least one trade subjects";
                ViewBag.Approve = "active";
                ViewBag.SchoolID = school.ID;
                TempData["DisplaySchoolName"] = school.SchoolName;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(fetchFinal);

            }
            if (lstTraded.Count < 1)
            {
                ViewBag.TradeSubject = "No Trade Subject.There must be at least a trade subject with MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION before a school can be recognised";
                // ViewBag.StorageAll = storageAll;
                ViewBag.Approve = "active";
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.SchoolID = school.ID;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(fetchFinal);
                //  return PartialView("DisplaySubjectNotApproved",new {schoolid=sdc});
            }
            if (allList.Count < 8)
            {
                ViewBag.atLeast8Subject = "Minimum of 8 subjects is required including MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION with at least a trade subject";
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.SchoolID = school.ID;
                TempData["Message"] = CurrentUser.EmailAddress;
                ViewBag.Approve = "active";
                return View(fetchFinal);
                //return Json(new { holdAtLeast8 = ViewBag.atLeast8Subject });
                // return RedirectToAction("DisplaySubjectNotApproved", new { schoolid = sdc });
            }
            //ViewBag.StorageAll = storageAll;

            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.SchoolID = school.ID;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Approve = "active";
            return View(fetchFinal);
        }
        public void PrepareApprovalMessages(long sddc, int state, int classId)
        {
            
            var reasonStaffSubject = clsContent.FetchReasonsFromStaff2SubjectClass(sddc, classId);

            var facility1 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 1, false);
            var facility2 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 2, false);
            var facility3 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 3, false);
            var facility8 = clsContent.FetchReasonsFromItemSectionFacility(sddc, 8, false);

            var bioF =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioE =
                facility1
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var bioR =
                facility1
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var bioL =
               facility1
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);



            var chemF =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemE =
                facility2
                    .Any(x => x.EquipmentFacility == 1 && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var chemR =
                facility2
                    .Any(x => x.EquipmentFacility == null && x.ReagentFacility == 1 && x.LaboratoryFacility == null);

            var chemL =
               facility2
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == 1);

            var phyF = facility3
                   .Any(x => x.EquipmentFacility == null && x.ReagentFacility == null && x.LaboratoryFacility == null);

            var phyE = facility3
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var phyL = facility3
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);



            var agricF = facility8
                   .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == null);

            var agricE = facility8
                  .Any(x => x.EquipmentFacility == 1 && x.LaboratoryFacility == null);

            var agricL = facility8
                  .Any(x => x.EquipmentFacility == null && x.LaboratoryFacility == 1);

            string bioMessage1 = "";
            string bioMessage2 = "";
            string bioMessage3 = "";
            string chemMessage1 = "";
            string chemMessage2 = "";
            string chemMessage3 = "";
            string phyMessage1 = "";
            string phyMessage3 = "";
            string agricMessage1 = "";
            string agricMessage3 = "";
            string staffmessageSubject = "";
            var allStaffMessageSubject = new List<string>();
            var allStaffMessageSubjectItem = new List<string>();
            var messageStaffList = new List<string>();
            var messageStaffSubjectList = new List<int>();


            if (bioF == true && bioE == false)
            {
                bioMessage1 = "BIOLOGY has Inadequate Equipment";
            }
            if (bioF == true && bioR == false)
            {
                bioMessage2 = "BIOLOGY has Inadequate Reagent";
            }
            if (bioF == true && bioL == false)
            {
                bioMessage3 = "BIOLOGY has Inadequate Laboratory";
            }

            if (chemF == true && chemE == false)
            {
                chemMessage1 = "CHEMISTRY has Inadequate Equipment";
            }
            if (chemF == true && chemR == false)
            {
                chemMessage2 = "CHEMISTRY has Inadequate Reagent";
            }
            if (chemF == true && chemL == false)
            {
                chemMessage3 = "CHEMISTRY has Inadequate Laboratory";
            }

            if (phyF == true && phyE == false)
            {
                phyMessage1 = "PHYSICS has Inadequate Equipment";
            }
            if (phyF == true && phyL == false)
            {
                phyMessage3 = "PHYSICS has Inadequate Laboratory";
            }
            if (agricF == true && agricE == false)
            {
                agricMessage1 = "AGRIC SCIENCE has Inadequate Equipment";
            }
            if (agricF == true && agricL == false)
            {
                agricMessage3 = "AGRIC SCIENCE has Inadequate Laboratory";
            }

            object[] array = new object[50];
            var myList = new List<KeyValuePair<string, int>>();

            foreach (var vxx in reasonStaffSubject)
            {
                staffmessageSubject = vxx.LongName;
                string messageStaff = "";
                if (vxx.SubjectID == 1 || vxx.SubjectID == 2 || vxx.SubjectID == 3 || vxx.SubjectID == 8)
                {

                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                }
                else
                {
                    messageStaff = "The teacher for " + vxx.LongName + " is not qualified to teach the subject";
                    myList.Add(new KeyValuePair<string, int>(messageStaff, vxx.SubjectID));
                    // messageStaffSubjectList.Add(vxx.SubjectID);
                }

                messageStaffList.Add(messageStaff);


            }

            
            myList.Add(new KeyValuePair<string, int>(bioMessage1, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage2, 1));
            myList.Add(new KeyValuePair<string, int>(bioMessage3, 1));
            myList.Add(new KeyValuePair<string, int>(chemMessage1, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage2, 2));
            myList.Add(new KeyValuePair<string, int>(chemMessage3, 2));
            myList.Add(new KeyValuePair<string, int>(phyMessage1, 3));
            myList.Add(new KeyValuePair<string, int>(phyMessage3, 3));
            myList.Add(new KeyValuePair<string, int>(agricMessage1, 8));
            myList.Add(new KeyValuePair<string, int>(agricMessage3, 8));
            var storeViewBag = new List<string>();
            foreach (var L in myList)
            {
                storeViewBag.Add(L.Key);
            }
            ViewBag.ReasonMessages = storeViewBag;


          
            foreach (var mes in myList)
            {
                if (mes.Key == "" && mes.Value != 0)
                {
                    continue;
                }

                if (mes.Key == "BIOLOGY has Inadequate Equipment" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "BIOLOGY has Inadequate Reagent" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "BIOLOGY has Inadequate Laboratory" && mes.Value == 1)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Equipment" && mes.Value == 2)
                {
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Reagent" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 2;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "CHEMISTRY has Inadequate Laboratory" && mes.Value == 2)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "AGRIC SCIENCE has Inadequate Equipment" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "AGRIC SCIENCE has Inadequate Laboratory" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "PHYSICS has Inadequate Equipment" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 1;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                else if (mes.Key == "PHYSICS has Inadequate Laboratory" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 3;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }

                //"The teacher for " + vxx.LongName + " is not qualified to teach subject"
                else if (mes.Key == "The teacher for PHYSICS is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for CHEMISTRY is not qualified to teach subject" && mes.Value == 3)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for AGRIC SCIENCE is not qualified to teach subject" && mes.Value == 8)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else if (mes.Key == "The teacher for BIOLOGY is not qualified to teach subject" && mes.Value == 1)
                {
                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var deletedMessages = clsContent.ProcDeleteSchoolFootMessage(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);
                }
                else
                {

                    int subjectId = mes.Value;
                    var errorMesage = mes.Key;
                    int statuss = 4;
                    // var fromDB = clsContent.GetSchoolFootMessageRecog(sdc);
                    var schoolSubjectMessage = clsContent.AddSchoolSubjectMessage(sddc, errorMesage, subjectId, statuss);



                }

            }



            var allList = new List<SchoolSubjectViewModel>();
            var subjectApproved = clsContent.GetSchoolSubjectsApproved(sddc);
            var compulsorySubject = clsContent.GetCompulsorySubjects();

            var tradedSubject = clsContent.GetTradeSubjects();

            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in subjectApproved
                       where compulsorySubject.Any(
                           x => x.ID == lst1.SubjectID)
                       select lst1).ToList();


            var lstTraded = (from lst1 in subjectApproved
                             where tradedSubject.Any(
                                 x => x.ID == lst1.SubjectID)
                             select lst1).ToList();

            var lstotherSubject = (from lst1 in subjectApproved
                                   where anyotherSubject.Any(
                                       x => x.ID == lst1.SubjectID)
                                   select lst1).ToList();

            // TODO show error message and stop
            //lst.AddRange(lstTraded);
            //lst.AddRange(lstotherSubject);
            allList.AddRange(lst);
            allList.AddRange(lstTraded);
            allList.AddRange(lstotherSubject);
            var school = clsContent.GetSchoolById(sddc);
            if (lst.Count < 3)
            {
                ViewBag.HoldCS = "School cannot be recognised for Inadequate Compulsory Subject basically Mathematics, English-Language, Civic-Education and at least one trade subjects";
                

            }
            if (lstTraded.Count < 1)
            {
                ViewBag.TradeSubject = "No Trade Subject.There must be at least a trade subject with MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION before a school can be recognised";
               
                //  return PartialView("DisplaySubjectNotApproved",new {schoolid=sdc});
            }
            if (allList.Count < 8)
            {
                ViewBag.atLeast8Subject = "Minimum of 8 subjects is required including MATHEMATICS, ENGLISH LANGUAGE and CIVIC EDUCATION with at least a trade subject";
               
                //return Json(new { holdAtLeast8 = ViewBag.atLeast8Subject });
                // return RedirectToAction("DisplaySubjectNotApproved", new { schoolid = sdc });
            }
            
        }

        [HttpPost]
        public async Task<bool> SendEmailForRecognition(SchoolProfileViewModel1 model, long schoolid)
        {
            
            var getSchool = clsContent.GetSchoolById(schoolid);
            var staffCount = clsContent.GetSchoolStaffCountX(schoolid);
            var body = new StringBuilder();
            body.Append("<div class='school-wrapper'>");
            body.Append("<div>");
            body.Append("<h3> <span style='color: black'>" + getSchool.SchoolNameEdit + "</span> is <span class='text-success'>successfully inspected</span></h3>");
            body.Append(
                  "<p style='color: black;font-weight:bold'> The number of staff in school is <span class='badge' style='font-weight:bold'>" + staffCount.StaffCount + "</span></p>");
            body.Append("</div>");
            body.Append("<br />");
            body.Append("<h2>Subject with required Facilities</h2>");
            body.Append("<table id='searchtable' class='table table-bordered table-striped'>");
            body.Append("<thead>");
            body.Append("<tr style='background-color:lightgrey'>");

            body.Append("<th>School Subject(s)</th>");
            body.Append("<th>Equipment Required</th>");
            body.Append("<th>Equipment Observed</th>");
            body.Append("<th>Reagent Required</th>");
            body.Append("<th>Reagent Observed</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            //  body.Append("@foreach (var item in (IEnumerable<vwSchoolSubjectFacilityReport>)ViewBag.Facilityreport)");
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(schoolid);
            foreach (var itemx in schoolsubjectFacility)
            {
                // body.Append("{");
                body.Append("<tr>");
                body.Append("<td>" + itemx.LongName + "</td>");
                body.Append(" <td>" + itemx.Total_Equipment + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolEquipment + "</td>");
                body.Append(" <td>" + itemx.Total_Reagent + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolReagent + "</td>");
                //  body.Append(" <td>@item.TotalSchoolReagent</td>");
                body.Append("</tr>");
            }
            // body.Append("}");
            body.Append("</tbody>");
            body.Append(" </table>");
            body.Append("<table class='table table - bordered table - striped'>");
            body.Append("<thead>");
            body.Append("<tr>");
            body.Append("<th style='background-color: lightgrey' colspan='3'>SUBJECTS OFFERED BY THE SCHOOL</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            body.Append("<tr>");

            var counter = 1;
            //  var tableBody = "";
            var schoolSubjects = clsContent.GetSchoolSubjects(schoolid);// (IEnumerable<vwSchoolSubjectReport>)ViewBag.SchoolSubjects;
            var totalItems = schoolSubjects.Count();
            var rows = Math.Ceiling((double)totalItems / 3);
            var remainderColumns = totalItems % rows;
            var rowsDisplayed = 0;
            body.Append("<td>" + rows + "</td>");
            body.Append("<td>" + remainderColumns + "</td>");
            body.Append("<td>" + totalItems + "</td>");
            body.Append("</tr>");
            foreach (var item in schoolSubjects)
            {

                if (counter <= 3)
                {
                    if (counter == 1)
                    {
                        // tableBody += "<tr>";
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        body.Append("<tr>");
                        body.Append("<td>" + item.LongName + "</td>");
                        counter++;
                    }
                    else
                    {
                        body.Append("<td>" + item.LongName + "</td>");
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        if (counter == 3)
                        {
                            // tableBody += "</tr>";
                            body.Append("</tr>");
                            counter = 1;
                        }
                        else
                        {
                            counter++;
                        }

                    }
                }

            }
           
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append(" </div>");
           

            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            mess.IsBodyHtml = true;
            var userEmail = CurrentUser.EmailAddress;// CurrentUser.EmailAddress;

           // var userEmailInfo = clsContent.GetAUserInfoForSED(userEmail);
            mess.From = new MailAddress(userEmail, "SCHOOL INSPECTION SUMMARY REPORT");
            //GetSEDEmail 
            var getschool = clsContent.GetSchoolByID(schoolid);
            var stateAdmin = clsContent.GetStateToAdmin(int.Parse(getschool.StateID));
            var sedEmail = clsContent.GetSEDEmail();
            mess.To.Add(sedEmail.EmailAddress);

            // mess.Body = "An inspection of the above named school was carried out on {date} and the following details were observed:\n Subject with required facilities are+"+ schoolSubjectFacilityRep.Total_Equipment+"out of "+ schoolSubjectFacilityRep.TotalSchoolEquipment+" and   schoolSubjectFacilityRep" + schoolSubjectFacilityRep.SurName + " from" + userEmailInfo.StateName + " office have registered my details on the school recognition platform,please sir/ma,activate me to be able to login";
            mess.Body = body.ToString();
            //mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            // mess.Body = "I " + schoolSubjectFacilityRep.SurName + " from" + userEmailInfo.StateName + " office have registered my details on the school recognition platform,please sir/ma,activate me to be able to login";

            mess.Subject = "School Recognition";
            mess.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(sedEmail.EmailAddress, "welcome");
                smtpClient.Send(mess);
                var status = "Completed";
                clsContent.UpdateSchoolProfileStatus(schoolid, status);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //TempData["response"] = "<script>alert('Staff details submitted successfully.');</script>";
            return await Task.FromResult(false);

        }



        public bool SendEmailForNotRecognition(string administratorEmail, string useremail) //emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            var url = new UrlHelper(ControllerContext.RequestContext);
            var userInfo = clsUsers.GetUserLogin(useremail);
            mess.From = new MailAddress(administratorEmail, "Mail Testing");
            // mess.To.Add("ujimadiyi@waec.org.ng");
            mess.To.Add(useremail);
            mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            // mess.Body = "You have been activated. to login click <a href ="http://localhost:12027">
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(useremail, "welcome");
                smtpClient.Send(mess);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public ActionResult OptionEmail1(string message1, long sdc)
        {
            var userSchoolEmail = clsContent.GetUserForSchool(sdc);
            var isDone = SendEmailForNotRecognition("paogbekhilu@waec.org.ng", userSchoolEmail.EmailAddress);
            return Json(new { status = isDone }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptionEmail12(string message2, long sdc)
        {
            var userSchoolEmail = clsContent.GetUserForSchool(sdc);
            var isDone = SendEmailForNotRecognition("paogbekhilu@waec.org.ng", userSchoolEmail.EmailAddress);
            return Json(new { status = isDone }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptionEmail13(string message3, long sdc)
        {
            var userSchoolEmail = clsContent.GetUserForSchool(sdc);
            var isDone = SendEmailForNotRecognition("paogbekhilu@waec.org.ng", userSchoolEmail.EmailAddress);
            return Json(new { status = isDone }, JsonRequestBehavior.AllowGet);
        }
        public int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            //Excel documentation says "COMPLETE calendar years in between dates"
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month &&// if the start month and the end month are the same
                endDate.Day < startDate.Day// AND the end day is less than the start day
                || endDate.Month < startDate.Month)// OR if the end month is less than the start month
            {
                years--;
            }

            return years;
        }

        public ActionResult ClassroomResult(string radioValue, long sdc)
        {
            var school = clsContent.GetSchoolEditByID(sdc);
            var itemSection = "Classroom-Section";
            // var year = DateTime.Parse(school.YearEstablished);
            var now = DateTime.Now.Year;
            int thenYear = int.Parse(school.YearEstablished);
            int result = now - thenYear;

            if (radioValue == "Passed")
            {
                var finalRating = 1;
                var itemSectonAssessment = clsContent.AddNewItemSection(sdc, itemSection, finalRating);
                return Json(new { status = true, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult LaboratoryResult(int rowId, string longName, int facilityId, string typeofFacility, string radioValue, long sdc, int subjectId, int updatedid = 0)
        //{
        //    var itemSection = longName + "-" + typeofFacility;
        //    var passOrFail = "";
        //    //GetAssessmentUpdateRow
        //    //  var itemSectionRow = clsContent.GetItemSectionRow(updatedid);
        //    var itemSectonAssessmentL = new LaboratoryApprovalModel();
        //    var itemSectonAssessmentIT = new ItemSectionAssessmentModel();
        //    var checkIfRowExist= new ItemSectionAssessmentModel();

        //    if (radioValue == "Passed")
        //    {
        //         checkIfRowExist = clsContent.GetAssessmentUpdateRowSchSub(sdc, subjectId);
        //        if (checkIfRowExist == null)
        //        {
        //            facilityId = 1;
        //            itemSectonAssessmentL = clsContent.AddNewItemSection3(rowId, sdc, itemSection, subjectId, facilityId);

        //            return
        //                Json(
        //                    new
        //                    {
        //                        status = true,
        //                        message = "Value saved successfully",
        //                        updatedid = itemSectonAssessmentL.ID,
        //                    }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {

        //            checkIfRowExist.LaboratoryFacility = 1;
        //            itemSectonAssessmentIT = clsContent.UpdateNewItemSectionLa(checkIfRowExist.LaboratoryFacility, sdc,subjectId);

        //            return
        //                Json(
        //                    new
        //                    {
        //                        status = true,
        //                        message = "Value updated successfully",
        //                        updatedid = itemSectonAssessmentIT.ID
        //                    }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    if (radioValue == "Failed")
        //    {
        //        checkIfRowExist = clsContent.GetAssessmentUpdateRowSchSub(sdc, subjectId);
        //        if (checkIfRowExist == null)
        //        {
        //             facilityId = 0;
        //            itemSectonAssessmentL = clsContent.AddNewItemSection3(rowId, sdc, itemSection, subjectId, facilityId);

        //            //  var itemSectonAssessment = clsContent.AddNewItemSection(sdc, itemSection, finalRating, subjectId);
        //            return
        //                Json(
        //                    new
        //                    {
        //                        status = true,
        //                        message = "Value saved successfully",
        //                        updatedid = itemSectonAssessmentL.ID,
        //                    }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            checkIfRowExist.LaboratoryFacility = 0;
        //            var itemSectonAssessment = clsContent.UpdateNewItemSectionLa(checkIfRowExist.LaboratoryFacility, sdc,subjectId);
        //            return
        //                Json(
        //                    new
        //                    {
        //                        status = true,
        //                        message = "Value updated successfully",
        //                        updatedid = itemSectonAssessment.ID
        //                    }, JsonRequestBehavior.AllowGet);
        //        }


        //    }
        //    return Json(new { status = false, message = "Value failed to save successfully" }, JsonRequestBehavior.AllowGet);

        //    //  return Json(new { status = false, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);

        //}


        public ActionResult Result(int rowId, string longName, int facilityId, string typeofFacility, string radioValue, long sddc, int subjectId, int updatedid = 0)
        {
            var itemSection = longName + "-" + typeofFacility;
            int x, y, z;
            int? a, b, c;
            //GetAssessmentUpdateRow
            //  var itemSectionRow = clsContent.GetItemSectionRow(updatedid);
            var itemSectonAssessmentF = new FacilityApprovalModel();
            var itemSectonAssessmentIT = new ItemSectionAssessmentModel();
            if (radioValue == "Passed")
            {
                var checkIfRowExist = clsContent.GetAssessmentUpdateRowSchSub(sddc, subjectId, facilityId);

                if (checkIfRowExist == null || checkIfRowExist.ItemSection != itemSection)
                {

                    if (facilityId == 1)
                    {
                        x = 1;
                        a = 1;
                        itemSectonAssessmentF = clsContent.AddNewItemSection1(rowId, sddc, itemSection, subjectId, a, x);
                    }
                    if (facilityId == 2)
                    {
                        y = 2;
                        b = 1;
                        itemSectonAssessmentF = clsContent.AddNewItemSection2(rowId, sddc, itemSection, subjectId, b, y);

                    }
                    if (facilityId == 3)
                    {
                        z = 3;
                        c = 1;
                        itemSectonAssessmentF = clsContent.AddNewItemSection3(rowId, sddc, itemSection, subjectId, c, z);

                    }


                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                                // updatedid = itemSectonAssessmentF.ID,
                            }, JsonRequestBehavior.AllowGet);
                }
                else
                {

                    var finalRating = 1; //update
                    if (facilityId == 1)
                    {
                        x = 1;
                        checkIfRowExist.EquipmentFacility = 1;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionEq(checkIfRowExist.EquipmentFacility, sddc, subjectId, x);

                    }
                    if (facilityId == 2)
                    {
                        y = 2;
                        checkIfRowExist.ReagentFacility = 1;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionRe(checkIfRowExist.ReagentFacility, sddc, subjectId, y);

                    }
                    if (facilityId == 3)
                    {
                        z = 3;
                        checkIfRowExist.LaboratoryFacility = 1;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionLa(checkIfRowExist.LaboratoryFacility, sddc, subjectId, z);

                    }

                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value updated successfully"
                                // updatedid = itemSectonAssessmentIT.ID
                            }, JsonRequestBehavior.AllowGet);
                }
            }
            if (radioValue == "Failed")
            {
                var checkIfRowExist = clsContent.GetAssessmentUpdateRowSchSub(sddc, subjectId, facilityId);
                if (checkIfRowExist == null || checkIfRowExist.ItemSection != itemSection)
                {
                    var finalRating = 0;
                    if (facilityId == 1)
                    {
                        x = 1;
                        //facilityId = 0;

                        itemSectonAssessmentF = clsContent.AddNewItemSection1(rowId, sddc, itemSection, subjectId, null, x);
                    }
                    if (facilityId == 2)
                    {
                        y = 2;

                        itemSectonAssessmentF = clsContent.AddNewItemSection2(rowId, sddc, itemSection, subjectId, null, y);
                    }
                    if (facilityId == 3)
                    {
                        z = 3;
                        //facilityId = 0;
                        itemSectonAssessmentF = clsContent.AddNewItemSection3(rowId, sddc, itemSection, subjectId, null, z);
                    }


                    //  var itemSectonAssessment = clsContent.AddNewItemSection(sdc, itemSection, finalRating, subjectId);
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                                //updatedid = itemSectonAssessmentF.ID,
                            }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var finalRating = 0; //update
                    if (facilityId == 1)
                    {
                        x = 1;
                        //checkIfRowExist.EquipmentFacility = 0;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionEq(null, sddc, subjectId, x);
                    }
                    if (facilityId == 2)
                    {
                        y = 2;
                        // checkIfRowExist.ReagentFacility = 0;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionRe(null, sddc, subjectId, y);
                    }
                    if (facilityId == 3)
                    {
                        z = 3;
                        //checkIfRowExist.LaboratoryFacility = 0;
                        itemSectonAssessmentIT = clsContent.UpdateNewItemSectionLa(null, sddc, subjectId, z);
                    }

                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value updated successfully",
                                // updatedid = itemSectonAssessmentIT.ID
                            }, JsonRequestBehavior.AllowGet);
                }


            }
            return Json(new { status = false, message = "Value failed to save successfully" }, JsonRequestBehavior.AllowGet);

            //  return Json(new { status = false, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult StaffSummaryResultB(string radioValue, long sddc, string subjectName, int subjectId, int classId,
           int updatedid = 0, int staffId = 0)
        {
            //consider if the three subject has staff teaching them
            //consider if the trade subject has staff taken them
            //there are 8 subjects;
            //var checkIfRowExist = new SchoolSubjectViewModel();
            var school = clsContent.GetSchoolEditByID(sddc);
            //var itemSection = longName + "-" + typeofFacility;
            var itemSection = "StaffSection" + "-" + subjectName;
            var checkIfRowExist = new SchoolSubjectViewModel();
            if (radioValue == "Passed")
            {
                //var checkIfRowExist = clsContent.GetStaffSubClassAssessmentUpdateRow(updatedid);
                checkIfRowExist = clsContent.FetchStaffSubjectToClass(classId, staffId, subjectId);

                if (checkIfRowExist.FinalRating == 0)
                {
                    checkIfRowExist.FinalRating = 1;
                    //var staffsectionSubject = clsContent.GetStaffSectionSubjectBySchool(sdc);
                    //staffsectionSubject.First().QualifiedStaff=1;
                    var updatedstaffsubject = clsContent.UpdateStaffSubjectSubjectReport2(staffId, subjectId, 2);
                    var updateRow1 = clsContent.UpdateStaffSubject2Class(classId,checkIfRowExist.RealRowForUpdate, checkIfRowExist.FinalRating);
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                            }, JsonRequestBehavior.AllowGet);

                }
            }
            else if (radioValue == "Failed")
            {
                checkIfRowExist = clsContent.FetchStaffSubjectToClass(classId,staffId, subjectId);
                if (checkIfRowExist.FinalRating == 1)
                {
                    checkIfRowExist.FinalRating = 0;
                    //update staff subject set staffreport1/2
                    var updatedstaffsubject = clsContent.UpdateStaffSubjectSubjectReport2(staffId, subjectId, 2);
                    var updateRow0 = clsContent.UpdateStaffSubject2Class(classId,checkIfRowExist.RealRowForUpdate, checkIfRowExist.FinalRating);
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                            }, JsonRequestBehavior.AllowGet);

                }
            }
            return Json(new { status = false, message = "Value failed to save successfully" }, JsonRequestBehavior.AllowGet);
        }




        public ActionResult StaffSummaryResult(string radioValue, long sddc, string subjectName, int subjectId, int classId,
            int updatedid = 0, int staffId = 0)
        {
            //consider if the three subject has staff teaching them
            //consider if the trade subject has staff taken them
            //there are 8 subjects;
            //var checkIfRowExist = new SchoolSubjectViewModel();
            var school = clsContent.GetSchoolEditByID(sddc);
            //var itemSection = longName + "-" + typeofFacility;
            var itemSection = "StaffSection" + "-" + subjectName;
            SchoolSubjectViewModel checkIfRowExist;
            if (radioValue == "Passed")
            {
                //var checkIfRowExist = clsContent.GetStaffSubClassAssessmentUpdateRow(updatedid);
                checkIfRowExist = clsContent.FetchStaffSubjectToClass(classId, staffId, subjectId);

                if (checkIfRowExist.FinalRating == 0)
                {
                    checkIfRowExist.FinalRating = 1;
                    //var staffsectionSubject = clsContent.GetStaffSectionSubjectBySchool(sdc);
                    //staffsectionSubject.First().QualifiedStaff=1;
                    var updatedstaffsubject = clsContent.UpdateStaffSubjectSubjectReport2(staffId, subjectId,null);
                    var updateRow1 = clsContent.UpdateStaffSubject2Class(classId,checkIfRowExist.RealRowForUpdate, checkIfRowExist.FinalRating);
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                            }, JsonRequestBehavior.AllowGet);

                }
            }
            else if (radioValue == "Failed")
            {
                checkIfRowExist = clsContent.FetchStaffSubjectToClass(classId, staffId, subjectId);
               
                    checkIfRowExist.FinalRating = 0;
                    //update staff subject set staffreport1/2
                    var updatedstaffsubject = clsContent.UpdateStaffSubjectSubjectReport2(staffId, subjectId,2);
                    var updateRow0 = clsContent.UpdateStaffSubject2Class(classId,checkIfRowExist.RealRowForUpdate, checkIfRowExist.FinalRating);
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully"
                            }, JsonRequestBehavior.AllowGet);

                
            }
            return Json(new { status = false, message = "Value failed to save successfully" }, JsonRequestBehavior.AllowGet);
        }





        public ActionResult SubjectResult(string radioValue, long sdc)
        {
            //consider if the three subject has staff teaching them
            //consider if the trade subject has staff taken them
            //there are 8 subjects;
            var school = clsContent.GetSchoolEditByID(sdc);
            var itemSection = "Subject-Section";
            if (radioValue == "Passed")
            {
                var finalRating = 1;
                var itemSectonAssessment = clsContent.AddNewItemSection(sdc, itemSection, finalRating);
                return Json(new { status = true, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Value saved successfully" }, JsonRequestBehavior.AllowGet);
        }
      
        public ActionResult ViewCredential(int staffid)
        {
            var StaffImages = clsContent.GetStaffCredentials(staffid);
            return Json(new
            {
                Images = StaffImages

            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StaffSummaryB(long sddc)
        {
       
            var schid = sddc;
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolStaffListForView = new List<SchoolStaffList>();
            var staffList = clsContent.GetStaffBySchoolId(schid);
            foreach (var staff in staffList)
            {
              //  var schoolStaffClassList = clsContent.GetStaffClassesByStaffId(staff.ID);
                var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySTaffId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffIdAss(staff.ID);
                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                var schoolStaffSubjectClassList = clsContent.GetStaffSubjectClassTaughtByStaffID(schid, staff.ID);
                var StaffImages = clsContent.GetStaffCredentials(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    //   SchoolStafflassList = schoolStaffClassList,
                    SchoolStafflassList = schoolStaffSubjectClassList,
                    SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,
                    Credentials = StaffImages

                });
            }
            ViewBag.Staff = "active";
            return View(schoolStaffListForView);

        }


        public ActionResult StaffSummary(long sddc)
        {
            var schid = sddc;
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolStaffListForView = new List<SchoolStaffList>();
            var staffList = clsContent.GetStaffBySchoolId(schid);
            foreach (var staff in staffList)
            {
                var schoolStaffClassList = clsContent.GetStaffClassesByStaffId(staff.ID);
                var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySTaffId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffIdAss(staff.ID);
                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                var schoolStaffSubjectClassList = clsContent.GetStaffSubjectClassTaughtByStaffID(schid, staff.ID);
                var StaffImages = clsContent.GetStaffCredentials(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    //   SchoolStafflassList = schoolStaffClassList,
                    SchoolStafflassList = schoolStaffSubjectClassList,
                    SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,
                    Credentials = StaffImages

                });
            }
            ViewBag.Staff = "active";
            return View(schoolStaffListForView);

        }
        private void GetSubjectItemsFormData(int? subjectid)
        {
            var schy = clsContent.GetCouncilSubjectsItems().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;

        }
        public ActionResult LaboratoryFacility(long schoolid, int subjectid = 0, int page = 1)
        {
            // var schid =long.Parse(schoolid);


            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            GetSubjectItemsFormData(subjectid);
            pageIndex = page;


            IPagedList<LaboratoryViewModel> lab = null;
            if (subjec == 0)
            {
                //GetSchoolLaboratoryForAssessment
                lab = clsContent.GetSchoolLaboratoryForAssessment(subjec, schoolid).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                lab = clsContent.GetSchoolLaboratoryForAssessment(subjec, schoolid).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            TempData["Message"] = CurrentUser.EmailAddress;
            //ViewBag.SchoolSubjectList;
            ViewBag.subjectid = subjectid;
            ViewBag.Facility = "active";
            return View(lab);


        }
        public ActionResult Detail(int id, int sddc, int page = 1)
        {
            ViewBag.Facility = "active";
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var subjectList = GetSchoolSubjectItems(sddc, id);
            ViewBag.subject = subjectList?.FirstOrDefault(sub => sub.Value == id.ToString());
            int pageIndex = 1;
            var subjec = id;
            int pageSize = 20;
            pageIndex = page;

            ViewBag.CurrentSchool = sddc;
            IPagedList<SchoolEquipConfigViewModel> eqp = null;
            List<SchoolEquipConfigViewModel> eqpall =new List<SchoolEquipConfigViewModel>();
            var summaryList = clsContent.GetSchoolupdateEquipmentDigitValue(sddc);
            var schoolEqps = clsContent.GetSchoolEquipment(subjec);
            foreach (var summary in schoolEqps)
            {
                //SchoolFacilityValueID
                var schoolEqp = summaryList.Where(sum => sum.SchoolFacilityValueID == summary.FacilityID).FirstOrDefault();
                if (schoolEqp != null)
                {
                    summary.EquipmentValue = schoolEqp.EquipmentValue;
                    summary.DigitValue = schoolEqp.EquipmentValue>=summary.EquipmentValue?1:0;
                    eqpall.Add(summary);
                    //  schoolEqp.EquipmentValue = summary.EquipmentValue;
                    //schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).EquipmentValue = summary.EquipmentValue;
                    //schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).DigitValue = summary.DigitValue;
                }
                eqpall.Add(summary);
                // schoolEqp.EquipmentValue = 
            }
            //to load template for new school
            eqpall = eqpall.Distinct().ToList();
            eqp = eqpall.OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);

            //var school = clsContent.GetSchoolById(null);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.FacilityManagement = "active";
            ViewBag.totalgradess = eqpall.Sum(x => x.DigitValue);
            ViewBag.subjectid = subjec;
            ViewBag.FacilityManagement = "active";

            return View(eqp);
        }

        public ActionResult ReagentDetail(int id, int sddc, int page = 1)
        {
            ViewBag.Facility = "active";
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var subjectList = GetSchoolSubjectItems(sddc, id);
            ViewBag.subject = subjectList?.FirstOrDefault(sub => sub.Value == id.ToString());
            int pageIndex = 1;
            var subjec = id;
            int pageSize = 20;
            pageIndex = page;
            ViewBag.subjectid = id;
            IPagedList<ReagentViewModel> reag = null;
            var summaryList = clsContent.GetSchoolupdateReagentDigitValue(sddc);
            var schoolReagents = clsContent.GetSchoolReagent(subjec);
            foreach (var summary in summaryList)
            {

                var schoolReag = schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                if (schoolReag != null)
                {
                    schoolReag.ReagentValue = summary.ReagentValue;
                    schoolReag.DigitValue = summary.DigitValue;
                    schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                    //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                    //ViewBag.SchReagentID = schoolReag.SchReagentID;
                }
                //picks the last record
                // ViewBag.SchReagentID = schoolReag.SchReagentID;
            }

            reag = schoolReagents.OrderBy
                      (m => m.ID).ToPagedList(pageIndex, pageSize);

            ViewBag.SchReagentID = reag.AsEnumerable()
                          .Select(o => new ReagentViewModel()
                          {
                              SchReagentID = o.SchReagentID
                          }).ToList();
            ViewBag.totalgradess = reag.Sum(x => x.DigitValue);
            TempData["Message"] = CurrentUser.EmailAddress;
            //  ViewBag.SchoolSubjectList;

            ViewBag.FacilityManagement = "active";
            return View(reag);
        }



        public ActionResult LaboratorySummary(int totalLaboratoryItems, int subjectid, int facilityCategoryId, bool generalAssessment)
        {
            var schoo = Session.GetDataFromSession<string>("SchoolID");
            var schoolid = long.Parse(schoo);
            int decision = 0;
            decision = generalAssessment == true ? 1 : 0;

            var result = clsContent.SaveLaboratorySummary(totalLaboratoryItems, subjectid, facilityCategoryId, schoolid, decision);
            if (result.ID > 0)
            {
                return Json(new { status = true });
            }
            return Json(new { status = false });

        }


        public ActionResult LabDetail(int id, int sddc, int page = 1)
        {
            ViewBag.Facility = "active";
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var subjectList = GetSchoolSubjectItems(sddc, id);
            ViewBag.subject = subjectList?.FirstOrDefault(sub => sub.Value == id.ToString());
            int pageIndex = 1;
            var subjec = id;
            int pageSize = 20;
            pageIndex = page;

            IPagedList<LaboratoryViewModel> eqp = null;
            eqp = clsContent.GetSchoolLaboratoryForAssessment(subjec, sddc).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);

            //var labConfigViewModel = eqp.FirstOrDefault();


            //var school = clsContent.GetSchoolById(null);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.FacilityManagement = "active";

            return View(eqp);
        }

    }
}
