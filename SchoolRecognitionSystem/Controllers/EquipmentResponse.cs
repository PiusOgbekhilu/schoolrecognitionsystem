﻿namespace SchoolRecognitionSystem.Controllers
{
    public class EquipmentResponse
    {
        public long gradd { get; set; }
        public bool status { get; set; }
        public string definition { get; set; }
        public string message { get; set; }
        public string recordId { get; set; }
    }
}