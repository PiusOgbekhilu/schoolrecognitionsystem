﻿using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.Security.Claims;
using Microsoft.IdentityModel;
using System.Net.Mail;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Security;
using Microsoft.Ajax.Utilities;
using SchoolRecognitionSystem.Infrastructure;
using WebMatrix.WebData;

namespace SchoolRecognitionSystem.Controllers
{

    public class HomeController : BaseController
    {
        private void GetFormData()
        {
            IEnumerable<SelectListItem> objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");
            IEnumerable<SelectListItem> objRank = new SelectList(clsContent.GetRank(), "ID", "RankTitle");
            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");
            //IEnumerable<SelectListItem> objCouncilSubject = new SelectList(clsContent.GetCouncilSubjects(), "ID", "LongName");
            IEnumerable<SelectListItem> objOffice = new SelectList(clsContent.GetOffices(), "ID", "OfficeName");

            ViewBag.Rank = objRank;
            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            ViewBag.OfficeList = objOffice;

        }
        public ActionResult Index(string emailAddress, string returnUrl)
        {
            if (CurrentUser != null && CurrentUser.IsAuthenticated)
            {
                return RedirectToAction("SectionPage", "Schools");
            }
            bool isAd = false;
           ViewBag.EmailAddress = emailAddress;
           ViewBag.ReturnUrl = returnUrl;
            if (isAd)
            {
                return View("AD_Authentication");
            }
            else
            {
                return View();
            }
        }

        public ActionResult GeneralIndex()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult confirmation()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult AccessDenied()
        {
            ViewBag.Message = "NOT ACCESSIBLE";

            return View();
        }



        public ActionResult ForgotPassword()
        {
            return View();
        }

        //public ActionResult Login(string returnUrl)
        //{
        //    ViewBag.ReturnUrl = returnUrl;
        //    return View("Index");
        //}
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserViewModel user)
        {
            //var office = clsContent.GetOffices().FirstOrDefault(v => v.OfficeName == "YABA");

            try
            {

                if (ModelState.IsValid)
                {
                    var aUser = clsContent.GetAUserEmail(user.EmailAddress);

                    if (aUser.EmailAddress != null)
                    {
                        var encryptedPassword = clsEncryption.EncryptPassword(user.OldPassword);
                        if (aUser.Password.SequenceEqual(encryptedPassword))
                        {
                            var encryptedNewPassword = clsEncryption.EncryptPassword(user.NewPassword);

                            // var decyrptedNewPassword = clsEncryption.EncryptPassword(NewPassword);
                            clsContent.ChangePassword(user.EmailAddress, aUser.Password, encryptedNewPassword);
                            TempData["Message"] = "Password changed successfully";
                        }
                        else
                        {
                            TempData["Message"] = "Password not changed successfully";
                        }
                    }

                }
                else
                    TempData["Message"] = "Please provide missing entries before you proceed.";
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message;
            }

            return View(user);
        }

        // ChangePassword


        [HttpPost]
        [ValidateAntiForgeryToken]
       // [ValidateModelState]
        public ActionResult Login(string EmailAddress, string Password, string rememberMe, string returnUrl)
        {
            try
            {
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.EmailAddress = EmailAddress;
                if (ModelState.IsValid)
                {
                    //Get the Details from Database

                    //user-role table management

                    var user = clsUsers.GetUserLogin22(EmailAddress, Password, "");


                    //user table management
                    ////  var user = clsUsers.GetUserLogin(EmailAddress, Password);



                    //GetAUserRole
                    if (user == null)
                    {
                        ShowMessage("Invalid user credentials", AlertType.Danger);
                    }
                    else
                    {
                        TempData["TackUser"] = user.EmailAddress;
                        var javaScriptSerializer = new JavaScriptSerializer();
                        var userInfo = new CurrentUserModel
                        {
                            UserId = user.ID,
                            Surname = user.Surname,
                            OtherNames = user.OtherNames,
                            EmailAddress = user.EmailAddress,
                            OfficeId = user.OfficeID,
                            PhoneNumber = user.PhoneNumber,
                            RoleId = user.RoleID,
                            RoleName = user.Rolename,
                            IsActive = user.isActive
                        };
                        var userInfoJson = javaScriptSerializer.Serialize(userInfo);
                        var userInfoJsonBytes = Encoding.UTF8.GetBytes(userInfoJson);
                        var userInfoJsonBase64Encode = Convert.ToBase64String(userInfoJsonBytes);
                        if (rememberMe == null)
                        {
                            ConstantsService.CreateCookie(ConstantsService.CookieSchoolRecognitionTokenKey, userInfoJsonBase64Encode);
                        }
                        else
                        {
                            var dateTime = DateTime.Now.AddDays(30);
                            ConstantsService.CreateCookie(ConstantsService.CookieSchoolRecognitionTokenKey, userInfoJsonBase64Encode, dateTime);

                        }
                        if (!string.IsNullOrEmpty(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }

                        return RedirectToAction("SectionPage", "Schools");
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Error: " + ex.Message, AlertType.Danger);
            }
            return RedirectToAction("Index", "Home", new { EmailAddress = EmailAddress });
        }
        [HttpGet]
        private void GetSchoolFormData()
        {
            IEnumerable<SelectListItem> objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");

            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");
            //IEnumerable<SelectListItem> objCouncilSubject = new SelectList(clsContent.GetCouncilSubjects(), "ID", "LongName");



            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            //ViewBag.CouncilSubjectList = objCouncilSubject;
        }
      //  [SchoolRecognitionAuthorized]
        public ActionResult Register()
        {
           
            //Get Form Data
            GetFormData();
            ViewBag.Register = "active";
            // GetFormData();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
       // [SchoolRecognitionAuthorized]
        //[ValidateModelState]
        public ActionResult Register(RegisterUserViewModel user, HttpPostedFileBase addedSignature)
        {
            try
            {
                //Get Form Data
                GetFormData();
                if (ModelState.IsValid)
                {
                    if (addedSignature == null)
                    {
                      ShowMessage("Please upload your signature.", AlertType.Warning);
                        return View(user);
                    }
                    //var myImage = convr.
                    if (!addedSignature.ContentType.Equals("image/jpeg") && !addedSignature.ContentType.Equals("image/jpg") && !addedSignature.ContentType.Equals("image/png"))
                    {
                        ShowMessage("Only jpg or png signature images are supported..", AlertType.Warning);
                        return View(user);
                    }
                    var image = Image.FromStream(addedSignature.InputStream);
                    if (image.Width != 200 && image.Height != 50)
                    {
                        ShowMessage("Please resize your image to the dimension (width: 200px, height: 50px).", AlertType.Warning);
                        return View(user);
                    }
                   
                    //LOAD THE DATA ELEMENTS
                    user.ID = (user.ID != 0 || user.ID != null) ? user.ID : 0;
                    // var domain = user.EmailAddress.Split('@');
                    //if (domain[1].Equals("waec.org.ng"))
                    // {
                    var fileName = Guid.NewGuid().ToString("N") + ".jpg";
                    var filePath = Server.MapPath(ConstantsService.SignaturePath + fileName);
                   // filePath.Replace(@"\\", @"\");
                    addedSignature.SaveAs(filePath);
                    user.Signature = filePath;
                   

                    var result = clsUsers.AddUpdateUser(user);
                    
                    if (result != null)
                    {
                        var insertUserRole = clsUsers.AddUserIdRoleIDToUserRole(result.ID, result.RoleID, result.PhoneNumber, result.EmailAddress);
                        Session.SetDataInSession("UserID", string.Format("{0}", result.ID));
                        Session.SetDataInSession("UserEmail", result.EmailAddress);
                       
                          ViewBag.EmailMessage =clsContent.GetAUserEmail(result.EmailAddress);
                        //var returnemail = clsContent.SendEmailOnRegister(result.EmailAddress).
                        //  ContinueWith((ev) =>
                        //  {
                        //                                                                                   //DO somet

                        //  }); ;
                        //if (returnemail.IsCompleted)
                        //{
                        ShowMessage("You have successfuly created your profile. Check your waec email for activation to perform school recognition portal", AlertType.Success);
                        return RedirectToAction("confirmation");
                        // }
                        //else
                        //{
                        //    TempData["EmailMessage"] = "Email does not exist";
                        //}
                    }

                }
                else ShowMessage("Please provide missing entries before you proceed.", AlertType.Warning);

            }
            catch (Exception ex)
            {

                ShowMessage(ex.Message, AlertType.Warning);
            }
            return View(user);
        }

        public bool SendEmailOnActive(string superAdmin, string useremail) //emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            var url = new UrlHelper(ControllerContext.RequestContext);
            var userInfo = clsUsers.GetUserLogin(useremail);
            mess.From = new MailAddress(superAdmin, "Mail Testing");
            // mess.To.Add("ujimadiyi@waec.org.ng");
            mess.To.Add(useremail);
            //put te site link there
            mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            // mess.Body = "You have been activated. to login click <a href ="http://localhost:12027">
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(useremail, "welcome");
                smtpClient.Send(mess);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }



        public ActionResult SendEmail1(string emailmessage)//emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();

            mess.From = new MailAddress("paogbekhilu@waec.org.ng", "Mail Testing");
            // mess.To.Add("ujimadiyi@waec.org.ng");
            mess.To.Add(emailmessage);
            mess.Body = "please activate me";
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential("paogbekhilu@waec.org.ng", "welcome");
                // smtpClient.Send(mess);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            //var mess = new MailMessage("paogbekhilu@waec.org.ng", "juimadiyi@waec.org.ng");
            //mess.Body = "please activate me";
            //mess.Subject = "Activation";
            //mess.IsBodyHtml = false;
            //var client = new SmtpClient();
            //client.Port = 25;
            //client.EnableSsl = false;
            //client.Host = "mail.waec.org.ng";
            //var net = new NetworkCredential("paogbekhilu@waec.org.ng", "welcome");
            //client.UseDefaultCredentials = true;
            //client.Credentials = net;
            //client.Send(mess);
            ViewBag.Email = "Email has been sent successfully";
            return Json(new { message = "Email sent" }, JsonRequestBehavior.AllowGet);
            //Response.Write("Email sent"));
        }

        [HttpGet]
        public ActionResult AD_Authentication()
        {
            return View();
        }

        //private readonly IAuthenticationManager authenticationManager;
        [HttpPost]
        public ActionResult AD_Authentication(string EmailAddress, string Password, string rememberMe, string returnUrl)
        {
            UserPrincipal userPrincipal = null;
            
                var checkDomain = EmailAddress.Split('@');
                if (!checkDomain[1].Equals("waec.org.ng"))
                {
                    ShowMessage("You can only use waec email to login", AlertType.Danger);
                    return View();
                }
                    const ContextType authenticationType = ContextType.Domain;
                    const string domain = "waec.org.ng";
                    
            try
                        {
                var principalContext = 
                         new PrincipalContext(authenticationType, domain,EmailAddress, Password);
           
                             bool isAuthenticated = principalContext.ValidateCredentials(EmailAddress, Password, ContextOptions.Negotiate);
              
                if (!isAuthenticated)
                            {
                            ShowMessage("Invalid user credentials please contact the administrator", AlertType.Danger);
                            return View();
                            }

                                userPrincipal = UserPrincipal.FindByIdentity(principalContext, EmailAddress);
                                ViewBag.ReturnUrl = returnUrl;
                                var encryptedAuthPassword = clsEncryption.EncryptPassword(Password);
                                var existingUserProfile = clsUsers.GetUserLogin(EmailAddress);
                            if (userPrincipal == null)
                            {
                                ShowMessage("Invalid login credentials.", AlertType.Danger);
                                return View();
                            }
                                if (existingUserProfile == null) //always routed to go create profile
                                {
                                     TempData["EmailAddress"] = userPrincipal.EmailAddress;
                                    TempData["Password"] = Password;
                                    return RedirectToAction("Register");

                                }
                                    if (!existingUserProfile.isActive)
                                    {
                                    ShowMessage("please check your waec email to see if you have been activated",AlertType.Danger);
                                    return View();
                                       
                                    }
                                   
                                        
                                if (!existingUserProfile.Password.SequenceEqual(encryptedAuthPassword))
                                  {
                                    var updatePasswordChanged =
                                        clsContent.UpdateUserPasswordChanged(userPrincipal.EmailAddress,
                                            encryptedAuthPassword);
                                      if (updatePasswordChanged == null)
                                      {
                                        ShowMessage("Your credentials couldn't be updated.", AlertType.Danger);
                                        return View();
                                    }
                                    }
                                
                                    Session.SetDataInSession("AdminActivation",
                                        string.Format("{0}", existingUserProfile.EmailAddress));

                                    if (existingUserProfile.isAdmin == null)
                                    {
                                        Session.SetDataInSession("UserAdminID",
                                            string.Format("{0}", existingUserProfile.ID));

                                    }
                                    if (existingUserProfile.isSuperAdmin == null)
                                    {
                                        Session.SetDataInSession("SuperAdminID",
                                            string.Format("{0}", existingUserProfile.ID));
                                    }
                                    //Check whether Account has been approved
                                    Session.SetDataInSession("UserID", string.Format("{0}", existingUserProfile.ID));
                                     Session.SetDataInSession("EmailAddress", existingUserProfile.EmailAddress);
                                    Session.SetDataInSession("UserFullName",string.Format("{0}", existingUserProfile.EmailAddress));

                            var javaScriptSerializer = new JavaScriptSerializer();
                            var userInfo = new CurrentUserModel
                            {
                                UserId = existingUserProfile.ID,
                                Surname = existingUserProfile.Surname,
                                OtherNames = existingUserProfile.OtherNames,
                                EmailAddress = existingUserProfile.EmailAddress,
                                OfficeId = existingUserProfile.OfficeID,
                                PhoneNumber = existingUserProfile.PhoneNumber,
                                RoleId = existingUserProfile.RoleID,
                                RoleName = existingUserProfile.Rolename,
                                IsActive = existingUserProfile.isActive
                            };
                            var userInfoJson = javaScriptSerializer.Serialize(userInfo);
                            var userInfoJsonBytes = Encoding.UTF8.GetBytes(userInfoJson);
                            var userInfoJsonBase64Encode = Convert.ToBase64String(userInfoJsonBytes);
                            if (rememberMe == null)
                            {
                                ConstantsService.CreateCookie(ConstantsService.CookieSchoolRecognitionTokenKey, userInfoJsonBase64Encode);
                            }
                            else
                            {
                                var dateTime = DateTime.Now.AddDays(30);
                                ConstantsService.CreateCookie(ConstantsService.CookieSchoolRecognitionTokenKey, userInfoJsonBase64Encode, dateTime);

                            }
                            TempData["RoleName"] = existingUserProfile.Rolename;
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return RedirectToAction("SectionPage", "Schools");
            }
            catch (Exception ex)
            {
            ShowMessage(ex.Message, AlertType.Danger);;
            return View();
            }

        }





    }
}