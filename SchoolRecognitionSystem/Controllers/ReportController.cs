﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Models;
using SchoolRecognitionSystem.Pdf;
//using Computer_Manager.Models;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using SchoolRecognitionSystem.Classes;

namespace SchoolRecognitionSystem.Controllers
{
   // [Authorize(Roles = "Administrator , Entry&ScanningManager")]
    public class ReportController : Controller
    {
        private SchoolSubjectRecognitionContext db = new SchoolSubjectRecognitionContext();
       
  
        public ActionResult Route()
        {

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
