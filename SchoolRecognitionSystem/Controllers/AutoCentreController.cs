﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Classes;

namespace SchoolRecognitionSystem.Controllers
{
    public class AutoCentreController : Controller
    {
        // GET: AutoCentre
        public ActionResult Index(int page=1)
        {
            //var userId = int.Parse(Session.GetDataFromSession<string>("UserID").ToString());
            var allSchools = clsContent.GetSchoolsApproved(page, 20);
            TempData["Message"] = Session.GetDataFromSession<string>("UserFullName");
            ViewBag.ApprovedSchools = "active";
            return View(allSchools);
        }
        private void GetGenerateCentreFormData()
        {
            IEnumerable<SelectListItem> examType = new SelectList(clsContent.GetExamType(), "ID", "ExamType");

            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");
            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");
            IEnumerable<SelectListItem> objSchools = new SelectList(clsContent.GetSchoolsApproved(), "ID", "SchoolName");
            ViewBag.ExamType = examType;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            ViewBag.Schools = objSchools;
            //ViewBag.CouncilSubjectList = objCouncilSubject;
        }
        public ActionResult Create()
        {
            return View();
        }//GenerateCentre
        [HttpGet]
        public ActionResult GenerateCentre()
        {
           // var examtype = clsContent.GetExamType();
           // var getSchool = clsContent.GetSchoolByID(sddc);
            GetGenerateCentreFormData();
            ViewBag.GenerateCentre = "active";
            return View();
        }
    }
}