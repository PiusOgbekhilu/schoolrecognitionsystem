﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;

namespace SchoolRecognitionSystem.Controllers
{
    public class EditSchoolController : Controller
    {
        // GET: EditSchool
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult EditSchool(int sdc)
        {
            var getSchool = clsContent.GetSchoolByID(sdc);
            var objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName", getSchool.YearEstablished);
            var objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category", getSchool.CategoryID);
           // var objStates = new SelectList(clsContent.GetStates(), "Id", "StateName", getSchool.StateID);
            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.State = clsContent.GetStatesById(getSchool.StateID);
            ViewBag.Lgas = new SelectList(clsContent.GetLgas(getSchool.StateID), "ID", "Name", getSchool.LgaID);
            ViewBag.EditSchool = "active";
            return View(getSchool);
        }
        [HttpPost]
        public ActionResult EditSchool(SchoolProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var getSchool = clsContent.GetSchoolByID(model.ID);
                var objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName", getSchool.YearEstablished);
                var objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category",
                    getSchool.CategoryID);
                // var objStates = new SelectList(clsContent.GetStates(), "Id", "StateName", getSchool.StateID);
                ViewBag.AvailableYears = objYears;
                ViewBag.CategoryList = objSchCategory;
                ViewBag.State = clsContent.GetStatesById(getSchool.StateID);
                ViewBag.Lgas = new SelectList(clsContent.GetLgas(getSchool.StateID), "ID", "Name", getSchool.LgaID);
                return RedirectToAction("Details", new {schoolid = model.ID });
            }
            return View(model);
        }

        public ActionResult Details()
        {
            return View();
        }
        public ActionResult EditSubject()
        {
            return View();
        }
        public ActionResult EditTextbook()
        {
            return View();
        }
        public ActionResult EditClass()
        {
            return View();
        }
        public ActionResult EditStaff()
        {
            return View();
        }
        public ActionResult EditEquipment()
        {
            return View();
        }
        public ActionResult EditReagent()
        {
            return View();
        }
        public ActionResult EditLab()
        {
            return View();
        }
        public ActionResult EditLibrary()
        {
            return View();
        }
        public ActionResult EditExam()
        {
            return View();
        }
        public ActionResult EditRecreational()
        {
            return View();
        }

    }
}