﻿namespace SchoolRecognitionSystem.Controllers
{
    public class Response
    {
        public bool status { get; set; }
        public int staffId { get; set; }
        public long schid { get; set; }
        public int imageError { get; set; }
        public string imageErrorMess { get; set; }
        public string message { get; set; }
    }
}