﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Models;

namespace SchoolRecognitionSystem.Controllers
{
    public class BaseController : Controller
    {
        /// <summary>
        /// A global notification for users
        /// </summary>
        /// <param name="message">Message to display</param>
        /// <param name="alertType">Alert type to use</param>
        public void ShowMessage(string message, AlertType alertType)
        {
            TempData["notification"] = new NotificationViewModel(message, alertType);
        }
/// <summary>
/// Creates the current user infomation
/// </summary>
/// <returns></returns>
        public static CurrentUserModel CurrentUser 
        {
        get{
            var userInfoCookieBase64Token = ConstantsService.GetCookie(ConstantsService.CookieSchoolRecognitionTokenKey);
            if (userInfoCookieBase64Token != null)
            {
                    var base64EncodedBytes = Convert.FromBase64String(userInfoCookieBase64Token.Value);
                    var userInfoJson = Encoding.UTF8.GetString(base64EncodedBytes);
                    var javaScriptSerializer = new JavaScriptSerializer();
                    var userInfo = javaScriptSerializer.Deserialize<CurrentUserModel>(userInfoJson);
                    return userInfo;
                }
                    return new CurrentUserModel();

            }
        }
}
}