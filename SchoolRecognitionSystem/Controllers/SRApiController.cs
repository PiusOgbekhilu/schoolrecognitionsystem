﻿using PagedList;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;

namespace SchoolRecognitionSystem.Controllers
{
    public class SRApiController : ApiController
    {
        // GET api/<controller>
        public List<ReagentViewModel> GetReagentFacilityPerSchool(long schid, int? subjectid = 0, int page = 1)
        {
            var school = clsContent.GetSchoolById(schid);           
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            //GetSchoolSubjectItems(subjectid);
            pageIndex = page;
            List<ReagentViewModel> reag = null;
            if (subjec == 0)
            {
                reag = clsContent.GetSchoolReagent(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateReagentDigitValue(schid);
                var schoolReagents = clsContent.GetSchoolReagent(subjec);
                foreach (var summary in summaryList)
                {

                    var schoolReag = schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                    if (schoolReag != null)
                    {
                        schoolReag.ReagentValue = summary.ReagentValue;
                        schoolReag.DigitValue = summary.DigitValue;
                        schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                        //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                        //ViewBag.SchReagentID = schoolReag.SchReagentID;
                    }
                    //picks the last record
                    // ViewBag.SchReagentID = schoolReag.SchReagentID;
                }
                ClassAllocationModel classAlloc = clsContent.GetClassForProportion(schid);
                long totalstudents = classAlloc.TotalStudents;
                var outreagents = new List<ReagentViewModel>();
                foreach (var r in schoolReagents)
                {
                    if (totalstudents > 50)
                    {
                        decimal? t = (r.QuantityRequired / 50) * totalstudents;

                        r.QuantityRequired = Math.Abs((decimal)t);
                    }
                    outreagents.Add(r);
                }
                //reag = outreagents.OrderBy
                //          (m => m.ID).ToList();
                reag = schoolReagents.OrderBy
                          (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }
       
            return reag;


        }
        
        public List<ReagentViewModel> GetAllReagentFacilityPerSchool(long schid, int? subjectid = 0)
        {
            
            var school = clsContent.GetSchoolById(schid);
            var subjec = subjectid;
            List<ReagentViewModel> reag = null;
            if (subjec == 0)
            {
                reag = clsContent.GetSchoolReagent(subjec).OrderBy
                    (m => m.ID).ToList();// (pageIndex, pageSize).ToList();
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateReagentDigitValue(schid);
                var schoolReagents = clsContent.GetSchoolReagent(subjec);
                foreach (var summary in summaryList)
                {

                    var schoolReag = schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                    if (schoolReag != null)
                    {
                        schoolReag.ReagentValue = summary.ReagentValue;
                        schoolReag.DigitValue = summary.DigitValue;
                        schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                        //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                        //ViewBag.SchReagentID = schoolReag.SchReagentID;
                    }
                    //picks the last record
                    // ViewBag.SchReagentID = schoolReag.SchReagentID;
                }
                ClassAllocationModel classAlloc = clsContent.GetClassForProportion(schid);
                long totalstudents = classAlloc.TotalStudents;
                var outreagents = new List<ReagentViewModel>();
                foreach (var r in schoolReagents)
                {
                    if (totalstudents > 50)
                    {
                        decimal? t = (r.QuantityRequired/50)*totalstudents;

                        r.QuantityRequired =Math.Round((decimal)t,MidpointRounding.AwayFromZero);
                    }
                    outreagents.Add(r);
                }
                reag = outreagents.OrderBy
                          (m => m.ID).ToList();// ToPagedList(pageIndex, pageSize).ToList();
            }

            return reag;


        }

        public List<SchoolSubjectViewModel> GetSchoolSubjects(long schid)
        {
            var schy = clsContent.GetSchoolSubjectItems(schid).ToList();            
            return schy;
        }

        //[System.Web.Http.HttpPost]
        //public EquipmentResponse SaveReagent(long schid,int subjectIds, string dd, string user, int record_id = 0, int totalDbRecords = 0, int totalComputed = 0, int existingReagentValueId = 0)
        //{

        //    String result = String.Empty;

        //    //use the record id to get the ID from SchoolFacilityValue table since is unique
        //    //then use the ID and the schoolid from schoolequipmentvalue  to get the 
        //    //ID value from schoolequipmentvalue and then update the record
        //    //var schoolids = Session.GetDataFromSession<string>("SchoolID");
        //    var schoolid = schid;// long.Parse(schoolids);


        //    var uv = new ReagentViewModel();
        //    if (dd != null && user != null)
        //    {
        //        int facid = int.Parse(dd);
        //        var existingRecord = clsContent.FetchExistingReagentRecord(schoolid, facid);

        //        if (existingRecord == null)
        //        {
        //            if (record_id == 0)
        //            {
        //                uv = clsContent.AddReagentValue(facid, user, schoolid);

        //                if (uv.ReagentValue >= uv.QuantityRequired)
        //                {

        //                    var row = clsContent.GetGradeExcell("Excellent");
        //                    uv.FacilityID = row.ID;
        //                    uv.Definition = row.Definition;
        //                    uv.DigitValue = 1;
        //                    //   uv.DateEntry = DateTime.Now;
        //                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);
        //                    //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
        //                }
        //                else
        //                {
        //                    var row = clsContent.GetgradeVGood("Very Good");
        //                    uv.FacilityID = row.ID;
        //                    uv.Definition = row.Definition;
        //                    uv.DigitValue = 0;
        //                    // uv.DateEntry = DateTime.Now;
        //                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);

        //                    //very good
        //                }


        //                return
        //                    new EquipmentResponse
        //                        {
        //                            status = true,
        //                            gradd = uv.DigitValue,
        //                            definition = uv.Definition,
        //                            message = "Value saved successfully",
        //                            recordId = uv.SchoolReagentValueID.ToString()
        //                        };
        //            }

        //        }
        //        else
        //        {
        //            record_id = existingRecord.SchoolReagentValueID;
        //            if (record_id != 0)
        //            {

        //                //var sfv = clsContent.GetSchoolFacilityvalueID(record_id);
        //                //  vwSchoolFacilityValueModel SchFacValID = clsContent.GetSchoolFacilityValuebyRecordID(record_id);
        //                //GetSchoolEquipmentIDByRecordID

        //                //  var EquipID = clsContent.GetSchoolEquipmentIDByRecordID(SchFacValID.ID, schoolid);
        //                //record_id = existingReagentValueId;
        //                uv = clsContent.UpdateSchoolReagentValue(user, record_id);

        //                //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);

        //                if (uv.ReagentValue >= uv.QuantityRequired)
        //                {

        //                    var row = clsContent.GetGradeExcell("Excellent");
        //                    uv.FacilityID = row.ID;
        //                    uv.Definition = row.Definition;
        //                    uv.DigitValue = 1;
        //                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);

        //                }
        //                else
        //                {
        //                    var row = clsContent.GetgradeVGood("Very_Good");
        //                    uv.FacilityID = row.ID;
        //                    uv.Definition = row.Definition;
        //                    uv.DigitValue = 0;
        //                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);
        //                }
        //                return
                            
        //                        new EquipmentResponse
        //                        {
        //                            status = true,
        //                            definition = uv.Definition,
        //                            message = "Value updated",
        //                            recordId = uv.SchoolReagentValueID.ToString()
        //                        };

        //            }
        //        }

        //    }


        //    else
        //    {
        //        return new EquipmentResponse { status = false, message = "Value is required." };
        //    }

        //    return new EquipmentResponse { status = false, message = "Error occurred." };
        //}

        [System.Web.Http.HttpPost]
        public EquipmentResponse SaveReagent(int schoolid, ReagentViewModel item)
        {
            String result = String.Empty;

            //use the record id to get the ID from SchoolFacilityValue table since is unique
            //then use the ID and the schoolid from schoolequipmentvalue  to get the 
            //ID value from schoolequipmentvalue and then update the record
            //var schoolids = Session.GetDataFromSession<string>("SchoolID");
            //  var schoolid = schid;// long.Parse(schoolids);

            int record_id = 0;
            var uv = new ReagentViewModel();
            if (item == null)
            {
                return new EquipmentResponse
                {

                    message = "Invalid Item"
                };
            }

            var existingRecord = clsContent.FetchExistingReagentRecord(schoolid, item.SchReagentID);

            if (existingRecord == null)
            {
                uv = clsContent.AddReagentValue(item.SchReagentID, item.ReagentValue+"", schoolid);

                if (uv.ReagentValue >= uv.QuantityRequired)
                {

                    var row = clsContent.GetGradeExcell("Excellent");
                    uv.FacilityID = row.ID;
                    uv.Definition = row.Definition;
                    uv.DigitValue = 1;
                    //   uv.DateEntry = DateTime.Now;
                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);
                    //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                }
                else
                {
                    var row = clsContent.GetgradeVGood("Very Good");
                    uv.FacilityID = row.ID;
                    uv.Definition = row.Definition;
                    uv.DigitValue = 0;
                    // uv.DateEntry = DateTime.Now;
                    uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);

                    //very good
                }


                return
                    new EquipmentResponse
                    {
                        status = true,
                        gradd = uv.DigitValue,
                        definition = uv.Definition,
                        message = "Value saved successfully",
                        recordId = uv.SchoolReagentValueID.ToString()
                    };

            }
            else
            {
                record_id = existingRecord.SchoolReagentValueID;
                if (record_id != 0)
                {

                    //var sfv = clsContent.GetSchoolFacilityvalueID(record_id);
                    //  vwSchoolFacilityValueModel SchFacValID = clsContent.GetSchoolFacilityValuebyRecordID(record_id);
                    //GetSchoolEquipmentIDByRecordID

                    //  var EquipID = clsContent.GetSchoolEquipmentIDByRecordID(SchFacValID.ID, schoolid);
                    //record_id = existingReagentValueId;
                    uv = clsContent.UpdateSchoolReagentValue(item.ReagentValue+"", record_id);

                    //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);

                    if (uv.ReagentValue >= uv.QuantityRequired)
                    {

                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);

                    }
                    else
                    {
                        var row = clsContent.GetgradeVGood("Very_Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID, uv.DigitValue, schoolid);
                    }
                    return

                            new EquipmentResponse
                            {
                                status = true,
                                definition = uv.Definition,
                                message = "Value updated",
                                recordId = uv.SchoolReagentValueID.ToString()
                            };
                }
            }


            return new EquipmentResponse { status = false, message = "Error occurred." };
        }

        public List<SchoolEquipConfigViewModel> GetEquipmentFacilityPerSchool(long schid, int? subjectid = 0, int page = 1)
        {
            var school = clsContent.GetSchoolById(schid);
            var subjec = subjectid;
            int pageIndex = 1;
            int pageSize = 20;

            List<SchoolEquipConfigViewModel> eqp = null;
            if (subjec == 0)
            {
                eqp = clsContent.GetSchoolEquipment(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateEquipmentDigitValue(schid);
                var schoolEqps = clsContent.GetSchoolEquipment(subjec);
                foreach (var summary in summaryList)
                {
                    //SchoolFacilityValueID
                    var schoolEqp = schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID);
                    if (schoolEqp != null)
                    {
                        //  schoolEqp.EquipmentValue = summary.EquipmentValue;
                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).EquipmentValue = summary.EquipmentValue;

                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).DigitValue = summary.DigitValue;
                    }
                    // schoolEqp.EquipmentValue = 
                }
                //to load template for new school
                eqp = schoolEqps.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }

            return eqp;


        }

        public List<SchoolEquipConfigViewModel> GetAllEquipmentFacilityPerSchool(long schid, int? subjectid = 0)
        {
            var school = clsContent.GetSchoolById(schid);
            var subjec = subjectid;
            int pageIndex = 1;
            int pageSize = 20;

            List<SchoolEquipConfigViewModel> eqp = null;
            //eqp=clsContent.GetSchoolupdateEquipmentDigitValue(school.ID, subjec);
            if (subjec == 0)
            {
                eqp = clsContent.GetSchoolEquipment(subjec).OrderBy
                    (m => m.ID).ToList();//.ToPagedList(pageIndex, pageSize);
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateEquipmentDigitValue(schid);
                var schoolEqps = clsContent.GetSchoolEquipment(subjec);
                var schoolEqpslst = new List<SchoolEquipConfigViewModel>();
                for (int i=0;i<summaryList.Count;i++)
                {
                    var summary = summaryList[i];
                    //SchoolFacilityValueID
                    var schoolEqp = schoolEqps.Where(sum => sum.Unique_row == summary.SchoolFacilityValueID).FirstOrDefault();
                    if (schoolEqp != null)
                    {
                        schoolEqps.Remove(schoolEqp);
                        schoolEqp.EquipmentValue = summary.EquipmentValue;
                        schoolEqp.DigitValue = summary.DigitValue;
                        schoolEqps.Add(schoolEqp);
                        //schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).EquipmentValue = summary.EquipmentValue;
                        //schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).DigitValue = summary.DigitValue;
                    }
                    // schoolEqp.EquipmentValue = 
                }
                //to load template for new school
                eqp = schoolEqps.OrderBy
                    (m => m.ItemDescription).ToList();//.ToPagedList(pageIndex, pageSize);
            }
            int c=eqp.Sum(k => k.DigitValue);
            return eqp;


        }

        [System.Web.Http.HttpPost]
        public EquipmentResponse SaveEquipment(int schoolid, SchoolEquipConfigViewModel item)
        {
            if(item==null)
            {
                return new EquipmentResponse
                {
                  
                    message = "Invalid Item"
                };
            }
            SchoolEquipConfigViewModel uv = null;
            var lst = clsContent.GetSchoolupdateEquipmentDigitValue(schoolid);

            var uvl = lst.Where(x=> x.SchoolFacilityValueID==item.Unique_row).FirstOrDefault();
            if (uvl==null)
            {
                uv = clsContent.AddEQuipmentValue((int)(item.SubjectID==null?0: item.SubjectID), 
                    item.Unique_row, 
                    item.EquipmentValue+"", 
                    schoolid);
            }
            else
            {
                uv=clsContent.UpdateEquipFacilityValue(uvl.ID, (decimal)item.EquipmentValue,item.DigitValue);
            }
            if (uv == null)
            {
                return new EquipmentResponse
                {
                    message = "Invalid Item"
                };
            }
            if (uv.EquipmentValue >= uv.QuantityRequired)
            {
                var row = clsContent.GetGradeExcell("Excellent");
                uv.FacilityID = row.ID;
                uv.Definition = row.Definition;
                uv.DigitValue = 1;
                uv.DateEntry = DateTime.Now;
                uv = clsContent.UpdateFacility((int)item.SubjectID, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                    schoolid);
                //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
            }
            else
            {
                var row = clsContent.GetgradeVGood("Very Good");
                uv.FacilityID = row.ID;
                uv.Definition = row.Definition;
                uv.DigitValue = 0;
                uv.DateEntry = DateTime.Now;
                uv = clsContent.UpdateFacility((int)item.SubjectID, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                    schoolid);

                //very good
            }
            return new EquipmentResponse
            {
                gradd = uv.DigitValue,
                status = true,
                definition = uv.Definition,
                message = "Value saved successfully",
                recordId = uv.Equipmentvalue_ID.ToString()
            };
        }

        [System.Web.Http.HttpPost]
        public EquipmentResponse SaveEquipment(long schid, int bagg, int subjectIds, 
            int facCategoryId, string dd, string user,
          int record_id = 0, int totalDbRecords = 0,
          int totalComputed = 0)
        {
            String result = String.Empty;
            var schoolid = schid;
            var uv = new SchoolEquipConfigViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);


                    uv = clsContent.AddEQuipmentValue(subjectIds, facid, user, schoolid);

                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {
                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);
                        //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                    }
                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }
                    return
                            new EquipmentResponse
                            {
                                gradd = uv.DigitValue,
                                status = true,
                                definition = uv.Definition,
                                message = "Value saved successfully",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            };
                }
                else if (record_id != 0)
                {
                    var uservalue = decimal.Parse(user);
                    uv = clsContent.UpdateEquipFacilityValue(record_id, uservalue);

                    //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);
                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {

                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                    }

                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }

                    return
                        new EquipmentResponse
                        {
                            status = true,
                            definition = uv.Definition,
                            message = "Value updated",
                            recordId = uv.Equipmentvalue_ID.ToString()
                        };

                }
            }
            else
            {
                return new EquipmentResponse
                { status = false, message = "Value is required." };
            }

            return new EquipmentResponse
            { status = false, message = "Error occurred." };
        }
    }
}