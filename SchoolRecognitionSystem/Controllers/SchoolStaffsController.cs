﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using LinqToExcel;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Models;
using SchoolRecognitionSystem.Pdf;
using DataTable = System.Data.DataTable;

namespace SchoolRecognitionSystem.Controllers
{
    [SchoolRecognitionAuthorized]
    public class SchoolStaffsController : BaseController
    {
        //private RecogntionSystemContext db = new RecogntionSystemContext();

        //GET: SchoolStaffs
        public ActionResult Index()
        {
            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schoolid = long.Parse(schoolToken.Value);
            var schoolStaffs = clsContent.GetSchoolStaffs(schoolid);
            // db.SchoolStaffs.Include(s => s.Degree).Include(s => s.SchoolProfile).Include(s => s.SchoolStaffCategory);

            var schoolsummary = clsContent.GetSchoolSummary(schoolid);

            var noteachers = schoolsummary.Count(x => x.Category == "TEACHER");
            ViewBag.noteachers = noteachers;

            //GetSchoolStudentClass

            var noNCE = schoolsummary.Count(x => x.DegreeType == "NCE");
            ViewBag.noNCE = noNCE;

            var noHND = schoolsummary.Count(x => x.DegreeType == "HND");
            //from the point of entering classallocation data get the total student for each class via viewbag
            int classid = 2;

            var classstudentrecord = clsContent.GetSchoolStudentClass(schoolid, classid);

            int SchoolSS2figure = classstudentrecord.TotalStudents;

            var NoUniversityGrad =
                schoolsummary.Count(x => x.DegreeType == "BTECH." || x.DegreeType == "BSc." || x.DegreeType == "BEd.");

            ViewBag.StaffRecords = "active";
            return View(schoolStaffs.ToList());
        }

        //GET: SchoolStaffs/Details/5
        public ActionResult Details(int id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var schoolStaff = clsContent.GetSchoolStaffByID(id, schoolid);


            //GetSchoolSummary
            if (schoolStaff == null)
            {
                return HttpNotFound();
            }
            return View(schoolStaff);
        }


        public bool SaveFile(HttpPostedFileBase file)
        {
            bool tf = false;
            if (file != null)
            {
                string fileName = Path.GetFileName(file.FileName);
                ;
                string extension = System.IO.Path.GetExtension(fileName);
                if (extension == ".png")
                {

                    var path = Path.Combine(Server.MapPath("~/images/" + fileName));
                    file.SaveAs(path);

                    tf = true;

                }
            }
            return tf;


        }

        private void GetStaffSubjectToClassFormData(long schoolId)
        {
            //var schoolSubjectRecord = clsContent.GetSchoolSubjectsByTextbook(schoolid);
            var staffSubject2Classes = clsContent.GetStaffSubjectClasses(schoolId);
            var subject2Classes = new List<SelectListItem>();
            foreach (var subjectClass in clsContent.GetStaffSubjectClasses(schoolId))
            {
                if (staffSubject2Classes == null)
                {
                    subject2Classes.Add(new SelectListItem
                    {
                        Text = subjectClass.ClassName,
                        Value = subjectClass.StaffSubjectToClassID.ToString()
                        //FirstName= subjectClass.FirstName,
                        //LongName=subjectClass.LongName,
                        //ClassName=subjectClass.ClassName,
                        //ID=subjectClass.ID,
                        //SubjectID=subjectClass.SubjectID


                    });
                }
                else
                {
                    if (staffSubject2Classes.FirstOrDefault(sc => sc.StaffSubjectToClassID == subjectClass.StaffSubjectToClassID) == null)
                    {
                        subject2Classes.Add(new SelectListItem
                        {
                            Text = subjectClass.ClassName,
                            Value = subjectClass.StaffSubjectToClassID.ToString()
                            //FirstName = subjectClass.FirstName,
                            //LongName = subjectClass.LongName,
                            //ClassName = subjectClass.ClassName,
                            //ID = subjectClass.ID,
                            //SubjectID = subjectClass.SubjectID,
                            //StaffSubjectToClassID = subjectClass.StaffSubjectToClassID


                        });
                    }
                }
            }
            //ViewBag.Classes = subjects;
            ViewBag.StaffSubject2Classes = staffSubject2Classes;

        }
        public ActionResult DeleteStaffSubject(int staffsubjectid,long schoolid)
        {
            var returnedvalue = clsContent.DeleteStaffSubjectX(staffsubjectid);
            var staffsubjects = clsContent.GetStaffsSubjects(schoolid);
            ViewBag.StaffSubjectList = staffsubjects;
            if (returnedvalue)
            {
                //return Json(new
                //{
                //    redirectUrl = Url.Action("StaffSubject", "SchoolStaffs"),
                //    isRedirect = true
                //});
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return Json(new { status = true, redirectUrl = Url.Action("StaffSubject", "SchoolStaffs"), tokenId = staffsubjectid, JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });

        }
        //DeleteStaffSubjectClass
        public ActionResult DeleteStaffSubjectClass(int token)
        {
            var returnedvalue = clsContent.DeleteStaffSubjectClass(token);
            if (returnedvalue)
            {
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return Json(new { status = true, tokenId = token, JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });

        }
        public ActionResult SaveStaffClass(int staffId = 0, int classId = 0, int sub = 0, int staffSubjectRowId = 0)
        {
            //staffid subjectid staffsubjecttoclassid classname


            var getRowid = clsContent.GetStaffSubjectRowId(staffId, sub);
            ViewBag.StaffSubjectRowId = getRowid;
            var teacher2Class = clsContent.CheckIfClassAddedForTeacher(staffId, classId, sub);
            if (classId < 1)
            {
                TempData["SelectClass"] = "Please select a class";
                return Json(new { status = 1, selectdata = TempData["SelectClass"] }, JsonRequestBehavior.AllowGet);
            }
            if (teacher2Class)
            {
                TempData["ClassAlreadyAdded2Staff"] = "Class is already assigned to staff for the selected subject";


                return Json(new { status = 2, xxtdata = TempData["ClassAlreadyAdded2Staff"] }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var insertedClass = clsContent.InsertAStaffSubjectClass(getRowid.StaffSubjectRowId, classId);
                string fullName = insertedClass.FirstName + " " + insertedClass.LastName;
                if (insertedClass != null)
                {
                    return Json(new
                    {
                        status = true,
                        classId = insertedClass.ClassID,
                        subjectId = insertedClass.SubjectID,
                        firstName = fullName,
                        Id = insertedClass.StaffSubjectToClassID,
                        staffsubjecttoclassid = insertedClass.StaffSubjectToClassID

                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = false, xxtdata = TempData["ClassAlreadyAdded2Staff"] }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult StaffSubjectClass(int staffId = 0, int subjectId = 0)
        {
            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schid = long.Parse(schoolToken.Value);
            var school = clsContent.GetSchoolById(schid);
            var getRowid = clsContent.GetStaffSubjectRowId(staffId, subjectId);
            //fetch staffsubjcettoclassid
            // var staffsubjcettoclassid=clsContent.row
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;

            // clsContent.GetStaffSubjectClasses(schid);
            GetStaffSubjectToClassFormData(schid);
            var allStaff = new SelectList(clsContent.GetStaffBySchoolId(schid).ToList().Select(s => new {
                FullName =
                $"{s.FirstName} {s.LastName}",
                FID = s.ID
            }), "FID", "FullName", staffId);
            //   var allStaff= new SelectList(clsContent.GetStaffBySchoolId(schid), "ID", "FirstName", staffId);
            ViewBag.ClassID = new SelectList(clsContent.GetClass(schid), "ID", "ClassName");
            var allStaffToSubject = clsContent.GetSubjectByStaffSchoolID(schid, staffId);

            var allStaffSubjects = new SelectList(allStaffToSubject, "SubjectID", "LongName", subjectId);
            List<SchoolStaffModel> eqp = null;
            ViewBag.Staffs = allStaff;
            ViewBag.SchoolID = schid;
            ViewBag.StaffSubjects = allStaffSubjects;
            ViewBag.StaffSubjectClass = "active";
            eqp = clsContent.GetStaffMembers(schid);
          //  TempData["DisplaySchoolName"] = school.SchoolName;//GetSubjectByStaffSchoolID
            TempData["Message"] = CurrentUser.EmailAddress;// CurrentUser.EmailAddress;
            return View(eqp);
        }

        public ActionResult SchooStafflRecords()
        {
            var schoolId = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schid = long.Parse(schoolId.Value);

            var school = clsContent.GetSchoolById(schid);
            var schoolStaffListForView = new List<SchoolStaffList>();
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            var staffList = clsContent.GetStaffBySchoolId(schid);
            foreach (var staff in staffList)
            {
                //GetStaffClassesByStaffId
                //  var schoolStaffClassList = clsContent.GetStaffClassesByStaffId(staff.ID);
                var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySTaffId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffId(staff.ID);
                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                var credentials = clsContent.GetStaffCredentials(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    // SchoolStafflassList=schoolStaffClassList,
                    SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,
                    Credentials = credentials

                });
            }
            TempData["Message"] = CurrentUser.EmailAddress;// CurrentUser.EmailAddress;
            ViewBag.SchooStafflRecords = "active";
            return View(schoolStaffListForView);
        }

        public ActionResult StaffSubject()
        {
            var schoolId = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schid = long.Parse(schoolId.Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
           // var staffDegreeCourse = clsContent.GetStaffDegreeCourse()
            ViewBag.SchoolStaffName = new SelectList(clsContent.GetSchoolStaffBySchoolId(schid), "ID", "SchoolStaffName");
            ViewBag.SubjectIDss = clsContent.GetSchoolSubjects(schid, true).Select(a => new SelectListItem
            {
                Text = a.LongName,
                Value = a.SubjectID.ToString()
            });
            //ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            //ViewBag.CourseID = new SelectList(clsContent.GetCourse(), "CoursID", "CourseTitle");
            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolsubjects = clsContent.GetSchoolSubjects(schid);
            var staffsubjects = clsContent.GetStaffsSubjects(schid);
            ViewBag.StaffSubjectList = staffsubjects;
            var schoolSubjectTableID = new List<long>(); 
            var listOfSubjects = new List<SelectListItem>();
            foreach (var schoolSubject in schoolsubjects)
            {
                // var xx = Tuple.Create(schoolSubject.ID, schoolSubject.SubjectID, schoolSubject.LongName);

                schoolSubjectTableID.Add(schoolSubject.ID);
                listOfSubjects.Add(new SelectListItem
                {
                    Text = schoolSubject.LongName,
                    Value = schoolSubject.SubjectID.ToString()
                });

            }
            var model = new vmSchoolStaffQualification();
            model.SchoolSubjectID = schoolSubjectTableID;
            ViewBag.SubjectListing = schoolsubjects;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.StaffSubject = "active";
            //GetSchoolStaffBySchoolID

            return View(model);
        }

        [HttpPost]
        public ActionResult StaffSubject(vmSchoolStaffQualification model)
        {
            var schoolId = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schid = long.Parse(schoolId.Value);
            TempData["response"] = null;
            var school = clsContent.GetSchoolById(schid);
           TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.SchoolStaffName = new SelectList(clsContent.GetSchoolStaffBySchoolId(schid), "ID", "SchoolStaffName");
            ViewBag.SubjectIDss = clsContent.GetSchoolSubjects(schid, true).Select(a => new SelectListItem
            {
                Text = a.LongName,
                Value = a.SubjectID.ToString()
            });
            var staffsubjects = clsContent.GetStaffsSubjects(schid);
            ViewBag.StaffSubjectList = staffsubjects;
            var schoolsubjects = clsContent.GetSchoolSubjects(schid);
            ViewBag.SubjectListing = schoolsubjects;
            if (model.SubjectIDss != null)
            {
                var staffrowid = int.Parse(model.SchoolStaffName);
                foreach (var su in model.SubjectIDss)
                {

                    //insert in table addstaffsubject(v,staffid)
                    var staffSubjectsReturned = clsContent.AddStaffSubject(staffrowid, su, false);
                    //staffSubjectListSecond = clsContent.AddStaffSubject2(ret.ID, su);

                }
                

            }
            var response = new Response
            {
                status = true,
                staffId = model.ID,
                message = "Staff subjects submitted successfully."

            };
            TempData["response"] = "<script>alert('Staff subjects submitted successfully.');</script>";
            ModelState.Clear();
            return RedirectToAction("StaffSubject");
            //ModelState.Clear();

            // return View(new vmSchoolStaffQualification());
            // return Json(new { status = true, staffId = model.ID,schid= schoolid, message = "Staff subjects submitted successfully." },
            //           JsonRequestBehavior.AllowGet);


        }
        // GET: SchoolStaffs/Create
        public ActionResult Create()
        {

            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schoolid = long.Parse(schoolToken.Value);
            //ViewBag.SchoolID = schoolid;
            var school = clsContent.GetSchoolById(schoolid);
            if (school == null)
            {
                return HttpNotFound();
            }
            ViewBag.SchoolID = schoolid;
            ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
            ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
            ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
            ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");
             var model = new vmSchoolStaffQualification();
            model.Credentials = new List<SchoolStaffModel>();
            TempData["Message"] = CurrentUser.EmailAddress;
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;

            ViewBag.StaffRecords = "active";
            // ViewBag.HTab_Create = "active";
            return View(model);

        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(vmSchoolStaffQualification schoolStaff, string otherDegree)
        {
            if (Request.Files.Count < 1)
            {
                 return Json(new {status = false, message = "Please upload staff credentials before submitting"}, JsonRequestBehavior.AllowGet);
            }
            
            using (var sc = new TransactionScope())
                {
                     var staff = clsContent.AddSchoolStaff(schoolStaff);
                    if (staff.ID > 0)
                    {
                        if (schoolStaff.DegreeID[0] != 0)
                        {
                            foreach (var d in schoolStaff.DegreeID)
                            {
                              clsContent.AddStaffDegree(staff.ID, d, schoolStaff.CoursID);
                            }
                        }
                        if (otherDegree != null)
                        {
                            var otherdegree = otherDegree.Split(',');
                            foreach (var d in otherdegree)
                            {
                                clsContent.AddSecondDegree(staff.ID, d);
                            }
                        }
                    }
                    var files = Request.Files;
                    if (files.Count > 0)
                    {
                        for (var i = 0; i < files.Count; i++)
                        {
                            var file = files[i];
                            var fileName = Guid.NewGuid().ToString("N") + ".jpg";
                            if (file == null) continue;
                            file.SaveAs(Server.MapPath("~/images/" + fileName));
                            clsContent.AddStaffCredentials(staff.ID, fileName);
                        }
                    }
               
                    var response = new Response
                    {
                        status = true,
                        staffId = staff.ID,
                        schid = schoolStaff.SchoolID,
                        message = "Staff details submitted successfully."

                    };
                    sc.Complete();
                    ConstantsService.CreateCookie(ConstantsService.CookieManageStaffIdKey, staff.ID.ToString());
                    return Json(response, JsonRequestBehavior.AllowGet);



            }

        }
    
        public ActionResult PrintReport(long sdc)
        {
            var subjectData = clsContent.GetCoreSubjectsViewBySchoolId(sdc);

            // var vwReportModel = new List<vwReportModel>();
            //Get all subjects
            var objSubjects = clsContent.GetSubjectBySchool(sdc);

            //Get core subjects
            var objSubjectListing = objSubjects.Where(c => c.IsCore == true).OrderBy(c => c.SubjectID).ToList();

            var footMessage = clsContent.GetFooterReportMessage();

            var sortedFootMessage = footMessage.Where(xc => xc.SchoolID == sdc).ToList();

            var tradeCoreSubject = objSubjects.Where(c => c.IsTrade == true).OrderBy(c => c.SubjectID).ToList();
            if (tradeCoreSubject.Count > 0)
                objSubjectListing.Add(new vwSchoolSubject()
                {
                    Category = "CORE SUBJECT(S)",
                    IsCore = true,
                    IsTrade = true,
                    SchoolID = tradeCoreSubject[0].SchoolID,
                    SubjectID = tradeCoreSubject[0].SubjectID,
                    LongName = tradeCoreSubject[0].LongName,
                    SubjectCode = tradeCoreSubject[0].SubjectCode
                });

            //get other subjects
            var objOtherSubject =
                objSubjects.Where(c => c.IsCore == false && !objSubjectListing.Exists(d => d.SubjectID == c.SubjectID))
                    .OrderBy(c => c.SubjectID)
                    .ToList();

            foreach (var subj in objOtherSubject)
            {
                objSubjectListing.Add(subj);
            }
            //sortedFootMessage
            var otherReportDetail = clsContent.GetSchoolReportDetails(sdc);
            var reportPath = Path.Combine(Server.MapPath("~/Report"), "SchoolLetter.rpt");
            DataTable tblSchool = RemoveNullableClass.ToDataTable(otherReportDetail);
            DataTable tblSchoolSubjects = RemoveNullableClass.ToDataTable(objSubjectListing);
            DataTable tblsortedFootMessage = RemoveNullableClass.ToDataTable(sortedFootMessage);
            return new CrystalReportPdfResult(reportPath, tblSchool, tblSchoolSubjects, tblsortedFootMessage);
        }
        [HttpGet]
        public ActionResult MailAttachment(int x=0)
        {

            return View(new MailModel());

        }
        [HttpPost]
        public async Task<ActionResult> MailAttachmentToSupervisor(long schoolId)
        {

            //var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            //var schoolid = long.Parse(schoolToken.Value);
            var getSchool = clsContent.GetSchoolById(schoolId);
            var staffCount = clsContent.GetSchoolStaffCountX(schoolId);
            var body = new StringBuilder();
            body.Append("<div class='school-wrapper'>");
            body.Append("<div>");
            body.Append("<h3> <span style='color: black'>"+getSchool.SchoolNameEdit+"</span> is <span class='text-success'>successfully inspected</span></h3>");
            body.Append(
                "<p style='color: black;font-weight:bold'> The number of staff in school is <span class='badge' style='font-weight:bold'>"+staffCount.StaffCount+"</span></p>");
            body.Append("</div>");
            body.Append("<br />");
            body.Append("<h2>Subject with required Facilities</h2>");
            body.Append("<table id='searchtable' class='table table-bordered table-striped'>");
            body.Append("<thead>");
            body.Append("<tr style='background-color:lightgrey'>");

            body.Append("<th>School Subject(s)</th>");
            body.Append("<th>Equipment Required</th>");
            body.Append("<th>Equipment Observed</th>");
            body.Append("<th>Reagent Required</th>");
            body.Append("<th>Reagent Observed</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            //  body.Append("@foreach (var item in (IEnumerable<vwSchoolSubjectFacilityReport>)ViewBag.Facilityreport)");
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(schoolId);
            foreach (var itemx in schoolsubjectFacility)
            {
                // body.Append("{");
                body.Append("<tr>");
                body.Append("<td>" + itemx.LongName + "</td>");
                body.Append(" <td>" + itemx.Total_Equipment + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolEquipment + "</td>");
                body.Append(" <td>" + itemx.Total_Reagent + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolReagent + "</td>");
                //  body.Append(" <td>@item.TotalSchoolReagent</td>");
                body.Append("</tr>");
            }
            // body.Append("}");
            body.Append("</tbody>");
            body.Append(" </table>");
            body.Append("<table class='table table - bordered table - striped'>");
            body.Append("<thead>");
            body.Append("<tr>");
            body.Append("<th style='background-color: lightgrey' colspan='3'>SUBJECTS OFFERED BY THE SCHOOL</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            body.Append("<tr>");

            var counter = 1;
            //  var tableBody = "";
            var schoolSubjects = clsContent.GetSchoolSubjectReport(schoolId);
           // var schoolSubjects = clsContent.GetSchoolSubjects(schoolId);// (IEnumerable<vwSchoolSubjectReport>)ViewBag.SchoolSubjects;
            var totalItems = schoolSubjects.Count();
            var rows = Math.Ceiling((double)totalItems / 3);
            var remainderColumns = totalItems % rows;
            var rowsDisplayed = 0;
            //body.Append("<td>" + rows + "</td>");
            //body.Append("<td>" + remainderColumns + "</td>");
            //body.Append("<td>" + totalItems + "</td>");
            body.Append("</tr>");
            foreach (var item in schoolSubjects)
            {

                if (counter <= 3)
                {
                    if (counter == 1)
                    {
                        // tableBody += "<tr>";
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        body.Append("<tr>");
                        body.Append("<td>" + item.LongName + "</td>");
                        counter++;
                    }
                    else
                    {
                        body.Append("<td>" + item.LongName + "</td>");
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        if (counter == 3)
                        {
                            // tableBody += "</tr>";
                            body.Append("</tr>");
                            counter = 1;
                        }
                        else
                        {
                            counter++;
                        }

                    }
                }

            }
            // @Html.Raw(tableBody)
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append("<div class='pull-right' style='margin-right: 20px;' >");
            var supervisorSignPathArray = staffCount.SupervisorSign.Split('\\');
            var supervisorSign = supervisorSignPathArray[supervisorSignPathArray.Length - 1];
            //        <br>
            //        <br>
            //        <div class="col-md-5" style="margin-top: 35px;">
            //            <label>Signature: </label>
            //        </div>
            body.Append("<div class='col-md-6'><img id='signature-img' src='http:172.21.13.44/Content/images/signatures/"+ supervisorSign + "' width='200' height='50'>");

            //        <div class="col-md-6"><img id="signature-img" src="http:172.21.13.44/Content/images/signatures/3b63e6f75d5940ff85f0ec005b0ff468.jpg" width="200" height="50">
            //        </div>
            //        <div class="clearfix"></div>
            //        <hr style="margin-top: 10px;">
            //        <p>Zonal coordinator / Branch controller / Officer in charge</p>
            //    </div>
            body.Append(" </div>");


            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            //string SMTP_SERVER = "smtp.gmail.com";
            //int SMTP_PORT =587;

            var mess = new MailMessage();
            mess.IsBodyHtml = true;
            var userEmail = CurrentUser.EmailAddress;
           // var userEmailInfo = clsContent.GetAUserInfoForSupervisor(userEmail);
            mess.From = new MailAddress(userEmail, "SCHOOL INSPECTION SUMMARY REPORT");
            //GetSEDEmail 
            var getschool = clsContent.GetSchoolByID(schoolId);

            var supervisorInfo = clsContent.procGetSupervisorRoleOffice(int.Parse(getschool.OfficeID));
            mess.To.Add(supervisorInfo.EmailAddress);
            mess.Body = body.ToString();
            mess.Subject = "School Recognition";
            mess.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(supervisorInfo.EmailAddress, "welcome");
                smtpClient.Send(mess);
                ConstantsService.RemoveCookie(ConstantsService.CookieSchoolIdKey);
                ConstantsService.RemoveCookie(ConstantsService.CookieClassIdKey);
                ConstantsService.RemoveCookie(ConstantsService.CookieExaminationIdKey);
                ConstantsService.RemoveCookie(ConstantsService.CookieSubjectSchoolId);
                ConstantsService.RemoveCookie(ConstantsService.CookieFacilityManagementIdKey);
                ConstantsService.RemoveCookie(ConstantsService.CookieLibraryIdKey);

               string schoolStatus = "Completed";
                clsContent.UpdateSchoolProfileStatus(schoolId, schoolStatus);
                ShowMessage("School created successfully and sent to supervisor.", AlertType.Success);
                return RedirectToAction("NewSchool","Schools");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            ShowMessage("Error occurred while sending school to supervisor.", AlertType.Danger);
            return RedirectToAction("Finish", "SchoolStaffs"); ;

        }

       



        public ActionResult PrintReportX()
        {
            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schoolid = long.Parse(schoolToken.Value);
            // var vwReportModel = new List<vwReportModel>();
            //Get all subjects 
            var schoolSubjectFacilityRep = clsContent.GetSchoolSubjectFacilityReport(schoolid);

            var schoolsubjectReport = clsContent.GetSchoolSubjectReport(schoolid);
            
            //sortedFootMessage
            var otherReportDetail = clsContent.GetSchoolAndStaffReportDetails(schoolid);
            var reportPath = Path.Combine(Server.MapPath("~/Report"), "OfficeLetter.rpt");
            DataTable tblSchool = RemoveNullableClass.ToDataTable(otherReportDetail);
            DataTable tblSchoolSubjects = RemoveNullableClass.ToDataTable(schoolSubjectFacilityRep);
            DataTable tblschoolsubjectReport = RemoveNullableClass.ToDataTable(schoolsubjectReport);
            return new CrystalReportPdfResult(reportPath, tblSchool, tblSchoolSubjects, tblschoolsubjectReport);
        }
        public ActionResult Finish()
        {
            //set schoolprfile and centre file status either completed or pending
            // by default it is pending 
            //centre file could be pending for subject recognition


            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schoolid = long.Parse(schoolToken.Value);
            var userEMail = CurrentUser.EmailAddress;
            var sedEmailAddress = clsContent.GetSEDEmail();
            
            var userEmailInfo = clsContent.GetAUserInfoForSED(userEMail);
            var stateofSchool = clsContent.GetStatesByUser(userEmailInfo.ID);
            ViewBag.UserEmail = userEmailInfo.EmailAddress;
            var getschool = clsContent.GetSchoolByID(schoolid);
           // getschool.DateRecognition=DateTime.Now;
            //var dateofInspection = clsContent.UpdateDateRecognisedForSchool(getschool.DateRecognition,schoolid);
            //sp_GetSchoolStaffCount

            ViewBag.SchoolID = schoolid;
            var getSchool = clsContent.GetSchoolById(schoolid);
            ViewBag.SchoolName = getSchool.SchoolNameEdit;
            var staffCount = clsContent.GetSchoolStaffCountX(schoolid);
            ViewBag.StaffNumber = staffCount.StaffCount;
            //procSchoolSubectsReport
            //procSchoolSubectsFacilityReport
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(schoolid);
            ViewBag.Facilityreport = schoolsubjectFacility;

           var schoolsubject = clsContent.GetSchoolSubjectReport(schoolid);
            ViewBag.SchoolSubjects = schoolsubject;
            // var modelList=new List<SchoolSubjectCompletionReportModel>();
            
            TempData["DisplaySchoolName"] = getSchool.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            //ConstantsService.RemoveCookie(ConstantsService.CookieSchoolIdKey);
            //ConstantsService.RemoveCookie(ConstantsService.CookieClassIdKey);
            //ConstantsService.RemoveCookie(ConstantsService.CookieExaminationIdKey);
            //ConstantsService.RemoveCookie(ConstantsService.CookieSubjectSchoolId);
            //ConstantsService.RemoveCookie(ConstantsService.CookieFacilityManagementIdKey);
            //ConstantsService.RemoveCookie(ConstantsService.CookieLibraryIdKey);
            ViewBag.Finish = "active";

            return View();
        }

        public ActionResult _schoolSubjects(long schid)
        {
            var data = clsContent.GetSchoolSubjects(schid);
            return PartialView(data);
        }
        public ActionResult _schoolStaffs(long schid)
        {
            var data = clsContent.GetSchoolStaffBySchoolId2(schid);
            return PartialView(data);
        }

        [HttpPost]
        public ActionResult DeleteImage(int credentialId)
        {
            //retrieve image path from database and delete from  GetAStaffCredential
            var credential = clsContent.GetAStaffCredential(credentialId);
            var filePath = Server.MapPath(ConstantsService.CredentialPath  +  credential.Image);
            var deleted = clsContent.DeleteStaffCredentials(credentialId);
            if (deleted != null)
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                return Json(new { status = true, message = "Credential deleted successfully." }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, message = "Error occurred while deleting credential." }, JsonRequestBehavior.AllowGet);


        }

        public ActionResult AddImageToCredentials(HttpPostedFileBase file, string combinedSecondDegree, int staffId = 0)
        {
            if (file == null || !file.ContentType.Equals("image/jpeg"))
            {
                return Json(new { status = false, mesage = "Only image file is allowed." }, JsonRequestBehavior.AllowGet);
            }
            /** 
             * TODO Create precedure to save image
             */
            var staffImage = new SchStaffCredential();
            string failureMessage = String.Empty; string dbpath2 = "";
            if (file.ContentLength != 0)
            {

                string filePath = "";
               
                var fileName = Path.GetFileName(file.FileName);
                filePath = @"\images\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                var path = Path.Combine(Server.MapPath(@"~\images\" +fileName));
                dbpath2 = @"~\images\" + fileName;
               // staffImage = clsContent.AddStaffCredentials(staffId, "/images/" + fileName);

                staffImage = clsContent.AddStaffCredentials(staffId, dbpath2);



                if (staffImage != null)
                {
                    file.SaveAs(path);
                }
                else
                {
                    failureMessage = "file failed to upload";
                }

                //file.SaveAs(Path.Combine(filePath, file.FileName));

            }

            //return Json(new { status = true, CredentialID = staffImage.ID, Image = "/images/" + file.FileName, failedReply = failureMessage }, JsonRequestBehavior.AllowGet);

            return Json(new { status = true, CredentialID = staffImage.ID, Image =dbpath2, failedReply = failureMessage }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult AddImageToCredentialsX(HttpPostedFileBase file, string combinedSecondDegree, int staffId = 0)
        {
            if (file == null || !file.ContentType.Equals("image/jpeg"))
            {
                return Json(new { status = false, mesage = "Only image file is allowed." }, JsonRequestBehavior.AllowGet);
            }
            /** 
             * TODO Create precedure to save image
             */
            var staffImage = new SchStaffCredential();
            string failureMessage = String.Empty; string dbpath2 = "";
            if (file.ContentLength != 0)
            {

                string filePath = "";

                var fileName = Path.GetFileName(file.FileName);
                filePath = @"\images\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                var path = Path.Combine(Server.MapPath(@"~\images\" + fileName));
               // dbpath2 = @"~\images\" + fileName;
                // staffImage = clsContent.AddStaffCredentials(staffId, "/images/" + fileName);

                staffImage = clsContent.AddStaffCredentials(staffId, dbpath2);



                if (staffImage != null)
                {
                    file.SaveAs(path);
                }
                else
                {
                    failureMessage = "file failed to upload";
                }

                //file.SaveAs(Path.Combine(filePath, file.FileName));

            }

            //return Json(new { status = true, CredentialID = staffImage.ID, Image = "/images/" + file.FileName, failedReply = failureMessage }, JsonRequestBehavior.AllowGet);

            return Json(new { status = true, CredentialID = staffImage.ID, Image = dbpath2, failedReply = failureMessage }, JsonRequestBehavior.AllowGet);

        }

        //  GET: SchoolStaffs/Edit/5
        public ActionResult Edit(int id)
        {
            var sddc = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
             var schoolStaffEdit = clsContent.GetSchoolStaffForEdit(id, sddc);
            var schoolStaff = clsContent.GetStaffByAStaffID2(id);
            if (schoolStaffEdit == null || schoolStaff == null)
            {
                ShowMessage("The sfatt you want to edit was not found.", AlertType.Danger);
                return RedirectToAction("SchooStafflRecords", new {sddc = sddc});
            }
            var listofstrings = clsContent.GetStaffOtherDegreeByStaffId(id).Select(e => string.Join(",", e.OtherDegree));
            if (schoolStaff.OtherDegree != "")
            {
                schoolStaff.OtherDegree = string.Join(",", listofstrings); 
            }
           
            schoolStaff.Credentials = clsContent.GetStaffCredentialss(id);
            var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffId(schoolStaff.ID);

            ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label", schoolStaffEdit.TitleID);
            ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType", schoolStaffEdit.DegreeID);
           
            ViewBag.StaffSubject = schoolStaffSubjectTaughtList.Skip(1).ToList();
            var subjectmodel = clsContent.GetSubjectEdit();
            ViewBag.SubjectID = new SelectList(clsContent.GetSubject(), "ID", "LongName", schoolStaffEdit.SubjectID);

            ViewBag.SchoolSubjectID = new SelectList(clsContent.GetSchoolSubjects(sddc, true), "ID", "LongName", schoolStaffEdit.SubjectID);

            ViewBag.SubjectIdListbox = schoolStaffEdit.SubjectID;
            ViewBag.AllSubjects = subjectmodel;
            ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle", schoolStaffEdit.CoursID);
             ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
            ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category", schoolStaffEdit.CategoryID);
            ViewBag.StaffRecords = "active";
            TempData["Message"] = CurrentUser.EmailAddress;
            return View(schoolStaff);
        }

        //POST: SchoolStaffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(vmSchoolStaffQualification schoolStaff)
        {
           
            if (Request.Files.Count < 1)
            {
                 return Json(new {status = false, message = "Please upload staff credentials before submitting"}, JsonRequestBehavior.AllowGet);
            }
            
            using (var sc = new TransactionScope())
                {
                    // var staff = clsContent.AddSchoolStaff(schoolStaff);
                    var result = clsContent.UpdateSchoolStaffByID(schoolStaff.ID, schoolStaff.SchoolID, schoolStaff.TitleID, schoolStaff.FirstName, 
                        schoolStaff.LastName, schoolStaff.DateEmployed, schoolStaff.CategoryID, schoolStaff.TRCNText);
                    var degrees = clsContent.UpdateSchoolStaffDegreeByID(result.ID, schoolStaff.DegreeID[0], schoolStaff.CoursID);

                    if (result.ID > 0)
                    {
                        if (schoolStaff.OtherDegree != null)
                        {
                            var otherdegree = schoolStaff.OtherDegree.Split(',');
                            clsContent.DeleteSecondDegree(schoolStaff.ID);
                            foreach (var d in otherdegree)
                            {
                              clsContent.AddSecondDegree(result.ID, d);
                            }
                        }
                    }
                    var files = Request.Files;
                    var credentials = clsContent.GetStaffCredentials(result.ID);
                    credentials?.ForEach(credential =>
                    {
                        var filePath = Server.MapPath(ConstantsService.CredentialPath  +  credential.Image);
                        if (System.IO.File.Exists(filePath))
                        {
                            System.IO.File.Delete(filePath);
                        } 
                    });
                    var deleteCredentialResult = clsContent.DeleteStaffCredentialsByStaffID(result.ID);
                    if (files.Count > 0)
                    {
                        for (var i = 0; i < files.Count; i++)
                        {
                            var file = files[i];
                            
                            
                            
                            
                            var fileName = Guid.NewGuid().ToString("N") + ".jpg";
                            if (file == null) continue;
                            file.SaveAs(Server.MapPath(ConstantsService.CredentialPath  + fileName));
                            clsContent.AddStaffCredentials(result.ID, fileName);
                        }
                    }
               
                    var response = new Response
                    {
                        status = true,
                        staffId = result.ID,
                        schid = schoolStaff.SchoolID,
                        message = "Staff details edited successfully."

                    };
                    sc.Complete();

                    return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult DeleteStaff(int staffid = 0)
        {

            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schoolid = long.Parse(schoolToken.Value);
            var deleteStaff = clsContent.DeleteStaff(staffid, schoolid);
            // var staffsubjects = clsContent.GetStaffsSubjects(schoolid);
            //ViewBag.StaffSubjectList = staffsubjects;
            if (deleteStaff > 0)
            {
                //return Json(new
                //{
                //    redirectUrl = Url.Action("StaffSubject", "SchoolStaffs"),
                //    isRedirect = true
                //});
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return RedirectToAction("SchooStafflRecords", "SchoolStaffs", new { sddc = schoolid });
                //return Json(new { status = true,sddc=sddc, redirectUrl = Url.Action("SchoolClassList", "SchoolsEdit",new {sddc=sddc}), JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });

        }

    }
}
