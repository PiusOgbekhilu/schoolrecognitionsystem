﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Models;

namespace SchoolRecognitionSystem.Controllers
{
    public class StudentClassController : Controller
    {
        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();
        // GET: StudentClass
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreateStudentInClass()
        {
            var Classes = ctx.Classes.ToList();
            ViewBag.Classes = Classes.Select(schcat => new SelectListItem
            {
                Text = schcat.ClassName,
                Value = schcat.ID.ToString()
            });
            return View();
        }

        [HttpPost]
        public ActionResult CreateStudentInClass(ClassAllocation classalloc)
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;

            var Classes = ctx.Classes.ToList();
            ViewBag.Classes = Classes.Select(schcat => new SelectListItem
            {
                Text = schcat.ClassName,
                Value = schcat.ID.ToString()
            });

            ClassAllocation cls = new ClassAllocation();
            cls.ClassID = 1;
            cls.NoOfStreams = classalloc.NoOfStreams;
            cls.SchoolID = schoolid;
            cls.TotalStudents = classalloc.TotalStudents;
            ctx.ClassAllocations.Add(cls);
            ctx.SaveChanges();
            return View();
        }

        [HttpGet]
        public ActionResult CreateClassroomInfo()
        {
            int schoolid = 2;
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;
            return View();
        }
        [HttpPost]
        public ActionResult CreateClassroomInfo(ClassRoom cr)
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());

            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName =  sch.SchoolName;
            ClassRoom crm = new ClassRoom();
            crm.SchoolID = schoolid;
            crm.NumberofClassroomInUse = cr.NumberofClassroomInUse;
            crm.CapacityOfEachClassRoom = cr.CapacityOfEachClassRoom;
            crm.DeskChairsAdequate = cr.DeskChairsAdequate;
            ctx.ClassRooms.Add(crm);
            ctx.SaveChanges();
            return View();
        }

        [HttpGet]
        public ActionResult CreateExaminationInfo()
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName =  sch.SchoolName;
            return View();
        }
        [HttpPost]
        public ActionResult CreateExaminationInfo(ExaminationHall exh)
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ExaminationHall exam = new ExaminationHall();
            exam.SchoolID = schoolid;
            exam.NumberofDesksAndChairsAvailableInExamHalls = exh.NumberofDesksAndChairsAvailableInExamHalls;
            exam.NumberofExaminationHall = exh.NumberofExaminationHall;
            exam.CapacityofEachExaminationsHall = exh.CapacityofEachExaminationsHall;
            ctx.ExaminationHalls.Add(exam);
            ctx.SaveChanges();
            return View(exam);
        }
    }
}