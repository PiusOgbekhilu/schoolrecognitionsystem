﻿using PagedList;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Data.Entity;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using System.Globalization;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.WebSockets;
using System.Transactions;
using System.Xml.Linq;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Pdf;
using SchoolRecognitionSystem.Report;
using WebGrease.Css.Extensions;
using RemoveNullableClass = SchoolRecognitionSystem.Classes.RemoveNullableClass;

namespace SchoolRecognitionSystem.Controllers
{
    //[AuthorizeAccess(Roles = "admin,super_admin")]
    [SchoolRecognitionAuthorized]
    public class SchoolsController : BaseController
    {
        private SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();

        public ActionResult ReadCentres()
        {
            var centres = System.IO.File.ReadAllLines(Server.MapPath("~/WAEC/CentreFile.txt"));
            var formattedCentres = new List<SchoolCentre>();
            foreach (var centre in centres)
            {
                var real = centre.Substring(2);
                var centreNumber = real.Substring(0, 7);
                var centreName = real.Substring(8);
                formattedCentres.Add(new SchoolCentre {CentreName = centreName, CentreNumber = centreNumber});
                clsContent.InsertSchoolCentre(centreName, centreNumber);
                //if existing dont save else save

            }

            ViewBag.Centres = centres;
            return View(formattedCentres);
        }


        private void GetSchoolSubjectItems(int? subjectid)
        {
            var schoo = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schoolid = long.Parse(schoo);
            var schy = clsContent.GetSchoolSubjectItems(schoolid).Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;


        }


        private void GetSubjectItemsFormData(int? subjectid)
        {
            var schy = clsContent.GetCouncilSubjectsItems().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;

        }

        private void GetClassFormData()
        {
            //GetStudentClassRecord

            IEnumerable<SelectListItem> classes = new SelectList(clsContent.GetClass(), "ID", "ClassName");

            ViewBag.Classes = classes;

        }





        private void GetRecognitionTypeFormData()
        {
            IEnumerable<SelectListItem> TypeRec = new SelectList(clsContent.GetSchoolRecogntion(), "ID",
                "TypeofRecognition");

            ViewBag.TypeofRecognition = TypeRec;
        }

        private SchoolSubjectViewModel GetFacilityFormData()
        {
            return new SchoolSubjectViewModel
            {
                AvailableSubjects = clsContent.GetCouncilSubjects(),
                RequestedSubjects = new List<SubjectModel>()
            };
        }

        private void GetSchoolStaffFormData()
        {
            //  IEnumerable<SelectListItem> titles = new SelectList(clsContent.GetTitle(), "TitleID", "TitleName");
            //  IEnumerable<SelectListItem> staffcatgory = new SelectList(clsContent.GetSchoolStaffCategory(), "DegreeID", "Category");
            //  IEnumerable<SelectListItem> Degrees = new SelectList(clsContent.GetSchoolStaffCategory(), "CategoryID", "Category");
            //IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetCourse(), "ID", "Category");


            //return new SchoolSubjectViewModel { AvailableSubjects = clsContent.GetCouncilSubjects(), RequestedSubjects = new List<Subject>() };
        }

        private void GetSchoolFormData()
        {
            IEnumerable<SelectListItem> objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");

            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");

            //IEnumerable<SelectListItem> objCouncilSubject = new SelectList(clsContent.GetCouncilSubjects(), "ID", "LongName");



            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            //ViewBag.CouncilSubjectList = objCouncilSubject;
        }

        public ActionResult SendEmail(Gmail gmail)
        {
            var mess = new MailMessage("paogbekhilu@waec.org.ng", gmail.To);
            mess.Body = gmail.Body;
            mess.Subject = gmail.Subject;
            mess.IsBodyHtml = false;
            var client = new SmtpClient();
            client.Port = 25;
            client.EnableSsl = true;
            client.Host = "mail.waec.org.ng";
            var net = new NetworkCredential("paogbekhilu@waec.org.ng", "welcome");
            client.UseDefaultCredentials = true;
            client.Credentials = net;
            client.Send(mess);
            ViewBag.Email = "Email has been sent successfully";
            return View();
        }

        private void GetStaffFormData()
        {
            IEnumerable<SelectListItem> objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");

            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");
            //IEnumerable<SelectListItem> objCouncilSubject = new SelectList(clsContent.GetCouncilSubjects(), "ID", "LongName");



            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            //ViewBag.CouncilSubjectList = objCouncilSubject;
        }

        public ActionResult DefineOption()
        {
            ViewBag.DefineOptions = "active";
            return View();

        }

        public ActionResult ActionPerform()
        {
            ViewBag.ActionPerform = "active";
            return View();

        }


        [HttpGet]
        public ActionResult SubjectInfo(int subject_id, long schoolid)
        {

            TempData["Message"] = CurrentUser.EmailAddress;

            var AschoolSubjectInfo = clsContent.GetAASchoolSubjectApprovedBySchoolID(subject_id, schoolid);


            return Json(new
            {
                status = true,
                hasitem = AschoolSubjectInfo.HasItem,
                adequateEquipment = AschoolSubjectInfo.AdequateEquipment,
                adequateReagent = AschoolSubjectInfo.AdequateReagent,
                adequateLaboratory = AschoolSubjectInfo.AdequateLaboratory,
                adequateQualifiedStaff = AschoolSubjectInfo.QualifiedStaff,
                adequateExam = AschoolSubjectInfo.ExaminationMalpractice

            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SubjectDerecogntion(long sdc)
        {

            TempData["Message"] = CurrentUser.EmailAddress;

            var school = clsContent.GetSchoolById(sdc);
            var reasonForDerecogition = clsContent.ReasonsForDerecognition();
            ViewBag.SchoolIdd = sdc;
            ViewBag.ReasonForDerecogition = reasonForDerecogition;
            var subjectmodel = clsContent.GetSchoolSubjectApprovedBySchoolId(sdc);
            ViewBag.SideSubjects = clsContent.GetSchoolSubjectApprovedBySchoolId(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.UpdateSubject = "active";
            ViewBag.SchoolId = sdc;
            ViewBag.Subjectmodel = new SelectList(clsContent.GetSchoolSubjectApprovedBySchoolId(sdc), "SubjectID",
                "LongName");
            // var subjectmodel = new SelectList(clsContent.GetSchoolSubjectApprovedBySchoolId(sdc), "SubjectID", "LongName");

            return View(subjectmodel);
        }

        [HttpPost]
        public ActionResult SubjectDerecogntion(SchoolSubjectViewModel model)
        {
            //TempData["Message"] = CurrentUser.EmailAddress;

            //var school = clsContent.GetSchoolById(model.SchoolID);
            //var reasonForDerecogition = clsContent.ReasonsForDerecognition();
            //ViewBag.ReasonForDerecogition = reasonForDerecogition;
            var subjectmodel = clsContent.GetSchoolSubjectApprovedBySchoolId(model.SchoolID);
            //TempData["DisplaySchoolName"] = school.SchoolName;
            //ViewBag.UpdateSubject = "active";
            //ViewBag.SchoolId = model.SchoolID;
            //ViewBag.Subjectmodel = new SelectList(clsContent.GetSchoolSubjectApprovedBySchoolId(model.SchoolID), "SubjectID", "LongName");
            // var subjectmodel = new SelectList(clsContent.GetSchoolSubjectApprovedBySchoolId(sdc), "SubjectID", "LongName");

            return View(subjectmodel);
        }

        public ActionResult GetSchoolByValidCentre(string centreNumber)
        {
            if (!string.IsNullOrEmpty(centreNumber))
            {
                var returnValue = new SchoolProfileViewModel1();
                try
                {

                    returnValue = clsContent.GetSchoolByValidCentre(centreNumber);

                }
                catch
                {

                }
                if (returnValue != null)
                    return
                        Json(
                            new
                            {
                                exists = true,
                                school = returnValue.SchoolLocalGovName,
                                ID = returnValue.ID
                                //schools = returnValue.Select(item => new { item.ID, item.SchoolLocalGovName })
                            },
                            JsonRequestBehavior.AllowGet);
            }
            return Json(new {exists = false}, JsonRequestBehavior.AllowGet);


        }

        public ActionResult GetSchoolByCentre(string centreNumber)
        {
            if (!string.IsNullOrEmpty(centreNumber))
            {
                var returnValue = new List<SchoolProfileViewModel1>();
                try
                {

                    returnValue = clsContent.GetSchoolByCentre(centreNumber);

                }
                catch
                {

                }

                if (returnValue != null)
                    return
                        Json(
                            new
                            {
                                exists = true,
                                schools = returnValue.Select(item => new
                                {
                                    item.ID,
                                    item.CentreName,
                                    item.CentreNo,
                                    item.SchoolName,
                                    item.SchoolLocation,
                                    item.Name,
                                    item.StateName,
                                    item.EmailAddress,
                                    item.WaecStaff,
                                    item.isRecognised,
                                    item.Status
                                }).ToList()
                            },
                            JsonRequestBehavior.AllowGet);
            }
            return Json(new {exists = false}, JsonRequestBehavior.AllowGet);


        }

        //[AuthorizeAccess(Roles = "Admin,SuperAdmin")]

        public ActionResult SearchCentre()
        {

            // TempData["Message"] = CurrentUser.EmailAddress;

            return View();
        }

        public ActionResult SearchSchool()
        {

            // TempData["Message"] = CurrentUser.EmailAddress;

            return View();
        }

        // [AuthorizeAccess(Roles = "SuperAdmin")]
        public ActionResult SearchPage()
        {

            // TempData["Message"] = CurrentUser.EmailAddress;

            return View();
        }

        //  [AuthorizeAccess(Roles = "User,SuperAdmin")]

        public ActionResult StartPage()
        {
            TempData["Message"] = CurrentUser.EmailAddress;

            return View();
        }

        //DefineOptionact
        public ActionResult _DefineOption()
        {
            // GetSchoolByCentre
            //var schoolname = clsContent.GetSchoolByCentre(centrenumber);
            ViewBag.ActionPerform = "active";
            return PartialView();

        }

        public ActionResult _ActionPerform()
        {
            // GetSchoolByCentre
            //var schoolname = clsContent.GetSchoolByCentre(centrenumber);
            ViewBag.ActionPerform = "active";
            return PartialView();
        }


        //ActionPerform
        private SubjectModel GetCouncilSubjectFormData()
        {
            var subjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList();
            var compulsorySubjects = (from sub in subjects
                where
                    sub.LongName.ToLower().Equals("CIVIC EDUCATION".ToLower()) ||
                    sub.LongName.ToLower().Equals("ENGLISH LANGUAGE".ToLower()) ||
                    sub.LongName.ToLower().Equals("MATHEMATICS".ToLower())
                select sub.ID)
                .Select(dummy => (int?) dummy).ToList();
            return new SubjectModel
            {
                AvailableSubjects = subjects,
                AvailableSelected = compulsorySubjects,
                // RequestedSubjects = new List<Subject>()
            };
        }

        //SelectedSchoolSubjectViewModel
        //SchoolSubjectViewModel
        //private SchoolSubjectViewModel GetSchoolSubjectsById(long? schoolId)
        //{
        //    var subjectsAddedBySchool = clsContent.GetSchoolSubjectById(schoolId);
        //    var subjectIds = subjectsAddedBySchool.Select(item => item.SubjectID).Select(dummy => (int?) dummy).ToList();
        //    return new SchoolSubjectViewModel
        //    {
        //        AvailableSubjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList(),
        //        AvailableSelected = subjectIds,
        //        SchoolID = schoolId
        //        //RequestedSubjects = new List<Subject>()
        //    };
        //}


        // GET: Schools

        public ActionResult Index(int page = 1)
        {
            var userid = CurrentUser.UserId;
            var getauser = clsContent.GetAUser(userid);
            var relatedSchools = clsContent.GetCentres(page, 500, getauser.StateID);
            TempData["AllCenters"] =
                TempData["Message"] = CurrentUser.EmailAddress;
            return View(relatedSchools);
        }

       

        public ActionResult LogOut()
        {
            Session.Abandon();
            FormsAuthentication.SignOut();
            ConstantsService.RemoveCookie(ConstantsService.CookieSchoolRecognitionTokenKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieSchoolIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieClassIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieSubjectSchoolId);
            ConstantsService.RemoveCookie(ConstantsService.CookieExaminationIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieFacilityManagementIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieLibraryIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieManageStaffIdKey);
            ConstantsService.RemoveCookie(ConstantsService.CookieRecreationalIdKey);
             Session.Abandon();
             Session.Clear();
            bool isAd = false;
            //if (isAd)
            //{
            //    return RedirectToAction("AD_Authentication", "Home");
            //}
            //else
            //{
            return RedirectToAction("AD_Authentication", "Home");
            // }

        }

        public ActionResult EditSchool(int id)
        {
            return View();
        }

        public ActionResult RenderSelectLga(string selectedId = "")
        {
            var model = clsContent.GetLgas(selectedId);
            ViewBag.selectedId = selectedId;
            return PartialView(model);
        }

        //[AuthorizeAccess(Roles = "Admin,SuperAdmin")]
        public ActionResult _SchoolProfile()
        {
            var schoolToken = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            long schid = 0;
            if (!string.IsNullOrEmpty(schoolToken?.Value))
            {
             schid =  long.Parse(schoolToken.Value);
            }

            ViewBag.UserID = CurrentUser.UserId;
            var email = CurrentUser.EmailAddress;
            var getSchool = clsContent.GetSchoolByIDEdit(schid);
            var userinfo = clsContent.GetUser(email);
            TempData["StateName"] = userinfo.OfficeName;
            TempData["StateID"] = userinfo.OfficeID;
            ViewBag.StateIDD = userinfo.StateCode;
            if (getSchool != null)
            {
                Session.Add("LGA", getSchool.LgaID);
            }
            else
            {
                Session.Add("LGA", -2);
            }
            GetSchoolFormData();
            TempData["Message"] = "Welcome " + CurrentUser.EmailAddress;
            if (getSchool == null)
            {
                return PartialView();
            }

            return PartialView(getSchool);

        }


        public ActionResult NewSchool()
        {
            
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolProfile = "active";
            return View();
        }

        [HttpPost]
        public JsonResult AddNewSchool(SchoolProfileViewModel schoolprofile)
        {
            try
            {
                var school = new SchoolProfileViewModel();
                var returnedschool = clsContent.AddNewSchool(schoolprofile);

                Session.SetDataInSession("ReturnedSchool", string.Format("{0}", returnedschool.ID));

                return Json(new {schoolid = returnedschool.ID,}, JsonRequestBehavior.AllowGet);
                // return Json("Records added Successfully.");
            }
            catch
            {
                return Json("Records not added,");
            }
        }

        [HttpPost]
        public JsonResult SaveSchoolProfile(SchoolProfileViewModel schoolprofile)
        {
            //ViewBag.Lgas = clsContent.GetLgas(schoolprofile.StateID).Select(s => new SelectListItem
            //{
            //    Text = s.Name,
            //    Value = s.LgaID.ToString()
            //}).ToList();
            //  var sessionedSchool =long.Parse(Session.GetDataFromSession<string>("SchoolID"));
            var httpCookie = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            if (httpCookie == null)
            {

                var ret = clsContent.AddNewSchool(schoolprofile);
                if (ret == null) 
                return Json(new {message = "Error occurred", status = false});
               // Session.SetDataInSession("SchoolID", string.Format("{0}", ret.ID));
                ConstantsService.CreateCookie(ConstantsService.CookieSchoolIdKey, ret.ID.ToString());
                return Json(new { schoolId = ret.ID, message = "School profile created successfully", status = true });

            }

            else
            {
                var sessionedSchoolID = long.Parse(httpCookie.Value);
                var getSchool = clsContent.GetSchoolByID(sessionedSchoolID);
                schoolprofile.ID = sessionedSchoolID;
                getSchool.SchoolLocation = schoolprofile.SchoolLocation;
                getSchool.SchoolName = schoolprofile.SchoolName;
                getSchool.StateID = schoolprofile.StateID;
                getSchool.AproximateLandArea = schoolprofile.AproximateLandArea;
                getSchool.LgaID = schoolprofile.LgaID;
                getSchool.YearEstablished = schoolprofile.YearEstablished;
                getSchool.CategoryID = schoolprofile.CategoryID;
                getSchool.IsJointProprietorship = schoolprofile.IsJointProprietorship;
                getSchool.IsAdmissionRegisterAvailable = schoolprofile.IsAdmissionRegisterAvailable;
                getSchool.IsLandAreaAdequate = schoolprofile.IsLandAreaAdequate;
                getSchool.IsScheemOfWorkAvailable = schoolprofile.IsScheemOfWorkAvailable;
                getSchool.IsSchoolFencedRound = schoolprofile.IsSchoolFencedRound;
                getSchool.isClassRegistersAvaialable = schoolprofile.isClassRegistersAvaialable;
                getSchool.isCurricullumAvailable = schoolprofile.isCurricullumAvailable;
                getSchool.isDiaryofWorksAvaialble = schoolprofile.isDiaryofWorksAvaialble;
                var updatedSchool = clsContent.UpdateNewSchoolProfile2(getSchool);
                ConstantsService.CreateCookie(ConstantsService.CookieSchoolIdKey, updatedSchool.ID.ToString());
                Session.Add("NewSchool", getSchool);
                Session.Add("LGA", getSchool.LgaID);
                return Json(new { schoolId = getSchool.ID, message= "School profile updated successfully", status = updatedSchool != null });
            }




        }

        public ActionResult _GeneralSummaryPartialView()
        {

            ;

            return PartialView();

        }

        public ActionResult _SS2Student()
        {


            return PartialView();

        }

        public ActionResult _NCE()
        {


            return PartialView();

        }

        public ActionResult _UniversityHnd()
        {


            return PartialView();

        }

        public ActionResult FetchStudent(int classid)
        {
            return PartialView("_StudentTable");
        }

        //FetchStudent
        public ActionResult _noteachers()
        {


            return PartialView();

        }


        //GetSchoolPreview1
        public ActionResult GeneralSummary()
        {

            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);


            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            //var principalschool = clsContent.GetSchoolNameAndPrincipal(schid);

            //TempData["PrincipalName"] = principalschool.FirstName+' '+principalschool.LastName;



            var degreeHolder = clsContent.CountDegreeHolder(schid);
            if (degreeHolder != null)
            {
                TempData["NumberOfDegreeHolder"] = degreeHolder.NumberOfDegreeHolder;
            }
            var nonDegreeHolder = clsContent.CountNonDegreeHolder(schid);
            if (nonDegreeHolder != null)
            {
                TempData["nonDegreeHolder"] = nonDegreeHolder.CountNonDegreeHolder;
            }

            if (degreeHolder != null && nonDegreeHolder != null)
            {
                var totalTeachers = degreeHolder.NumberOfDegreeHolder + nonDegreeHolder.CountNonDegreeHolder;
                TempData["totalTeachers"] = totalTeachers;
            }


            var GetSchoolSS2 = clsContent.GetSchoolSs2(schid);
            if (GetSchoolSS2 != null && GetSchoolSS2 != null)
            {
                TempData["GetSchoolSS2"] = GetSchoolSS2.TotalStudents;
            }

            ViewBag.GeneralDeatils = "active";
            clsContent.GetGeneralSummary(schid);
            var xx = clsContent.GetGeneralSummary(schid);

            return View(xx);

        }

        [HttpPost]
        public ActionResult CheckIfSchoolNameExists(string schoolname, int local)
        {
            SchoolProfile returnValue = new SchoolProfile();
            try
            {

                returnValue = clsContent.CheckIfSchoolNameExists(schoolname, local);

            }
            catch
            {

            }
            if (returnValue != null)
                return Json(new {exists = true}, JsonRequestBehavior.AllowGet);

            return Json(new {exists = false}, JsonRequestBehavior.AllowGet);


        }



        public ActionResult Subjects()
        {
            var schoolId = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey);
            var schid = long.Parse(schoolId.Value);
            var subjectSchoolIdSessionCookie = ConstantsService.GetCookie(ConstantsService.CookieSubjectSchoolId);
            if (subjectSchoolIdSessionCookie == null)
            {   
                var school = clsContent.GetSchoolById(schid);
                TempData["DisplaySchoolName"] = school.SchoolNameEdit;
                var subjectmodel = GetCouncilSubjectFormData();
                ViewBag.RecommendedSubjects = "active";
                ConstantsService.CreateCookie(ConstantsService.CookieSubjectSchoolId, schid.ToString());
                // return Json(new { status = result, subjectadded = result }, JsonRequestBehavior.AllowGet);
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(subjectmodel);
            }
            else
            {
                var schoolid = long.Parse(subjectSchoolIdSessionCookie.Value);
                var subjectmodel = GetSchoolSubjectsById(schoolid);
                ViewBag.SchoolID = schoolid;
                ViewBag.RecommendedSubjects = "active";
                TempData["Message"] = CurrentUser.EmailAddress;
                var school = clsContent.GetSchoolById(schoolid);
                // return Json(new { status = result, subjectadded = result }, JsonRequestBehavior.AllowGet);
                TempData["DisplaySchoolName"] = school.SchoolNameEdit;
                return View(subjectmodel);

            }
        }

        [HttpPost]
        public ActionResult Subjects(SubjectModel schoolsubjects)
        {
            var subjectSchoolIdSessionCookie = ConstantsService.GetCookie(ConstantsService.CookieSubjectSchoolId);
            var schoolids = long.Parse(subjectSchoolIdSessionCookie.Value);
            var schlists = new List<SubjectModel>();
            if (schoolsubjects.AvailableSelected == null)
            {
                return
                    Json(
                        new
                        {
                            status = false,
                            message =
                                "Please select English, Maths, Civic, One traded subject and any other subjects. Remember: All must be at leats 8 subjects."
                        }, JsonRequestBehavior.AllowGet);
            }

            for (var i = 0; i < schoolsubjects.AvailableSelected.Count; i++)
            {
                var sub = clsContent.GetSelectedSubjectById(schoolsubjects.AvailableSelected[i]);
                schlists.Add(sub);
            }

            var compulsorySubject = clsContent.GetCompulsorySubjects();
            var tradedSubject = clsContent.GetTradeSubjects();
            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in schlists
                where compulsorySubject.Any(
                    x => x.ID == lst1.ID)
                select lst1).ToList();

            if (lst.Count < 3)
            {
                return Json(new {status = false, message = "Add Math, English and Civic."},
                    JsonRequestBehavior.AllowGet);
            }

            var lstTraded = (from lst1 in schlists
                where tradedSubject.Any(
                    x => x.ID == lst1.ID)
                select lst1).ToList();
            if (lstTraded.Count < 1)
            {
                return Json(new {status = false, message = "Add at least one traded subject."},
                    JsonRequestBehavior.AllowGet);

            }
            var lstotherSubject = (from lst1 in schlists
                where anyotherSubject.Any(
                    x => x.ID == lst1.ID)
                select lst1).ToList();

            // TODO show error message and stop
            lst.AddRange(lstTraded);
            lst.AddRange(lstotherSubject);



            if (lst.Count < 8)
            {
                return Json(new {status = false, message = "You must add at least 8 subjects."},
                    JsonRequestBehavior.AllowGet);
            }
            var dbSubjects = clsContent.GetSchoolSubjectById(schoolids);
            foreach (var vx in dbSubjects)
            {
                //TODO: Check subjects that was sent from the view. If anyone is not in the list of subjects assign to the school. Delete it.
                if (!(lst.Any(z => z.ID == vx.SubjectID)))
                {

                    var subFromTextbook = clsContent.DeleteSchoolSubjectTextsBooks(schoolids, vx.SubjectID);
                    var subFromStaffSubject = clsContent.DeleteStaffSubjectBack(schoolids, vx.SubjectID);
                    var DeleteStaffSubjectToClassDependency = clsContent.DeleteStaffSubjectBack(schoolids, vx.SubjectID);
                    var deletedSchoolSubject = clsContent.DeleteSchoolSubjectsDependency(schoolids, vx.SubjectID);

                }
            }

            foreach (var vx in lst)
            {
                if (dbSubjects.All(z => z.SubjectID != vx.ID))
                {

                    //Todo: New subject added to school in the view. Add the subject to the school

                    var schoolsubjectList = clsContent.AddASchoolsubject2(vx, schoolids, true, 0, 0, 0, 0, 0);
                   


                }
            }
            return Json(new { status = true, message = "Subjects saved successfully." },
                        JsonRequestBehavior.AllowGet);
        }

        // var xvids = clsContent.DeleteSchoolSubjects(schoolids);

          

        


        private SubjectModel GetSchoolSubjectsById(long schoolId)
        {
/* var compulsorySubjects = (from sub in subjects
                where
                    sub.LongName.ToLower().Equals("CIVIC EDUCATION".ToLower()) ||
                    sub.LongName.ToLower().Equals("ENGLISH LANGUAGE".ToLower()) ||
                    sub.LongName.ToLower().Equals("MATHEMATICS".ToLower())
                select sub.ID)
                .Select(dummy => (int?) dummy).ToList();*/
            var subjectsAddedBySchool = clsContent.GetSchoolSubjectById(schoolId);
            var subjectIds = subjectsAddedBySchool.Select(item => item.SubjectID).Select(dummy => (int?) dummy).ToList();
            return new SubjectModel
            {
                AvailableSubjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList(),
                AvailableSelected = subjectIds,
                //RequestedSubjects = new List<Subject>()
            };
        }




        private void GetClassFormData(long schoolid)
        {

            var studentClassRecord = clsContent.GetStudentClassRecord(schoolid);
            var classes = new List<SelectListItem>();
            foreach (var stdC in clsContent.GetClass())
            {
                if (studentClassRecord == null)
                {
                    classes.Add(new SelectListItem
                    {
                        Text = stdC.ClassName,
                        Value = stdC.ID.ToString()
                    });
                }
                else
                {
                    if (studentClassRecord.FirstOrDefault(item => item.ClassID == stdC.ID) == null)
                    {
                        classes.Add(new SelectListItem
                        {
                            Text = stdC.ClassName,
                            Value = stdC.ID.ToString()
                        });
                    }
                }

            }
            ViewBag.ClassSchoolid = schoolid;
            ViewBag.Classes = classes;
            ViewBag.ClassesRegistered = studentClassRecord;
        }

        /*public ActionResult _CreateStudent(int scid = 0, int sddc = 0)
        {
            var school = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;
            var schoolid = long.Parse(school);
            var aschool = clsContent.GetSchoolById(schoolid);
            var schoolClasses = clsContent.GetSchoolClassBySchoolId(schoolid);
            TempData["DisplaySchoolName"] = aschool.SchoolName;
            GetClassFormData(schoolid);
            ViewBag.StudentRecords = "active";
            ViewBag.ClassSchoolid = schoolid;
            TempData["Message"] = CurrentUser.EmailAddress;

            return PartialView(schoolClasses);
        }*/

        public ActionResult CreateStudent()
        {
            var scho = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schoolid = long.Parse(scho);
            var aschool = clsContent.GetSchoolById(schoolid);
                var schoolClasses = clsContent.GetSchoolClassBySchoolId(schoolid);
                TempData["DisplaySchoolName"] = aschool.SchoolNameEdit;
                GetClassFormData(schoolid);
                ViewBag.StudentRecords = "active";
                ViewBag.ClassSchoolid = schoolid;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(schoolClasses);

        }

        [HttpPost]
        public ActionResult CreateStudent(ClassAllocationvModel classalloc)
        {
            GetClassFormData(classalloc.SchoolID);
            // int index = 0;
            var indexsddc = new ClassAllocation();
            if (classalloc.NoOfStreams != null && classalloc.TotalStudents != null)
            {
                indexsddc = clsContent.AddClasses(classalloc.SchoolID, classalloc);
            }

            var resultsddc = indexsddc.ID > 0;
            ConstantsService.CreateCookie(ConstantsService.CookieClassIdKey, indexsddc.ID.ToString());
               ViewBag.FacilityManagement = "active";
            //TempData["facility"] = sch.SchoolName;
            return Json(
                new {status = resultsddc, scid = indexsddc.ID, sddc = indexsddc.SchoolID},
                JsonRequestBehavior.AllowGet);
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SchoolClassList(long sddc)
        {
            var schoolClasses = clsContent.GetSchoolClassBySchoolId(sddc);
            var getschool = clsContent.GetSchoolEditByID(sddc);
            TempData["DisplaySchoolName"] = getschool.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.EditClass = "active";
            return View(schoolClasses);
        }

        public ActionResult EditClass(int scid, long sddc)
        {
            ViewBag.SchoolID = sddc;
            ViewBag.Class = new SelectList(clsContent.GetClass(), "ID", "ClassName", scid);
            var schoolClasses = clsContent.GetASchoolClass(scid);
            var getschool = clsContent.GetSchoolEditByID(schoolClasses.SchoolID);
            TempData["DisplaySchoolName"] = getschool.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.StudentRecords = "active";
            return View(schoolClasses);
        }

        [HttpPost]
        public ActionResult EditClass(ClassAllocationvModel model)
        {
            ViewBag.Class = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            var fetchClass = clsContent.GetASchoolClass(model.ID);

            fetchClass.ClassID = model.ClassID;
            fetchClass.NoOfStreams = model.NoOfStreams;
            fetchClass.TotalStudents = model.TotalStudents;
            var updtatedclass = clsContent.UpdateASchoolClass(fetchClass);
            var getschool = clsContent.GetSchoolEditByID(fetchClass.SchoolID);
            TempData["DisplaySchoolName"] = getschool.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.StudentRecords = "active";
            return RedirectToAction("CreateStudent", new {scid = model.ID, sddc = fetchClass.SchoolID});
        }



        public JsonResult SaveLaboratory(string dd, string user, int record_id = 0)
        {
            var result = String.Empty;
            var schoolid = int.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            // long? schoolid = 66;
            // int schoolid = int.Parse(Session["schoolid"].ToString());
            var uv = new LaboratoryViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    // var config = clsContent.GetSchoolEquipConfigByID(facid);

                    // uv = clsContent.AddEquipFacilityValue(facid, user, record_id, schoolid);
                    uv = clsContent.AddLaboratoryValue(facid, user, record_id, schoolid);

                    return
                        Json(new {status = true, message = "Value saved successfully", recordId = uv.ID.ToString()},
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateLaboratoryValue(user, record_id);


                    return Json(new {status = true, message = "Value updated successfully", recordId = uv.ID.ToString()});

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveRecreational(string dd, string user, int record_id = 0)
        {
            
            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            //long? schoolid = 66;
            //int schoolid = int.Parse(Session["schoolid"].ToString());
            var uv = new RecreationalViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    uv = clsContent.AddRecreational(facid, user, schoolid);
                   
                    ConstantsService.CreateCookie(ConstantsService.CookieRecreationalIdKey, uv.ID.ToString());
                    if (ConstantsService.GetCookie(ConstantsService.CookieLibraryIdKey) != null && ConstantsService.GetCookie(ConstantsService.CookieExaminationIdKey) != null)
                    {
                        ConstantsService.CreateCookie(ConstantsService.CookieFacilityManagementIdKey, uv.ID.ToString());
                    }
                    return
                        Json(new {status = true, message = "Value saved successfully", recordId = uv.ID.ToString()},
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {

                    uv = clsContent.UpdateRecreationalValue(user, record_id);

                    return Json(new {status = true, message = "Value updated", recordId = uv.ID.ToString()});

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SaveExam(string dd, string user, int record_id = 0)
        {
            var scho = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schoolid = long.Parse(scho);
            var uv = new ExamHallViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);

                    uv = clsContent.AddExaminationHall(facid, user, schoolid);
                    ConstantsService.CreateCookie(ConstantsService.CookieExaminationIdKey, uv.ID.ToString());
                    if (ConstantsService.GetCookie(ConstantsService.CookieLibraryIdKey) != null)
                    {
                        ConstantsService.CreateCookie(ConstantsService.CookieFacilityManagementIdKey, uv.ID.ToString());
                    }
                    return
                        Json(new {status = true, message = "Value saved successfully", recordId = uv.ID.ToString()},
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateExamHallValue(user, record_id);



                    return Json(new {status = true, message = "Value updated", recordId = uv.ID.ToString()});

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveLibrary(string dd, string user, int record_id = 0)
        {
           var schoo = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schoolid = long.Parse(schoo);
            var uv = new LibraryViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    uv = clsContent.AddLibraryValue(facid, user, schoolid);
                    ConstantsService.CreateCookie(ConstantsService.CookieLibraryIdKey, uv.ID.ToString());
                    if (ConstantsService.GetCookie(ConstantsService.CookieExaminationIdKey) != null)
                    {
                        ConstantsService.CreateCookie(ConstantsService.CookieFacilityManagementIdKey, uv.ID.ToString());
                    }
                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully",
                                schoolid = schoo,
                                recordId = uv.ID.ToString()
                            },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateLibraryValue(user, record_id);



                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value updated",
                                schoolid = schoo,
                                recordId = uv.ID.ToString()
                            });

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }



        //  dd: d_id, user: elem.value, record_id: record_id, subjectIds: subjectId, totalDbRecords: totalDbRecords, totalComputed: totalComputed
        public JsonResult SaveReagent(int subjectIds, string dd, string user, int record_id = 0, int totalDbRecords = 0,
            int totalComputed = 0, int existingReagentValueId = 0)
        {

            String result = String.Empty;

            //use the record id to get the ID from SchoolFacilityValue table since is unique
            //then use the ID and the schoolid from schoolequipmentvalue  to get the 
            //ID value from schoolequipmentvalue and then update the record



            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;
            var uv = new ReagentViewModel();
            if (dd != null && user != null)
            {
                int facid = int.Parse(dd);
                var existingRecord = clsContent.FetchExistingReagentRecord(schoolid, facid);

                if (existingRecord == null)
                {
                    if (record_id == 0)
                    {


                        //GetSchoolFacilityvalueID

                        //  var sfv = clsContent.GetSchoolFacilityvalueID(facid);

                        // uv = clsContent.AddEQuipmentValue(subjectIds, facid, user, schoolid);




                        uv = clsContent.AddReagentValue(facid, user, schoolid);

                        if (uv.ReagentValue >= uv.QuantityRequired)
                        {

                            var row = clsContent.GetGradeExcell("Excellent");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 1;
                            //   uv.DateEntry = DateTime.Now;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, schoolid);
                            //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                        }
                        else
                        {
                            var row = clsContent.GetgradeVGood("Very Good");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 0;
                            // uv.DateEntry = DateTime.Now;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, schoolid);

                            //very good
                        }


                        return
                            Json(
                                new
                                {
                                    status = true,
                                    gradd = uv.DigitValue,
                                    definition = uv.Definition,
                                    message = "Value saved successfully",
                                    recordId = uv.SchoolReagentValueID.ToString()
                                },
                                JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    record_id = existingRecord.SchoolReagentValueID;
                    if (record_id != 0)
                    {

                        //var sfv = clsContent.GetSchoolFacilityvalueID(record_id);
                        //  vwSchoolFacilityValueModel SchFacValID = clsContent.GetSchoolFacilityValuebyRecordID(record_id);
                        //GetSchoolEquipmentIDByRecordID

                        //  var EquipID = clsContent.GetSchoolEquipmentIDByRecordID(SchFacValID.ID, schoolid);
                        //record_id = existingReagentValueId;
                        uv = clsContent.UpdateSchoolReagentValue(user, record_id);

                        //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);

                        if (uv.ReagentValue >= uv.QuantityRequired)
                        {

                            var row = clsContent.GetGradeExcell("Excellent");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 1;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, schoolid);

                        }
                        else
                        {
                            var row = clsContent.GetgradeVGood("Very_Good");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 0;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, schoolid);
                        }
                        return
                            Json(
                                new
                                {
                                    status = true,
                                    definition = uv.Definition,
                                    message = "Value updated",
                                    recordId = uv.SchoolReagentValueID.ToString()
                                });

                    }
                }

            }


            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }





        public JsonResult facuipment(int bagg, int subjectIds, int facCategoryId, string dd, string user,
            int record_id = 0, int totalDbRecords = 0, int totalComputed = 0)
        {
            String result = String.Empty;

            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;

            var uv = new SchoolEquipConfigViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);


                    uv = clsContent.AddEQuipmentValue(subjectIds, facid, user, schoolid);

                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {
                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);
                        //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                    }
                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }
                    return
                        Json(
                            new
                            {
                                gradd = uv.DigitValue,
                                status = true,
                                definition = uv.Definition,
                                message = "Value saved successfully",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    var uservalue = decimal.Parse(user);
                    uv = clsContent.UpdateEquipFacilityValue(record_id, uservalue);

                    //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);
                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {

                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                    }

                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }

                    return
                        Json(
                            new
                            {
                                status = true,
                                definition = uv.Definition,
                                message = "Value updated",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            });

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }

        //SaveSchoolFacility
        public ActionResult SaveSchoolFacility()
        {
            return View();
        }


        public ActionResult SectionPage()
        {
            TempData["Message"] = CurrentUser.EmailAddress;
            return View();
        }

        public ActionResult Welcome()
        {
            TempData["Message"] = CurrentUser.EmailAddress;
            Session.Remove("SchoolID");
            Session.Remove("SubjectSchoolIDSession");
            return View();
        }

        public ActionResult SchoolProfile(int? schooleditid)
        {
             string email = CurrentUser.EmailAddress;
            var userinfo = clsContent.GetUser(email);
            TempData["StateName"] = userinfo.StateName;
            TempData["StateID"] = userinfo.StateID;
            //Call the form variable method
            GetSchoolFormData();


            TempData["Message"] = "Welcome " + CurrentUser.EmailAddress;
            return View(new SchoolProfileViewModel());
        }



        public ActionResult _TextBook()
        {
            return PartialView();
        }

        //UpdateTextBook
        [HttpGet]
        public ActionResult UpdateTextBook(long schoolid)
        {
            int pageIndex = 1;

            int pageSize = 15;
            IPagedList<SchoolSubjectViewModel> eqp = null;
            eqp = clsContent.GetSchoolSubjects(schoolid, true).OrderBy(m => m.ID).ToPagedList(pageIndex, pageSize);

            //ViewBag.SchoolSubjectList;
            ViewBag.UpdateTextBook = "active";
            ViewBag.SchoolId = schoolid;
            return View(eqp);
        }

        [HttpGet]
        public ActionResult TextBook(int subjectId = 0)
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            List<SchoolSubjectViewModel> eqp = null;
            eqp = clsContent.GetSchoolSubjects(schid, true);
            GetSchoolSubjectFormData(schid);
            var allSchoolSubjects = new SelectList(clsContent.GetSchoolSubjects(schid, true), "SubjectID", "LongName",
                subjectId);

            ViewBag.AllSchoolSubjects = allSchoolSubjects;

            var allTextbboks = new SelectList(clsContent.GetAllAvailableTextBookBank(subjectId), "SubjectID",
                "AvailableTextbook");
            ViewBag.AvailableTextbooks = allTextbboks;


            // var allTextbboksBank = new SelectList(clsContent.GetAllAvailableTFSAVETEXTextBook(), "SubjectID", "AvailableTextbooks");


            TempData["Message"] = CurrentUser.EmailAddress;
            //ViewBag.SchoolSubjectList;
            ViewBag.Textbook = "active";
            return View(eqp);
        }

        public ActionResult SaveTexbook(string textBook = "", int subjectId = 0, string existingTextBook = "")
        {
            //insert into textbookbank table+==>the new textbook and the subjectid
            //insert into schoolsubject table the textbook and set istextbookAttached=true;
            //populate a table that the subject
            var schid =long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);

            GetSchoolSubjectFormData(schid);
            var getSchoolSubjectRowId = clsContent.GetSchoolSubjectRowId(schid, subjectId);
            if (existingTextBook.Trim() == "select textbooks")
            {
                return
                    Json(
                        new
                        {
                            status = "selecttextbook",
                        }, JsonRequestBehavior.AllowGet);
            }
            if (subjectId < 1)
            {
                return
                    Json(
                        new
                        {
                            status = "selectsubject",
                        }, JsonRequestBehavior.AllowGet);
            }

            if (existingTextBook != "")
            {

                var textbookAdded2SchoolAlready =
                    clsContent.CheckIfTextBooksAdded2School(getSchoolSubjectRowId.TextbookToSchoolSubjectRowId,
                        existingTextBook);
                if (textbookAdded2SchoolAlready != null)
                {
                    TempData["textbookAdded2SchoolAlready"] = "Textbook is already added to school textbook list";
                }
                else
                {
                    var addExistingTextBook =
                        clsContent.AddTextbookToSchoolSubject(getSchoolSubjectRowId.TextbookToSchoolSubjectRowId,
                            existingTextBook, true);

                    if (addExistingTextBook != null)
                    {

                        return
                            Json(
                                new
                                {
                                    status = true,
                                    schoolId = addExistingTextBook.SchoolID,
                                    subjectId = addExistingTextBook.SubjectID,
                                    Id = addExistingTextBook.ID,
                                    textbookToSchoolSubjectRowId = addExistingTextBook.TextbookToSchoolSubjectRowId
                                }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            else if (textBook != "")
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    var textbookExistInTextBookbank =
                        clsContent.CheckIfTextBooksInTextbookBank(subjectId, textBook);
                    if (textbookExistInTextBookbank != null)
                    {
                        TempData["textbookExistInTextBookbank"] = "Textbook is already added to textbook bank list";
                    }
                    else
                    {
                        var newTextBook = new SchoolSubjectViewModel();
                        var addNewTextbook = clsContent.AddNewTextbook(textBook, subjectId);
                        if (addNewTextbook != null)
                        {
                            newTextBook =
                                clsContent.AddTextbookToSchoolSubject(
                                    getSchoolSubjectRowId.TextbookToSchoolSubjectRowId, textBook, true);
                        }
                        scope.Complete();
                        if (newTextBook != null)
                        {

                            return
                                Json(
                                    new
                                    {
                                        status = true,
                                        schoolId = newTextBook.SchoolID,
                                        subjectId = newTextBook.SubjectID,
                                        Id = newTextBook.ID,
                                        textbookToSchoolSubjectRowId = newTextBook.TextbookToSchoolSubjectRowId


                                    }, JsonRequestBehavior.AllowGet);
                        }
                    }

                }

            }
            return Json(new {status = false, xxtdata = TempData["textbookExistInTextBookbank"]},
                JsonRequestBehavior.AllowGet);

        }


        //return Json(new { subject =updateSchoolSubject.LongName,textbook=updateSchoolSubject.AvailableTextbooks }, JsonRequestBehavior.AllowGet);




        private void GetSchoolSubjectFormData(long schoolid)
        {
            //   var studentClassRecord = clsContent.GetStudentClassRecord(schoolid);  GetSchoolSubjectsByTextbook
            var schoolSubjectRecord = clsContent.GetSchoolSubjectsByTextbook(schoolid);


            //    var classes = new List<SelectListItem>();
            var subjects = new List<SelectListItem>();
            foreach (var schoolSubject in clsContent.GetSchoolSubjects(schoolid, true))
            {
                if (schoolSubjectRecord == null)
                {
                    subjects.Add(new SelectListItem
                    {
                        Text = schoolSubject.LongName,
                        Value = schoolSubject.ID.ToString()
                    });
                }
                else
                {
                    if (
                        schoolSubjectRecord.FirstOrDefault(s => s.AvailableTextbooks == schoolSubject.AvailableTextbooks) ==
                        null)
                    {
                        subjects.Add(new SelectListItem
                        {
                            Text = schoolSubject.LongName,
                            Value = schoolSubject.ID.ToString()
                        });
                    }
                }
            }
            ViewBag.Classes = subjects;
            ViewBag.schoolSubjectRecordTextbook = schoolSubjectRecord;



        }

        public ActionResult FetchSubjectName(int selectedSubjectId)
        {
            var getSubjectName = clsContent.GetSubjectById(selectedSubjectId);
            return Json(new
            {
                SubjectName = getSubjectName.LongName
            },
                JsonRequestBehavior.AllowGet);
        }

        //SaveTexbook



        public ActionResult DeleteTextBook(int textbookToSchoolSubjectRowId)
        {
            var returnedvalue = clsContent.DeleteTextBook(textbookToSchoolSubjectRowId);
            if (returnedvalue)
            {
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return Json(new {status = true, token = textbookToSchoolSubjectRowId, JsonRequestBehavior.AllowGet});
            }
            return Json(new {status = false, JsonRequestBehavior.AllowGet});


        }

        //[HttpPost]
        //public ActionResult TextBook(SchoolSubjectViewModel[] Checkeditems, SchoolSubjectViewModel[] Textbooks)
        //{
        //    var schoolids = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;

        //    long schid = int.Parse(schoolids);
        //   var result= clsContent.SetIsTextBookAttached(Checkeditems, Textbooks, schid);

        //    //for (int p = 0; p < intSelcted.Length; p++)
        //        //{
        //        //    //SetIsTextBookAttached
        //        //    var selectedidrow = intSelcted[p];
        //        //    selected = true;
        //        //    ret = clsContent.SetIsTextBookAttached(selectedidrow, selected);
        //        //    //result = ret.ID > 0 ? "1" : "0";
        //        //}


        //    //result = ret.ID > 0 ? "1" : "0";
        //    return Json(new {status = result}, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult LoadSchoolSubject()
        {
            var data = clsContent.GetCouncilSubjects();
            // return View(yall);
            return Json(new {data = data}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexDemo()
        {
            CountryModel objcountrymodel = new CountryModel();
            objcountrymodel.CountryList = new List<Country>();
            objcountrymodel.CountryList = CountryDate();
            return View(objcountrymodel);
        }

        public List<Country> CountryDate()
        {
            List<Country> objcountry = new List<Country>();
            objcountry.Add(new Country {CountryName = "America"});
            objcountry.Add(new Country {CountryName = "India"});
            objcountry.Add(new Country {CountryName = "Sri Lanka"});
            objcountry.Add(new Country {CountryName = "China"});
            objcountry.Add(new Country {CountryName = "Pakistan"});
            return objcountry;
        }




        public ActionResult _Staff()
        {
           GetSchoolFormData();
            TempData["Message"] = "Welcome " + CurrentUser.EmailAddress;
            return PartialView();
        }


        // [AuthorizeAccess(Roles = "Admin")]

        public ActionResult LaboratoryFacility(int? subjectid = 0, int page = 1)
        {
           var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
          
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            //  GetSubjectItemsFormData(subjectid);
            pageIndex = page;

            GetSchoolSubjectItems(subjectid);
            IPagedList<LaboratoryViewModel> lab = null;
            if (subjec == 0)
            {
                lab = clsContent.GetSchoolLaboratory(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                lab = clsContent.GetSchoolLaboratory(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }

            //ViewBag.SchoolSubjectList;
            ViewBag.subjectid = subjectid;

            ViewBag.SubjectFacilityManagement = "active";
            return View(lab);


        }

        public ActionResult RecreationalFacility( int page = 1)
        {

            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageSize = 20;
            ViewBag.StaffID = new SelectList(clsContent.GetStaffNamebyschool(schoolid), "ID", "FirstName");
            IPagedList<RecreationalViewModel> recreational = null;
            recreational = clsContent.GetSchoolRecreational(schoolid).OrderBy
                (m => m.ID).ToPagedList(page, pageSize);
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolSectionFacilityManagement = "active";
            return View(recreational);


        }

        public ActionResult AssessmentFacility(int page = 1)
        {
            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            int pageIndex = 1;

            int pageSize = 12;

            pageIndex = page;
            //int schoolid = int.Parse(Session["schoolid"].ToString());

            ViewBag.FacilityManagement = "active";
            IPagedList<AssessmentViewModel> eqp = null;

            eqp = clsContent.GetAccessmentFacility().OrderBy(m => m.ID).ToPagedList(pageIndex, pageSize);

            return View(eqp);

        }


        public ActionResult SaveAssessmentSummary(string[] grades, string[] items)
        {
            string[] ItemsArray = items.Distinct().ToArray();

            int[] intitemsarray = ItemsArray.Select(int.Parse).ToArray();

            int[] intgradesarray = grades.Select(int.Parse).ToArray();
            var vb = new AssessmentSummary();
            var returenedassessment = new AssessmentSummary();
            int schoolid = int.Parse(Session["schoolid"].ToString());
            for (int i = 0; i < 1; i++)
            {
                for (int p = 0; p < intgradesarray.Length; p++)
                {
                    returenedassessment = clsContent.AddAssessmentSummary(intitemsarray[p], intgradesarray[p], schoolid);

                }
            }
            String result = String.Empty;

            if (returenedassessment.ID > 0)
            {

                result = "1";
            }
            else
            {
                result = "0";
            }


            return Json(new {status = result}, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult ExaminationFacility()
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            int pageIndex = 1;
            int pageSize = 20;
            var  exam = clsContent.GetSchoolExamHall(schid).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolSectionFacilityManagement = "active";
            return View(exam);
        }



        public ActionResult LibraryFacility(int? subjectid = 0, int page = 1)
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

            var aschool = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = aschool.SchoolName;

            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            GetSubjectItemsFormData(subjectid);
            pageIndex = page;


            IPagedList<LibraryViewModel> library = null;
            if (subjec == 0)
            {
                library = clsContent.GetSchoolLibrary(schid).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                library = clsContent.GetSchoolLibrary(schid).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            //ViewBag.SchoolSubjectList;
            ViewBag.subjectid = subjectid;
            ViewBag.FacilityManagement = "active";
            return View(library);


        }

        [HttpPost]
        public ActionResult UpdateSchoolSubjectItems()
        {
            return View();
        }

        //schoolid and subjectid
        public ActionResult UpdateSchoolSubjectItems(int subjectid = 0, int schoolid = 0, int page = 1)
        {
            GetSchoolSubjectItems(subjectid);
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;

            pageIndex = page;

            IPagedList<SchoolEquipConfigViewModel> eqp = null;
            if (subjec == 0)
            {
                eqp = clsContent.GetSchoolFacilitiesBySchoolID(schoolid, subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                eqp = clsContent.GetSchoolFacilitiesBySchoolID(schoolid, subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            //ViewBag.SchoolSubjectList;
            ViewBag.subjectid = subjectid;
            ViewBag.UpdateSchoolSubjectItems = "active";
            return View(eqp);
        }

        //public ActionResult UpdateTextBook(int schoolid, int page = 1)
        //{

        //    var subjectmodel = GetSchoolSubjectsById(schoolid);

        //    ViewBag.UpdateSubject = "active";
        //    // return Json(new { status = result, subjectadded = result }, JsonRequestBehavior.AllowGet);

        //    return View(subjectmodel);
        //}


        public ActionResult UpdateSubject(long sdc = 0, int? page = 1)
        {
            // sdc = 21642;
            // TempData["Message"] = CurrentUser.EmailAddress;

            var school = clsContent.GetSchoolById(sdc);

            var subjectmodel = GetSchoolSubjectsById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.UpdateSubject = "active";
            ViewBag.SchoolId = sdc;
            ViewBag.CompulsorySubjects = clsContent.GetCompulsorySubjects().Select(item => item.ID).ToList();
            ViewBag.TradeSubjects = clsContent.GetTradeSubjects().Select(item => item.ID).ToList();
            return View(subjectmodel);
        }

        [HttpPost]
        public ActionResult UpdateSubject(int[] AvailableSelected, int schoolid)
        {
            List<SchoolSubject> ret = null;

            var schlists = new List<SubjectModel>();
            if (AvailableSelected == null)
            {
                return
                    Json(
                        new
                        {
                            status = false,
                            message =
                                "Please select English, Maths, Civic, One traded subject and any other subjects. Remember: All must be at leats 8 subjects."
                        }, JsonRequestBehavior.AllowGet);
            }
            for (int i = 0; i < AvailableSelected.Length; i++)
            {
                var sub = clsContent.GetSelectedSubjectById(AvailableSelected[i]);
                schlists.Add(sub);
            }

            var compulsorySubject = clsContent.GetCompulsorySubjects();

            var tradedSubject = clsContent.GetTradeSubjects();

            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in schlists
                where compulsorySubject.Any(
                    x => x.ID == lst1.ID)
                select lst1).ToList();

            if (lst.Count < 3)
            {
                return Json(new {status = false, message = "Add Math, English and Civic."}, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstTraded = (from lst1 in schlists
                    where tradedSubject.Any(
                        x => x.ID == lst1.ID)
                    select lst1).ToList();
                if (lstTraded.Count < 1)
                {
                    return Json(new {status = false, message = "Add at least one traded subject."},
                        JsonRequestBehavior.AllowGet);

                }
                else
                {
                    var lstotherSubject = (from lst1 in schlists
                        where anyotherSubject.Any(
                            x => x.ID == lst1.ID)
                        select lst1).ToList();

                    // TODO show error message and stop
                    lst.AddRange(lstTraded);
                    lst.AddRange(lstotherSubject);

                    if (lst.Count < 8)
                    {
                        return Json(new {status = false, message = "You must add at least 8 subjects."},
                            JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            bool check = clsContent.DeleteSchoolSubjects(schoolid);
                            if (check == true)

                                clsContent.UpdateSchoolSjubject2(lst, schoolid);
                            ViewBag.UpdateSubject = "active";
                            scope.Complete();
                            return Json(new {status = true, message = "Subjects saved successfully."},
                                JsonRequestBehavior.AllowGet);

                        }


                    }
                }
            }






        }

        public ActionResult DetailStaff(int schoolid)
        {
            return RedirectToAction("CreateSubjectItems");
        }

        public ActionResult DownloadStaff(int schoolid)
        {
            return RedirectToAction("CreateSubjectItems");
        }

        public ActionResult UpdateSchoolStaff(int schoolid)
        {

            //  var schoolstaffs = clsContent.GetSchoolStaffBySchoolId(schoolid);

            IPagedList<SchoolStaffModel> schoolstaffs = null;
            //  var office = clsContent.GetOffices().FirstOrDefault(v => v.OfficeName == "YABA");

            int pageIndex = 1;

            int pageSize = 20;

            //GetUsersForRoles
            schoolstaffs = clsContent.GetSchoolStaffBySchoolId(schoolid).OrderBy
                (m => m.FirstName).ToPagedList(pageIndex, pageSize);
            ViewBag.UpdateSchooStaff = "active";

            // GetUsers();

            return View(schoolstaffs);


        }


       


        [HttpGet]
        public ActionResult SchoolFacility(int page = 1)
        {
            var sch = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(sch);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageSize = 20;
            IPagedList<LibraryViewModel> library;
            library = clsContent.GetSchoolLibrary(sch).OrderBy
                (m => m.ID).ToPagedList(page, pageSize);
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolSectionFacilityManagement = "active";
            return View(library);
        }


        [HttpGet]
        public ActionResult FacilityTabbing(int subjectId = 0, int page = 1, int facid = 0)
        {
            var sch = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            ViewBag.CurrentSchool = sch;
            var schoolname = clsContent.GetSchoolById(sch);

            var school = clsContent.GetSchoolById(null);

            GetSchoolSubjectItems(subjectId);
            int pageIndex = 1;
            var subjec = subjectId;
            int pageSize = 20;

            pageIndex = page;


            /*First when equipment table value for a school is null*/

            IPagedList<SchoolEquipConfigViewModel> eqp = null;
            if (subjec == 0)
            {
                eqp = clsContent.GetSchoolEquipment(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);

            }
            else
            {


                var summaryList = clsContent.GetSchoolupdateEquipmentDigitValue(sch);
                var schoolEqps = clsContent.GetSchoolEquipment(subjec);
                foreach (var summary in summaryList)
                {
                    //SchoolFacilityValueID
                    var schoolEqp = schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID);
                    if (schoolEqp != null)
                    {
                        //  schoolEqp.EquipmentValue = summary.EquipmentValue;
                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).EquipmentValue
                            = summary.EquipmentValue;

                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).DigitValue =
                            summary.DigitValue;
                    }
                    // schoolEqp.EquipmentValue = 
                }
                //to load template for new school
                eqp = schoolEqps.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);


            }
            TempData["DisplaySchoolName"] = schoolname.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.totalgradess = eqp.Sum(x => x.DigitValue);
            ViewBag.subjectid = subjectId;
            ViewBag.FacilityManagement = "active";

            return View(eqp);
        }

        public JsonResult SaveEquipment(int bagg, int subjectIds, int facCategoryId, string dd, string user,
            int record_id = 0, int totalDbRecords = 0, int totalComputed = 0)
        {
            String result = String.Empty;

            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;

            var uv = new SchoolEquipConfigViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);


                    uv = clsContent.AddEQuipmentValue(subjectIds, facid, user, schoolid);

                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {
                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);
                        //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                    }
                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }
                    return
                        Json(
                            new
                            {
                                gradd = uv.DigitValue,
                                status = true,
                                definition = uv.Definition,
                                message = "Value saved successfully",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    var uservalue = decimal.Parse(user);
                    uv = clsContent.UpdateEquipFacilityValue(record_id, uservalue);

                    //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);
                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {

                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                    }

                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            schoolid);

                        //very good
                    }

                    return
                        Json(
                            new
                            {
                                status = true,
                                definition = uv.Definition,
                                message = "Value updated",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            });

                }
            }

            else
            {
                return Json(new {status = false, message = "Value is required."}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {status = false, message = "Error occurred."}, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ReagentFacility(int? subjectid = 0, int page = 1)
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

            ViewBag.CurrentSchool = schid;

            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            GetSchoolSubjectItems(subjectid);
            // GetSubjectItemsFormData(subjectid);
            pageIndex = page;
            ViewBag.subjectid = subjectid;

            IPagedList<ReagentViewModel> reag = null;
            if (subjec == 0)
            {
                reag = clsContent.GetSchoolReagent(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateReagentDigitValue(schid);
                var schoolReagents = clsContent.GetSchoolReagent(subjec);
                foreach (var summary in summaryList)
                {

                    var schoolReag =
                        schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                    if (schoolReag != null)
                    {
                        schoolReag.ReagentValue = summary.ReagentValue;
                        schoolReag.DigitValue = summary.DigitValue;
                        schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                        //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                        //ViewBag.SchReagentID = schoolReag.SchReagentID;
                    }
                    //picks the last record
                    // ViewBag.SchReagentID = schoolReag.SchReagentID;
                }

                reag = schoolReagents.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            ViewBag.SchReagentID = reag.AsEnumerable()
                .Select(o => new ReagentViewModel()
                {
                    SchReagentID = o.SchReagentID
                }).ToList();
            ViewBag.totalgradess = reag.Sum(x => x.DigitValue);
            TempData["Message"] = CurrentUser.EmailAddress;
            //  ViewBag.SchoolSubjectList;

            ViewBag.FacilityManagement = "active";
            return View(reag);


        }

        public ActionResult ReagentFacilityy()
        {
            
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            ViewBag.SubjectFacilityManagement = "active";
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolId = schid;
            return View();
        }

        public ActionResult FacilityTabbingg()
        {

            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var getSchool = clsContent.GetSchoolById(schid);
           
            TempData["DisplaySchoolName"] = getSchool.SchoolNameEdit;

            ViewBag.SubjectFacilityManagement = "active";
            //var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);;
            //var schid = long.Parse(schoolid);
            //use getschoolbyid edit is enough
            var school = clsContent.GetSchoolById(schid);
            //TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolId = schid;
            return View();
        }


        public List<ReagentViewModel> GetReagentFacility(int? subjectid = 0, int page = 1)
        {
         
            if (ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value == null)
            {
                TempData["Information"] = "Register a School to proceed.";
            }
            var schoolid = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schid = long.Parse(schoolid);
            ViewBag.CurrentSchool = schid;

            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            GetSchoolSubjectItems(subjectid);
            // GetSubjectItemsFormData(subjectid);
            pageIndex = page;
            ViewBag.subjectid = subjectid;

            //IPagedList<ReagentViewModel> reag = null;
            List<ReagentViewModel> reag = null;
            if (subjec == 0)
            {
                reag = clsContent.GetSchoolReagent(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateReagentDigitValue(schid);
                var schoolReagents = clsContent.GetSchoolReagent(subjec);
                foreach (var summary in summaryList)
                {

                    var schoolReag =
                        schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                    if (schoolReag != null)
                    {
                        schoolReag.ReagentValue = summary.ReagentValue;
                        schoolReag.DigitValue = summary.DigitValue;
                        schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                        //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                        //ViewBag.SchReagentID = schoolReag.SchReagentID;
                    }
                    //picks the last record
                    // ViewBag.SchReagentID = schoolReag.SchReagentID;
                }

                reag = schoolReagents.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize).ToList();
            }
            ViewBag.SchReagentID = reag.AsEnumerable()
                .Select(o => new ReagentViewModel()
                {
                    SchReagentID = o.SchReagentID
                }).ToList();
            ViewBag.totalgradess = reag.Sum(x => x.DigitValue);
            TempData["Message"] = CurrentUser.EmailAddress;
            //  ViewBag.SchoolSubjectList;

            ViewBag.FacilityManagement = "active";
            return reag;


        }

        [HttpPost]
        public ActionResult FacilityTabbingUpdate(int updateSubjectId, int updateSchoolId, string updateSubjectReason,
            bool hasItemsCheckboxUpdate, int page = 1)
        {
            
            var schoolids = ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value;
            var schoolid = long.Parse(schoolids);
            //var school = clsContent.GetSchoolById(schoolid);
            //TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            var subjectModel = clsContent.GetSubjectById(updateSubjectId);
            //GetSchoolSubjectItems(updateSubjectId);
            ViewBag.Subject = subjectModel;
            ViewBag.Reason = updateSubjectReason;
            int pageIndex = page;
            var subjec = updateSubjectId;
            int pageSize = 20;
            IPagedList<SchoolEquipConfigViewModel> eqp = null;
            eqp = clsContent.GetExistingSchoolEquipValue(subjec, schoolid, 1).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);

            //ViewBag.SchoolSubjectList;
            ViewBag.subjectid = updateSubjectId;
            var schoolEquipConfigViewModel = eqp.FirstOrDefault();
            //if (schoolEquipConfigViewModel != null)
            //    ViewBag.totalgradess = schoolEquipConfigViewModel.DigitValue;
            ViewBag.Facility = "active";
            return View(eqp);
        }

        
        public ActionResult DataTableHtml()
        {
            return View("DataTableHtml");
        }

        public ActionResult ManageSchool()
        {
            return View();
        }

        public ActionResult CreateCategory(int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {
            const int pageSize = 10;
            var totalRows = clsContent.GetSchoolCategoryCount();
            //   coun mobjMo.CountCategory();//GetSchoolCategoryCount

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var categorylists = clsContent.GetSchoolCategoryP(page, pageSize, sort, Dir);
            var data = new PagedCustomerModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Category = categorylists
            };
            return View(data);
        }

        [HttpGet]
        public JsonResult UpdateRecordSchCate(string id, string catee)
        {
            //UpdateSchoolCategory
            int idcat = int.Parse(id);
            bool result = false;
            try
            {
                result = clsContent.UpdateSchoolCategory(idcat, catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new {result}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteRecordCategory(int id)
        {
            //DeleteSchoolCategory
            bool result = false;
            try
            {
                result = clsContent.DeleteSchoolCategory(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new {result}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecordCat(string catee)
        {
            bool result = false;
            try
            {
                result = clsContent.AddSchoolCategory(catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new {result}, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult CreateSubjectItems(int? recid, int? facid, int page = 1, string factext = "")
        //{
        //    //GetRecognitionTypeFormData();

        //    var schoolRecognitionType = clsContent.GetSchoolRecogntion().Select(s => new SelectListItem

        //    {
        //        Text = s.TypeofRecognition,
        //        Value = s.ID.ToString(),
        //        Selected = (s.ID == recid)
        //    });

        //    ViewBag.FacilityNames = new List<SelectListItem>();
        //    if (facid != null)
        //    {
        //        var facNames = clsContent.GetFacilityCategories(recid).Select(s => new SelectListItem
        //        {
        //            Text = s.TypeOfFacility,
        //            Value = s.ID.ToString(),
        //            Selected = (s.ID == facid)
        //        });

        //        ViewBag.FacilityNames = facNames;
        //    }
        //    ViewBag.Subjectss = clsContent.GetCouncilSubjects().Select(s => new SelectListItem
        //    {
        //        Text = s.LongName,
        //        Value = s.ID.ToString()
        //    });// new SelectList(ctx.Subjects, "ID", "LongName", new Subject().ID);
        //    ViewBag.schoolRecognitionTypes = schoolRecognitionType;
        //    ViewBag.RecognitionId = recid;
        //    ViewBag.FacilityId = facid;
        //    //var faciltycat = ctx.FacilityCategories.Where(x => x.ID == facid).FirstOrDefault();
        //    IPagedList<SchoolEquipConfigViewModel> eqp = new PagedList<SchoolEquipConfigViewModel>(new List<SchoolEquipConfigViewModel>(), page, 20);
        //    ViewBag.factext = factext;
        //    IPagedList<LaboratoryViewModel> Leqp = new PagedList<LaboratoryViewModel>(new List<LaboratoryViewModel>(), page, 20);
        //    if ((recid != null && facid != null) && (factext == "EQUIPMENT FACILITY" || factext == "REAGENT FACILITY"))
        //    {
        //        eqp =clsContent.GetSchoolEquipConfigAdmin(facid,recid).OrderBy
        //          (m => m.ID).ToPagedList(page, 20);
        //        //GetSchoolEquipConfigAdmin
        //    }

        //    else if (recid != null && facid != null && factext == "LABORATORY FACILITY")
        //    {
        //        Leqp = clsContent.GetSchoolLabConfigAdmin(facid,recid).OrderBy(m => m.ID).ToPagedList(page, 20);
        //        ViewBag.labEquipment = Leqp;
        //    }
        //    return View(eqp);


        //}

        [HttpPost]
        public ActionResult GetFacility(string recid)
        {


            int? reid = int.Parse(recid);
            List<SelectListItem> facNames = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(recid))
            {
                reid = Convert.ToInt32(recid);
                List<vmFacilityCategory> faccate = clsContent.GetFacilityCategories(reid).ToList();
                faccate.ForEach(x =>
                {
                    facNames.Add(new SelectListItem {Text = x.TypeOfFacility, Value = x.ID.ToString()});
                });
            }
            return Json(facNames, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubjectList()
        {

            var Subjects = clsContent.GetCouncilSubjects().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString()
            }).ToList();

            return Json(Subjects, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult Pdf()
        //{
        //    //List<Facility> model = new List<Facility>();
        //    var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

        //    var schoolprofilemodel = clsContent.GetSchoolProfile();

        //    var principalmodel = clsContent.GetSchoolProfile2();

        //    var nullableData = schoolprofilemodel.Where(s => s.ID == schoolid).ToList();


        //    var studentclass = clsContent.GetStudentClass((int)schoolid);



        //    // var schy = clsContent.GetSubjectItems();




        //  //  Schoolreport rpt = new Schoolreport();

        //    //var schoollabs = new List<List<vwSchoolLaboratoryModel>>();
        //    //foreach (var s in schy)
        //    //{
        //    //    schoollabs.Add(clsContent.GetSchoolLaboratory2(schoolid, s.ID));

        //    //}
        //    //List<DataTable> dataTables = new List<DataTable>();
        //    //schoollabs.ForEach(data =>
        //    //{
        //    //    dataTables.Add(RemoveNullableClass.ToDataTable(data));
        //    //});

        //    //DataTable xc = RemoveNullableClass.ToDataTable(First());

        //    DataTable c = RemoveNullableClass.ToDataTable(nullableData);





        //    //  schoolProfile rpt=new schoolProfile();

        //    DataTable cc = RemoveNullableClass.ToDataTable(studentclass);

        //    rpt.Load();

        //    //   rpt.Subreports[0].GetComponentName();//SetDataSource(cc);
        //    rpt.Subreports[0].SetDataSource(cc);
        //    //for (var i = 1; i < dataTables.Count; i++)
        //    //{
        //    //    rpt.Subreports[i].SetDataSource(dataTables[(i - 1)]);

        //    //}

        //    rpt.SetDataSource(c);


        //    Stream ss = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(ss, "application/pdf");

        //}

        public ActionResult CreateSubjectItems(int? recid, int? facid, int page = 1, string factext = "")
        {
            //GetRecognitionTypeFormData();

            var schoolRecognitionType = clsContent.GetSchoolRecogntion().Select(s => new SelectListItem

            {
                Text = s.TypeofRecognition,
                Value = s.ID.ToString(),
                Selected = (s.ID == recid)
            });

            ViewBag.FacilityNames = new List<SelectListItem>();
            if (facid != null)
            {
                var facNames = clsContent.GetFacilityCategories(recid).Select(s => new SelectListItem
                {
                    Text = s.TypeOfFacility,
                    Value = s.FacilityCategoryID.ToString(),
                    Selected = (s.FacilityCategoryID == facid)
                });

                ViewBag.FacilityNames = facNames;
            }
            ViewBag.Subjectss = clsContent.GetCouncilSubjects().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString()


            }); // new SelectList(ctx.Subjects, "ID", "LongName", new Subject().ID);
            ViewBag.schoolRecognitionTypes = schoolRecognitionType;
            ViewBag.RecognitionId = recid;
            ViewBag.FacilityId = facid;
            //var faciltycat = ctx.FacilityCategories.Where(x => x.ID == facid).FirstOrDefault();
            IPagedList<SchoolEquipConfigViewModel> eqp =
                new PagedList<SchoolEquipConfigViewModel>(new List<SchoolEquipConfigViewModel>(), page, 20);
            ViewBag.factext = factext;
            IPagedList<LaboratoryViewModel> Leqp = new PagedList<LaboratoryViewModel>(new List<LaboratoryViewModel>(),
                page, 20);

            IPagedList<LibraryViewModel> Libr = new PagedList<LibraryViewModel>(new List<LibraryViewModel>(), page, 20);
            if ((recid != null && facid != null) && (factext == "EQUIPMENT FACILITY" || factext == "REAGENT FACILITY"))
            {
                eqp = clsContent.GetSchoolEquipConfigAdmin(facid, recid).OrderByDescending(m => m.ID)
                    .ToPagedList(page, 20);
                //GetSchoolEquipConfigAdmin
            }

            else if (recid != null && facid != null && factext == "LABORATORY FACILITY")
            {
                Leqp = clsContent.GetSchoolLabConfigAdmin(facid, recid).OrderBy(m => m.ID).ToPagedList(page, 20);
                ViewBag.labEquipment = Leqp;
            }


            else if (recid != null && facid != null && factext == "LIBRARY FACILITY")
            {
                Libr = clsContent.GetSchoolLibraryConfigAdmin(facid, recid).OrderBy(m => m.ID).ToPagedList(page, 20);
                ViewBag.libraryEquipment = Libr;
            }

            ViewBag.CreateSubjectItems = "active";
            return View(eqp);


        }

        public ActionResult TestHtml()
        {
            return View();
        }

        public ActionResult Navigate()
        {
            ViewBag.Report = "active";
            return View();
        }

        public ActionResult ExaminationView(long sdc)
        {
            var school = clsContent.GetSchoolById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            // ViewBag.Classroom = "active";
            var examModel = clsContent.GetExamHallBySchoolId(sdc);
            ViewBag.GeneralDeatils = "active";
            return View(examModel);
        }

        //ExaminationView
        public ActionResult ClassroomView()
        {
            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            // ViewBag.Classroom = "active";
            GetClassFormData(schoolid);
            ViewBag.GeneralDeatils = "active";
            return View();
        }

        //ClassroomView
        public ActionResult RecreationalView()
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            var recreaModel = clsContent.GetSchoolReacreationalForAssess(schid);

            ViewBag.GeneralDeatils = "active";

            return View(recreaModel);
        }

        //RecreationalView
        public ActionResult LibraryView()
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            var libraryModel = clsContent.GetSchoolLibraryForAssess(schid);

            ViewBag.GeneralDeatils = "active";

            return View(libraryModel);
        }

        public ActionResult LaboratoryView()
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Facility = "active";

            var summarylist = clsContent.GetLaboratorySummary(schid);
            ViewBag.GeneralDeatils = "active";
            return View(summarylist);
        }

        //LaboratoryView
        public ActionResult FacilityView()
        {
            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var school = clsContent.GetSchoolById(schid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.Facility = "active";

            var summarylist = clsContent.GetFacilitySummary(schid);
            ViewBag.GeneralDeatils = "active";
            return View(summarylist);
        }


        public ActionResult SchoolStaffView()
        {

            //GetStaffBySchoolId
            //GetStaffOwnedDegreeBySchoolId
            //GetStaffSubjectTaughtBySchoolId

            var schid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

            var school = clsContent.GetSchoolById(schid);
            var schoolStaffListForView = new List<SchoolStaffList>();
            TempData["DisplaySchoolName"] = school.SchoolName;
            var staffList = clsContent.GetStaffBySchoolId(schid);
            foreach (var staff in staffList)
            {

                var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySchoolId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtBySchoolId(staff.ID);
                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,

                });
            }
            ViewBag.GeneralDeatils = "active";
            return View(schoolStaffListForView);
        }

        //SchoolStaffView

        public ActionResult SchoolSubjectView()
        {
            //public ActionResult Classroom(long sch, bool isAppoved, ClassAllocationvModel[] items)

            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);


            var schoolSubjectView = new List<SchoolSubjectList>();
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.Schoolid = schoolid;

            var schoolsubjectList = clsContent.GetSchoolSubjects(schoolid, true);

            var tradeSubjectsFromDb = clsContent.GetSchoolTradeSubjects();
            var tradeSubjectsSelected = new List<SchoolSubjectViewModel>();

            var compulsorySchoolSubjectsFromDb = clsContent.GetCompulsorySchoolSubjects();
            var compulsorySubjectsSelected = new List<SchoolSubjectViewModel>();

            var otherSubjects = new List<SchoolSubjectViewModel>();
            foreach (var sub in schoolsubjectList)
            {
                if (compulsorySchoolSubjectsFromDb.Any(comp => comp.ID == sub.SubjectID))
                {
                    compulsorySubjectsSelected.Add(sub);
                }
                else if (tradeSubjectsFromDb.Any(comp => comp.ID == sub.SubjectID))
                {
                    tradeSubjectsSelected.Add(sub);
                }
                else
                {
                    otherSubjects.Add(sub);
                }

            }
            schoolSubjectView.Add(new SchoolSubjectList
            {

                Subject = otherSubjects,
                SchoolCompulsorySubject = compulsorySubjectsSelected,
                SchoolLeastTradeSubject = tradeSubjectsSelected
            });


            ViewBag.GeneralDeatils = "active";
            // clsContent.UpdateClassForApproval(isAppoved);
            return View(schoolSubjectView);

        }


        public ActionResult GeneralView()
        {


            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);
            var schoolSubjectView = new List<SchoolSubjectList>();
            var school = clsContent.GetSchoolById(schoolid);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolmodel = clsContent.GetSchoolByIDSum(schoolid);
            ViewBag.Schoolid = schoolid;
            ViewBag.GeneralDeatils = "active";
            return View(schoolmodel);
        }

        //GeneralView
        public ActionResult SchoolSummary()
        {

            return View();
        }

        [HttpGet]
        public ActionResult SubjectDercogntion(long sdc)
        {
            TempData["Message"] = CurrentUser.EmailAddress;

            var school = clsContent.GetSchoolById(sdc);

            var subjectmodel = GetSchoolSubjectsById(sdc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.UpdateSubject = "active";
            ViewBag.SchoolId = sdc;
            ViewBag.CompulsorySubjects = clsContent.GetCompulsorySubjects().Select(item => item.ID).ToList();
            ViewBag.TradeSubjects = clsContent.GetTradeSubjects().Select(item => item.ID).ToList();

            return View(subjectmodel);
        }

        public ActionResult ProcessSubject(int schoolid, int subjectid, string reasonName, bool isChecked,int classId)
        {
            //agric and physics
            var procSchoolSubjectDerec = clsContent.ProcSchoolSubjectDerec(schoolid, subjectid);
            //var procItemSectionAssessment = clsContent.ProcItemSectionAssessment(schoolid, subjectid);
            var procStaffSummaryDerec = clsContent.ProcStaffSummaryDerec(schoolid, subjectid);





            var getEquipmentFacilityRated1 = clsContent.GetEquipmentFacilityRated(schoolid, 1, 1);
            var getEquipmentFacilityRated2 = clsContent.GetEquipmentFacilityRated(schoolid, 2, 1);
            var getEquipmentFacilityRated3 = clsContent.GetEquipmentFacilityRated(schoolid, 3, 1);
            var getEquipmentFacilityRated8 = clsContent.GetEquipmentFacilityRated(schoolid, 8, 1);
            var getLaboratoryFacilityRated1 = clsContent.GetLaboratoryFacilityRated(schoolid, 1, 3);
            var getLaboratoryFacilityRated2 = clsContent.GetLaboratoryFacilityRated(schoolid, 2, 3);
            var getLaboratoryFacilityRated3 = clsContent.GetLaboratoryFacilityRated(schoolid, 3, 3);
            var getLaboratoryFacilityRated8 = clsContent.GetLaboratoryFacilityRated(schoolid, 8, 3);
            var getReagentFacilityRated1 = clsContent.GetReagentFacilityRated(schoolid, 1, 2);
            var getReagentFacilityRated2 = clsContent.GetReagentFacilityRated(schoolid, 2, 2);
            var itemsectionSubject = clsContent.GetItemSectionSubjectBySchool(schoolid);
            var staffsectionSubject = clsContent.GetStaffSectionSubjectBySchool(schoolid,classId);

            if (reasonName == "Adequate Equipment")
            {
                if (isChecked)
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec1(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.SubjectID == 1 && staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                    getReagentFacilityRated1.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }

                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec1(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.SubjectID == 2 && staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                    getReagentFacilityRated2.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 3)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec1(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.SubjectID == 3 && staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated3.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 8)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec1(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.SubjectID == 8 && staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }


                    }
                    return Json(new {status = false}, JsonRequestBehavior.AllowGet);
                }

                else
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == null &&
                                    getReagentFacilityRated1.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == null &&
                                    getReagentFacilityRated2.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 3)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated3.EquipmentFacility == null &&
                                    getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 8)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentEquip(schoolid,
                                subjectid);

                            procItemSectionAssessment.EquipmentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.EquipmentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.SubjectID == 8 && staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated8.EquipmentFacility == null &&
                                    getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateEquipment = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateEquipment);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else if (reasonName == "Adequate Reagent")
            {
                if (isChecked)
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentReagent(
                                schoolid, subjectid);

                            procItemSectionAssessment.ReagentFacility = 1;

                            //UpdateEquipmentFacilityDerec1
                            var updateReagentFacilityDerec = clsContent.UpdateReagentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.ReagentFacility);

                            // procSchoolSubjectDerec.AdequateReagent = 1;


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                    getReagentFacilityRated1.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(
                                        schoolid, subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateReagent = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecReagentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateReagent);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentReagent(
                                schoolid, subjectid);

                            procItemSectionAssessment.ReagentFacility = 1;

                            //UpdateEquipmentFacilityDerec1
                            var updateReagentFacilityDerec = clsContent.UpdateReagentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.ReagentFacility);

                            // procSchoolSubjectDerec.AdequateReagent = 1;


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                    getReagentFacilityRated2.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(
                                        schoolid, subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateReagent = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecReagentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateReagent);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {

                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentReagent(schoolid,
                                subjectid);

                            procItemSectionAssessment.ReagentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateReagentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.ReagentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                    getReagentFacilityRated1.ReagentFacility == null &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateReagent = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateReagent);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {

                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentReagent(schoolid,
                                subjectid);

                            procItemSectionAssessment.ReagentFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateReagentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.ReagentFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                    getReagentFacilityRated2.ReagentFacility == null &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateReagent = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateReagent);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }

            else if (reasonName == "Adequate Laboratory")
            {
                if (isChecked)
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                    getReagentFacilityRated1.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }

                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                    getReagentFacilityRated2.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 1;
                                        var updateSchoolSubjecLaboratoryFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 3)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated3.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated3.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 1;
                                        var updateSchoolSubjeclaboratoryFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 8)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = 1;

                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);


                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated8.LaboratoryFacility == 1)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, true);
                                    if (schoolSubjectTrue.IsApproved)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 1;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecLabFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }

                    }
                }

                else
                {
                    if (subjectid == 1)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                                    getReagentFacilityRated1.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated1.LaboratoryFacility == null)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 2)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                                    getReagentFacilityRated2.ReagentFacility == 1 &&
                                    getLaboratoryFacilityRated2.LaboratoryFacility == null)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 3)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateEquipmentFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated3.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated3.LaboratoryFacility == null)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecEquipmentFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                    if (subjectid == 8)
                    {
                        using (var tran = new TransactionScope())
                        {
                            var procItemSectionAssessment = clsContent.ProcItemSectionAssessmentLabora(schoolid,
                                subjectid);

                            procItemSectionAssessment.LaboratoryFacility = null;
                            //procSchoolSubjectDerec.AdequateEquipment = 0;
                            var updateEquipmentFacilityDerec = clsContent.UpdateLaboratoryFacilityDerec(schoolid,
                                subjectid, procItemSectionAssessment.LaboratoryFacility);

                            foreach (var staffSubject in staffsectionSubject)
                            {
                                if (staffSubject.FinalRating == 1 &&
                                    getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                                    getLaboratoryFacilityRated8.LaboratoryFacility == null)
                                {
                                    //UpdateSchoolSubjecToTrueIfAfter
                                    var schoolSubjectTrue = clsContent.UpdateSchoolSubjecToTrueIfAfter(schoolid,
                                        subjectid, false);
                                    if (schoolSubjectTrue.IsApproved == false)
                                    {
                                        procSchoolSubjectDerec.AdequateLaboratory = 0;
                                        var updateSchoolSubjecEquipmentFacilityDerec = clsContent
                                            .UpdateSchoolSubjecLabFacilityDerec(
                                                schoolid, subjectid, procSchoolSubjectDerec.AdequateLaboratory);
                                    }

                                }
                            }
                            tran.Complete();
                            return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else if (reasonName == "Qualified Staff")
            {
                if (isChecked)
                {
                    //using (var tran = new TransactionScope())
                    //{
                    procStaffSummaryDerec.FinalRating = 1;

                    var updateQualifiedStaffStatus1 = clsContent.UpdateQualifiedStaffStatus1(schoolid,
                        subjectid, procStaffSummaryDerec.FinalRating);
                    //procSchoolSubjectDerec
                    if (subjectid == 1)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 1 &&
                            getEquipmentFacilityRated1.EquipmentFacility == 1 &&
                            getLaboratoryFacilityRated1.LaboratoryFacility == 1 &&
                            getReagentFacilityRated1.ReagentFacility == 1)
                        {
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, true, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 2)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 1 &&
                            getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                            getLaboratoryFacilityRated2.LaboratoryFacility == 1 &&
                            getReagentFacilityRated2.ReagentFacility == 1)
                        {
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, true, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 3)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 1 &&
                            getLaboratoryFacilityRated3.LaboratoryFacility == 1 &&
                            getEquipmentFacilityRated3.EquipmentFacility == 1)
                        {
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, true, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 8)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 1 &&
                            getLaboratoryFacilityRated8.LaboratoryFacility == 1 &&
                            getEquipmentFacilityRated8.EquipmentFacility == 1)
                        {
                            procSchoolSubjectDerec.QualifiedStaff = 1;
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, true, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid != 1 && subjectid != 2 && subjectid != 3 && subjectid != 8)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 1)
                        {
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, true, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    procStaffSummaryDerec.FinalRating = 0;

                    var updateQualifiedStaffStatus1 = clsContent.UpdateQualifiedStaffStatus1(schoolid,
                        subjectid, procStaffSummaryDerec.FinalRating);

                    if (subjectid == 1)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 0 &&
                            getEquipmentFacilityRated8.EquipmentFacility == 1 &&
                            getLaboratoryFacilityRated1.LaboratoryFacility == 1 &&
                            getReagentFacilityRated1.ReagentFacility == 1)
                        {
                            procSchoolSubjectDerec.QualifiedStaff = 0;
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, false, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 2)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 0 &&
                            getEquipmentFacilityRated2.EquipmentFacility == 1 &&
                            getLaboratoryFacilityRated2.LaboratoryFacility == 1 &&
                            getReagentFacilityRated2.ReagentFacility == 1)
                        {
                            procSchoolSubjectDerec.QualifiedStaff = 0;
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, false, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 3)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 0 &&
                            getLaboratoryFacilityRated3.LaboratoryFacility == 1 &&
                            procSchoolSubjectDerec.EquipmentFacility == 1)
                        {
                            procSchoolSubjectDerec.QualifiedStaff = 0;
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, false, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid == 8)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 0 &&
                            getLaboratoryFacilityRated8.LaboratoryFacility == 1 &&
                            getEquipmentFacilityRated8.EquipmentFacility == 1)
                        {
                            procSchoolSubjectDerec.QualifiedStaff = 0;
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, false, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                    if (subjectid != 1 && subjectid != 2 && subjectid != 3 && subjectid != 8)
                    {
                        if (updateQualifiedStaffStatus1.FinalRating == 0)
                        {
                            var updateSchoolSubject4QualifiedStaff = clsContent.UpdateSchoolSubject4QualifiedStaff(
                                schoolid, subjectid, false, procSchoolSubjectDerec.QualifiedStaff);
                        }
                        return Json(new {status = true}, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            else if (reasonName == "Examination Malpractice")
            {
                if (isChecked)
                {
                    //using (var tran = new TransactionScope())
                    //{
                    procSchoolSubjectDerec.ExaminationMalpractice = 1;

                    var updateSchoolSubjectExaminationMalpractice = clsContent
                        .UpdateSchoolSubjectExamMalPracticeDerec(
                            schoolid, subjectid, true, procSchoolSubjectDerec.ExaminationMalpractice);


                    //   tran.Complete();
                    return Json(new {status = true}, JsonRequestBehavior.AllowGet);

                    // }
                }
                else
                {
                    using (var tran = new TransactionScope())
                    {
                        procSchoolSubjectDerec.ExaminationMalpractice = 0;

                        var updateSchoolSubjectExaminationMalpractice = clsContent
                            .UpdateSchoolSubjectExamMalPracticeDerec(
                                schoolid, subjectid, false, procSchoolSubjectDerec.ExaminationMalpractice);


                        tran.Complete();
                        return Json(new {status = false}, JsonRequestBehavior.AllowGet);
                    }
                }
            }






            return Json(new {status = "No operation was done"}, JsonRequestBehavior.AllowGet);
        }

        //fetch subject from school subject table
        //fetch subject from facility summary table
        //fetch subject from laboratory table
        //fetch subject from staff subject to class
        //subject or sstaffsubject to class table and do update in all


        //Derecognised
        public ActionResult DerecogniseSchool()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateSchoolSubject(int updateSubjectId, int? updateSchoolId)
        {

            return View();
        }

        public ActionResult RemoveUpdateSubFromSchoolSub(int[] AvailableStateSelectedEdit, long schoolid, string options,
            int removesubjectid, bool removehasitems)
        {
            /*to remove subject if based on malpractise=============a subject on malpractice can derecognise a school=======
             * go set the active status in the subject-school-item table to false if has item
             * the date-deactivated if not
             * Then go to staff subject table and set the active status to false and relatively
             * the staff degree table and staff credential table
             * and then finally the school-subject table indicate the barred status is barred set to true and the date with the userid
             * to remove subject if based on Facility==========
             * go set the active status in the subject-school-item table to false
             * the date-deactivated
             * Then go to staff subject table and set the active status to false and relatively
             * the staff degree table and staff credential table
             * and then finally the school-subject table indicate the barred status and isbarred set to true and the date with the userid
             * to remove subject if based on Teacher==========
             * go set the active status in the subject-school-item table to false if has item
             * the date-deactivated if not
             * Then go to staff subject table and set the active status to false and relatively
             * the staff degree table and staff credential table
             * and then finally the school-subject table indicate the barred status is barred set to true and the date with the userid
             * to remove subject if based on Teacher==========
             *go set the active status in the subject-school-item table to false if has item
             * the date-deactivated if not
             * Then go to staff subject table and set the active status to false and relatively
             * the staff degree table and staff credential table
             * and then finally the school-subject table indicate the barred status is barred set to true and the date with the userid               
             */
            if (options == "Malpratice")
            {
                if (removehasitems == true)
                {
                    using (TransactionScope xx = new TransactionScope())
                    {
                        clsContent.GetSchoolsIsactive();

                        clsContent.UpdateForDerecognition_X(schoolid);
                        var malpractisederecognised = clsContent.UpdateSchoolForDerecognition_A(schoolid);
                        if (malpractisederecognised)
                        {
                            TempData["schoolderecognised"] = "school successfully derecognised for malpractice";
                        }
                    }
                }

            }
            if (options == "Facility")
            {

            }
            if (options == "Teacher")
            {

            }
            if (options == "TextBook")
            {

            }
            return RedirectToAction("UpdateSubject");
        }

        public ActionResult AddSubToSchoolSubCheck(int? subjectid, int[] subjectArray, int? schoolid,
            string radiobuttonvalue = "")
        {
            return View();
        }

        //AddSubToSchoolSubCheck
        public ActionResult AddSubToSchoolSub(int? subjectid, int[] subjectArray, int? schoolid,
            string radiobuttonvalue = "")
        {
            return View();
        }

        /*  TempData["Message"] = CurrentUser.EmailAddress;
            var user = clsContent.GetAUserNoRole(userId);
            UserViewModel userModel=new UserViewModel();
           // userModel.RoleID = user.RoleID;
            userModel.isActive = user.isActive;
            userModel.EmailAddress = user.EmailAddress;
            userModel.RoleID = user.RoleID;
            userModel.RoleName = user.RoleName;
            // var roles = clsContent.GetRoles();
            //var stateViewModel=clsContent.GetStateByUserId(userId);
            //ViewBag.States = stateViewModel.States;
            var roless = clsContent.GetRole();
            
            //var stateViewModel = GetStateFormData();
            //ViewBag.States = stateViewModel.States;

            return Json(new {
                EmailAddress = userModel.EmailAddress,
                //RoleId = userModel.RoleID,
                isActive = userModel.isActive,
                RoleName = userModel.RoleName,
                Roles = roless, /*states = stateViewModel.States,*/
        //statesSelected = stateViewModel.AvailableStateSelected
        //  },
        //   JsonRequestBehavior.AllowGet);
        //return RedirectToAction("AssignRole");*/
        //EditItem
        public ActionResult EditItem(int editid)
        {
            if (editid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var item = clsContent.GetExistingSchoolEquipValueById(editid);
            SchoolEquipConfigViewModel itemview = new SchoolEquipConfigViewModel();
            itemview.ItemDescription = item.ItemDescription;
            itemview.QuantityRequired = item.QuantityRequired;
            itemview.EquipmentValue = item.EquipmentValue;
            itemview.DigitValue = item.DigitValue;
            TempData["Message"] = CurrentUser.EmailAddress;
            //Date updated
            //updated by or entry updated by
            //is barred barred by userid
            return Json(new
            {
                ItemDescription = itemview.ItemDescription,
                QuantityRequired = itemview.QuantityRequired,
                EquipmentValue = itemview.EquipmentValue,
                DigitValue = itemview.DigitValue
            },
                JsonRequestBehavior.AllowGet);

        }




        public ActionResult LaboratorySummary(int totalLaboratoryItems, int subjectid, int facilityCategoryId,
            bool generalAssessment)
        {
            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

            int decision = 0;
            decision = generalAssessment == true ? 1 : 0;

            // clsContent.SaveEquipmentSummary(totalEquipmentItems, subjectid, totalSum, facilityCategoryId, percentage, sddc);

            var result = clsContent.SaveLaboratorySummary(totalLaboratoryItems, subjectid, facilityCategoryId, schoolid,
                decision);
            if (result.ID > 0)
            {
                return Json(new {status = true});
            }
            return Json(new {status = false});

        }

        public ActionResult EquipmentSummary(int totalEquipmentItems, int subjectsid, int totalsum,
            int facilityCategoryId)
        {
            var schoolid = long.Parse(ConstantsService.GetCookie(ConstantsService.CookieSchoolIdKey).Value);

            FacilityApprovalModel result = null;
            var facilitysummary = clsContent.GetFacilitySummaryBySchoolId(schoolid, facilityCategoryId, subjectsid);
            if (facilitysummary == null)
            {
                double computed = ((double) totalsum/(double) totalEquipmentItems)*100;
                // var percentage = computed + "%";
                var percentage = Math.Round(computed, 2).ToString(CultureInfo.InvariantCulture);
                result = clsContent.SaveEquipmentSummary(totalEquipmentItems, subjectsid, totalsum, facilityCategoryId,
                    percentage, schoolid);
                if (result.ID > 0)
                {
                    return Json(new {status = true});
                }
                return Json(new {status = false});

            }
            else
            {
                double computed = ((double) totalsum/(double) totalEquipmentItems)*100;
                // var percentage = computed + "%";
                var percentage = Math.Round(computed, 2).ToString(CultureInfo.InvariantCulture);
                var facilitysummary2 = clsContent.UpdateFacilitySummaryBySchId(schoolid, facilityCategoryId, subjectsid,
                    totalsum, percentage);
                if (facilitysummary2.ID > 0)
                {
                    return Json(new {status = true});
                }
                return Json(new {status = false});
            }


        }

        public ActionResult ReagentSummary(int totalEquipmentItems, int subjectid, int totalSum, int facilityCategoryId,
            long sddc)
        {
            FacilityApprovalModel result = null;
            var facilitysummary = clsContent.GetFacilitySummaryBySchoolId(sddc, facilityCategoryId, subjectid);
            if (facilitysummary == null)
            {
                double computed = ((double) totalSum/(double) totalEquipmentItems)*100;
                //var percentage = computed + "%";
                var percentage = Math.Round(computed, 2).ToString(CultureInfo.InvariantCulture);
                result = clsContent.SaveEquipmentSummary(totalEquipmentItems, subjectid, totalSum, facilityCategoryId,
                    percentage, sddc);
                if (result.ID > 0)
                {
                    return Json(new {status = true});
                }
                return Json(new {status = false});

            }
            else
            {
                double computed = ((double) totalSum/(double) totalEquipmentItems)*100;
                var percentage = computed + "%";
                var facilitysummary2 = clsContent.UpdateFacilitySummaryBySchId(sddc, facilityCategoryId, subjectid,
                    totalSum, percentage);
                if (facilitysummary2.ID > 0)
                {
                    return Json(new {status = true});
                }
                return Json(new {status = false});
            }

        }


        public ActionResult VetRegisteredSchools(string state = "", int page = 1, int pageSize = 5)
        {
             var userId = CurrentUser.UserId;
            var user = clsContent.GetUserById(userId);
            IPagedList<SchoolProfileViewModel1> schools = null;
            schools = clsContent.GetSchoolsByOfficeId(user.OfficeID, page, pageSize);
            // var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;
            return View(schools);
        }


        

        public ActionResult GetSignature(int userId)
        {
            var user = clsContent.GetUserById(userId);
            return user != null ? Json(new {signature = user.Signature, status = true}, JsonRequestBehavior.AllowGet) 
                : 
                Json(new {message = "User not found", status = false}, JsonRequestBehavior.AllowGet);
        }
    }
}


