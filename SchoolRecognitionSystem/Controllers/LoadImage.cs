﻿using System.Web;

namespace SchoolRecognitionSystem.Controllers
{
    public class LoadImage
    {
        public HttpPostedFileBase fileData { get; set; }
        public string name { get; set; }
    }
}