﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Models;


namespace SchoolRecognitionSystem.Controllers
{
    public class FacilityController : Controller
    {
        // GET: Facility


        public static List<Student2> Students = new List<Student2>()
            {
                new Student2 {
                    ID = 1,
                    Name = "Adam Worth",
                    email = "adam.worthh@abc.edu",
                },
                new Student2 {
                    ID = 2,
                    Name = "John Doe",
                    email = "john.doe@abc.edu",
                },
                new Student2 {
                    ID = 3,
                    Name = "Gorge Klene",
                    email = "gorge.klene@abc.edu",
                }
            };
    
        public ActionResult Index()
        {
            StudentModel _objstudentmodel = new StudentModel();
            _objstudentmodel.StudentList = new List<Student>();
            _objstudentmodel.StudentList.Add(new Student { Name = "Name1", ClassOfStudent = "12th", Section = "B", Address = "Address1" });
            _objstudentmodel.StudentList.Add(new Student { Name = "Name2", ClassOfStudent = "5th", Section = "A", Address = "Address2" });
            _objstudentmodel.StudentList.Add(new Student { Name = "Name3", ClassOfStudent = "10th", Section = "C", Address = "Address3" });
            _objstudentmodel.StudentList.Add(new Student { Name = "Name4", ClassOfStudent = "1st", Section = "A", Address = "Address4" });
            _objstudentmodel.StudentList.Add(new Student { Name = "Name5", ClassOfStudent = "8th", Section = "B", Address = "Address5" });
            return View(_objstudentmodel);
        }
        [HttpPost]
        public ActionResult Index(StudentModel _objstudentmodel)
        {
            return View(_objstudentmodel);
        }

        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();


        public ActionResult FacilityValue1()
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            var schoolsuject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid).ToList();
            var suj = schoolsuject.Select(y => new SelectListItem
            {
                Text = y.Subject.LongName,
                Value = y.Subject.ID.ToString()
            });
            ViewBag.Subjects = suj;

            //var sus = ctx.Subjects.Select(y => new SelectListItem
            //{
            //    Text = y.LongName,
            //    Value = y.ID.ToString()

            //});
            //ViewBag.Subjects = sus;

            return View(new Subject());
        }
        public ActionResult PartialViewDemo()
        {
            List<Subject> allCourse = ctx.Subjects.ToList();
            return View(allCourse);
        }

        public ActionResult PartialViewDemo2()
        {
            
            var ss = ctx.Subjects.ToList();
            
            Subject cv = new Subject();
            cv.Subjects= (from d in ss
                                     select new SelectListItem
                                     {
                                         Value = d.ID.ToString(),
                                         Text = d.LongName
                                     }).ToList();
            return View(cv);
        }
        //[HttpPost]
        //public ActionResult FacilityValue2(string subjectid)
        //{
        //    //IEnumerable<Course> allCourses = _repository.GetCourses();
        //    //var selectedCourseId = (from c in allCourses where c.CourseName == courseName select c.CourseId).FirstOrDefault();

        //    //IEnumerable<Faculty> allFaculties = _repository.GetFaculties();
        //    //var facultiesForCourse = allFaculties.Where(f => f.AllotedCourses.Any(c => c.CourseId == selectedCourseId)).ToList();
        //    IEnumerable<Subject> allSubjects = ctx.Subjects.ToList();
        //    //DataSet xc = allSubjects;
        //    var selectedSubject = (from s in allSubjects where s.ID == int.Parse(subjectid) select s.ID).FirstOrDefault();

        //    var allFacility = ctx.FacilityTypeSettings.Include(m => m.FacilityUserValues).ToList();
        //    var facility4Subject = allFacility.Where(f => f.FacilitySubjectSettings.Any(fs => fs.SubjectID == selectedSubject)).ToList();

        //    //var facilityusers = ctx.FacilityUserValues.ToList();
        //    //var facilitySubject = facilityusers.Where(fu => fu.FacilityTypeSetting.FacilitySubjectSettings.Any(fs => fs.SubjectID == int.Parse(subjectid))).ToList();



        //    var qw = from ee in facility4Subject
        //             select new FacilityTypeSetting
        //             {
        //                 ID = ee.ID,
        //                 ItemDescription = ee.ItemDescription,
        //                 FacilityUserValues = ee.FacilityUserValues.Select(f => new FacilityUserValue
        //                 {
        //                     UserValue = f.UserValue
        //                 }).ToList(),

        //             };





        //    return PartialView("_ShowFacilityPV", qw);
        //}
        

        public ActionResult Index2()
        {
            return View(People.GetPeople());
        }

        [HttpPost]
        public ActionResult Test(FormCollection collection)
        {
            return PartialView("~/Views/Home/_Person.cshtml", new People
            {
                GivenName = "Tony",
                Surname = "Sheridan",
                Age = 73
            });
        }
        public ActionResult Indexgrid2()
        {
            return View(Students);
        }
        [HttpPost]
        public JsonResult InsertData(Student2 inStudent)
        {
            String result = String.Empty;

            Models.Student2 dup = Students.Find(p => p.ID == inStudent.ID);

            if (dup == null)
            {
                Students.Add(inStudent);
                result = "1";
            }
            else
            {
                result = "0";
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveData(Models.Student2 student)
        {
            int index = 0;
            String result = String.Empty;

            index = Students.FindIndex(p => p.ID == student.ID);
            if (index >= 0)
            {
                Students[index] = student;
                result = "1";
            }
            else
                result = "0";

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteData(Models.Student2 student)
        {
            String result = String.Empty;

            Models.Student2 s = Students.Find(p => p.ID == student.ID);
            if (s != null)
            {
                Students.Remove(s);
                result = "1";
            }
            else
                result = "0";

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }







}




