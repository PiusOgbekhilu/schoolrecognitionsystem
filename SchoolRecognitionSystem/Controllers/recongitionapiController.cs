﻿using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace SchoolRecognitionSystem.Controllers
{

    [RoutePrefix("api/recog")]
    public class recongitionapiController : ApiController
    {

        [Route("GetState")]
        [HttpGet]
        public IHttpActionResult GetStates()
        {
            IHttpActionResult ret = null;
            
            try
            {
                var objResult = clsContent.GetStates();

                if (objResult != null)
                    ret = Ok(objResult);
                else ret = NotFound();
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }

        //[Route("GetSchoolName")]
        //[HttpGet]
        //public IHttpActionResult GetSchoolName(string schoolname)
        //{
        //    IHttpActionResult ret = null;

        //    try
        //    {
        //        var objResult = clsContent.CheckIfSchoolNameExists(schoolname);

        //        if (objResult != null)
        //            ret = Ok("School name exist");
        //        //else ret = Ok("School name ");
        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        return Conflict();
        //    }
        //    return ret;
        //}
        //[Route("CheckIfSchoolNameExists")]
        //[HttpGet]
        //public IHttpActionResult CheckIfSchoolNameExists(string schoolname)
        //{
        //    IHttpActionResult ret = null;

        //    try
        //    {
        //        var objResult = clsContent.CheckIfSchoolNameExists(schoolname);

        //        if (objResult != null)
        //            ret = Ok(objResult);
        //        else ret = NotFound();
        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        return Conflict();
        //    }
        //    return ret;
        //}


        [Route("GetCourse")]
        [HttpGet]
        public IHttpActionResult GetCourse(int SubjectID)
        {
            IHttpActionResult ret = null;

            try
            {
                var objResult = clsContent.GetCourseBySubjectId((int)SubjectID);

                if (objResult != null)
                    ret = Ok(objResult);
                else ret = NotFound();
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }
        [Route("GetStaffCourse")]
        [HttpGet]
        public IHttpActionResult GetStaffCourse(int StaffID)
        {
            IHttpActionResult ret = null;

            try
            {
                var objResult = clsContent.GetStaffDegreeCourse((int)StaffID);

                if (objResult != null)
                    ret = Ok(objResult);
                else ret = NotFound();
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }
        [Route("GetStaffDegree")]
        [HttpGet]
        public IHttpActionResult GetStaffDegree(int StaffID)
        {
            IHttpActionResult ret = null;

            try
            {
                var objResult = clsContent.GetStaffDegreeCourse((int)StaffID);

                if (objResult != null)
                    ret = Ok(objResult);
                else ret = NotFound();
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }






        [Route("GetLgas")]
        [HttpGet]
        public IHttpActionResult GetLgas(string StateID)
        {
            IHttpActionResult ret = null;

            try
            {
                var objResult = clsContent.GetLgas(StateID);
                //TempData["Employee"] = 8;
                //Session.SetDataInSession("UserAdminID", string.Format("{0}", user.ID));
                if (objResult != null)
                    ret = Ok(objResult);
                else ret = NotFound();
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }
       
        [Route("SaveSchoolProfile")]
        [ResponseType(typeof(SchoolProfileViewModel))]
        [HttpPost]
        public IHttpActionResult SaveSchoolProfile(SchoolProfileViewModel model)
        {
            IHttpActionResult ret = null;
            if (model == null)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var objResult = clsContent.AddNewSchool(model);

                if (objResult != null)
                    ret = Ok("School Profile Submitted.");
              
                else ret = Ok("School Profile submission failed.");
            }
            catch (DbUpdateException ex)
            {
                return Conflict();
            }
            return ret;
        }
    }
}
