﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Models;
using SchoolRecognitionSystem.Service;
using System.Transactions;
using SchoolRecognitionSystem.Pdf;
using System.IO;
using System.Net;
using SchoolRecognitionSystem.Report;
using System.Data;
using PagedList;
using SchoolRecognitionSystem.Classes;

namespace SchoolRecognitionSystem.Controllers
{
    public class SchoolController : Controller
    {
        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();
        // GET: School


        public ActionResult LogOut()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", "Home");
        }


        public ActionResult NewSchool()
        {

            return View();
        }
        public ActionResult Welcome()
        {
            if (Session.GetDataFromSession<string>("UserID") == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TempData["Message"] = "Welcome " + Session.GetDataFromSession<string>("UserFullName");
            return View();
        }



        [Authorize]
        public ActionResult Index()
        {
            var lists = ctx.SchoolProfiles.ToList();
            return View(lists);
        }

        public JsonResult SchoolExists(string SchoolName)
        {

            var aschool = ctx.SchoolProfiles.Where(u => u.SchoolName == SchoolName).FirstOrDefault();
            if (aschool != null)
            {
                return Json(!String.Equals(SchoolName, aschool.SchoolName, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                return null;
            }
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var sch = ctx.SchoolProfiles.Where(c => c.ID == id).FirstOrDefault();
            var vwsch =ctx.vwSchoolProfileReports.Where(c=>c.SchID== (int)sch.ID).FirstOrDefault();
            if (sch == null)
            {
                return HttpNotFound();
            }
            return View(vwsch);
        }

        public ActionResult Pdf(int SchID)
        {

           
            var nullableData = (from F in ctx.vwSchoolProfileReports
                                where F.SchID == SchID && F.Category == "PROPRIETOR"
                                select new
                                {
                                    F.SchoolName,
                                    F.SchoolLocation,
                                    F.YearEstablished,
                                    F.IsJointProprietorship,
                                    F.IsSchoolFencedRound,
                                    F.AproximateLandArea,
                                    F.ContactAddress,
                                    F.isCurricullumAvailable,
                                }).ToList();

            DataTable c = RemoveNullableClass.ToDataTable(nullableData);

            schoolProfile rpt = new schoolProfile();
            rpt.Load();
            rpt.SetDataSource(c);
            Stream s = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(s, "application/pdf");

            //List<SchoolEquipView> model = new List<SchoolEquipView>();
            //model.Add(new SchoolEquipView { FacilityID=9, TypeOfFacility = "Joe Blogs" });
            //string reportPath = Path.Combine(Server.MapPath("~/Report"), "schoolProfile.rpt");
            //return new CrystalReportPdfResult(reportPath, nullableData);
        }
        public ActionResult Edit(int id)
        {
            var school =ctx.SchoolProfiles.Find(id);
          //  ViewBag.category = new SelectList(db.RankTbls, "ID", "Rank", waec_staff.RANK);
            ViewBag.category = new SelectList(ctx.SchoolCategories.ToList(), "ID", "Category");
            return View(school);
        }

        //
        // POST: /Person/Edit/5

        [HttpPost]
        public ActionResult Edit(SchoolProfile school)
        {
            ctx.Entry(school).State = EntityState.Modified;
            ViewBag.category = new SelectList(ctx.SchoolCategories.ToList(), "ID", "Category");
            ctx.SaveChanges();
            return RedirectToAction("Index");
        }




        [HttpGet]
        public ActionResult SchoolSubjectOffered()
        {
            //Here  MyDatabaseEntities  is our datacontext 
            // List<SchoolSubject> allSchoolSubject = new List<SchoolSubject>();
            // var allSchoolSubject = ctx.SchoolSubjects.Where(sc => sc.SchoolProfile.ID == 1).Select(x => new { x.ID, x.Subject.LongName }).ToList();
            // var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var itemss = ctx.SchoolSubjects.Where(sc => sc.SchoolProfile.ID == schoolid).ToList();

            var yall = from p in itemss
                       select new SchoolSubject
                       {
                           ID = p.ID,
                           //LongName = p.Subject.LongName
                       };

            return View(yall);

        }
        [HttpPost]
        public ActionResult SchoolSubjectOffered(string[] ids)
        {
            int index = 0;
            String result="";
            int schoolid = int.Parse(Session["schoolid"].ToString());
            List<SchoolSubject> allSub = new List<SchoolSubject>();

            allSub = ctx.SchoolSubjects.OrderBy(a => a.ID).ToList();
            int[] intSelcted = ids.Select(int.Parse).ToArray();
            //================================================================================
          
            if (intSelcted != null)
            {

                for (int p = 0; p < intSelcted.Length; p++)
                {
                    var selectedid = intSelcted[p];
                    var ReturnedSchool = ctx.SchoolSubjects.Where(sc => selectedid == sc.ID).FirstOrDefault();
                    
                    ReturnedSchool.isTextBookAttached =true;
                    ctx.SaveChanges();
                }
            }
            index = 1;
            if (index > 0)
            {

                result = "1";
            }
            else
            {
                result = "0";
            }
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            //// return RedirectToAction("FacilityValue1", "School");
            TempData["facility"] = sch.SchoolName;
            return Json(result, JsonRequestBehavior.AllowGet);
        }
      
            
           



        [HttpGet]

        public ActionResult CreateSchoolSubject()
        {
            //int schoolid = 1;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;
            vmSchoolSubject SchoolSubject = new vmSchoolSubject { AvailableSubjects = ctx.Subjects.ToList(), RequestedSubjects = new List<Subject>() };
            return View(SchoolSubject);

        }
        [HttpPost]
        public ActionResult CreateSchoolSubject(vmSchoolSubject model)
        {
            // long schoolid = 1;
            int schoolid =int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            //sch.Subjects = new List<Subject>();


            foreach (var s in model.AvailableSelected)
            {

                var Subject = ctx.Subjects.Find(s);
                //sch.Subjects.Add(Subject);
            }
            //  string loguser = Session["HoldUser"].ToString();
            // var uu = ctx.Users.Where(u => u.UserName == loguser).First();
            //foreach (var schsub in sch.Subjects)
            //{
            //    var schoolSub = new SchoolSubject()
            //    {
            //        SchoolID = schoolid,
            //        SubjectID = schsub.ID,
            //        DateUpdated = DateTime.Now,
            //        UpdatedBy = 2


            //    };

            //    ctx.SchoolSubjects.Add(schoolSub);
            //    ctx.SaveChanges();

            //}


            TempData["CreateSchoolStaff"] = sch.SchoolName;
            return RedirectToAction("CreateSchoolStaff", "School");
            // return RedirectToAction("CreateExaminationInfo", "School");
        }


        [HttpGet]

        public ActionResult CreateExaminationInfo()
        {
            // long schoolid = 1;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;
            return View(new ExaminationHall());
        }
        [HttpPost]
        public ActionResult CreateExaminationInfo(ExaminationHall exh)
        {
            // long schoolid = 1;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ExaminationHall exam = new ExaminationHall();
            exam.SchoolID = schoolid;
            exam.NumberofDesksAndChairsAvailableInExamHalls = exh.NumberofDesksAndChairsAvailableInExamHalls;
            exam.NumberofExaminationHall = exh.NumberofExaminationHall;
            exam.CapacityofEachExaminationsHall = exh.CapacityofEachExaminationsHall;
            ctx.ExaminationHalls.Add(exam);
            ctx.SaveChanges();
            TempData["classroom"] = sch.SchoolName;
            return RedirectToAction("CreateClassroomInfo", "School");
        }




        [HttpGet]
        public ActionResult CreateClassroomInfo()
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;
            return View();
        }



        [HttpPost]
        public ActionResult CreateClassroomInfo(ClassRoom cr)
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
            ViewBag.SchoolName = sch.SchoolName;
            ClassRoom crm = new ClassRoom();
            crm.SchoolID = schoolid;
            crm.NumberofClassroomInUse = cr.NumberofClassroomInUse;
            crm.CapacityOfEachClassRoom = cr.CapacityOfEachClassRoom;
            crm.DeskChairsAdequate = cr.DeskChairsAdequate;
            ctx.ClassRooms.Add(crm);
            ctx.SaveChanges();
            TempData["studet"] = sch.SchoolName;
            return RedirectToAction("CreateStudentInClass", "School");
        }




        [HttpGet]
        public ActionResult CreateStudentInClass()
        {
            var Classes = ctx.Classes.ToList();
            ViewBag.Classes = Classes.Select(schcat => new SelectListItem
            {
                Text = schcat.ClassName,
                Value = schcat.ID.ToString()
            });
            return View();
        }

        [HttpPost]
        public ActionResult CreateStudentInClass(ClassAllocation classalloc)
        {
            //long schoolid = 1;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(sh => sh.ID == schoolid).FirstOrDefault();
            int index = 0;
            String result = String.Empty;
            using (TransactionScope cc = new TransactionScope())
            {
               // int schoolid = 1;// int.Parse(Session["schoolid"].ToString());
               // var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();
                var Classes = ctx.Classes.ToList();
                ViewBag.Classes = Classes.Select(schcat => new SelectListItem
                {
                    Text = schcat.ClassName,
                    Value = schcat.ID.ToString()
                });

                ClassAllocation cls = new ClassAllocation();
                cls.ClassID = classalloc.ClassID;
                cls.NoOfStreams = classalloc.NoOfStreams;
                cls.SchoolID = schoolid;
                cls.TotalStudents = classalloc.TotalStudents;
                ctx.ClassAllocations.Add(cls);
                ctx.SaveChanges();
                index = 1;
                if (index > 0)
                {

                    result = "1";
                }
                else
                {
                    result = "0";
                }
                //// return RedirectToAction("FacilityValue1", "School");
                cc.Complete();
            }
            TempData["facility"] = sch.SchoolName;
            return Json(result, JsonRequestBehavior.AllowGet);
        }



        public ActionResult FillCity(int state)
        {
            var locals = ctx.LocalGovernments.Where(c => c.StateId == state);
            return Json(locals, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateSchool()
        {
            var categories = ctx.SchoolCategories.ToList();
            ViewBag.Categories = categories.Select(schcat => new SelectListItem
            {
                Text = schcat.Category,
                Value = schcat.ID.ToString()
            });

            var titles = ctx.Titles.ToList();
            ViewBag.titles = titles.Select(tt => new SelectListItem
            {
                Text = tt.Label,
                Value = tt.ID.ToString()
            });
                   
            var degs = ctx.Degrees.ToList();
            ViewBag.degrees = degs.Select(dg => new SelectListItem
            {
                Text = dg.DegreeType,
                Value = dg.ID.ToString()
            });


            var states = ctx.States.ToList();
            ViewBag.States = states.Select(st => new SelectListItem
            {
                Text = st.StateName,
                Value = st.Id.ToString()
            });


            var inspectionTypes = ctx.InspectionTypes.ToList();
            ViewBag.inspectionTypes = inspectionTypes.Select(st => new SelectListItem
            {
                Text = st.TypeOfInspection,
                Value = st.ID.ToString()
            });

            return View(new SchoolProfile());
        }
        public List<LocalGovernment> GetLgasByStateId(int? stateId)
        {
            var model = ctx.LocalGovernments.Where(l => l.StateId == stateId).ToList();
            return model;
        }




        [HttpPost]
        public ActionResult CreateSchool(SchoolProfile schoolprofile)
        {

            var categories = ctx.SchoolCategories.ToList();
            ViewBag.Categories = categories.Select(schcat => new SelectListItem
            {
                Text = schcat.Category,
                Value = schcat.ID.ToString()
            });


            var states = ctx.States.ToList();
            //ViewBag.AvailableStates = states;
            ViewBag.States = states.Select(st => new SelectListItem
            {
                Text = st.StateName,
                Value = st.Id.ToString()
            });

            var titles = ctx.Titles.ToList();
            ViewBag.titles = titles.Select(tt => new SelectListItem
            {
                Text = tt.Label,
                Value = tt.ID.ToString()
            });

            var degs = ctx.Degrees.ToList();
            ViewBag.degrees = degs.Select(dg => new SelectListItem
            {
                Text = dg.DegreeType,
                Value = dg.ID.ToString()
            });

            ViewBag.Lgas = GetLgasByStateId(schoolprofile.StateID).Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString()
            }).ToList();



            var inspectionTypes = ctx.InspectionTypes.ToList();
            ViewBag.inspectionTypes = inspectionTypes.Select(st => new SelectListItem
            {
                Text = st.TypeOfInspection,
                Value = st.ID.ToString()
            });

            //    var getschool = ctx.SchoolProfiles.Where(c => c.SchoolName == schoolprofile.SchoolName).FirstOrDefault();

            //{
            if (ModelState.IsValid)
            {
                var sch = new SchoolProfile
                {
                    CategoryID = schoolprofile.CategoryID,
                    SchoolName = schoolprofile.SchoolName,
                    SchoolLocation = schoolprofile.SchoolLocation,
                    StateID = schoolprofile.StateID,
                    LgaID = schoolprofile.LgaID,
                    YearEstablished = schoolprofile.YearEstablished,
                    IsJointProprietorship = schoolprofile.IsJointProprietorship,
                    AproximateLandArea = schoolprofile.AproximateLandArea,
                    IsLandAreaAdequate = schoolprofile.IsLandAreaAdequate,
                    IsSchoolFencedRound = schoolprofile.IsSchoolFencedRound,
                    IsAdmissionRegisterAvailable = schoolprofile.IsAdmissionRegisterAvailable,
                    isClassRegistersAvaialable = schoolprofile.isClassRegistersAvaialable,
                    isDiaryofWorksAvaialble = schoolprofile.isDiaryofWorksAvaialble,
                    isCurricullumAvailable = schoolprofile.isCurricullumAvailable,
                    IsScheemOfWorkAvailable = schoolprofile.IsScheemOfWorkAvailable,
                    OtherSpecify = schoolprofile.OtherSpecify
                };


                // sch.InspectionTypeID = schoolprofile.InspectionTypeID;
                ctx.SchoolProfiles.Add(sch);
                ctx.SaveChanges();
                TempData["Success"] = "School Profile is submitted successfully";
                ModelState.Clear();

                Session["schoolid"] = sch.ID;

                ViewBag.SchoolName = sch.SchoolName;

                return RedirectToAction("CreateSchoolSubject", "School");
            }

            else
            {

                TempData["Success"] = "Sorry ! Error creating School Profile";
                return View();
            }


        }








        public ActionResult RenderSelectLga(int selectedId = 0)
        {
            var model = GetLgasByStateId(selectedId);
            ViewBag.selectedId = selectedId;
            return PartialView(model);
        }


        //public ActionResult LibraryFacility()
        //{
        //    long schoolid =long.Parse(Session["schoolid"].ToString());
        //    //int schoolid = int.Parse(Session["schoolid"].ToString());
        //    var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

        //    var itemss = ctx.SchoolLibraryConfigs.ToList();

        //    var yall = from p in itemss
        //               select new Facility
        //               {
        //                   ID = p.ID,
        //                   ItemDescription = p.Facility.ItemDescription,
        //                   userValue = String.Empty

        //               };


        //    return PartialView("_ShowLibraryFacilityPV", yall);

           
        //}

     

        //public ActionResult RecreationalFacility()
        //{
        //    long schoolid =long.Parse(Session["schoolid"].ToString());
        //    //int schoolid = int.Parse(Session["schoolid"].ToString());
        //    var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

        //    var itemss = ctx.SchoolRecreatConfigs.ToList();

        //    var yall = from p in itemss
        //               select new Facility
        //               {
        //                   ID = p.ID,
        //                   ItemDescription = p.Facility.ItemDescription,
        //                   userValue = String.Empty

        //               };


        //    return PartialView("_ShowRecreationalFacilityPV", yall);


        //}




       



        public ActionResult ReagentFacility(int? subjectid = 0, int page = 1)
        {
            long schoolid = long.Parse(Session["schoolid"].ToString());
                               //int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

            var schoolsubject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid && s.Subject.HasItem == true).ToList();

            int pageSize = 20;
            int pageIndex = 1;
            var subjec = subjectid;
            var schy = schoolsubject.Select(s => new SelectListItem
            {
                Text = s.Subject.LongName,
                Value = s.Subject.ID.ToString(),
                Selected = (s.Subject.ID == subjec)
            });
            ViewBag.Subjects = schy;

            //   Session["subjectid"] = subjec;
            pageIndex = page;

            IPagedList<SchoolReagentConfig> eqp = null;
            eqp = ctx.SchoolReagentConfigs.Where(s => s.SubjectID == subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            ViewBag.subjectid = subjectid;
            return View(eqp);
        }
        
        public ActionResult LabFacility(int? subjectid = 0, int page = 1)
        {
            long schoolid = long.Parse(Session["schoolid"].ToString());
                               //int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

            var schoolsubject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid && s.Subject.HasItem == true).ToList();

            int pageSize = 20;
            int pageIndex = 1;
            var subjec = subjectid;
            var schy = schoolsubject.Select(s => new SelectListItem
            {
                Text = s.Subject.LongName,
                Value = s.Subject.ID.ToString(),
                Selected = (s.Subject.ID == subjec)
            });
            ViewBag.Subjects = schy;

            //   Session["subjectid"] = subjec;
            pageIndex = page;

            IPagedList<SchoolLabConfig> eqp = null;
            eqp = ctx.SchoolLabConfigs.Where(s => s.SubjectID == subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            ViewBag.subjectid = subjectid;
            return View(eqp);
        }
        


        public JsonResult SaveRecreationalFacility(string dd, string user, int record_id = 0)
        {

            String result = String.Empty;
            // long? schoolid = 66;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    var fac = ctx.SchoolRecreatConfigs.Where(f => f.ID == facid).FirstOrDefault();
                    var uv = new SchRecreaFacilityValue();
                    uv.SchRecreatConfigID = fac.ID;
                    uv.Value = user;
                    uv.SchoolID = schoolid;
                    ctx.SchRecreaFacilityValues.Add(uv);
                    ctx.SaveChanges();
                    return Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                                       JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uv = ctx.SchLIBFacilityValues.Find(record_id);
                    if (uv != null)
                    {
                        uv.userValue = user;
                        ctx.SaveChanges();
                        return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() },
                                        JsonRequestBehavior.AllowGet);

                    }
                }


            }
            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }



        public ActionResult RecreationalFacility(int page = 1)
        {
            long schoolid = long.Parse(Session["schoolid"].ToString());
                               //int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

           // var schoolsubject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid && s.Subject.HasItem == true).ToList();

            int pageSize = 20;
            int pageIndex = 1;
            //  var subjec = subjectid;
            //var schy = schoolsubject.Select(s => new SelectListItem
            //{
            //    Text = s.Subject.LongName,
            //    Value = s.Subject.ID.ToString(),
            //    Selected = (s.Subject.ID == subjec)
            //});
            //ViewBag.Subjects = schy;

            //   Session["subjectid"] = subjec;
            pageIndex = page;

            IPagedList<SchoolRecreatConfig> eqp = null;
            eqp = ctx.SchoolRecreatConfigs.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            //  ViewBag.subjectid = subjectid;
            return View(eqp);
        }
        public ActionResult LibraryFacility(int page = 1)
        {
            long schoolid =  long.Parse(Session["schoolid"].ToString());
                               //int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

            var schoolsubject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid && s.Subject.HasItem == true).ToList();

            int pageSize = 20;
            int pageIndex = 1;
          //  var subjec = subjectid;
            //var schy = schoolsubject.Select(s => new SelectListItem
            //{
            //    Text = s.Subject.LongName,
            //    Value = s.Subject.ID.ToString(),
            //    Selected = (s.Subject.ID == subjec)
            //});
            //ViewBag.Subjects = schy;

            //   Session["subjectid"] = subjec;
            pageIndex = page;

            IPagedList<SchoolLibraryConfig> eqp = null;
            eqp = ctx.SchoolLibraryConfigs.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
          //  ViewBag.subjectid = subjectid;
            return View(eqp);
        }



        public ActionResult FacilityValue1(int? subjectid = 0, int page = 1)
        {
            long schoolid =long.Parse(Session["schoolid"].ToString());
             //int schoolid = int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(ss => ss.ID == schoolid).FirstOrDefault();

            var schoolsubject = ctx.SchoolSubjects.Where(s => s.SchoolID == schoolid && s.Subject.HasItem == true).ToList();

            int pageSize = 20;
            int pageIndex = 1;
            var subjec = subjectid;
            var schy = schoolsubject.Select(s => new SelectListItem
            {
                Text = s.Subject.LongName,
                Value = s.Subject.ID.ToString(),
                Selected = (s.Subject.ID == subjec)
            });
            ViewBag.Subjects = schy;
           
            //   Session["subjectid"] = subjec;
            pageIndex = page;
       
            IPagedList<SchoolEquipConfig> eqp = null;
                    eqp = ctx.SchoolEquipConfigs.Where(s => s.SubjectID == subjec).OrderBy
                            (m => m.ID).ToPagedList(pageIndex, pageSize);
            ViewBag.subjectid = subjectid;
            return View(eqp);
        }

        [HttpPost]
        public JsonResult SaveLabFacility(Facility facinstance)
        {

            String result = String.Empty;     
            // long? schoolid = 61;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            if (facinstance != null)
            {
                var fac = ctx.SchoolLabConfigs.Where(f => f.ID == facinstance.ID).FirstOrDefault();

                //string subjectgrid = Session["subjectgrid"].ToString();
                //var subjectgridp = int.Parse(subjectgrid);
                //var facid = fac.SchoolEquipConfigs.Where(p => p.FacilityID == fac.ID).FirstOrDefault();
                //foreach (var user in facid)
                //{
                // var schoolsuject = ctx.SchoolSubjects.Where(xc => xc.SchoolID == schoolid).FirstOrDefault();
               SchoolLabValue uv = new SchoolLabValue();
                uv.SchoolLabConfigID = fac.ID;
                //uv.UserValue = facinstance.userValue;
                uv.SchoolID = schoolid;
                ctx.SchoolLabValues.Add(uv);
                ctx.SaveChanges();

            }

            //    result = "1";
            //}

            //else
            //{
            //    result = "0";
            //}

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        //public IPagedList<SchoolEquipConfig> SearchMerchantByStorename(string merchantStorename, int page = 1, int pagesize = 20)
        //{
        //    var merchants = _merchantRepository.Where(m => m.StoreName.Contains(merchantStorename), m => m.User);
        //    return merchants.Select(m => new MerchantDto()
        //    {
        //        Id = m.Id,
        //        FirstName = m.User.FirstName,
        //        LastName = m.User.LastName,
        //        PhoneNumber = m.User.PhoneNumber,
        //        StoreName = m.StoreName

        //    }).OrderByDescending(m => m.FirstName).ToPagedList(page, pagesize);
        //    //  return Mapper.Map<List<MerchantDto>>(merchants);
        //}
        



        //public ActionResult _ShowFacilitynewPV(FilterModel model, string subjectid, int page = 1, int pagesize = 20)
        //{
            
        //    FilterModel returnedmodel = null;
        //    if (Session["suj"] == null)
        //    {
        //        Session["suj"] = subjectid;
        //        var sessionsubject = Session["suj"].ToString();
        //        if (subjectid != null)
        //        {
        //            Session["suj"] = subjectid;
        //            EquipmentService service = new EquipmentService();
        //            var subjectidp = int.Parse(sessionsubject);
        //            returnedmodel = service.GetFilterModel(model, subjectid);
        //        }
        //    }
        //    else
        //    {
        //        var sessionsub = Session["suj"].ToString();
        //        EquipmentService service = new EquipmentService();
        //        // var subjectidp = int.Parse(sessionsub);
        //        returnedmodel = service.GetFilterModel(model, sessionsub);
        //    }

        //  ///  var hhucv = (from p in returnedmodel.DataModel)

        //    return PartialView(returnedmodel);
        //}

        //public ActionResult LaboratoryValue2(string subjectid)
        //{
        //    var subjectidp = int.Parse(subjectid);
        //    //var schoolSubject = (from ss in ctx.SchoolSubjects where ss.SubjectID == subjectidp select ss.SubjectID).FirstOrDefault();
        //    if (subjectidp != 0)
        //    {
        //        var itemss = ctx.SchoolLabConfigs.ToList();

        //        var yall = from p in itemss
        //                   where p.SubjectID == subjectidp
        //                   select new Facility
        //                   {
        //                       ID = p.ID,
        //                       ItemDescription = p.Facility.ItemDescription,                              
        //                       userValue = String.Empty

        //                   };


        //        return PartialView("_ShowLabFacilitypv", yall);
        //    }
        //    else
        //    {
        //        return RedirectToAction("FacilityValue1", "School");
        //    }



        //}

        [HttpPost]
        
        public ActionResult FacilityValue3(string[] ids)
        {
            return View();
        }



        public JsonResult SaveLibraryFacility(string dd, string user, int record_id = 0)
        {

            String result = String.Empty;
            // long? schoolid = 66;
            int schoolid = int.Parse(Session["schoolid"].ToString());
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    var fac = ctx.SchoolLibraryConfigs.Where(f => f.ID == facid).FirstOrDefault();
                    var uv = new SchLIBFacilityValue();
                    uv.SchLLIBConfigID = fac.ID;
                    uv.userValue = user;
                    uv.SchoolID = schoolid;
                    ctx.SchLIBFacilityValues.Add(uv);
                    ctx.SaveChanges();
                    return Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                                       JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uv = ctx.SchLIBFacilityValues.Find(record_id);
                    if (uv != null)
                    {
                        uv.userValue = user;
                        ctx.SaveChanges();
                        return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() },
                                        JsonRequestBehavior.AllowGet);

                    }
                }


            }
            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }







        public JsonResult SaveLaboratoryFacility(string dd, string user, int record_id = 0)
        {

            String result = String.Empty;
            // long? schoolid = 66;
            int schoolid =int.Parse(Session["schoolid"].ToString());
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    var fac = ctx.SchoolLabConfigs.Where(f => f.ID == facid).FirstOrDefault();
                    var uv = new SchoolLabValue();
                    uv.SchoolLabConfigID = fac.ID;
                    uv.UserValue = user;
                    uv.SchoolID = schoolid;
                    ctx.SchoolLabValues.Add(uv);
                    ctx.SaveChanges();
                    return Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                                       JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uv = ctx.SchoolLabValues.Find(record_id);
                    if (uv != null)
                    {
                        uv.UserValue = user;
                        ctx.SaveChanges();
                        return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() },
                                        JsonRequestBehavior.AllowGet);

                    }
                }


            }
            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }





        [HttpPost]
        public JsonResult SaveReagent(string dd, string user, int record_id = 0)
        {

            String result = String.Empty;
            // long? schoolid = 66;
            int schoolid =  int.Parse(Session["schoolid"].ToString());
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    var fac = ctx.SchoolReagentConfigs.Where(f => f.ID == facid).FirstOrDefault();
                    var uv = new SchoolFacilityValue();
                    uv.SchoolFacConfigID = fac.ID;
                    uv.UserValue = user;
                    uv.SchoolID = schoolid;
                    ctx.SchoolFacilityValues.Add(uv);
                    ctx.SaveChanges();
                    return Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                                       JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uv = ctx.SchoolFacilityValues.Find(record_id);
                    if (uv != null)
                    {
                        uv.UserValue = user;
                        ctx.SaveChanges();
                        return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() },
                                        JsonRequestBehavior.AllowGet);

                    }
                }


            }
            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult SaveEquipment(string dd, string user, int record_id=0)
        {

            String result = String.Empty;
            // long? schoolid = 66;
            int schoolid =  int.Parse(Session["schoolid"].ToString());
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    var fac = ctx.SchoolEquipConfigs.Where(f => f.ID == facid).FirstOrDefault();
                    var uv = new SchoolFacilityValue();
                    uv.SchoolFacConfigID = fac.ID;
                    uv.UserValue = user;
                    uv.SchoolID = schoolid;
                    ctx.SchoolFacilityValues.Add(uv);
                    ctx.SaveChanges();

                    return Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                                       JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var uv = ctx.SchoolFacilityValues.Find(record_id);
                    if(uv != null)
                    {
                        uv.UserValue = user;
                        ctx.SaveChanges();
                        return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() },
                                        JsonRequestBehavior.AllowGet);
                      
                    }
                }
               
                
            }
            else
            {
                return Json(new { status = false, message = "Value is required." },JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]

        //public ActionResult UpdateGridData(string gridData)
        //{
        //    String firstNum = String.Empty;
        //    String lastNum = String.Empty;
        //    //long? schoolid = 41;
        //    int schoolid = int.Parse(Session["schoolid"].ToString());
        //    var log = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string[]>>(gridData);
        //    var arr = log.Select(z => z.Value).ToArray();
        //    for (int i = 0; i < arr.Length; i++)
        //    {
        //        for (int j = 0; j < arr[i].Length; j++)
        //        {
        //            firstNum = arr[i][1];
        //            lastNum = arr[i][arr.Length];
        //        }
        //        int facID = Convert.ToInt32(firstNum);
        //        var fac = ctx.FacilityTypeSettings.Where(f => f.ID == facID).FirstOrDefault();
        //        string subjectgrid = Session["subjectgrid"].ToString();
        //        var facid = fac.FacilityUserValues.Where(p => p.FacilityTypeSettingsID == int.Parse(firstNum) && p.SubjectID == int.Parse(subjectgrid));

        //        foreach (var user in facid)
        //        {
        //            var schoolsuject = ctx.SchoolSubjects.Where(xc => xc.SchoolID == schoolid && xc.SubjectID == user.SubjectID).FirstOrDefault();
        //            SchoolFacilityUserValue uv = new SchoolFacilityUserValue();
        //            uv.FacilityUserValueID = user.ID;
        //            uv.UserValue = lastNum;
        //            uv.SchoolID = schoolsuject.SchoolID;
        //            ctx.SchoolFacilityUserValues.Add(uv);
        //            ctx.SaveChanges();

        //        }
        //    }

        //    return Json("Update Successfully");
        //}



        [HttpGet]
        public ActionResult CreateSchoolStaff()
        {
            var schid = Session["schoolid"].ToString();
            var titles = ctx.Titles.ToList();
            ViewBag.titles = titles.Select(tt => new SelectListItem
            {
                Text = tt.Label,
                Value = tt.ID.ToString()
            });

            var SchoolStaffCategories = ctx.SchoolStaffCategories.ToList();
            ViewBag.SchoolStaffCategories = SchoolStaffCategories.Select(ssf => new SelectListItem
            {
                Text = ssf.Category,
                Value = ssf.ID.ToString()
            });

            var degs = ctx.Degrees.ToList();
            ViewBag.degrees = degs.Select(dg => new SelectListItem
            {
                Text = dg.DegreeType,
                Value = dg.ID.ToString()
            });

            //  ViewBag.Degreess = new SelectList(ctx.Degrees, "ID", "DegreeType");


            //var deg = ctx.Degrees.Select(c => new {
            //    ID = c.ID,
            //    degreeType = c.DegreeType
            //}).ToList();
            //ViewBag.Degreess = new MultiSelectList(deg, "ID", "degreeType");

            // ViewBag.Degreess = ctx.Degrees.ToList();

            var id =long.Parse(schid);
            ViewBag.Subjects = ctx.SchoolSubjects.Where(s=>s.SchoolID==id).Select(p => new SelectListItem
            {
                Text = p.Subject.LongName,
                Value = p.SubjectID.ToString()
            });
          

            return View(new vmSchoolStaffQualification());
        }


        [HttpPost]
        public ActionResult CreateSchoolStaff(vmSchoolStaffQualification ss)
        {
           
            var degs = ctx.Degrees.ToList();
            ViewBag.degrees = degs.Select(dg => new SelectListItem
            {
                Text = dg.DegreeType,
                Value = dg.ID.ToString()
            });
            int schoolid =int.Parse(Session["schoolid"].ToString());
            var sch = ctx.SchoolProfiles.Where(sh => sh.ID == schoolid).FirstOrDefault();
            int index = 0;
            String result = String.Empty;

            using (TransactionScope cc = new TransactionScope())
            {
                var labeltitle = ctx.Titles.Where(x => x.ID == ss.TitleID).FirstOrDefault();
                SchoolStaff ssm = new SchoolStaff();
               // ssm.FullName = labeltitle.Label + " " + ss.FullName;
               
                ssm.ContactAddress = ss.ContactAddress;
              //  ssm.DateEmployed = ss.DateEmployed;
                
                ssm.SchoolID = schoolid;

                ctx.SchoolStaffs.Add(ssm);
                ctx.SaveChanges();

                SchStafDegCourse sdc = new SchStafDegCourse();
                //sdc.CourseOfStudy = ss.courseName;
                sdc.SchStaffID = ssm.ID;
                sdc.DegreeID = ss.DegreeID;
                
               

                ctx.SchStafDegCourses.Add(sdc);
               ctx.SaveChanges();


                //ssm.Subjects = new List<Subject>();
                //foreach (var d in ss.SubjectID)
                //{
                //    var suj = ctx.Subjects.Where(de => de.ID == d).FirstOrDefault();
                //    ssm.Subjects.Add(suj);
                //}
                
                foreach (var su in ss.SubjectID)
                {
                    var schstaffsu = new StaffSubject();


                    schstaffsu.SubjectID = su;
                    //SubjectID = ss.SubjectID,
                    schstaffsu.SchoolStaffID = ssm.ID;

                    ctx.StaffSubjects.Add(schstaffsu);
                   ctx.SaveChanges();

                }

                index = 1;
                if (index > 0)
                {

                    result = "1";
                }
                else
                {
                    result = "0";
                }
                //// return RedirectToAction("FacilityValue1", "School");
                cc.Complete();
            }
           
            TempData["examitio"] = sch.SchoolName;
            return Json(result, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Assessment()
        {
            //var ass = ctx.SchoolAssessmentSummaries.Include(m => m.CloudSchool).ToList();
            //AssessmentModel mod = new AssessmentModel();
            //mod.vmassessmentList = ass;
            var ass = ctx.Facilities.Where(m => m.FacilityCategoryID == 6).ToList();
            return View(ass);
        }
        [HttpPost]
        public ActionResult Assessment(string[] grades, string[] items)
        {
            int schoolid = int.Parse(Session["schoolid"].ToString());
            string[] ItemsArray = items.Distinct().ToArray();

            int[] intitemsarray = ItemsArray.Select(int.Parse).ToArray();

            int[] intgradesarray = grades.Select(int.Parse).ToArray();

            AssessmentSummary ss = new AssessmentSummary();

            if (intgradesarray != null && intitemsarray != null)
            {
                for (int i = 0; i < 1; i++)
                {
                    for (int p = 0; p < intgradesarray.Length; p++)
                    {

                        ss.FacilityTypeSettingID = intitemsarray[p];
                        ss.RatingID = intgradesarray[p];
                        ss.SchoolID = schoolid;
                        ctx.AssessmentSummaries.Add(ss);
                        ctx.SaveChanges();
                    }
                }

            }
            String result = String.Empty;
            int index = 1;
            if (index > 0)
            {

                result = "1";
            }
            else
            {
                result = "0";
            }


            return Json(result, JsonRequestBehavior.AllowGet);

        }





        public ActionResult AssementFacility()
        {
            //var ass = ctx.SchoolAssessmentSummaries.Include(m => m.CloudSchool).ToList();
            //AssessmentModel mod = new AssessmentModel();
            //mod.vmassessmentList = ass;
            var ass = ctx.Facilities.Where(m => m.FacilityCategoryID == 6).ToList();
            return View(ass);
        }
        [HttpPost]
        public ActionResult AssementFacility2(string[] grades, string[] items)
        {
            string[] ItemsArray = items.Distinct().ToArray();

            int[] intitemsarray = ItemsArray.Select(int.Parse).ToArray();

            int[] intgradesarray = grades.Select(int.Parse).ToArray();

            AssessmentSummary ss = new AssessmentSummary();

            if (intgradesarray != null && intitemsarray != null)
            {
                for (int i = 0; i < 1; i++)
                {
                    for (int p = 0; p < intgradesarray.Length; p++)
                    {

                        ss.FacilityTypeSettingID = intitemsarray[p];
                        ss.RatingID = intgradesarray[p];
                        ss.SchoolID = 41;
                        ctx.AssessmentSummaries.Add(ss);
                        ctx.SaveChanges();
                    }
                }

            }
            String result = String.Empty;
            int index = 1;
            if (index > 0)
            {

                result = "1";
            }
            else
            {
                result = "0";
            }


            return Json(result, JsonRequestBehavior.AllowGet);

        }

       
             

    }


}



