﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Security;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Pdf;
//using SchoolRecognitionSystem.Report;
using RemoveNullableClass = SchoolRecognitionSystem.Classes.RemoveNullableClass;

namespace SchoolRecognitionSystem.Controllers
{
    [SchoolRecognitionAuthorized]
    public class AdminController : BaseController
    {
        public ActionResult SchoolLaboratory(int schoolid)
        {
            return View();
        }
        public ActionResult SchoolEquipment(int schoolid = 0, int? subjectid = 0, int page = 1)
        {

            int pageIndex = 1;
            int pageSize = 2;
            GetSubjectItemsFormData(subjectid);
            pageIndex = page;

            IPagedList<SchoolProfileIndexModel> schoolmodel = null;

            schoolmodel = clsContent.SchoolPreviewModelIndex().OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);

            ViewBag.subjectid = subjectid;
            ViewBag.Report = "active";
            return View(schoolmodel);
            //var schoolmodel = clsContent.SchoolPreviewModelIndex();
            //ViewBag.Report = "active";
            //return View(schoolmodel);
            //GetSubjectItemsFormData(subjectid);
            //return View();
        }


        public ActionResult SchoolReagent(int? subjectid = 0, int page = 1)
        {
            int pageIndex = 1;
            int pageSize = 20;
            GetSubjectItemsFormData(subjectid);
            pageIndex = page;

            IPagedList<SchoolProfileIndexModel> schoolmodel = null;

            schoolmodel = clsContent.SchoolPreviewModelIndex().OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);


            ViewBag.Report = "active";
            return View(schoolmodel);
            //return View();
        }

        private void GetSubjectItemsFormData(int? subjectid)
        {
            var schy = clsContent.GetCouncilSubjectsItems().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;

        }

        //public ActionResult SchoolEquipment(int schoolid)
        //{

        //}
        //public ActionResult SchoolReagent(int schoolid)
        //{

        //}

        //SchoolLaboratory
        public ActionResult SchoolProfilePreview(int ID = 364)
        {
            /*
             get schoolprofile
             get ss2
             get bsc/hnd
             get nce


             */

            var preview = clsContent.GetSchoolPreview(ID);



            var schoolPreviewSummary = clsContent.GetSchoolSummary(ID);

            DataTable c_preview = RemoveNullableClass.ToDataTable(preview);

            // DataTable c_schoolPreviewSummary = RemoveNullableClass.ToDataTable(schoolPreviewSummary);
            /*Schoolreport rpt = new Schoolreport();
            rpt.Load();
            rpt.SetDataSource(c_preview);

            //rpt.SetDataSource(schoolPreviewSummary);

            Stream ss = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            return File(ss, "application/pdf");*/
            return Json(new {});
        }


        //AssignRole
        //CreateRole
        private SubjectModel GetCouncilSubjectFormData()
        {
            var subjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList();
            var compulsorySubjects = (from sub in subjects
                                      where
                                          sub.LongName.ToLower().Equals("CIVIC EDUCATION".ToLower()) ||
                                          sub.LongName.ToLower().Equals("ENGLISH LANGUAGE".ToLower()) ||
                                          sub.LongName.ToLower().Equals("MATHEMATICS".ToLower())
                                      select sub.ID)
                .Select(dummy => (int?)dummy).ToList();
            return new SubjectModel
            {
                AvailableSubjects = subjects,
                AvailableSelected = compulsorySubjects,
                // RequestedSubjects = new List<Subject>()
            };
        }



        [HttpGet]
        public ActionResult CreateRole()
        {
            TempData["Message"] = CurrentUser.EmailAddress;
            IPagedList<UserViewModel> eqp = null;
            //  var office = clsContent.GetAdministratorsOnly.FirstOrDefault(v => v.OfficeName == "YABA");

            int pageIndex = 1;

            int pageSize = 20;


            eqp = clsContent.GetAdministratorsOnly().OrderBy
                (m => m.RoleName).ToPagedList(pageIndex, pageSize);

            var stateViewModel = GetStateFormData();

            ViewBag.States = stateViewModel.States;

            if (eqp.Count == 0)
            {
                TempData["NoAdministrators"] = "No Administartor(s)";
            }

            //var mesage = TempData["Success"];
            //ViewBag.Message = mesage;

            // GetUsers();
           
            ViewBag.CreateRole = "active";
            return View(eqp);
        }
        private void GetFormData2()
        {
            IEnumerable<SelectListItem> objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            IEnumerable<SelectListItem> objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");
            IEnumerable<SelectListItem> objRank = new SelectList(clsContent.GetRank(), "ID", "RankTitle");
            IEnumerable<SelectListItem> objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName");
            //IEnumerable<SelectListItem> objCouncilSubject = new SelectList(clsContent.GetCouncilSubjects(), "ID", "LongName");
            IEnumerable<SelectListItem> objOffice = new SelectList(clsContent.GetOffices(), "ID", "OfficeName");

            ViewBag.Rank = objRank;
            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.StatesList = objStates;
            ViewBag.OfficeList = objOffice;

        }
        private void GetFormData()
        {
            IEnumerable<SelectListItem> objOffice = new SelectList(clsContent.GetOffices(), "ID", "OfficeName");
            ViewBag.OfficeList = objOffice;

        }




        public ActionResult confirmation()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        //AssignUserState
        //public ActionResult EquipmentReportByid(int schoolid, int subjectid)
        //{
        //    //int subjectid = 1;
        //    var schoolEquipment =
        //        clsContent.GetEquipmentReportBySubjectID(schoolid, subjectid);
        //    //.Where(c => c.SchoolID == schoolid && c.EquipSubjectID == subjectid)
        //    //.ToList();
        //    var equipmentdataTable = RemoveNullableClass.ToDataTable(schoolEquipment);
        //    EQUIPMENT rpt = new EQUIPMENT();
        //    rpt.Load();
        //    rpt.Subreports[0].SetDataSource(equipmentdataTable);
        //    //rpt.Refresh();
        //    Stream ss = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(ss, "application/pdf");
        //}

        //public ActionResult ReportByid(int ID)
        //{
        //    ViewBag.Report = "active";


        //    var schoolprofilemodel = clsContent.GetSchoolProfile();

        //    var principalmodel = clsContent.GetSchoolProfile2();

        //    var nullableData = schoolprofilemodel.Where(s => s.ID == ID).ToList();

        //    var studentclass = clsContent.GetStudentClass(ID);


        //    var schy = clsContent.GetSubjectItems();


        //    DataTable c = RemoveNullableClass.ToDataTable(nullableData);


        //    // var cc = RemoveNullableClass.ToDataTable(studentclass);

        //    Schoolreport rpt = new Schoolreport();

        //    var schoollabs = new List<List<vwSchoolLaboratoryModel>>();
        //    foreach (var s in schy)
        //    {
        //        schoollabs.Add(clsContent.GetSchoolLaboratory2(ID, s.ID));

        //    }
        //    List<DataTable> dataTables = new List<DataTable>();
        //    schoollabs.ForEach(data =>
        //    {
        //        dataTables.Add(RemoveNullableClass.ToDataTable(data));
        //    });

        //    rpt.Load();
        //    if (rpt.Subreports.Count > 0)
        //    {
        //        // rpt.Subreports[0].SetDataSource(cc);

        //        for (var i = 0; i < dataTables.Count; i++)
        //        {
        //            rpt.Subreports[i].SetDataSource(dataTables[(i)]);

        //        }

        //    }

        ////    rpt.SetDataSource(c);

        // //   rpt.Refresh();
        //  //  Stream ss = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(ss, "application/pdf");



        //}
        //public ActionResult Pdf(int ID)
        //{
        //    int schoolid = int.Parse(Session["schoolid"].ToString());

        //    var schoolprofilemodel = clsContent.GetSchoolProfile();

        //    var principalmodel = clsContent.GetSchoolProfile2();

        //    var nullableData = schoolprofilemodel.Where(s => s.ID == schoolid).ToList();


        //    var studentclass = clsContent.GetStudentClass(schoolid);


        //    var schy = clsContent.GetSubjectItems();




        //    //Schoolreport rpt = new Schoolreport();

        //    var schoollabs = new List<List<vwSchoolLaboratoryModel>>();
        //    foreach (var s in schy)
        //    {
        //        schoollabs.Add(clsContent.GetSchoolLaboratory2(schoolid, s.ID));

        //    }


        //    DataTable c = RemoveNullableClass.ToDataTable(nullableData);


        //    DataTable cc = RemoveNullableClass.ToDataTable(studentclass);

        //    rpt.Load();

        //    rpt.Subreports[0].SetDataSource(cc);

        //    rpt.SetDataSource(c);


        //    Stream ss = rpt.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
        //    return File(ss, "application/pdf");


        //}
        // GET: Admin
        //[AuthorizeAccess(Roles = "SuperAdmin")]
        public ActionResult Index(int page = 1)
        {
            int pageIndex = 1;
            int pageSize = 20;

            pageIndex = page;

            IPagedList<SchoolProfileIndexModel> schoolmodel = null;

            schoolmodel = clsContent.SchoolPreviewModelIndex().OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);


            ViewBag.Report = "active";
            return View(schoolmodel);
            //ViewBag.Report = "active";
            // return View(schoolmodel);
        }
        [HttpGet]
        public JsonResult UpdateRecordSchCate(string id, string catee)
        {
            //UpdateSchoolCategory
            int idcat = int.Parse(id);
            bool result = false;
            try
            {
                result = clsContent.UpdateSchoolCategory(idcat, catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult SaveRecordCat(string catee)
        {
            bool result = false;
            try
            {
                result = clsContent.AddSchoolCategory(catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult CreateCategory(int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {
            const int pageSize = 10;
            var totalRows = clsContent.GetSchoolCategoryCount();
            //   coun mobjMo.CountCategory();//GetSchoolCategoryCount

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var categorylists = clsContent.GetSchoolCategoryP(page, pageSize, sort, Dir);
            var data = new PagedCustomerModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Category = categorylists
            };
            ViewBag.SchoolCategory = "active";
            return View(data);
        }


        public ActionResult CreateSubjectItems(int? recid, int? facid, int page = 1, string factext = "")
        {
            //GetRecognitionTypeFormData();

            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolRecognitionType = clsContent.GetSchoolRecogntion().Select(s => new SelectListItem

            {
                Text = s.TypeofRecognition,
                Value = s.ID.ToString(),
                Selected = (s.ID == recid)
            });

            ViewBag.FacilityNames = new List<SelectListItem>();
            if (facid != null)
            {
                var facNames = clsContent.GetFacilityCategories(recid).Select(s => new SelectListItem
                {
                    Text = s.TypeOfFacility,
                    Value = s.FacilityCategoryID.ToString(),
                    Selected = (s.FacilityCategoryID == facid)
                });

                ViewBag.FacilityNames = facNames;
            }
            ViewBag.Subjectss = clsContent.GetCouncilSubjects().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString()


            });// new SelectList(ctx.Subjects, "ID", "LongName", new Subject().ID);
            ViewBag.schoolRecognitionTypes = schoolRecognitionType;
            ViewBag.RecognitionId = recid;
            ViewBag.FacilityId = facid;
            //var faciltycat = ctx.FacilityCategories.Where(x => x.ID == facid).FirstOrDefault();
            IPagedList<SchoolEquipConfigViewModel> eqp = new PagedList<SchoolEquipConfigViewModel>(new List<SchoolEquipConfigViewModel>(), page, 20);
            ViewBag.factext = factext;
            IPagedList<LaboratoryViewModel> Leqp = new PagedList<LaboratoryViewModel>(new List<LaboratoryViewModel>(), page, 20);

            IPagedList<LibraryViewModel> Libr = new PagedList<LibraryViewModel>(new List<LibraryViewModel>(), page, 20);
            if ((recid != null && facid != null) && (factext == "EQUIPMENT FACILITY" || factext == "REAGENT FACILITY"))
            {
                eqp = clsContent.GetSchoolEquipConfigAdmin(facid, recid).OrderByDescending(m => m.ID)
                  .ToPagedList(page, 20);
                //GetSchoolEquipConfigAdmin
            }

            else if (recid != null && facid != null && factext == "LABORATORY FACILITY")
            {
                Leqp = clsContent.GetSchoolLabConfigAdmin(facid, recid).OrderBy(m => m.ID).ToPagedList(page, 20);
                ViewBag.labEquipment = Leqp;
            }


            else if (recid != null && facid != null && factext == "LIBRARY FACILITY")
            {
                Libr = clsContent.GetSchoolLibraryConfigAdmin(facid, recid).OrderBy(m => m.ID).ToPagedList(page, 20);
                ViewBag.libraryEquipment = Libr;
            }

            ViewBag.CreateSubjectItems = "active";
            return View(eqp);


        }

        [HttpPost]
        public ActionResult GetFacility(string recid)
        {

            int? reid = int.Parse(recid);
            List<SelectListItem> facNames = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(recid))
            {
                reid = Convert.ToInt32(recid);
                List<vmFacilityCategory> faccate = clsContent.GetFacilityCategories(reid).ToList();
                faccate.ForEach(x =>
                {
                    facNames.Add(new SelectListItem { Text = x.TypeOfFacility, Value = x.FacilityCategoryID.ToString() });
                });
            }
            return Json(facNames, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SubjectList()
        {

            var subjects = clsContent.GetCouncilSubjects().Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString()
            }).ToList();

            return Json(subjects, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecordLab(string catee, string SU, int facilitycategoryid)
        {
            SchoolLabConfig result = null;
            try
            {
                result = clsContent.AddLabItem(catee, SU, facilitycategoryid);

            }
            catch (Exception ex)
            {

            }
            if (result != null)
                return Json(new { exists = true }, JsonRequestBehavior.AllowGet);

            return Json(new { exists = false }, JsonRequestBehavior.AllowGet);

        }
        //[CustomAuthorize(Roles = "admin,super")]
        [HttpGet]
        public JsonResult SaveRecordEquip(string catee, int SU, float Quant, string specification, int facilitycategoryid)
        {
            // "catee": Item, "SU": $(Sub).val(), "Quant": Quantity, "specification": Specification, "recid": recc
            SchoolEquipConfig result = null;
            try
            {
                result = clsContent.AddEquipmentItem(catee, SU, Quant, specification, facilitycategoryid);

            }
            catch (Exception ex)
            {

            }
            if (result != null)
                return Json(new { ID = result.ID, exists = true }, JsonRequestBehavior.AllowGet);

            return Json(new { exists = false }, JsonRequestBehavior.AllowGet);


            // return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateRecordLab(int id, string catee, string sub, int facilitycategoryid)
        {

            var Subj = int.Parse(sub);
            SchoolLabConfig result = null;
            try
            {

                result = clsContent.UpdateSchoolLabByID(id, catee, Subj, facilitycategoryid);

            }
            catch (Exception ex)
            {
            }
            if (result != null)
                return Json(new { exists = true }, JsonRequestBehavior.AllowGet);


            return Json(new { exists = false }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult UpdateRecordE(int id, string catee, string Quant, string sub, string Spec, int facilitycategoryid)
        {
            var Qty = double.Parse(Quant);
            var Subj = int.Parse(sub);
            SchoolEquipConfig result = null;
            try
            {

                result = clsContent.UpdateSchoolEquipmentByID(id, catee, Qty, Subj, Spec, facilitycategoryid);

            }
            catch (Exception ex)
            {
            }
            if (result != null)
                return Json(new { exists = true }, JsonRequestBehavior.AllowGet);


            return Json(new { exists = false }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateRecordReagent(int id, string catee, string Quant, string sub, string Spec, int facilitycategoryid)
        {
            var Qty = double.Parse(Quant);
            var Subj = int.Parse(sub);
            ReagentModelAdmin result = null;
            try
            {
                result = clsContent.UpdateSchoolReagentByID(id, catee, Qty, Subj, Spec, facilitycategoryid);

            }
            catch (Exception ex)
            {
            }
            if (result != null)
                return Json(new { exists = true }, JsonRequestBehavior.AllowGet);


            return Json(new { exists = false }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteRecordEquip(int id)
        {
            var result = false;
            try
            {
                result = clsContent.DeleteRecordEquip(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Navigate()
        {
            ViewBag.Report = "active";
            return View();
        }

        // GET: Categories/Details/5
        public ActionResult Detail(int userId)
        {
            var aUser = clsContent.GetAUser(userId);
            var userStates = clsContent.GetStatesByUser(userId);
            UserViewModel userModel = new UserViewModel();
            userModel.RoleName = aUser.RoleName;
            userModel.isActive = aUser.isActive;
            userModel.SurName = aUser.SurName;
            //var firstOrDefault = userStates.FirstOrDefault();
            //if (firstOrDefault != null)
            return Json(new { surname = userModel.SurName, rolename = userModel.RoleName, isactive = userModel.isActive, stateid = userStates.FirstOrDefault().StateCode, userstates = userStates }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FetchUser(int userid)
        {

            TempData["Message"] = CurrentUser.EmailAddress;
            var aUser = clsContent.GetAUser(userid);
            var roles = clsContent.GetRoles();
            var userStates = clsContent.GetUserStates(userid);


            if (userStates.Count > 0)
            {
                ViewBag.Alert = aUser.EmailAddress + " is already assigined state(s). Go to edit to update";
                ViewBag.AvailableStateSelected = userStates;
            }


            //error
            //foreach (var x in userStates)
            //{
            //    xv.AvailableStateSelected.Add(x.StateCode);
            //}



            return Json(new { EmailAddress = aUser.EmailAddress, roless = roles, AvailableStateSelected = ViewBag.AvailableStateSelected, AlertMessage = ViewBag.Alert }, JsonRequestBehavior.AllowGet);
        }
        // GET: Categories/Edit/5
        public ActionResult Edit(int userId)
        {
            TempData["Message"] = CurrentUser.EmailAddress;
            var user = clsContent.GetAUser(userId);
            var userModel = new UserViewModel();
            userModel.RoleID = user.RoleID;
            userModel.isActive = user.isActive;
            userModel.EmailAddress = user.EmailAddress;
            userModel.SurName = user.SurName;
            var stateViewModel = clsContent.GetStateByUserId(userId);
            var roless = clsContent.GetRole();

            return Json(new
            {
                SurName = userModel.SurName,
                EmailAddress = userModel.EmailAddress,
                RoleId = userModel.RoleID,
                isActive = userModel.isActive,
                Roles = roless,
                states = stateViewModel.States,
                statesSelected = stateViewModel.AvailableStateSelected
            },
                JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult Edit(int userId, string SurName, string[] States, int RoleId, bool isActive)
        {
            var returneduserupdate = new List<UserViewModel>();
            var returneduser = new UserViewModel();
            if (ModelState.IsValid)
            {
                using (var transactionScope = new TransactionScope())
                {
                    var chek = clsContent.DeleteUserState(userId);
                    //var role = clsContent.GetRoleNameById(RoleId);
                    returneduser = clsContent.UpdateUser(userId, SurName, RoleId, isActive);

                    foreach (var t in States)
                    {
                        returneduserupdate = clsContent.UpdateUserState(returneduser.ID, t);
                    }
                    transactionScope.Complete();
                }
                if (returneduserupdate != null && returneduserupdate.Count > 0)
                {
                    return Json(new { status = true }, JsonRequestBehavior.AllowGet);
                }
            }
            return RedirectToAction("CreateRole");

        }


        [HttpGet]
        public JsonResult DeleteRecordCategory(int id)
        {
            //DeleteSchoolCategory
            bool result = false;
            try
            {
                result = clsContent.DeleteSchoolCategory(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAssignRole(int id)
        {
            var result = false;
            try
            {
                result = clsContent.DeleteRecordEquip(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        // GET: Categories/Delete/5
        public ActionResult DeleteUser(int userId)
        {
            bool result = false;
            try
            {
                result = clsContent.DeleteAUser(userId);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        private RoleViewModel GetRolesFormData()
        {
            return new RoleViewModel
            {
                Roles = clsContent.GetRoles().OrderBy(b => b.Rolename).ToList(),

            };
        }

        private StateViewModel GetStateFormData()
        {
            return new StateViewModel
            {
                States = clsContent.GetStates().OrderBy(b => b.StateName).ToList(),

            };
        }
        private LocalModel GetStateFormDataX()
        {
            int x = 0;
           // var stateAddedForUser = clsContent.GetStatesByUser(userid);
            var stateIds = clsContent.GetLgas(x.ToString()).Select(item => item.LgaID).ToList();
            return new LocalModel
            {
                Names = clsContent.GetLgas(x.ToString()).OrderBy(b => b.Name).ToList(),
                AvailableStateSelectedX = stateIds.Select(dummy => dummy).ToList()
                //RequestedSubjects = new List<Subject>()
            };
        }
        private StateViewModel GetStateFormData(int userId)
        {
            return new StateViewModel
            {
                FilterStates = clsContent.GetStateByUserId(userId)
            };
        }
        public ActionResult AssignLga(int? stateId, int? officeId)
        {
            var lgasInState = new List<LocalModel>();
            var localViewModel = GetStateFormDataX();
            ViewBag.States = localViewModel.Names.ToList();
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.CreateRole = "active";
            ViewBag.StatesList = new SelectList(clsContent.GetStates(), "StateCode", "StateName", stateId);
            ViewBag.OfficeList = new SelectList(clsContent.GetOffices(), "ID", "OfficeName", officeId);

            if (stateId != null)
            {
                if (officeId == null)
                {
                    ShowMessage("Please select office first.", AlertType.Warning);
                }else
                lgasInState = clsContent.GetLgasByStateId(int.Parse(stateId.ToString()));
            }
            ViewBag.LGAList = lgasInState;
            return View(localViewModel);
        }
        [HttpPost]
        public ActionResult AssignLga(LocalModel localModel )
        {
            var returneduserupdate = new List<LocalModel>();
          
            if (ModelState.IsValid)
            {
                using (var transactionScope = new TransactionScope())
                {
                    foreach (var availableState in localModel.AvailableStateSelectedX)
                    {
                        returneduserupdate = clsContent.InsertOfficeLocalgovernment(int.Parse(localModel.OfficeID), localModel.StateID, availableState);
                    }
                    transactionScope.Complete();
                }
                if (returneduserupdate != null && returneduserupdate.Count > 0)
                {
                    ShowMessage("Lgas successfully assigned to office.", AlertType.Success);
                    return RedirectToAction("AssignLga");
                }
                ShowMessage("Error occurred while registering Lgas.", AlertType.Danger);
                return RedirectToAction("AssignLga", new {stateId = localModel.StateID, officeId = localModel.OfficeID});

            }
            ShowMessage("Error occurred while registering Lgas.", AlertType.Danger);
            return RedirectToAction("AssignLga", new {stateId = localModel.StateID, officeId = localModel.OfficeID});
        }


        public ActionResult AssignState(int userId)
        {
            var assignedStates = clsContent.GetAssignedStatesByRoleName(ConstantsService.RoleAdmin);
            var aUser = clsContent.GetUserById(userId);
            TempData["aUser"] = aUser.EmailAddress;
            ViewBag.UserID =  userId;
            var stateViewModel = GetStateFormData(userId);
            var states = new List<State>();
            foreach (var state in stateViewModel.FilterStates.States)
            {
                if (assignedStates.All(st => st.StateCode != state.StateCode))
                {
                    states.Add(state);
                }
            }

            ViewBag.AssignedStates = assignedStates;
            ViewBag.States = stateViewModel.FilterStates.States;
            ViewBag.AvailableStateSelected = stateViewModel.FilterStates.AvailableStateSelected;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.CreateRole = "active";
            return View(stateViewModel);
        }

        [HttpPost]
        public ActionResult AssignState(StateViewModel stateModel)
        {
            var userId = stateModel.UserID;
            var assignedStates = clsContent.GetAssignedStatesByRoleName(ConstantsService.RoleAdmin);
            var returneduserupdate = new List<UserViewModel>();
            var returneduser = new UserViewModel();
            if (ModelState.IsValid)
            {
                foreach (var selectedState in stateModel.FilterStates.AvailableStateSelected)
                {
                    var alreadyAssigned = assignedStates.FirstOrDefault(st => st.StateCode == int.Parse(selectedState));
                    if (alreadyAssigned != null)
                    {
                        if (alreadyAssigned.UserID != userId)
                        {
                            var state = clsContent.GetStatesById(alreadyAssigned.StateCode.ToString());
                            ShowMessage($"The state '{state.StateName}' has been assigned to another person.", AlertType.Danger);
                            return RedirectToAction("AssignState", new {userId = stateModel.UserID});
                        }
                    }
                }
                using (var transactionScope = new TransactionScope())
                {
                    var chek = clsContent.DeleteUserState(userId);

                    if (chek)
                    {
                        foreach (var t in stateModel.FilterStates.AvailableStateSelected)
                        {
                            returneduserupdate = clsContent.UpdateUserState(userId, t);
                        }
                    }

                    transactionScope.Complete();

                }
                if (returneduserupdate != null && returneduserupdate.Count > 0)
                {
                    TempData["Success"] = "State(s) saved successfully";
                    TempData["Message"] = CurrentUser.EmailAddress;

                    return RedirectToAction("CreateRole");


                }

                return Json(new { status = false }, JsonRequestBehavior.AllowGet);

            }
            return RedirectToAction("AssignState");
        }
      

        public ActionResult AddRole(int userId)
        {

            TempData["Message"] = CurrentUser.EmailAddress;
            var user = clsContent.GetAUserNoRole(userId);
            UserViewModel userModel = new UserViewModel();
            // userModel.RoleID = user.RoleID;
            userModel.isActive = user.isActive;
            userModel.EmailAddress = user.EmailAddress;
            userModel.RoleID = user.RoleID;
            userModel.RoleName = user.RoleName;
            userModel.OfficeName = user.OfficeName;
            userModel.ID = user.ID;
            var roless = clsContent.GetRole();

            return Json(new
            {
                EmailAddress = userModel.EmailAddress,
                OfficeName = userModel.OfficeName,
                RowID = userModel.ID,
                //RoleId = userModel.RoleID,
                isActive = userModel.isActive,
                RoleName = userModel.RoleName,
                Roles = roless, /*states = stateViewModel.States,*/
                //statesSelected = stateViewModel.AvailableStateSelected
            },
                JsonRequestBehavior.AllowGet);
            //return RedirectToAction("AssignRole");
        }

        //AddRole

        [HttpGet]
        public ActionResult AssignRole()
        {

            TempData["Message"] = CurrentUser.EmailAddress;
            IPagedList<UserViewModel> eqp = null;
            // var office = clsContent.GetOffices().FirstOrDefault(v => v.OfficeName == "YABA");
            //var office = clsContent.GetOffices();
            //if (office != null)
            //{
            //    int pageIndex = 1;

            //    int pageSize = 20;

            //    //GetUsersForRoles
            //    //eqp = clsContent.GetUsersForRoles(office.OfficeName).OrderBy
            //    //    (m => m.EmailAddress).ToPagedList(pageIndex, pageSize);
            //    eqp = clsContent.GetUsersForRoles().OrderBy
            //        (m => m.EmailAddress).ToPagedList(pageIndex, pageSize);
            //}
            int pageIndex = 1;

            int pageSize = 20;

            //GetUsersForRoles
            //eqp = clsContent.GetUsersForRoles(office.OfficeName).OrderBy
            //    (m => m.EmailAddress).ToPagedList(pageIndex, pageSize);
            eqp = clsContent.GetUsersForRoles().OrderBy
                (m => m.EmailAddress).ToPagedList(pageIndex, pageSize);

            var roleViewModel = GetRolesFormData();//GetRoles

            ViewBag.Roles = roleViewModel.Roles;
            // GetUsers();
            ViewBag.AssignRole = "active";
            return View(eqp);
        }
        [HttpPost]
        public ActionResult SaveUserRole(int userId, string EmailAddress, int[] Roles, bool isActive, string OfficeName)
        {
            TempData["Message"] = CurrentUser.EmailAddress;
            var returneduser = new UserViewModel();
            var useroletable = new UserViewModel();
            using (TransactionScope scope = new TransactionScope())
            {
                if (isActive == true)
                {
                    foreach (var t in Roles)
                    {

                        //GetUsersForRolesYaba
                        if (t == 1)
                        {
                            IPagedList<UserViewModel> eqp = null;
                            var office = clsContent.GetOffices().FirstOrDefault(v => v.OfficeName == OfficeName);
                            if (office != null && office.OfficeName == "YABA")
                            {
                                var getUserRole = clsContent.GetUserById(userId);
                                var getUserRole2 = clsContent.GetUserRole(userId, getUserRole.RoleID);
                                // returneduser = clsContent.UpdateUser(userId, EmailAddress, t, isActive);
                                returneduser = clsContent.UpdateUserRole(userId, t);

                                getUserRole2.UserID = returneduser.ID;
                                getUserRole2.RoleID = t;
                                useroletable = clsContent.UpdateUserIdRoleIdToUserRole(getUserRole2.UserRoleId,
                                    getUserRole2.UserID, getUserRole2.RoleID);
                            }
                            else
                            {
                                ViewBag.YabaStaff = "Only delegated Yaba Staff can be Administrators";
                            }
                        }
                        else
                        {
                            var getUserRole = clsContent.GetUserById(userId);
                            var getUserRole2 = clsContent.GetUserRole(userId, getUserRole.RoleID);
                            // returneduser = clsContent.UpdateUser(userId, EmailAddress, t, isActive);
                            returneduser = clsContent.UpdateUserRole(userId, t);

                            getUserRole2.UserID = returneduser.ID;
                            getUserRole2.RoleID = t;
                            useroletable = clsContent.UpdateUserIdRoleIdToUserRole(getUserRole2.UserRoleId,
                                getUserRole2.UserID, getUserRole2.RoleID);
                        }
                        //  SendEmailOnActive("paogebkhilu@waec.org.ng", EmailAddress);
                        scope.Complete();
                    }
                    //try to extract the email of the Dr.SED or SuperAdmin here
                }
                if (useroletable != null && useroletable.ID > 0)
                {

                    return Json(new { newurl = Url.Action("AssignRole"), status = true }, JsonRequestBehavior.AllowGet);
                    //TempData["message"] = new MessageModel { Status = true, Message = "User added to role successfully." };
                }
                //else
                //{
                //    return Json(new { status = isActive }, JsonRequestBehavior.AllowGet);
                //    //TempData["message"] = new MessageModel { Status = false, Message = "User failed to be added to role." };
                //}
                if (ViewBag.YabaStaff == "Only delegated Yaba Staff can be Administrators")
                {
                    return Json(new { status = false, yabaStaf = ViewBag.YabaStaff }, JsonRequestBehavior.AllowGet);
                }
            }


            return Json(new { isActive_false = isActive }, JsonRequestBehavior.AllowGet);

        }


        public ActionResult Activate(string activate, string deactivate, int userId, string emailAddress)
        {
            bool active = false;
            var returneduser = new UserViewModel();
            using (TransactionScope scope = new TransactionScope())
            {
                if (activate == "activate")
                {
                    active = true;
                    // var adminEmail = Session.GetDataFromSession<string>("AdminActivation");
                    returneduser = clsContent.UpdateUser(userId, active);
                    if (returneduser.EmailAddress == emailAddress)
                    {
                        SendEmailOnActive("confirmation@waec.org.ng", emailAddress).
                            ContinueWith((ev) =>
                            {
                                //DO somet
                                
                            });
                    }
                }
                else if (deactivate == "deactivate")
                {
                    active = false;
                    returneduser = clsContent.UpdateUser(userId, active);
                    if (returneduser.EmailAddress == emailAddress)
                    {
                        SendEmailOnActive("confirmation@waec.org.ng", emailAddress).
                            ContinueWith((ev) =>
                            {
                                //DO somet
                            }); ;
                    }
                }
                scope.Complete();
            }
            if (returneduser != null)
            {

                return Json(new { status = true, Message = "activated successfully" }, JsonRequestBehavior.AllowGet);
                //TempData["message"] = new MessageModel { Status = true, Message = "User added to role successfully." };
            }
            return Json(new { status = false, Message = "Activation failed" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Role()
        {

            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.Role = "active";
            return View();
        }
        [HttpPost]
        public ActionResult Role(RoleModel role)
        {

            TempData["Message"] = CurrentUser.EmailAddress;
            if (ModelState.IsValid)
            {

                var aRole = clsContent.AddRole(role.Rolename, role.isAdmin, role.isSuperAdmin);

                // ModelState.Clear();
                TempData["Role_Success"] = "Role successfull created";
            }
            else
            {
                TempData["Role_Failure"] = "please supply the role name";
            }
            ModelState.Clear();
            ViewBag.Role = "active";
            return View(new RoleModel());
        }


      

        public async Task<bool> SendEmailOnActive(string superAdmin, string useremail) //emailmessage
        {
            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            var url = new UrlHelper(ControllerContext.RequestContext);
            var userInfo = clsUsers.GetUserLogin(useremail);
            mess.From = new MailAddress(superAdmin, "School Recognition");
            // mess.To.Add("ujimadiyi@waec.org.ng");
            mess.To.Add(useremail);
            //put te site link there
            mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            // mess.Body = "You have been activated. to login click <a href ="http://localhost:12027">
            mess.Subject = "Activation";
            mess.IsBodyHtml = false;
            //   var decodedPassword = clsEncryption.RsaEncryptDecryptDemo(userInfo.Password);
            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(useremail, "welcome");
                smtpClient.Send(mess);
                return await Task.FromResult(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return await Task.FromResult(false);
        }

    }
}