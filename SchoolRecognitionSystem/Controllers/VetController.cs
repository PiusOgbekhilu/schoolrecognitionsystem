﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SchoolRecognitionSystem.Classes;
using SchoolRecognitionSystem.Models;
using System.Transactions;
using Microsoft.Office.Interop.Excel;
using PagedList.Mvc;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Data;
using System.Text;
using Newtonsoft.Json;
using SchoolRecognitionSystem.Infrastructure;
using SchoolRecognitionSystem.Pdf;

namespace SchoolRecognitionSystem.Controllers
{
    [SchoolRecognitionAuthorized]
    public class VetController : BaseController
    {
        // GET: SchoolsEdit
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult VetRegisteredSchoolsDetails(long sddc)
        {
            var userEMail = CurrentUser.EmailAddress;
            var userEmailInfo = clsContent.GetAUserInfoForSED(userEMail);
            ViewBag.UserEmail = userEmailInfo.EmailAddress;
            ViewBag.SchoolID = sddc;
            var getSchool = clsContent.GetSchoolByID(sddc);
            ViewBag.SchoolName = getSchool.SchoolNameEdit;
            var staffCount = clsContent.GetSchoolStaffCountX(sddc);
            ViewBag.StaffNumber = staffCount.StaffCount;
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(sddc);
            ViewBag.Facilityreport = schoolsubjectFacility;
            TempData["Message"] = CurrentUser.EmailAddress;
            var loginuser = clsContent.GetAUserEmail(CurrentUser.EmailAddress);
            ViewBag.UserId = loginuser.ID;
            var schoolsubject = clsContent.GetSchoolSubjectReport(sddc);
            ViewBag.SchoolSubjects = schoolsubject;
            ViewBag.Finish = "active";

            return View();
        }
        public ActionResult VetRegisteredSchools(string state = "", int page = 1, int pageSize = 5)
        {
            var userId = CurrentUser.UserId;
            var user = clsContent.GetUserById(userId);
            IPagedList<SchoolProfileViewModel1> schools = null;
            schools = clsContent.GetSchoolsByOfficeId(user.OfficeID, page, pageSize);
            // var allSchools = clsContent.GetSchoolsAdmin(state);
            TempData["Message"] = CurrentUser.EmailAddress;
            return View(schools);
        }

        public ActionResult School(long sddc = 0, string centernum = "")
        {
            var centreStateCode = 0;
            var localGovtCode = 0;
            var schoolstateLocal = new vwGetLocalDetailsByStateLocal();
            var staffEmailx = "";
            var userinfox = new vwUserListing2();
            var checkSchoolExist = new SchoolProfile();
            var centreObj = new CentreTableModel();
            var centernumR = centernum.Replace("-", "");
            if (centernumR != "")
            {
                centreStateCode = int.Parse(centernum.Substring(1, 2));
                localGovtCode = int.Parse(centernum.Substring(3, 2));
                schoolstateLocal = clsContent.GetSchoolStateLocalDetails(centreStateCode, localGovtCode);
                staffEmailx = CurrentUser.EmailAddress;

                userinfox = clsContent.GetUserEdit(staffEmailx);
                centreObj = clsContent.GetCentreNameByCentreNum(centernum);
                checkSchoolExist = clsContent.CheckIfSchoolNameExists2(centreObj.centrename);
            }
            //if (userinfox.OfficeID != schoolstateLocal.OfficeID)
            //{
            //    TempData["UserToSchoolError"] = "The school is not within your office or jurisdiction";
            //    return RedirectToAction("Index", "Schools");
            //}
            if (sddc == 0 && centernum != "")
            {


                //GetStateByStateCode


                //if ((checkSchoolExist.CenterNo != centernum && checkSchoolExist.SchoolName != CentreObj.centrename)|| checkSchoolExist.SchoolLocation!=null)
                if (checkSchoolExist == null)
                {

                    var insertedSchool = clsContent.AddCentreFileToSchoolProfile(centernum, centreObj.centrename,
                        schoolstateLocal.LgaID, schoolstateLocal.StateCode, schoolstateLocal.OfficeID);
                    if (insertedSchool != null)
                    {
                        sddc = insertedSchool.ID;
                    }
                }
                //else
                //{
                //    TempData["DisplaySchoolName"] = "School Cannot exist twice";
                //}


                // sddc = checkSchoolExist.ID;
                ViewBag.Schoolid = sddc;
                // var getSchool = clsContent.GetSchoolEditByID(sddc);
                //var getSchool = clsContent.GetSchoolByIDEdit2(sddc, centernum);
                var getSchool = clsContent.GetSchoolByIDEdit2(sddc);

                var objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName", getSchool.YearEstablished);
                var objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category",
                    getSchool.CategoryID);
                // var objStates = new SelectList(clsContent.GetStates(), "Id", "StateName", getSchool.StateID);
                ViewBag.AvailableYears = objYears;
                ViewBag.CategoryList = objSchCategory;
                var staffEmail = CurrentUser.EmailAddress;
                ViewBag.State = clsContent.GetStatesById(getSchool.StateID);

                ViewBag.State = getSchool.OfficeID;
                ViewBag.Lgas = getSchool.LgaID;
                var userinfo = clsContent.GetUserEdit(staffEmail);

                TempData["StateName"] = userinfo.OfficeName;
                TempData["StateID"] = userinfo.OfficeID;
                if (getSchool != null)
                {
                    Session.Add("LGA", getSchool.LgaID);
                }
                else
                {
                    Session.Add("LGA", -2);
                }
                // ViewBag.Lgas = new SelectList(clsContent.GetLgas(userinfo.StateID), "LgaID", "Name", getSchool.LgaID);
                ViewBag.EditSchool = "active";
                ViewBag.ID = sddc;
                TempData["DisplaySchoolName"] = getSchool.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(getSchool);
            }

            if (sddc != 0)
            {
                var getSchoolxx = clsContent.GetSchoolByIDEdit2(sddc);

                var objYearsxx = new SelectList(clsContent.GetYears(), "yearName", "yearName",
                    getSchoolxx.YearEstablished);
                var objSchCategoryxx = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category",
                    getSchoolxx.CategoryID);
                // var objStates = new SelectList(clsContent.GetStates(), "Id", "StateName", getSchool.StateID);
                ViewBag.AvailableYears = objYearsxx;
                ViewBag.CategoryList = objSchCategoryxx;
                var staffEmailxx = CurrentUser.EmailAddress;
                ViewBag.State = clsContent.GetStatesById(getSchoolxx.StateID);

                ViewBag.State = getSchoolxx.OfficeID;
                ViewBag.Lgas = getSchoolxx.LgaID;
                var userinfoxx = clsContent.GetUserEdit(staffEmailxx);

                TempData["StateName"] = userinfoxx.OfficeName;
                TempData["StateID"] = userinfoxx.OfficeID;
                if (getSchoolxx != null)
                {
                    Session.Add("LGA", getSchoolxx.LgaID);
                }
                else
                {
                    Session.Add("LGA", -2);
                }
                // ViewBag.Lgas = new SelectList(clsContent.GetLgas(userinfo.StateID), "LgaID", "Name", getSchool.LgaID);
                ViewBag.EditSchool = "active";
                ViewBag.ID = sddc;
                TempData["DisplaySchoolName"] = getSchoolxx.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(getSchoolxx);
            }
            if (sddc != 0 && centernum != "")
            {
                var getSchoolxx = clsContent.GetSchoolByIDEdit2(sddc);

                var objYearsxx = new SelectList(clsContent.GetYears(), "yearName", "yearName",
                    getSchoolxx.YearEstablished);
                var objSchCategoryxx = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category",
                    getSchoolxx.CategoryID);
                // var objStates = new SelectList(clsContent.GetStates(), "Id", "StateName", getSchool.StateID);
                ViewBag.AvailableYears = objYearsxx;
                ViewBag.CategoryList = objSchCategoryxx;
                var staffEmailxx = CurrentUser.EmailAddress;
                ViewBag.State = clsContent.GetStatesById(getSchoolxx.StateID);

                ViewBag.State = getSchoolxx.OfficeID;
                ViewBag.Lgas = getSchoolxx.LgaID;
                var userinfoxx = clsContent.GetUserEdit(staffEmailxx);

                TempData["StateName"] = userinfox.OfficeName;
                TempData["StateID"] = userinfox.OfficeID;
                if (getSchoolxx != null)
                {
                    Session.Add("LGA", getSchoolxx.LgaID);
                }
                else
                {
                    Session.Add("LGA", -2);
                }
                // ViewBag.Lgas = new SelectList(clsContent.GetLgas(userinfo.StateID), "LgaID", "Name", getSchool.LgaID);
                ViewBag.EditSchool = "active";
                ViewBag.ID = sddc;
                TempData["DisplaySchoolName"] = getSchoolxx.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(getSchoolxx);
            }
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> MailAttachmentToAdmin(SchoolProfileViewModel1 model, long schoolid = 0)
        {
           
            //var schoolSubjectListX = clsContent.GetSchoolSubjectFacilityReport(schoolid);
            //var otherReportDetailA = clsContent.GetSchoolAndStaffInformation(schoolid);
            //var schoolSubjectListY = clsContent.GetSchoolSubjectReport(schoolid);


            var getSchool = clsContent.GetSchoolById(schoolid);
            var staffCount = clsContent.GetSchoolStaffCountX(schoolid);
            var body = new StringBuilder();
            body.Append("<div class='school-wrapper'>");
            body.Append("<div>");
            body.Append("<span>This school has been vetted by me and having gone through the collated data I hereby recommend the school for recognition.</span>");
           body.Append("</div>");
            body.Append("<div>");
            body.Append("<h3> <span style='color: black'>"+getSchool.SchoolName+" </span> is <span class='text-success'>successfully inspected</span></h3>");
            body.Append(
                "<p style='color: black;font-weight:bold'> The number of staff in school is <span class='badge' style='font-weight:bold'>"+staffCount.StaffCount+"</span></p>");
            body.Append("</div>");
            body.Append("<br />");
            body.Append("<h2>Subject with required Facilities</h2>");
            body.Append("<table id='searchtable' class='table table-bordered table-striped'>");
            body.Append("<thead>");
            body.Append("<tr style='background-color:lightgrey'>");

            body.Append("<th>School Subject(s)</th>");
            body.Append("<th>Equipment Required</th>");
            body.Append("<th>Equipment Observed</th>");
            body.Append("<th>Reagent Required</th>");
            body.Append("<th>Reagent Observed</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            //  body.Append("@foreach (var item in (IEnumerable<vwSchoolSubjectFacilityReport>)ViewBag.Facilityreport)");
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(schoolid);
            foreach (var itemx in schoolsubjectFacility)
            {
                // body.Append("{");
                body.Append("<tr>");
                body.Append("<td>" + itemx.LongName + "</td>");
                body.Append(" <td>" + itemx.Total_Equipment + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolEquipment + "</td>");
                body.Append(" <td>" + itemx.Total_Reagent + "</td>");
                body.Append(" <td>" + itemx.TotalSchoolReagent + "</td>");
                //  body.Append(" <td>@item.TotalSchoolReagent</td>");
                body.Append("</tr>");
            }
            // body.Append("}");
            body.Append("</tbody>");
            body.Append(" </table>");
            body.Append("<table class='table table - bordered table - striped'>");
            body.Append("<thead>");
            body.Append("<tr>");
            body.Append("<th style='background-color: lightgrey' colspan='3'>SUBJECTS OFFERED BY THE SCHOOL</th>");
            body.Append("</tr>");
            body.Append("</thead>");
            body.Append("<tbody>");
            body.Append("<tr>");

            var counter = 1;
            //  var tableBody = "";
            var schoolSubjects = clsContent.GetSchoolSubjects(schoolid);// (IEnumerable<vwSchoolSubjectReport>)ViewBag.SchoolSubjects;
            var totalItems = schoolSubjects.Count();
            var rows = Math.Ceiling((double)totalItems / 3);
            var remainderColumns = totalItems % rows;
            var rowsDisplayed = 0;
            body.Append("<td>" + rows + "</td>");
            body.Append("<td>" + remainderColumns + "</td>");
            body.Append("<td>" + totalItems + "</td>");
            body.Append("</tr>");
            foreach (var item in schoolSubjects)
            {

                if (counter <= 3)
                {
                    if (counter == 1)
                    {
                        // tableBody += "<tr>";
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        body.Append("<tr>");
                        body.Append("<td>" + item.LongName + "</td>");
                        counter++;
                    }
                    else
                    {
                        body.Append("<td>" + item.LongName + "</td>");
                        //  tableBody += "<td>" + item.LongName + "</td>";
                        if (counter == 3)
                        {
                            // tableBody += "</tr>";
                            body.Append("</tr>");
                            counter = 1;
                        }
                        else
                        {
                            counter++;
                        }

                    }
                }

            }
            // @Html.Raw(tableBody)
            body.Append("</tbody>");
            body.Append("</table>");
            body.Append(" </div>");
          

            string SMTP_SERVER = "mail.waec.org.ng";
            int SMTP_PORT = 25;

            var mess = new MailMessage();
            mess.IsBodyHtml = true;
            var userEmail = CurrentUser.EmailAddress;
            var userEmailInfo = clsContent.GetAUserInfoForSED(userEmail);
            mess.From = new MailAddress(userEmailInfo.EmailAddress, "SCHOOL INSPECTION SUMMARY REPORT");
            //GetSEDEmail 
            var getschool = clsContent.GetSchoolByID(schoolid);
            var stateAdmin = clsContent.GetStateToAdmin(int.Parse(getschool.StateID));

            mess.To.Add(stateAdmin.EmailAddress);

            // mess.Body = "An inspection of the above named school was carried out on {date} and the following details were observed:\n Subject with required facilities are+"+ schoolSubjectFacilityRep.Total_Equipment+"out of "+ schoolSubjectFacilityRep.TotalSchoolEquipment+" and   schoolSubjectFacilityRep" + schoolSubjectFacilityRep.SurName + " from" + userEmailInfo.StateName + " office have registered my details on the school recognition platform,please sir/ma,activate me to be able to login";
            mess.Body = body.ToString();
            //mess.Body = string.Format("Dear {0}<br/> You have been activated, please click on the below link to login: <a href =\"{1}\" title =\"User Email Confirm\">{1}</a>", useremail, Url.Action("AD_Authentication", "Home", new { }, Request.Url.Scheme));
            // mess.Body = "I " + schoolSubjectFacilityRep.SurName + " from" + userEmailInfo.StateName + " office have registered my details on the school recognition platform,please sir/ma,activate me to be able to login";

            mess.Subject = "School Recognition";
            mess.IsBodyHtml = true;

            SmtpClient smtpClient = new SmtpClient(SMTP_SERVER);
            try
            {
                smtpClient.Port = SMTP_PORT;
                smtpClient.Credentials = new NetworkCredential(stateAdmin.EmailAddress, "welcome");
                smtpClient.Send(mess);
                
                string status= "Vetted";
                clsContent.UpdateSchoolProfileStatus(schoolid, status);
                ShowMessage("School sent successfully to administrator",AlertType.Success);
                return RedirectToAction("VetRegisteredSchoolsDetails", "vet"); ;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            //TempData["response"] = "<script>alert('Staff details submitted successfully.');</script>";
            //return await Task.FromResult(false);
            ShowMessage("Error occurred while sending school to administrator.", AlertType.Danger);
            return RedirectToAction("VetRegisteredSchoolsDetails", "vet",new {sddc= schoolid }); ;


        }

        public ActionResult PrintReportX(long sddc)
        {
            //  var schoolid = long.Parse(CurrentUser.UserId);

            // var vwReportModel = new List<vwReportModel>();
            //Get all subjects 
            var schoolSubjectFacilityRep = clsContent.GetSchoolSubjectFacilityReport(sddc);

            var schoolsubjectReport = clsContent.GetSchoolSubjectReport(sddc);

            //sortedFootMessage
            var otherReportDetail = clsContent.GetSchoolAndStaffReportDetails(sddc);
            var reportPath = Path.Combine(Server.MapPath("~/Report"), "OfficeLetter.rpt");
            System.Data.DataTable tblSchool = RemoveNullableClass.ToDataTable(otherReportDetail);
            System.Data.DataTable tblSchoolSubjects = RemoveNullableClass.ToDataTable(schoolSubjectFacilityRep);
            System.Data.DataTable tblschoolsubjectReport = RemoveNullableClass.ToDataTable(schoolsubjectReport);

            //rpt.Load();
           // rpt.setdatasource(c);
            //Stream s = rpr.ExpoertToSteam(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            //return File(s,"applicaton/pdf")
            return new CrystalReportPdfResult(reportPath, tblSchool, tblSchoolSubjects, tblschoolsubjectReport);
        }

        public ActionResult FinishX(long sddc = 0)
        {
            //set schoolprfile and centre file status either completed or pending
            // by default it is pending 
            //centre file could be pending for subject recognition


            // var schoolid = long.Parse(CurrentUser.UserId);
            var userEMail = CurrentUser.EmailAddress;
            var sedEmailAddress = clsContent.GetSEDEmail();

            var userEmailInfo = clsContent.GetAUserInfoForSED(userEMail);
            var stateofSchool = clsContent.GetStatesByUser(userEmailInfo.ID);
            ViewBag.UserEmail = userEmailInfo.EmailAddress;
            var getschool = clsContent.GetSchoolByID(sddc);
            // getschool.DateRecognition=DateTime.Now;
            //var dateofInspection = clsContent.UpdateDateRecognisedForSchool(getschool.DateRecognition,schoolid);
            //sp_GetSchoolStaffCount

            ViewBag.SchoolID = sddc;
            var getSchool = clsContent.GetSchoolByID(sddc);
            ViewBag.SchoolName = getSchool.SchoolNameEdit;
            var staffCount = clsContent.GetSchoolStaffCountX(sddc);
            ViewBag.StaffNumber = staffCount.StaffCount;
            //procSchoolSubectsReport
            //procSchoolSubectsFacilityReport
            var schoolsubjectFacility = clsContent.GetSchoolSubjectFacilityReport(sddc);
            ViewBag.Facilityreport = schoolsubjectFacility;
            TempData["Message"] = CurrentUser.EmailAddress;
            var schoolsubject = clsContent.GetSchoolSubjectReport(sddc);
            ViewBag.SchoolSubjects = schoolsubject;
            // var modelList=new List<SchoolSubjectCompletionReportModel>();


            ViewBag.Finish = "active";

            return View();
        }
        [HttpPost]
        public ActionResult School(SchoolProfileViewModel1 model)
        {

            var getSchool = clsContent.GetSchoolEditByID(model.ID);
            var objYears = new SelectList(clsContent.GetYears(), "yearName", "yearName");
            var objSchCategory = new SelectList(clsContent.GetSchoolCategory(), "ID", "Category");
            var objStates = new SelectList(clsContent.GetStates(), "StateCode", "StateName", getSchool.StateCode);
            ViewBag.AvailableYears = objYears;
            ViewBag.CategoryList = objSchCategory;
            ViewBag.State = clsContent.GetStatesById(getSchool.StateID);
            ViewBag.Lgas = new SelectList(clsContent.GetLgas(getSchool.StateID), "LgaID", "Name", getSchool.LgaID);
            ViewBag.EditSchool = "active";
            var updatedSchool = clsContent.UpdateNewSchoolProfile(model);
            if (updatedSchool != null)
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Schools", new { sddc = updatedSchool.ID }),
                    isRedirect = true
                });
            }

            //return RedirectToAction("SchDetails", new { sddc = model.ID });

            return View(model);
        }

        public ActionResult ReagentFacilityy(int sddc = 0)
        {
            var schoolid = CurrentUser.UserId;
            if (sddc != 0)
            {
                schoolid = sddc;
                Session.Add("SchoolID", schoolid);
            }

            
            ViewBag.EditEquipment = "active";
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolId = schoolid;
            return View();
        }

        public ActionResult FacilityTabbingg(int sddc = 0)
        {

            var schoolid = CurrentUser.UserId;
            if (sddc != 0)
            {
                Session.Add("SchoolID", schoolid);
            }
            
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.SchoolId = schoolid;
            ViewBag.EditEquipment = "active";
            return View();
        }

        public ActionResult SchDetails(long sddc)
        {
            var schoolSubjectView = new List<SchoolSubjectList>();
            var schoolmodel = clsContent.GetSchoolEditByID(sddc);
            TempData["DisplaySchoolName"] = schoolmodel.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            // var schoolmodel = clsContent.GetSchoolByIDSum(sddc);
            //ViewBag.Schoolid = sddc;
            ViewBag.EditSchool = "active";
            return View(schoolmodel);
        }

        //GetSchoolSubjectByIDGeneral
        private SubjectModel GetSchoolSubjectByIdGeneral(long schoolId)
        {
            /**/
            var subjectsAddedBySchool = clsContent.GetSchoolSubjectByIDGeneral(schoolId);
            var subjectIds = subjectsAddedBySchool.Select(item => item.SubjectID).Select(dummy => (int?)dummy).ToList();
            return new SubjectModel
            {
                AvailableSubjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList(),
                AvailableSelected = subjectIds,
                //RequestedSubjects = new List<Subject>()
            };
        }

        private SubjectModel GetSchoolSubjectsById(long schoolId)
        {
            /**/
            var subjectsAddedBySchool = clsContent.GetSchoolSubjectById(schoolId);
            var subjectIds = subjectsAddedBySchool.Select(item => item.SubjectID).Select(dummy => (int?)dummy).ToList();
            return new SubjectModel
            {
                AvailableSubjects = clsContent.GetCouncilSubjects().OrderBy(b => b.LongName).ToList(),
                AvailableSelected = subjectIds,
                //RequestedSubjects = new List<Subject>()
            };
        }

        [HttpGet]
        public ActionResult EditSubject(long sddc = 0)
        {
            var subjectmodel = GetSchoolSubjectsById(sddc);
            //Response.Write(subjectmodel.AvailableSelected.co);
            // result = subjectmodel.ID > 0 ? "1" : "0";
            ViewBag.SchoolID = sddc;
            var school = clsContent.GetSchoolEditByID(sddc);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.UpdateSubject = "active";
            var getschool = clsContent.GetSchoolByID(sddc);
            // ViewBag.isRecognised = getschool.isRecognised;
            // return Json(new { status = result, subjectadded = result }, JsonRequestBehavior.AllowGet);

            return View(subjectmodel);
        }

        [HttpPost]
        public ActionResult EditSubject(SubjectModel model)
        {
            var schoolSubjectTrueorFalse = clsContent.GetSchoolSubjectByIDGeneral(model.SchoolID);
            //var subjectmodel = GetSchoolSubjectsById(model.SchoolID);
            List<SchoolSubject> ret = null;

            var schlists = new List<SubjectModel>();
            if (model.AvailableSelected == null)
            {
                return
                    Json(
                        new
                        {
                            status = false,
                            message =
                                "Please select English, Maths, Civic, One traded subject and any other subjects. Remember: All must be at leats 8 subjects."
                        }, JsonRequestBehavior.AllowGet);
            }
            for (int i = 0; i < model.AvailableSelected.Count; i++)
            {
                var sub = clsContent.GetSelectedSubjectById(model.AvailableSelected[i]);
                schlists.Add(sub);
            }

            var compulsorySubject = clsContent.GetCompulsorySubjects();

            var tradedSubject = clsContent.GetTradeSubjects();

            var anyotherSubject = clsContent.GetAnyOtherSubject();
            var lst = (from lst1 in schlists
                       where compulsorySubject.Any(
                           x => x.ID == lst1.ID)
                       select lst1).ToList();

            if (lst.Count < 3)
            {
                return
                    Json(
                        new
                        {
                            status = false,
                            message = "Math, English and Civic are compulsory subject that must be added"
                        },
                        JsonRequestBehavior.AllowGet);
            }
            var lstTraded = (from lst1 in schlists
                             where tradedSubject.Any(
                                 x => x.ID == lst1.ID)
                             select lst1).ToList();
            if (lstTraded.Count < 1)
            {
                return Json(new { status = false, message = "Add at least one traded subject." },
                    JsonRequestBehavior.AllowGet);

            }
            var lstotherSubject = (from lst1 in schlists
                                   where anyotherSubject.Any(
                                       x => x.ID == lst1.ID)
                                   select lst1).ToList();

            // TODO show error message and stop
            lst.AddRange(lstTraded);
            lst.AddRange(lstotherSubject);

            if (lst.Count < 8)
            {
                return Json(new { status = false, message = "You must add at least 8 subjects." },
                    JsonRequestBehavior.AllowGet);
            }


            var diff = new List<SubjectModel>();

            var diffGreater = new List<SubjectModel>();

            var dummyTextBooks = clsContent.GetTextBooksBySchool(model.SchoolID);
            //GetSchoolSubjectsTrueFalse
            var subjectsfromdb = clsContent.GetSchoolSubjectsActive(model.SchoolID, true);

            if (lst.Count == subjectsfromdb.Count)
            {
                return Json(new
                {
                    status = true,
                    message = "School Subjects remains unchanged",
                }, JsonRequestBehavior.AllowGet);
            }

            //   var subjectsfromdb = clsContent.GetSubjectsSchool(model.SchoolID);
            if (lst.Count < subjectsfromdb.Count)
            {
                foreach (var std in subjectsfromdb)
                {

                    if (lst.FirstOrDefault(item => item.ID == std.SubjectID) == null)
                    {
                        diff.Add(std);
                    }
                }
            }
            if (lst.Count > subjectsfromdb.Count)
            {
                foreach (var std in lst)
                {

                    if (subjectsfromdb.FirstOrDefault(item => item.SubjectID == std.ID) == null)
                    {
                        diffGreater.Add(std);
                    }
                }
            }


            if (lst.Count < subjectsfromdb.Count)
            {
                var makeNotActive = new SchoolSubjectViewModel();
                using (TransactionScope scope = new TransactionScope())
                {
                    if (diff.Count > 0)
                    {
                        foreach (var dif in diff)
                        {


                            makeNotActive = clsContent.MakeSchoolSubjectNotActive(dif.SubjectID,
                                dif.SchoolID, false);
                            //check if subject is in staff table
                            //check if subject is in equip related item table
                            //if true set active to false by school and subjectid
                            //procStaffSubjectDetailBySchoolSubjectIDs
                            //procEquipmentDetailSchoolSubjectIDs
                            //var getStaffSubjectDetail =
                            //    clsContent.ProcStaffSubjectDetailBySchoolSubjectIDs(dif.SubjectID,
                            //        dif.SchoolID);
                            var setStaffSujectNotActive = clsContent.ProcUpdateStaffSubjectDetailBySchoolSubjectIDs(
                                true, dif.SubjectID,
                                dif.SchoolID);
                            if (makeNotActive.HasItem == true)
                            {
                                //var getEquipmentDetailofSchool = clsContent.ProcEquipmentDetailSchoolSubjectIDs(dif.SubjectID,
                                //    dif.SchoolID);
                                var setItemSubjectNotActive =
                                    clsContent.ProcUpdateEquipmentDetailSchoolSubjectIDs(false,
                                        dif.SubjectID,
                                        dif.SchoolID);
                            }


                            //var makeStaffSubjectNotActive = clsContent.MakeSchoolSubjectNotActive(dif.SubjectID,
                            //    dif.SchoolID, false);

                        }
                        //foreach (var text in dummyTextBooks)
                        //{
                        //    if (diff.FirstOrDefault(item => item.SubjectID == text.SubjectID) != null)
                        //    {
                        //        if (text.SubjectID == dif.SubjectID)
                        //        {
                        //            //delete from textbook
                        //            var deletedSubjectTextBook =
                        //                clsContent.DeleteSubjectTextbook(dif.SubjectID, dif.SchoolID);


                        //        }
                        //    }
                        //}
                        //var deleted = clsContent.DeleteSchoolSubjectsFromList(dif.SubjectID,
                        //    dif.SchoolID);
                        if (makeNotActive != null)
                        {
                            // clsContent.UpdateSchoolSjubject2(lst, model.SchoolID);
                            ViewBag.UpdateSubject = "active";
                            scope.Complete();

                            return Json(new
                            {
                                status = true,
                                message = "Subjects removed successfully.",
                                redirectUrl =
                                    Url.Action("EditSubject", "SchoolsEdit",
                                        new { sddc = model.SchoolID }),
                                isRedirect = true
                            }, JsonRequestBehavior.AllowGet);
                        }

                    }
                }


            }


            else if (lst.Count > subjectsfromdb.Count)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (diffGreater.Count > 0)
                    {
                        foreach (var dg in diffGreater)
                        {
                            if (schoolSubjectTrueorFalse.FirstOrDefault(item => item.SubjectID == dg.ID) !=
                                null)
                            {

                                var makeactive = clsContent.MakeSchoolSubjectActive(dg.ID, model.SchoolID, false);
                                var staffsubjectupdate =
                                    clsContent.ProcUpdateStaffSubjectDetailBySchoolSubjectIDs(true, dg.ID,
                                        model.SchoolID);
                                var makeItemSubjectActive = clsContent.ProcUpdateEquipmentDetailSchoolSubjectIDs(true,
                                    dg.ID,
                                    model.SchoolID);
                            }
                            else
                            {
                                var returnsubject = clsContent.AddNewSchoolSubjectEdit(dg.ID, model.SchoolID, true);
                            }
                        }
                        ViewBag.UpdateSubject = "active";
                        scope.Complete();

                        return Json(new
                        {
                            status = true,
                            message = "Subject(s) added successfully.",
                            redirectUrl =
                                Url.Action("EditSubject", "SchoolsEdit", new { sddc = model.SchoolID }),
                            isRedirect = true
                        });
                    }
                }
            }

            return View(new SubjectModel());
        }





        public ActionResult SubjectDetails(long sddc)
        {
            //var schoolids = CurrentUser.UserId;
            //var schoolid = long.Parse(schoolids);

            var schoolSubjectView = new List<SchoolSubjectList>();
            //  var school = clsContent.GetSchoolById(sddc);
            var school = clsContent.GetSchoolByIDEdit(sddc);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;

            ViewBag.Schoolid = sddc;

            var schoolsubjectList = clsContent.GetSchoolSubjects(sddc, true);

            var tradeSubjectsFromDb = clsContent.GetSchoolTradeSubjects();
            var tradeSubjectsSelected = new List<SchoolSubjectViewModel>();

            var compulsorySchoolSubjectsFromDb = clsContent.GetCompulsorySchoolSubjects();
            var compulsorySubjectsSelected = new List<SchoolSubjectViewModel>();

            var otherSubjects = new List<SchoolSubjectViewModel>();
            foreach (var sub in schoolsubjectList)
            {
                if (compulsorySchoolSubjectsFromDb.Any(comp => comp.ID == sub.SubjectID))
                {
                    compulsorySubjectsSelected.Add(sub);
                }
                else if (tradeSubjectsFromDb.Any(comp => comp.ID == sub.SubjectID))
                {
                    tradeSubjectsSelected.Add(sub);
                }
                else
                {
                    otherSubjects.Add(sub);
                }

            }
            schoolSubjectView.Add(new SchoolSubjectList
            {

                Subject = otherSubjects,
                SchoolCompulsorySubject = compulsorySubjectsSelected,
                SchoolLeastTradeSubject = tradeSubjectsSelected
            });

            //   ViewBag.Classroom = "active";
            ViewBag.UpdateSubject = "active";
            // clsContent.UpdateClassForApproval(isAppoved);
            return View(schoolSubjectView);


        }

        private void GetSchoolSubjectFormData(long schoolid)
        {
            //   var studentClassRecord = clsContent.GetASchoolTextBooks(schoolid);  GetSchoolSubjectsByTextbook
            // var schoolSubjectRecord = clsContent.GetSchoolSubjectsByTextbook(schoolid);

            var schoolSubjectRecord = clsContent.GetASchoolTextBooks(schoolid);

            //    var classes = new List<GetASchoolTextBooks>();
            var subjects = new List<SelectListItem>();
            //foreach (var schoolSubject in clsContent.GetSchoolSubjects(schoolid))
            //{
            //    if (schoolSubjectRecord == null)
            //    {
            //        subjects.Add(new SelectListItem
            //        {
            //            Text = schoolSubject.LongName,
            //            Value = schoolSubject.ID.ToString()
            //        });
            //    }
            //    else
            //    {
            //        if (schoolSubjectRecord.FirstOrDefault(s => s.AvailableTextbooks == schoolSubject.AvailableTextbooks) == null)
            //        {
            //            subjects.Add(new SelectListItem
            //            {
            //                Text = schoolSubject.LongName,
            //                Value = schoolSubject.ID.ToString()
            //            });
            //        }
            //    }
            //}
            //ViewBag.Classes = subjects;
            ViewBag.schoolSubjectRecordTextbook = schoolSubjectRecord;



        }

        [HttpGet]
        public ActionResult SchoolStaffIndex(long sddc)
        {
            //GetStaffInfoForEdit
            ViewBag.SchoolID = sddc;
            var staffIndex = clsContent.GetStaffInfoForEdit(sddc);
            return View(staffIndex);
        }

        public ActionResult EditStaffIndex(long sddc, int classid, int degreeid, int subjectid, int staffid,
            int courseid)
        {
            //GetStaffInfoForEdit
            var staff = clsContent.GetAStaffInfoForEdit(staffid);



            var teachersclass = new SelectList(clsContent.GetClass(), "ID", "ClassName", classid);
            var teachersdegree = new SelectList(clsContent.GetDegree(), "ID", "DegreeType", degreeid);
            var teacherssubject = new SelectList(clsContent.GetSubject(), "ID", "LongName", subjectid);
            var teacherscourse = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle", courseid);


            ViewBag.TeachersClass = teachersclass;
            ViewBag.TeachersDegree = teachersdegree;
            ViewBag.TeachersSubject = teacherssubject;
            ViewBag.TeachersCourse = teacherscourse;
            //var staffIndex = clsContent.GetStaffInfoForEdit(sddc);
            return View(staff);
        }


        [HttpGet]
        public ActionResult TextbookIndex(long sddc)
        {
            var schoolSubjectRecord = clsContent.GetASchoolTextBooks(sddc);
            return View(schoolSubjectRecord);
        }

        public ActionResult EditTextbook(long sddc = 0, int subjectId = 0, int textbookid = 0)
        {

            //get for a subject

            var allSchoolSubjects = new SelectList(clsContent.GetSchoolSubjects(sddc, true), "SubjectID", "LongName",
                subjectId);

            var allSchoolTextbook = new SelectList(clsContent.GetASchoolTextBooks(sddc), "ID", "AvaiilableTextbooks",
                textbookid);


            ViewBag.AllSchoolSubjects = allSchoolSubjects;
            ViewBag.AvailableTextbooks = allSchoolTextbook;


            //var allTextbboks = new SelectList(clsContent.GetAllAvailableTextBookBank(subjectId), "SubjectID", "AvailableTextbook");


            // var allTextbboksBank = new SelectList(clsContent.GetAllAvailableTextBook(), "SubjectID", "AvailableTextbooks");


            TempData["Message"] = CurrentUser.EmailAddress;
            //ViewBag.SchoolSubjectList;
            ViewBag.Textbook = "active";
            return View();
        }

        [HttpPost]
        public ActionResult EditTextbook(SchoolSubjectViewModel model)
        {
            return View();
        }

        //public ActionResult EditStaffClass(long sddc)
        //{
        //    return View();
        //}
        public ActionResult CreateStudent(int scid = 0, int sddc = 0)
        {
            var school = Session.GetDataFromSession<string>("SchoolID");
            if (Session.GetDataFromSession<string>("SchoolID") == null && sddc == 0)
            {
                TempData["Information"] = "Register a School to proceed.";
                TempData["schoolprofile"] = "Register school profile";
                ViewBag.EditClass = "active";
                return View();
            }
            if (CurrentUser.UserId == null && sddc != 0)
            {
                var aschool = clsContent.GetSchoolById(sddc);
                var schoolClasses = clsContent.GetSchoolClassBySchoolId(sddc);
                TempData["DisplaySchoolName"] = aschool.SchoolName;
                GetClassFormData(sddc);
                ViewBag.EditClass = "active";
                ViewBag.ClassSchoolid = sddc;
                TempData["Message"] = CurrentUser.EmailAddress;
                return View(schoolClasses);
            }
            if (school != null)
            {
                long schoolid = long.Parse(school);
                var aschool = clsContent.GetSchoolById(schoolid);
                var schoolClasses = clsContent.GetSchoolClassBySchoolId(schoolid);
                TempData["DisplaySchoolName"] = aschool.SchoolName;
                GetClassFormData(schoolid);
                ViewBag.EditClass = "active";
                ViewBag.ClassSchoolid = schoolid;
                TempData["Message"] = CurrentUser.EmailAddress;

                GetClassFormData(schoolid);
                return View(schoolClasses);

            }



            return View();

        }

        private void GetClassFormData(long schoolid)
        {

            var studentClassRecord = clsContent.GetStudentClassRecord(schoolid);
            var classes = new List<SelectListItem>();
            foreach (var stdC in clsContent.GetClass())
            {
                if (studentClassRecord == null)
                {
                    classes.Add(new SelectListItem
                    {
                        Text = stdC.ClassName,
                        Value = stdC.ID.ToString()
                    });
                }
                else
                {
                    if (studentClassRecord.FirstOrDefault(item => item.ClassID == stdC.ID) == null)
                    {
                        classes.Add(new SelectListItem
                        {
                            Text = stdC.ClassName,
                            Value = stdC.ID.ToString()
                        });
                    }
                }

            }
            ViewBag.ClassSchoolid = schoolid;
            ViewBag.Classes = classes;
            ViewBag.ClassesRegistered = studentClassRecord;
        }

        [HttpPost]
        public ActionResult CreateStudent(ClassAllocationvModel classalloc, int sddc = 0)
        {
            //long schoolid = 1;SchoolID=sddc
            if (classalloc.SchoolID == 0)
            {
                GetClassFormData(CurrentUser.UserId);
                // int index = 0;
                var index = new ClassAllocation();
                if (classalloc.NoOfStreams != null && classalloc.TotalStudents != null)
                {
                    index = clsContent.AddClasses(CurrentUser.UserId, classalloc);

                }
                //index = clsContent.AddClasses(schoolid, classalloc);
                var result = index.ID > 0 ? "1" : "0";

                string s = result;

                // clsContent.AddClasses(classalloc);


                ViewBag.FacilityManagement = "active";
                //TempData["facility"] = sch.SchoolName;
                return Json(new { status = result, scid = index.ID, sddc = index.SchoolID, studentAdded = s },
                    JsonRequestBehavior.AllowGet);
            }
            //var schoo = CurrentUser.UserId;
            // var schoolid = long.Parse(schoo);
            GetClassFormData(classalloc.SchoolID);
            // int index = 0;
            var indexsddc = new ClassAllocation();
            if (classalloc.NoOfStreams != null && classalloc.TotalStudents != null)
            {
                indexsddc = clsContent.AddClasses(classalloc.SchoolID, classalloc);

            }

            var resultsddc = indexsddc.ID > 0 ? "1" : "0";

            string ssddc = resultsddc;

            // clsContent.AddClasses(classalloc);


            ViewBag.FacilityManagement = "active";
            //TempData["facility"] = sch.SchoolName;
            return Json(
                new { status = resultsddc, scid = indexsddc.ID, sddc = indexsddc.SchoolID, studentAdded = ssddc },
                JsonRequestBehavior.AllowGet);
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SchoolClassList(long sddc = 0)
        {
            var schoolClasses = clsContent.GetSchoolClassBySchoolId(sddc);
            var getschool = clsContent.GetSchoolEditByID(sddc);
            TempData["DisplaySchoolName"] = getschool.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.EditClass = "active";
            return View(schoolClasses);
        }

        public ActionResult DeleteStaff(int staffid = 0, int sddc = 0)
        {
            var deleteStaff = clsContent.DeleteStaff(staffid, sddc);
            // var staffsubjects = clsContent.GetStaffsSubjects(schoolid);
            //ViewBag.StaffSubjectList = staffsubjects;
            if (deleteStaff > 0)
            {
                //return Json(new
                //{
                //    redirectUrl = Url.Action("StaffSubject", "SchoolStaffs"),
                //    isRedirect = true
                //});
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return RedirectToAction("StaffList", "SchoolsEdit", new { sddc = sddc });
                //return Json(new { status = true,sddc=sddc, redirectUrl = Url.Action("SchoolClassList", "SchoolsEdit",new {sddc=sddc}), JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });

        }
        public ActionResult DeleteClass(int scid = 0, int sddc = 0)
        {
            var deleteAClass = clsContent.DeleteAClass(scid, sddc);
            // var staffsubjects = clsContent.GetStaffsSubjects(schoolid);
            //ViewBag.StaffSubjectList = staffsubjects;
            if (deleteAClass > 0)
            {
                //return Json(new
                //{
                //    redirectUrl = Url.Action("StaffSubject", "SchoolStaffs"),
                //    isRedirect = true
                //});
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return RedirectToAction("SchoolClassList", "SchoolsEdit", new { sddc = sddc });
                //return Json(new { status = true,sddc=sddc, redirectUrl = Url.Action("SchoolClassList", "SchoolsEdit",new {sddc=sddc}), JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });
        }

        public ActionResult EditClass(int scid = 0, int sddc = 0)
        {
            if (sddc == 0)
            {
                ViewBag.Class = new SelectList(clsContent.GetClass(), "ID", "ClassName", scid);
                var schoolClasses = clsContent.GetASchoolClass(scid);
                var getschool = clsContent.GetSchoolEditByID(schoolClasses.SchoolID);
                TempData["DisplaySchoolName"] = getschool.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                ViewBag.EditClass = "active";
                return View(schoolClasses);
            }
            else
            {
                ViewBag.Class = new SelectList(clsContent.GetClass(), "ID", "ClassName", scid);
                var schoolClasses = clsContent.GetASchoolClass(scid);
                var getschool = clsContent.GetSchoolEditByID(sddc);
                TempData["DisplaySchoolName"] = getschool.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                ViewBag.EditClass = "active";
                return View(schoolClasses);
            }

        }

        [HttpPost]
        public ActionResult EditClass(ClassAllocationvModel model)
        {
            ViewBag.Class = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            var fetchClass = clsContent.GetASchoolClass(model.ID);
            fetchClass.ClassID = model.ClassID;
            fetchClass.NoOfStreams = model.NoOfStreams;
            fetchClass.TotalStudents = model.TotalStudents;
            var updtatedclass = clsContent.UpdateASchoolClass(fetchClass);
            var getschool = clsContent.GetSchoolEditByID(fetchClass.SchoolID);
            TempData["DisplaySchoolName"] = getschool.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.EditClass = "active";
            return RedirectToAction("SchoolClassList", new { scid = model.ID, sddc = fetchClass.SchoolID });
        }

        public JsonResult AddImageToCredentialsX(HttpPostedFileBase file, string combinedSecondDegree, int staffId = 0)
        {
            if (file == null || !file.ContentType.Equals("image/jpeg"))
            {
                return Json(new { status = false, mesage = "Only image file is allowed." }, JsonRequestBehavior.AllowGet);
            }
            /** 
             * TODO Create precedure to save image
             */
            var staffImage = new SchStaffCredential();
            string failureMessage = String.Empty;
            string dbpath2 = "";
            if (file.ContentLength != 0)
            {

                string filePath = "";

                var fileName = Path.GetFileName(file.FileName);
                filePath = @"\images\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                var path = Path.Combine(Server.MapPath(@"~\images\" + fileName));
                // dbpath2 = @"~\images\" + fileName;
                // staffImage = clsContent.AddStaffCredentials(staffId, "/images/" + fileName);

                staffImage = clsContent.AddStaffCredentials(staffId, dbpath2);



                if (staffImage != null)
                {
                    file.SaveAs(path);
                }
                else
                {
                    failureMessage = "file failed to upload";
                }

                //file.SaveAs(Path.Combine(filePath, file.FileName));

            }

            //return Json(new { status = true, CredentialID = staffImage.ID, Image = "/images/" + file.FileName, failedReply = failureMessage }, JsonRequestBehavior.AllowGet);

            return Json(
                new { status = true, CredentialID = staffImage.ID, Image = dbpath2, failedReply = failureMessage },
                JsonRequestBehavior.AllowGet);

        }

        public ActionResult CreateX(int sddc = 0)
        {
            if (sddc != 0)
            {
                var school = clsContent.GetSchoolById(sddc);
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
                ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                //ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
                //   ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
                ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
                ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");
                // ViewBag.NTCourseID = new SelectList(clsContent.NtGetCourse(), "ID", "CourseTitle");
                //ViewBag.SecondDegreeCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseName");

                ViewBag.SubjectListing = clsContent.GetSchoolStaffBySchoolId2(sddc);

                var model = new vmSchoolStaffQualification();


                model.Credentials = new List<SchoolStaffModel>();

                //IEnumerable<SelectListItem> objSubjectStaff = new SelectList(clsContent.GetSideStaffSubjectBySchoolId(schid), "SubjectID", "SubjectName");
                // ViewBag.SubjectHighlight = clsContent.GetSideStaffSubjectBySchoolId(schid);
                TempData["Message"] = CurrentUser.EmailAddress;

                ViewBag.StaffRecords = "active";
                // ViewBag.HTab_Create = "active";
                return View(model);
            }
            else
            {
                var school = clsContent.GetSchoolById(sddc);
                TempData["DisplaySchoolName"] = school.SchoolName;
                ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
                ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                //ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
                //   ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
                ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
                ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");
                // ViewBag.NTCourseID = new SelectList(clsContent.NtGetCourse(), "ID", "CourseTitle");
                //ViewBag.SecondDegreeCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseName");

                var model = new vmSchoolStaffQualification();


                model.Credentials = new List<SchoolStaffModel>();
                //IEnumerable<SelectListItem> objSubjectStaff = new SelectList(clsContent.GetSideStaffSubjectBySchoolId(schid), "SubjectID", "SubjectName");
                // ViewBag.SubjectHighlight = clsContent.GetSideStaffSubjectBySchoolId(schid);
                TempData["Message"] = CurrentUser.EmailAddress;

                ViewBag.StaffRecords = "active";
                // ViewBag.HTab_Create = "active";
                return View(model);
            }

        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult CreateX(vmSchoolStaffQualification schoolStaff, string TRCNText, string otherDegree,
            List<long> SchoolSubjectID, string txt, long sddc = 0)
        {
            TempData["response"] = null;
            HttpFileCollectionBase files = Request.Files;
            if (files.Count < 1)
            {
                TempData["response"] = "<script>alert('Please upload staff credentials before submitting');</script>";
                ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
                ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");
                ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
                return View();
            }

            //if (schoolStaff.SchoolID == 0)
            //{
            //var schoolids = CurrentUser.UserId;
            //var schoolid = long.Parse(schoolids);
            var getTemp = TempData["schoolSubjectTableID"];
            var school = clsContent.GetSchoolById(schoolStaff.SchoolID);
            TempData["DisplaySchoolName"] = school.SchoolName;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
            ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");
            //ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            //   ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            // ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
            ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
            //  ViewBag.NTCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");



            ViewBag.StaffRecords = "active";




            var list = new StaffSubject();
            var staffDegreeList = new List<StaffDegree>();
            // var staffClassList = new List<>();
            // staffClassList
            var degreeList = new StaffDegree();

            SecondDegree returnedSecondDegree = null;
            vmSchoolStaffQualification ret = null;
            int intForImage = 1;
            string imageErrorMessage = "";
            bool? returnPrincipal = false;

            using (var sc = new TransactionScope())
            {
                //schoolStaff.SchoolID = schoolid;
                ret = clsContent.AddSchoolStaff(schoolStaff);
                TempData["staffidtemp"] = ret.ID;
                if (ret.ID > 0)
                {
                    ViewBag.SubjectListing = clsContent.GetSchoolStaffBySchoolId2(ret.SchoolID);
                    if (schoolStaff.DegreeID[0] != 0)
                    {
                        foreach (var d in schoolStaff.DegreeID)
                        {
                            //insert in table addstaffsubject(v,staffid)
                            staffDegreeList = clsContent.AddStaffDegree(ret.ID, d, schoolStaff.CoursID);

                        }
                    }

                    var presentStaffCredens = clsContent.GetStaffCredentialss(ret.ID);
                    if (presentStaffCredens.Count == 0)
                    {
                        intForImage = 0;
                        imageErrorMessage = "Staff Credentials must be uploaded";
                    }


                    if (otherDegree != null)
                    {
                        string[] otherdegree = otherDegree.Split(',');
                        foreach (var d in otherdegree)
                        {
                            returnedSecondDegree = clsContent.AddSecondDegree(ret.ID, d);
                        }
                    }
                }

                sc.Complete();
            }
            //}

            //new
            //{
            //    status = true,
            //    staffId = ret.ID,
            //    schid = schoolid,
            //    imageError = intForImage,
            //    imageErrorMess = imageErrorMessage,
            //    message = "Staff details submitted successfully. Please proceed to upload staff credentials"
            //};
            var response = new Response
            {
                status = true,
                staffId = ret.ID,
                schid = schoolStaff.SchoolID,
                imageError = intForImage,
                imageErrorMess = imageErrorMessage,
                message = "Staff details submitted successfully. Please proceed to upload staff credentials"

            };
            TempData["response"] = "<script>alert('Staff details submitted successfully.');</script>";
            files = Request.Files;
            if (files.Count > 0)
            {
                foreach (HttpPostedFileBase file in files.GetMultiple("postedfile"))
                {
                    JsonResult resp = AddImageToCredentialsX(file, "", ret.ID);

                }
            }
            ViewBag.Response = response;
            var model = new vmSchoolStaffQualification();
            model.Credentials = new List<SchoolStaffModel>();
            //IEnumerable<SelectListItem> objSubjectStaff = new SelectList(clsContent.GetSideStaffSubjectBySchoolId(schid), "SubjectID", "SubjectName");
            // ViewBag.SubjectHighlight = clsContent.GetSideStaffSubjectBySchoolId(schid);
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.StaffRecords = "active";
            // ViewBag.HTab_Create = "active";
            //  schoolStaff = null;
            ModelState.Clear();
            return RedirectToAction("StaffList", new { sddc = schoolStaff.SchoolID });

        }




        public ActionResult StaffList(long sddc = 0)
        {
            //  var schoolid = CurrentUser.UserId;
            //var schid = long.Parse(schoolid);
            var school = clsContent.GetSchoolById(sddc);
            var schoolStaffListForView = new List<SchoolStaffList>();
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            var staffList = clsContent.GetStaffBySchoolId(sddc);
            var staffListDegrees = new List<SchoolStaffModel>();
            foreach (var staff in staffList)
            {
                //GetStaffClassesByStaffId
                var schoolStaffClassList = clsContent.GetStaffClassesByStaffId(staff.ID);
                var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySTaffId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffIdEdit(staff.ID);
                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    SchoolStafflassList = schoolStaffClassList,
                    SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,

                });
            }
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.EditStaff = "active";
            return View(schoolStaffListForView);
        }

        public ActionResult Create(int sddc)
        {
            var school = clsContent.GetSchoolById(sddc);
            ViewBag.SchoolID = school.ID;
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
            ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            //ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            //   ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
            ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
            //ViewBag.CourseID = new SelectList(clsContent.GetCourse(), "CoursID", "CourseTitle");
            // ViewBag.NTCourseID = new SelectList(clsContent.NtGetCourse(), "ID", "CourseTitle");
            //ViewBag.SecondDegreeCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseName");

            var staffwithSubject = clsContent.GetStaffWithSubject(sddc);
            var schoolsubjects = clsContent.GetSchoolSubjects(sddc, true);
            var schoolSubjectTableID = new List<long>();
            var listOfNotAssignedSubjects = new List<SelectListItem>();
            foreach (var schoolSubject in schoolsubjects)
            {
                var xx = Tuple.Create(schoolSubject.ID, schoolSubject.SubjectID, schoolSubject.LongName);

                schoolSubjectTableID.Add(schoolSubject.ID);
                listOfNotAssignedSubjects.Add(new SelectListItem
                {
                    Text = schoolSubject.LongName,
                    Value = schoolSubject.SubjectID.ToString()
                });

            }
            var model = new vmSchoolStaffQualification();
            model.SchoolSubjectID = schoolSubjectTableID;
            TempData["schoolSubjectTableID"] = schoolSubjectTableID;
            ViewBag.SubjectID = listOfNotAssignedSubjects;
            ViewBag.SubjectIDss = listOfNotAssignedSubjects;

            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.EditStaff = "active";
            // ViewBag.HTab_Create = "active";
            return View(model);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(vmSchoolStaffQualification schoolStaff, string otherDegree,
            List<HttpPostedFileBase> postedFiles2, List<HttpPostedFileBase> postedFiles,
            string principalproprietoroption, List<long> SchoolSubjectID, int sddc)
        {

            var schoolids = CurrentUser.UserId;

            var getTemp = TempData["schoolSubjectTableID"];
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label");
            ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            //ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            //   ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            // ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
            ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category");
            //  ViewBag.NTCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle");



            ViewBag.StaffRecords = "active";


            var result = String.Empty;

            var staffSubjectListFirst = new List<StaffSubject>();

            var staffSubjectListSecond = new List<StaffSubject>();

            var staffRange = new List<StaffSubject>();

            var list = new StaffSubject();
            var staffDegreeList = new List<StaffDegree>();
            // var staffClassList = new List<>();
            // staffClassList
            var degreeList = new StaffDegree();
            var staffClassListFirst = new List<StaffForClass>();
            staffSubjectListFirst = new List<StaffSubject>();
            SecondDegree returnedSecondDegree = null;
            vmSchoolStaffQualification ret = null;
            bool? returnPrincipal = false;

            using (var sc = new TransactionScope())
            {

                schoolStaff.SchoolID = sddc;

                if (schoolStaff.FirstName == null || schoolStaff.LastName == null ||
                    schoolStaff.CategoryID < 1 || schoolStaff.SubjectID == null || schoolStaff.DateEmployed == null ||
                    schoolStaff.CoursID < 1 || schoolStaff.DegreeID == null)
                {
                    TempData["Fill"] = "Please fill all empty fields";


                }
                else if (postedFiles[0] == null)
                {
                    TempData["Certificates"] =
                        "Please upload your relevant credentials [with TRCN if available]";

                }
                //check if principal exist
                else if (schoolStaff.CategoryID == 3)
                {
                    var principalExist = clsContent.CheckIfPrincipalExist(sddc, schoolStaff.CategoryID);
                    if (principalExist)
                    {
                        returnPrincipal = true;
                        TempData["PrincipalExist"] = "Principal has already been added";
                    }
                }
                else
                {
                    ret = clsContent.AddSchoolStaff(schoolStaff);

                    if (ret.ID > 0)
                    {
                        foreach (var su in schoolStaff.SubjectID)
                        {
                            //insert in table addstaffsubject(v,staffid) AddSchoolStaffSubject
                            staffSubjectListFirst = clsContent.AddStaffSubject(ret.ID, su, true);
                            //  staffSubjectListFirst = clsContent.AddStaffSubject2(ret.ID, su);

                        }
                        staffRange.AddRange(staffSubjectListFirst);
                        //  var corse =int.Parse(schoolStaff.CourseTitle);
                        if (schoolStaff.DegreeID[0] != 0)
                        {
                            foreach (var d in schoolStaff.DegreeID)
                            {
                                //insert in table addstaffsubject(v,staffid)
                                staffDegreeList = clsContent.AddStaffDegree(ret.ID, d, schoolStaff.CoursID);

                            }
                        }


                        if (schoolStaff.SubjectIDss != null)
                        {
                            foreach (var su in schoolStaff.SubjectIDss)
                            {
                                //insert in table addstaffsubject(v,staffid)
                                staffSubjectListSecond = clsContent.AddStaffSubject(ret.ID, su, true);
                                //staffSubjectListSecond = clsContent.AddStaffSubject2(ret.ID, su);

                            }
                            staffRange.AddRange(staffSubjectListSecond);

                        }


                        var staffwithSubject = clsContent.GetStaffWithSubject(sddc);
                        var schoolsubjects = clsContent.GetSchoolSubjects(sddc, true);


                        var listOfNotAssignedSubjects = new List<SelectListItem>();
                        if (staffwithSubject != null)
                        {
                            foreach (var schoolSubject in schoolsubjects)
                            {
                                listOfNotAssignedSubjects.Add(new SelectListItem
                                {
                                    Text = schoolSubject.LongName,
                                    Value = schoolSubject.SubjectID.ToString()
                                });
                            }
                        }
                        ViewBag.SubjectIDss = listOfNotAssignedSubjects;
                        ViewBag.SubjectID = listOfNotAssignedSubjects;
                        if (otherDegree != "")
                        {
                            string[] otherdegree = otherDegree.Split(',');
                            foreach (var d in otherdegree)
                            {
                                returnedSecondDegree = clsContent.AddSecondDegree(ret.ID, d);
                            }
                        }

                        string pathForSaving = Server.MapPath("~/images/");
                        if (!Directory.Exists(pathForSaving))
                        {
                            Directory.CreateDirectory(pathForSaving);
                        }
                        String fileName = String.Empty;
                        bool isUploaded = false;
                        string message = "File upload failed";

                        foreach (var myFile in postedFiles)
                        {
                            if (myFile.ContentLength != 0)
                            {
                                try
                                {
                                    string filePath = "";
                                    fileName = Path.GetFileName(myFile.FileName);
                                    filePath = @"~\images\" + fileName;

                                    if (System.IO.File.Exists(filePath))
                                    {
                                        System.IO.File.Delete(filePath);
                                    }
                                    myFile.SaveAs(Path.Combine(pathForSaving, myFile.FileName));
                                    clsContent.AddStaffCredentials(ret.ID, "/images/" + fileName);
                                    isUploaded = true;

                                    //ViewBag.Message = "File uploaded successfully!";
                                }
                                catch (Exception ex)
                                {
                                    message = string.Format("File upload failed: {0}", ex.Message);
                                }

                            }
                        }


                        sc.Complete();
                        TempData["DataSavedSuccessfully"] = "Data Submitted successfully!";
                        ModelState.Clear();
                        TempData["DataSavedSuccessfully"] = "Data Submitted successfully!";
                    }

                }
            }

            var staffwithSubject2 = clsContent.GetStaffWithSubject(sddc);
            var schoolsubjects2 = clsContent.GetSchoolSubjects(sddc, true);

            var listOfAssignedSubjects2 = new List<SelectListItem>();

            var result2 = String.Empty;

            var staffSubjectListFirst2 = new List<StaffSubject>();
            var staffSubjectListSecond2 = new List<StaffSubject>();
            var staffRange2 = new List<StaffSubject>();

            var list2 = new StaffSubject();
            var staffDegreeList2 = new List<StaffDegree>();
            var degreeList2 = new StaffDegree();

            var listOfNotAssignedSubjects2 = new List<SelectListItem>();
            if (staffwithSubject2 != null)
            {
                foreach (var schoolSubject in schoolsubjects2)
                {
                    listOfNotAssignedSubjects2.Add(new SelectListItem
                    {
                        Text = schoolSubject.LongName,
                        Value = schoolSubject.SubjectID.ToString()
                    });
                }
            }
            ViewBag.SubjectID = listOfNotAssignedSubjects2;
            ViewBag.SubjectIDss = listOfNotAssignedSubjects2;



            return View(new vmSchoolStaffQualification());

        }


        public ActionResult Staff(int scs = 0, int sddc = 0)
        {

            // int schoolid = int.Parse(Session["schoolid"].ToString());
            //var schoolStaff = clsContent.GetSchoolStaffByID(id,schoolid);GetAStaffWithSubject
            var schoolStaff = clsContent.GetStaffByAStaffID(scs);
            if (schoolStaff != null)
            {
                List<SchoolStaffModel> lst = clsContent.GetStaffCredentials(schoolStaff.ID);
                lst.ForEach(v =>
                {
                    v.Image = v.Image.Replace("\\", "/").Replace("~", "");
                });
                schoolStaff.Images = lst;
                var school = clsContent.GetSchoolById(schoolStaff.SchoolID);
                var schoolStaffListForView = new List<SchoolStaffList>();
                ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label", schoolStaff.TitleID);
                ViewBag.SchoolIDD = schoolStaff.SchoolID;
                TempData["DisplaySchoolName"] = school.SchoolNameEdit;
                TempData["Message"] = CurrentUser.EmailAddress;
                ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType", schoolStaff.Degree);

                ViewBag.SubjectIDs = new SelectList(clsContent.GetSchoolSubjects(schoolStaff.SchoolID, true), "SubId",
                    "LongName", schoolStaff.SubId);

                ViewBag.CoursID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle", schoolStaff.CoursID);

                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffIdEdit(schoolStaff.ID);


                ViewBag.StaffSubject = schoolStaffSubjectTaughtList.Skip(1).ToList();
                var subjectmodel = clsContent.GetSubjectEdit();
                ViewBag.AllSubjects = subjectmodel;
                // ViewBag.SubjectID = new SelectList(clsContent.GetSubject(), "ID", "LongName", schoolStaff.SubjectID);

                ViewBag.SchoolSubjectIDs = new SelectList(clsContent.GetSchoolSubjects(schoolStaff.SchoolID, true), "ID",
                    "LongName", schoolStaff.SubId);

                ViewBag.SubjectIdListbox = schoolStaff.SubjectID;

                if (schoolStaff == null)
                {
                    return HttpNotFound();
                }
                //SchoolSubjectRecognitionModel
                // var aStaffClass = clsContent.GetStaffClassesByStaffId(schoolStaff.ID);

                var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(schoolStaff.ID);

                var aStaffSubject = clsContent.GetStaffSubjectTaughtByStaffIdEdit(schoolStaff.ID);

                var aStaffownedDegree = clsContent.GetStaffOwnedDegreeBySTaffId(schoolStaff.ID);

                var subjectids = new List<int>();
                foreach (var sub in aStaffSubject)
                {
                    subjectids.Add(sub.SubjectID);
                }
                schoolStaff.Subject = subjectids.Cast<int>().ToArray();

                var classids = new List<int>();

                schoolStaff.ClassID = classids.Cast<int>().ToArray();
                var degrees = new List<int>();
                foreach (var cls in aStaffownedDegree)
                {
                    degrees.Add(cls.StaffDegreeId);
                }
                schoolStaff.DegreeID = degrees.Cast<int>().ToArray();
                var courses = new List<int>();
                foreach (var course in aStaffownedDegree)
                {
                    courses.Add(course.CourseID);
                }
                schoolStaff.CourseID = courses.Cast<int>().ToArray();

                //  ViewBag.TitleID = new SelectList(clsContent.GetTitle(), "ID", "Label", schoolStaff.TitleID);
                // ViewBag.DegreeID = new MultiSelectList(clsContent.GetDegree(), "ID", "DegreeType", schoolStaff.DegreeID);
                //ViewBag.Degrees = clsContent.GetDegree();
                //ViewBag.Courses= clsContent.GetCourse();
                //ViewBag.ClassID = clsContent.GetClass();
                //ViewBag.Subjects = clsContent.GetSchoolSubjects(school.ID);


                schoolStaff.SecondDegrees = clsContent.GetSecondDegreeForStaff(schoolStaff.ID)
                    .Select(x => x.OtherDegree);

                ViewBag.combinedSecondDegree = string.Join(",", schoolStaff.SecondDegrees);

                //ViewBag.SecondDegrees = clsContent.GetSecondDegreeForStaff(schoolStaff.ID).Select(x=>x.OtherDegree);
                // ViewBag.NTDegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
                //ViewBag.SecondDegreeID = new SelectList(clsContent.GetSecondDegree(), "ID", "OtherDegree");
                //ViewBag.CourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle", schoolStaff.CourseID);
                ViewBag.CategoryID = new SelectList(clsContent.GetSchoolStaffCategory(), "ID", "Category",
                    schoolStaff.CategoryID);
                //   ViewBag.NTCourseID = new SelectList(clsContent.GetCourse(), "ID", "CourseTitle", schoolStaff.NTCourseID);
                ViewBag.EditStaff = "active";
                return View(schoolStaff);
            }

            //else { ViewBag.StaffEditError="Staff"}

            return RedirectToAction("StaffList", "SchoolsEdit", new { sddc = sddc });

        }

        [HttpPost]
        public ActionResult DeleteImage(int imageId)
        {
            //retrieve image path from database and delete from  GetAStaffCredential
            var credential = clsContent.GetAStaffCredential(imageId);
            var filePath = Server.MapPath(credential.Image);
            var deleted = clsContent.DeleteStaffCredentials(imageId);
            if (deleted != null)
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                return Json(new { status = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = false }, JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult AddImageToCredentials(int staffId, HttpPostedFileBase file, string combinedSecondDegree)
        {
            if (file == null || !file.ContentType.Equals("image/jpeg"))
            {
                return Json(new { status = false, mesage = "Only image file is allowed." }, JsonRequestBehavior.AllowGet);
            }
            /** 
             * TODO Create precedure to save image
             */
            var staffImage = new SchStaffCredential();
            string failureMessage = String.Empty;
            if (file.ContentLength != 0)
            {

                string filePath = "";
                var fileName = Path.GetFileName(file.FileName);
                filePath = @"\images\" + fileName;

                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                var path = Path.Combine(Server.MapPath("/images/" + fileName));
                staffImage = clsContent.AddStaffCredentials(staffId, "/images/" + fileName);

                if (staffImage != null)
                {
                    file.SaveAs(path);
                }
                else
                {
                    failureMessage = "file failed to upload";
                }

                //file.SaveAs(Path.Combine(filePath, file.FileName));

            }

            return
                Json(
                    new
                    {
                        status = true,
                        CredentialID = staffImage.ID,
                        Image = "/images/" + file.FileName,
                        failedReply = failureMessage
                    }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Staff(SchoolStaffModelEdit model, int sddc = 0)
        {
            SchoolStaffModelEdit staffDegreeRow;
            if (ModelState.IsValid)
            {
                using (TransactionScope scope = new TransactionScope())
                {

                    var staffmodel = clsContent.UpdateSchoolStaffByID(model.ID, sddc, model.TitleID,
                        model.FirstName, model.LastName, model.DateEmployed, model.CategoryID, model.TRCNText);
                    //foreach (var cls in model.ClassID)
                    //{
                    //    var deletedclasses = clsContent.DeleteStaffClass(staffmodel.ID);

                    //}
                    //foreach (var cls in model.ClassID)
                    //{
                    //    var updatedClasses = clsContent.AddStaffClass(staffmodel.ID, cls);

                    //}

                    //   staffDegreeRow = clsContent.GetStaffDegreeRow(staffmodel.ID);

                    var updateStaffDegreeInfo = clsContent.UpdateDegreeForStaffDegree(staffmodel.ID, staffmodel.DegreID,
                        staffmodel.CoursID);

                    //fetch subjects from staff subject and compare with the front end selected subject
                    //skip if it is equal and newly inserted if not equal with the same staffid
                    var selectedStaff = clsContent.GetAStaffSubjects(staffmodel.ID);
                    if (model.SubjectIDs != null)
                    {

                        foreach (var sub in model.SubjectIDs)
                        {
                            if (selectedStaff.FirstOrDefault(k => k.SubjectID == sub) != null)
                            {
                                continue;
                            }
                            clsContent.AddStaffSubject(staffmodel.ID, sub, true);
                        }

                    }
                    //var insertIntoSecondDegree2 = clsContent.AddSecondDegree(updateStaffDegreeInfo.ID,
                    //    updateStaffDegreeInfo.OtherDegree);


                    if (model.combinedSecondDegree != null)
                    {
                        var deletedSecondDegree = clsContent.DeleteSecondDegree(staffmodel.ID);
                        if (deletedSecondDegree != null)
                        {
                            var splittedTexts = model.combinedSecondDegree.Split(',');
                            foreach (var xc in splittedTexts)
                            {
                                var insertSecondDegrees = clsContent.AddSecondDegree(staffmodel.ID, xc);
                            }
                        }
                    }

                    scope.Complete();
                    return RedirectToAction("StaffList", "SchoolsEdit", new { sddc = staffmodel.SchoolID });
                }
            }

            return View(model);

        }

        private void GetSchoolSubjectItems2(int? subjectid, long sdc)
        {
            //var schoo = CurrentUser.UserId;
            //var schoolid = long.Parse(schoo);
            var schy = clsContent.GetSchoolSubjectItems(sdc).Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;


        }

        private void GetSchoolSubjectItems(int? subjectid, long sdc)
        {
            var schy = clsContent.GetSchoolSubjectItems(sdc).Select(s => new SelectListItem
            {
                Text = s.LongName,
                Value = s.ID.ToString(),
                Selected = (s.ID == subjectid)
            });
            ViewBag.SchoolSubjectList = schy;


        }


        public ActionResult EditEquipment(long sddc, int subjectId = 0, int page = 1, int facid = 0)
        {
            // var schoolid = CurrentUser.UserId;
            //GetSubjectItemsFormData(subjectId);
            // var sch = long.Parse(schoolid);
            ViewBag.SchoolId = sddc;
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            GetSchoolSubjectItems2(subjectId, sddc);
            int pageIndex = 1;
            var subjec = subjectId;
            int pageSize = 20;

            pageIndex = page;


            /*First when equipment table value for a school is null*/

            IPagedList<SchoolEquipConfigViewModel> eqp = null;
            if (subjec == 0)
            {
                eqp = clsContent.GetSchoolEquipment(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);

            }
            else
            {


                var summaryList = clsContent.GetSchoolupdateEquipmentDigitValue(sddc);
                var schoolEqps = clsContent.GetSchoolEquipment(subjec);
                ViewBag.FacilityCategoryID = schoolEqps.First().FacilityCategoryID;
                foreach (var summary in summaryList)
                {
                    //SchoolFacilityValueID
                    var schoolEqp = schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID);
                    if (schoolEqp != null)
                    {
                        //  schoolEqp.EquipmentValue = summary.EquipmentValue;
                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).EquipmentValue
                            = summary.EquipmentValue;

                        schoolEqps.FirstOrDefault(sum => sum.Unique_row == summary.SchoolFacilityValueID).DigitValue =
                            summary.DigitValue;
                    }
                    // schoolEqp.EquipmentValue = 
                }
                //to load template for new school
                eqp = schoolEqps.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);


            }
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.totalgradess = eqp.Sum(x => x.DigitValue);
            ViewBag.subjectid = subjectId;
            ViewBag.CurrentSchool = sddc;
            ViewBag.EditEquipment = "active";

            return View(eqp);
        }

        public JsonResult SaveEquipment(int subjectIds = 0, int facCategoryId = 0, string dd = "", string user = "",
            int record_id = 0, int totalDbRecords = 0, int totalComputed = 0, long sddc = 0)
        {
            //dd: d_id, user: elem.value, record_id:$(elem).attr('data-recordId'), totalDbRecords: '@Model.TotalItemCount', totalComputed: $('#total-grades').html(), facilityCategoryId:'@ViewBag.FacilityCategoryID'
            String result = String.Empty;

            //var schoo = CurrentUser.UserId;
            //var schoolid = long.Parse(schoo);

            var uv = new SchoolEquipConfigViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);


                    uv = clsContent.AddEQuipmentValue(subjectIds, facid, user, sddc);

                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {
                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            sddc);
                        //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                    }
                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            sddc);

                        //very good
                    }
                    return
                        Json(
                            new
                            {
                                gradd = uv.DigitValue,
                                status = true,
                                definition = uv.Definition,
                                message = "Value saved successfully",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    var uservalue = decimal.Parse(user);
                    uv = clsContent.UpdateEquipFacilityValue(record_id, uservalue);

                    //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);
                    if (uv.EquipmentValue >= uv.QuantityRequired)
                    {

                        var row = clsContent.GetGradeExcell("Excellent");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 1;
                        uv.DateEntry = DateTime.Now;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            sddc);

                    }

                    else
                    {
                        var row = clsContent.GetgradeVGood("Very Good");
                        uv.FacilityID = row.ID;
                        uv.Definition = row.Definition;
                        uv.DigitValue = 0;
                        uv = clsContent.UpdateFacility(subjectIds, uv.Equipmentvalue_ID, uv.FacilityID, uv.DigitValue,
                            sddc);

                        //very good
                    }

                    return
                        Json(
                            new
                            {
                                status = true,
                                definition = uv.Definition,
                                message = "Value updated",
                                recordId = uv.Equipmentvalue_ID.ToString()
                            });

                }
            }

            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EquipmentSummary(int totalEquipmentItems, int subjectsid,
            int totalsum, int facilityCategoryId, long sddc = 0)
        {
             FacilityApprovalModel result = null;
            var facilitysummary = clsContent.GetFacilitySummaryBySchoolId(sddc, facilityCategoryId, subjectsid);
            if (facilitysummary == null)
            {
                double computed = ((double)totalsum / (double)totalEquipmentItems) * 100;
                var percentage = computed + "%";
                result = clsContent.SaveEquipmentSummary(totalEquipmentItems, subjectsid, totalsum, facilityCategoryId,
                    percentage, sddc);
                if (result.ID > 0)
                {
                    return Json(new { status = true });
                }
                return Json(new { status = false });

            }
            else
            {
                double computed = ((double)totalsum / (double)totalEquipmentItems) * 100;
                var percentage = computed + "%";
                var facilitysummary2 = clsContent.UpdateFacilitySummaryBySchId(sddc, facilityCategoryId, subjectsid,
                    totalsum, percentage);
                if (facilitysummary2.ID > 0)
                {
                    return Json(new { status = true });
                }
                return Json(new { status = false });
            }


        }



        public ActionResult EditReagent(long sddc, int? subjectid = 0, int page = 1)
        {
            var school = clsContent.GetSchoolById(sddc);
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            GetSchoolSubjectItems2(subjectid, sddc);
            // GetSubjectItemsFormData(subjectid);
            pageIndex = page;
            ViewBag.subjectid = subjectid;

            IPagedList<ReagentViewModel> reag = null;
            if (subjec == 0)
            {
                reag = clsContent.GetSchoolReagent(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                var summaryList = clsContent.GetSchoolupdateReagentDigitValue(sddc);
                var schoolReagents = clsContent.GetSchoolReagent(subjec);
                ViewBag.facilityCategoryId = schoolReagents.First().FacilityCategoryID;
                foreach (var summary in summaryList)
                {

                    var schoolReag =
                        schoolReagents.FirstOrDefault(sum => sum.SchoolReagentConfig_ID == summary.SchReagentID);
                    if (schoolReag != null)
                    {
                        schoolReag.ReagentValue = summary.ReagentValue;
                        schoolReag.DigitValue = summary.DigitValue;
                        schoolReag.SchoolReagentValueID = summary.SchoolReagentValueID;
                        //schoolReag.SchReagentID= summary.SchoolReagentValueID;
                        //ViewBag.SchReagentID = schoolReag.SchReagentID;
                    }
                    //picks the last record
                    // ViewBag.SchReagentID = schoolReag.SchReagentID;
                }

                reag = schoolReagents.OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            ViewBag.SchReagentID = reag.AsEnumerable()
                .Select(o => new ReagentViewModel()
                {
                    SchReagentID = o.SchReagentID
                }).ToList();
            ViewBag.totalgradess = reag.Sum(x => x.DigitValue);
            TempData["Message"] = CurrentUser.EmailAddress;
            //  ViewBag.SchoolSubjectList;

            ViewBag.EditReagent = "active";
            return View(reag);
        }

        public JsonResult SaveReagent(int subjectIds, string dd, string user, int record_id = 0, int totalDbRecords = 0,
            int totalComputed = 0, int existingReagentValueId = 0, long sddc = 0)
        {
            String result = String.Empty;

            var uv = new ReagentViewModel();
            if (dd != null && user != null)
            {
                int facid = int.Parse(dd);
                var existingRecord = clsContent.FetchExistingReagentRecord(sddc, facid);

                if (existingRecord == null)
                {
                    if (record_id == 0)
                    {

                        uv = clsContent.AddReagentValue(facid, user, sddc);

                        if (uv.ReagentValue >= uv.QuantityRequired)
                        {

                            var row = clsContent.GetGradeExcell("Excellent");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 1;
                            //   uv.DateEntry = DateTime.Now;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, sddc);
                            //clsContent.GetFacilityAssessment(schoolid, subjectIds, facilityid,totalDbRecords,totalComputed)
                        }
                        else
                        {
                            var row = clsContent.GetgradeVGood("Very Good");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 0;
                            // uv.DateEntry = DateTime.Now;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, sddc);

                            //very good
                        }


                        return
                            Json(
                                new
                                {
                                    status = true,
                                    gradd = uv.DigitValue,
                                    definition = uv.Definition,
                                    message = "Value saved successfully",
                                    recordId = uv.SchoolReagentValueID.ToString()
                                },
                                JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    record_id = existingRecord.SchoolReagentValueID;
                    if (record_id != 0)
                    {
                        uv = clsContent.UpdateSchoolReagentValue(user, record_id);

                        //   uv = clsContent.UpdateEquipFacilityValue(user, uv.Equipmentvalue_ID);

                        if (uv.ReagentValue >= uv.QuantityRequired)
                        {

                            var row = clsContent.GetGradeExcell("Excellent");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 1;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, sddc);

                        }
                        else
                        {
                            var row = clsContent.GetgradeVGood("Very_Good");
                            uv.FacilityID = row.ID;
                            uv.Definition = row.Definition;
                            uv.DigitValue = 0;
                            uv = clsContent.UpdateReagentFacilityValue(uv.SchoolReagentValueID, uv.FacilityID,
                                uv.DigitValue, sddc);
                        }
                        return
                            Json(
                                new
                                {
                                    status = true,
                                    definition = uv.Definition,
                                    message = "Value updated",
                                    recordId = uv.SchoolReagentValueID.ToString()
                                });

                    }
                }

            }


            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReagentSummary(int totalEquipmentItems, int subjectid, int totalSum, int facilityCategoryId,
            long sddc)
        {

            FacilityApprovalModel result = null;
            var facilitysummary = clsContent.GetFacilitySummaryBySchoolId(sddc, facilityCategoryId, subjectid);
            if (facilitysummary == null)
            {
                double computed = ((double)totalSum / (double)totalEquipmentItems) * 100;
                var percentage = computed + "%";
                result = clsContent.SaveReagentSummary(totalEquipmentItems, subjectid, totalSum, facilityCategoryId,
                    percentage, sddc);
                if (result.ID > 0)
                {
                    return Json(new { status = true });
                }
                return Json(new { status = false });

            }
            else
            {
                double computed = ((double)totalSum / (double)totalEquipmentItems) * 100;
                var percentage = computed + "%";
                var facilitysummary2 = clsContent.UpdateFacilitySummaryBySchId(sddc, facilityCategoryId, subjectid,
                    totalSum, percentage);
                if (facilitysummary2.ID > 0)
                {
                    return Json(new { status = true });
                }
                return Json(new { status = false });
            }

        }

        //SaveReagent  SaveLaboratory
        public ActionResult EditLab(int? subjectid = 0, int page = 1, long sddc = 0)
        {

            var school = clsContent.GetSchoolById(sddc);
            //   TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            TempData["Message"] = CurrentUser.EmailAddress;
            int pageIndex = 1;
            var subjec = subjectid;
            int pageSize = 20;
            //  GetSubjectItemsFormData(subjectid);
            pageIndex = page;

            GetSchoolSubjectItems2(subjectid, sddc);
            IPagedList<LaboratoryViewModel> lab = null;
            if (subjec == 0)
            {
                lab = clsContent.GetSchoolLaboratory(subjec).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }
            else
            {
                lab = clsContent.GetSchoolLaboratoryValues(subjec, sddc).OrderBy
                    (m => m.ID).ToPagedList(pageIndex, pageSize);
            }



            ViewBag.subjectid = subjectid;
            ViewBag.EditLab = "active";
            return View(lab);

        }

        public JsonResult SaveLaboratory(string dd, string user, int record_id = 0, long sddc = 0)
        {
            var result = String.Empty;
            //var schoolid = Session["SchoolID"].ToString();
            //long? schoolid = 66;
            //int schoolid = int.Parse(Session["schoolid"].ToString());
            var uv = new LaboratoryViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    int facid = int.Parse(dd);
                    // var config = clsContent.GetSchoolEquipConfigByID(facid);

                    // uv = clsContent.AddEquipFacilityValue(facid, user, record_id, schoolid);
                    uv = clsContent.AddLaboratoryValue(facid, user, record_id, sddc);

                    return
                        Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateLaboratoryValue(user, record_id);


                    return Json(new { status = true, message = "Value updated successfully", recordId = uv.ID.ToString() });

                }
            }

            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateSchoolLaboratory(int dd, string user, long sddc = 0, int record_id = 0)
        {
            var result = String.Empty;
            // var schoolid = int.Parse(Session["SchoolID"].ToString());
            // long? schoolid = 66;
            // int schoolid = int.Parse(Session["schoolid"].ToString());
            var uv = new LaboratoryViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    // int facid = int.Parse(dd);
                    // var config = clsContent.GetSchoolEquipConfigByID(facid);

                    // uv = clsContent.AddEquipFacilityValue(facid, user, record_id, schoolid);
                    uv = clsContent.AddLaboratoryValue(dd, user, record_id, sddc);

                    return
                        Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateLaboratoryValue(user, record_id);


                    return Json(new { status = true, message = "Value updated successfully", recordId = uv.ID.ToString() });

                }
            }

            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LaboratorySummary(int totalLaboratoryItems, int subjectid, int facilityCategoryId,
            bool generalAssessment, long sddc)
        {
            //var schoo = CurrentUser.UserId;
            //var schoolid = long.Parse(schoo);
            int decision = 0;
            decision = generalAssessment == true ? 1 : 0;

            var result = clsContent.SaveLaboratorySummary(totalLaboratoryItems, subjectid, facilityCategoryId, sddc,
                decision);
            if (result.ID > 0)
            {
                return Json(new { status = true });
            }
            return Json(new { status = false });

        }

        public ActionResult EditLibrary(long sddc = 0, int page = 1)
        {

            var aschool = clsContent.GetSchoolById(sddc);
            //  TempData["DisplaySchoolName"] = aschool.SchoolName;

            var school = clsContent.GetSchoolById(sddc);
            //  TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageIndex = 1;
            int pageSize = 20;
            pageIndex = page;


            IPagedList<LibraryViewModel> library = null;
            library = clsContent.GetSchoolLibrary(sddc).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);
            //ViewBag.SchoolSubjectList;
            ViewBag.EditLibrary = "active";
            TempData["Message"] = CurrentUser.EmailAddress;
            return View(library);
        }

        public ActionResult UpdateSchoolLibrary(long sddc, int dd, string user, int record_id = 0)
        //UpdateSchoolLibraryByLibrIDSchoolID
        {
            var uv = new LibraryViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    // int facid = int.Parse(dd);
                    uv = clsContent.AddLibraryValue(dd, user, sddc);

                    return
                        Json(
                            new
                            {
                                status = true,
                                message = "Value saved successfully",
                                schoolid = sddc,
                                recordId = uv.ID.ToString()
                            },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateLibraryValue(user, record_id);



                    return
                        Json(
                            new { status = true, message = "Value updated", schoolid = sddc, recordId = uv.ID.ToString() });

                }


                //    var updatedLabValue = clsContent.UpdateSchoolLibrary(dd, user, sddc);
                ////ViewBag.EditLab = "active";
                //if (updatedLabValue != null)
                //{
                //    return Json(new {status = true, message = "Value updated successfully"});
                //}
                //else
                //{

                //}

            }
            return Json(new { status = true, message = "Error occured saving data" });
        }

        public ActionResult EditExam(long sddc = 0)
        {
            var school = clsContent.GetSchoolById(sddc);
            //TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageIndex = 1;
            // var subjec = subjectid;
            int pageSize = 20;
            // GetSubjectItemsFormData(subjectid);
            IPagedList<ExamHallViewModel> exam = null;
            //GetSchoolExamHallValue
            exam = clsContent.GetSchoolExamHallValue(sddc).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);

            ViewBag.EditExaminational = "active";
            TempData["Message"] = CurrentUser.EmailAddress;
            return View(exam);

        }

        public ActionResult UpdateExamHallValue(long sddc, int dd, string user, int record_id = 0)
        {
            //var schoolid = long.Parse(scho);
            var uv = new ExamHallViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    // int facid = int.Parse(dd);

                    uv = clsContent.AddExaminationHall(dd, user, sddc);

                    return
                        Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {
                    uv = clsContent.UpdateExamHallValue(user, record_id);



                    return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() });

                }
            }

            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);




            var updatedLabValue = clsContent.UpdateSchoolExam(dd, user, sddc);
            //ViewBag.EditLab = "active";
            if (updatedLabValue != null)
            {
                return Json(new { status = true, message = "Value updated successfully" });
            }
            return Json(new { status = true, message = "Error occured saving data" });
        }

        public ActionResult EditRecreational(long sddc = 0, int page = 1)
        {
            var school = clsContent.GetSchoolById(sddc);
            // TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            int pageIndex = 1;
            TempData["Message"] = CurrentUser.EmailAddress;
            int pageSize = 20;
            pageIndex = page;
            ViewBag.StaffID = new SelectList(clsContent.GetStaffNamebyschool(sddc), "ID", "FirstName", 503);

            IPagedList<RecreationalViewModel> library = null; // GetRecreationalFacilityValue
            library = clsContent.GetSchoolRecreational(sddc).OrderBy
                (m => m.ID).ToPagedList(pageIndex, pageSize);

            //ViewBag.SchoolSubjectList;

            ViewBag.EditRecreational = "active";
            return View(library);
        }

        public ActionResult DeleteStaffSubjectClass(int token)
        {
            var returnedvalue = clsContent.DeleteStaffSubjectClass(token);
            if (returnedvalue)
            {
                //  var updateSchoolSubject = clsContent.UpdateASchoolSubject(schoolSubject, schid, subjectId);
                return Json(new { status = true, tokenId = token, JsonRequestBehavior.AllowGet });
            }
            return Json(new { status = false, JsonRequestBehavior.AllowGet });

        }

        public ActionResult SaveStaffClass(int staffId = 0, int classId = 0, int sub = 0, int staffSubjectRowId = 0)
        {
            //staffid subjectid staffsubjecttoclassid classname


            var getRowid = clsContent.GetStaffSubjectRowId(staffId, sub);
            ViewBag.StaffSubjectRowId = getRowid;
            var teacher2Class = clsContent.CheckIfClassAddedForTeacher(staffId, classId, sub);
            if (classId < 1)
            {
                TempData["SelectClass"] = "Please select a class";
                return Json(new { status = 1, selectdata = TempData["SelectClass"] }, JsonRequestBehavior.AllowGet);
            }
            if (teacher2Class)
            {
                TempData["ClassAlreadyAdded2Staff"] = "Class is already assigned to staff for the selected subject";


                return Json(new { status = 2, xxtdata = TempData["ClassAlreadyAdded2Staff"] },
                    JsonRequestBehavior.AllowGet);
            }
            else
            {

                var insertedClass = clsContent.InsertAStaffSubjectClass(getRowid.StaffSubjectRowId, classId);
                string fullName = insertedClass.FirstName + " " + insertedClass.LastName;
                if (insertedClass != null)
                {
                    return Json(new
                    {
                        status = true,
                        classId = insertedClass.ClassID,
                        subjectId = insertedClass.SubjectID,
                        firstName = fullName,
                        Id = insertedClass.StaffSubjectToClassID,
                        staffsubjecttoclassid = insertedClass.StaffSubjectToClassID

                    }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = false, xxtdata = TempData["ClassAlreadyAdded2Staff"] },
                JsonRequestBehavior.AllowGet);
        }

        private void GetStaffSubjectToClassFormData(long schoolId)
        {
            //var schoolSubjectRecord = clsContent.GetSchoolSubjectsByTextbook(schoolid);
            var staffSubject2Classes = clsContent.GetStaffSubjectClasses(schoolId);
            var subject2Classes = new List<SelectListItem>();
            foreach (var subjectClass in clsContent.GetStaffSubjectClasses(schoolId))
            {
                if (staffSubject2Classes == null)
                {
                    subject2Classes.Add(new SelectListItem
                    {
                        Text = subjectClass.ClassName,
                        Value = subjectClass.StaffSubjectToClassID.ToString()
                    });
                }
                else
                {
                    if (
                        staffSubject2Classes.FirstOrDefault(
                            sc => sc.StaffSubjectToClassID == subjectClass.StaffSubjectToClassID) == null)
                    {
                        subject2Classes.Add(new SelectListItem
                        {
                            Text = subjectClass.ClassName,
                            Value = subjectClass.StaffSubjectToClassID.ToString()

                        });
                    }
                }
            }
            //ViewBag.Classes = subjects;
            ViewBag.StaffSubject2Classes = staffSubject2Classes;

        }

        public ActionResult _schoolSubjects(long schid)
        {
            var data = clsContent.GetSchoolSubjects(schid);
            return PartialView(data);
        }

        public ActionResult StaffSubject(long sddc = 0)
        {
            var school = clsContent.GetSchoolById(sddc);
            var schoolStaffListForView = new List<SchoolStaffList>();
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;
            var staffList = clsContent.GetStaffBySchoolId(sddc);
            var staffListDegrees = new List<SchoolStaffModel>();
            foreach (var staff in staffList)
            {
                //GetStaffClassesByStaffId
                // var schoolStaffClassList = clsContent.GetStaffClassesByStaffId(staff.ID);
                // var schoolStaffDegreeList = clsContent.GetStaffOwnedDegreeBySTaffId(staff.ID);
                var schoolStaffSubjectTaughtList = clsContent.GetStaffSubjectTaughtByStaffIdEdit(staff.ID);
                //  var schoolStaffOtherDegreeList = clsContent.GetStaffOtherDegreeByStaffId(staff.ID);
                schoolStaffListForView.Add(new SchoolStaffList
                {
                    Staff = staff,
                    // SchoolStafflassList = schoolStaffClassList,
                    //  SchoolStaffDegreeList = schoolStaffDegreeList,
                    SchoolStaffSubjectTaughtList = schoolStaffSubjectTaughtList,
                    // SchoolStaffOtherDegreeList = schoolStaffOtherDegreeList,

                });
            }
            TempData["Message"] = CurrentUser.EmailAddress;
            //ViewBag.EditStaff = "active";
            ViewBag.StaffSubjectEdit = "active";
            return View(schoolStaffListForView);

















            //  //GetStaffDegreeCourse
            // // var schoolid = CurrentUser.UserId;
            ////  var schid = long.Parse(schoolid);
            //  var school = clsContent.GetSchoolById(sddc);
            //  TempData["DisplaySchoolName"] = school.SchoolName;
            //  // var staffDegreeCourse = clsContent.GetStaffDegreeCourse()
            //  ViewBag.SchoolStaffName = new SelectList(clsContent.GetSchoolStaffBySchoolId(sddc), "ID", "SchoolStaffName");
            //  ViewBag.SubjectIDss = clsContent.GetSchoolSubjects(sddc, true).Select(a => new SelectListItem
            //  {
            //      Text = a.LongName,
            //      Value = a.SubjectID.ToString()
            //  });
            //  //ViewBag.DegreeID = new SelectList(clsContent.GetDegree(), "ID", "DegreeType");
            //  //ViewBag.CourseID = new SelectList(clsContent.GetCourse(), "CoursID", "CourseTitle");
            //  TempData["Message"] = CurrentUser.EmailAddress;
            //  var schoolsubjects = clsContent.GetSchoolSubjects(sddc);
            //  var staffsubjects = clsContent.GetStaffsSubjects(sddc);
            //  var subjectmodel = clsContent.GetSubjectEdit();
            //  ViewBag.AllSubjects = subjectmodel;
            //  ViewBag.StaffSubjectList = staffsubjects;
            //  var schoolSubjectTableID = new List<long>(); var listOfSubjects = new List<SelectListItem>();
            //  foreach (var schoolSubject in schoolsubjects)
            //  {
            //      // var xx = Tuple.Create(schoolSubject.ID, schoolSubject.SubjectID, schoolSubject.LongName);

            //      schoolSubjectTableID.Add(schoolSubject.ID);
            //      listOfSubjects.Add(new SelectListItem
            //      {
            //          Text = schoolSubject.LongName,
            //          Value = schoolSubject.SubjectID.ToString()
            //      });

            //  }
            //  var model = new vmSchoolStaffQualification();
            //  model.SchoolSubjectID = schoolSubjectTableID;
            //  ViewBag.SubjectListing = schoolsubjects;
            //  ViewBag.StaffSubjectEdit = "active";
            //  //GetSchoolStaffBySchoolID

            //  return View(model);
        }

        [HttpPost]
        public ActionResult StaffSubject(vmSchoolStaffQualification model)
        {
            // var schoolid = CurrentUser.UserId;
            //var schid = long.Parse(schoolid);
            var school = clsContent.GetSchoolById(model.SchoolID);
            TempData["DisplaySchoolName"] = school.SchoolName;
            ViewBag.SchoolStaffName = new SelectList(clsContent.GetSchoolStaffBySchoolId(model.SchoolID), "ID",
                "SchoolStaffName");
            ViewBag.SubjectIDss = clsContent.GetSchoolSubjects(model.SchoolID, true).Select(a => new SelectListItem
            {
                Text = a.LongName,
                Value = a.SubjectID.ToString()
            });

            var schoolsubjects = clsContent.GetSchoolSubjects(model.SchoolID);
            ViewBag.SubjectListing = schoolsubjects;

            if (model.SubjectIDss != null)
            {
                var staffrowid = int.Parse(model.SchoolStaffName);
                foreach (var su in model.SubjectIDss)
                {

                    //insert in table addstaffsubject(v,staffid)
                    var staffSubjectsReturned = clsContent.AddStaffSubject(staffrowid, su, false);
                    //staffSubjectListSecond = clsContent.AddStaffSubject2(ret.ID, su);

                }


            }
            var response = new Response
            {
                status = true,
                staffId = model.ID,
                schid = model.SchoolID,
                message = "Staff details submitted successfully. Please proceed to upload staff credentials"

            };
            TempData["response"] = "<script>alert('Staff details submitted successfully.');</script>";
            return View(model);
        }


        public ActionResult EditStaffSubject(int scs = 0, long sddc = 0)
        {
            //var school = clsContent.GetSchoolById(sddc);
            //TempData["DisplaySchoolName"] = school.SchoolName;
            //// var staffDegreeCourse = clsContent.GetStaffDegreeCourse()
            ViewBag.SchoolStaffNameId = new SelectList(clsContent.GetSchoolStaffBySchoolId(sddc), "ID",
                "SchoolStaffName", scs);
            ViewBag.SchId = sddc;
            var schoolsubjects = clsContent.GetSchoolSubjects(sddc);
            var staffsubjects = clsContent.GetStaffsSubjects(sddc, scs);
            var subjectmodel = clsContent.GetSubjectEdit();
            ViewBag.AllSubjects = subjectmodel;
            ViewBag.StaffSubjectList = staffsubjects;
            //var schoolSubjectTableID = new List<long>(); var listOfSubjects = new List<SelectListItem>();
            //foreach (var schoolSubject in schoolsubjects)
            //{
            //    // var xx = Tuple.Create(schoolSubject.ID, schoolSubject.SubjectID, schoolSubject.LongName);

            //    schoolSubjectTableID.Add(schoolSubject.ID);
            //    listOfSubjects.Add(new SelectListItem
            //    {
            //        Text = schoolSubject.LongName,
            //        Value = schoolSubject.SubjectID.ToString()
            //    });

            //}
            //var model = new vmSchoolStaffQualification();
            //model.SchoolSubjectID = schoolSubjectTableID;
            ViewBag.SubjectListing = schoolsubjects;
            ViewBag.StaffSubjectEdit = "active";

            return View();
        }

        [HttpPost]
        public ActionResult EditStaffSubject(vmSchoolStaffQualification model, int scs = 0, int sddc = 0)
        {
            var selectedStaff = clsContent.GetAStaffSubjects(model.SchoolStaffNameId);
            if (model.SubjectIDss != null)
            {
                var x = clsContent.DeleteStaffSubject(scs);
                if (x > 0)
                {
                    foreach (var sub in model.SubjectIDss)
                    {

                        //if (selectedStaff.FirstOrDefault(k => k.SubjectID == sub) != null)
                        //{
                        //    continue;
                        //}
                        clsContent.AddStaffSubject(model.SchoolStaffNameId, sub, false);
                    }
                }

            }
            TempData["Message"] = CurrentUser.EmailAddress;
            ViewBag.StaffSubjectEdit = "active";
            return RedirectToAction("StaffSubject", "SchoolsEdit", new { sddc = sddc });
        }



        // GET: SchoolStaffs/Create
        [HttpGet]
        public ActionResult StaffSubjectClass(int sddc = 0, int staffId = 0, int subjectId = 0)
        {
            //var schoolid = CurrentUser.UserId;
            //var schid = long.Parse(schoolid);
            var school = clsContent.GetSchoolById(sddc);
            // var getRowid = clsContent.GetStaffSubjectRowId(staffId, subjectId);
            //fetch staffsubjcettoclassid
            // var staffsubjcettoclassid=clsContent.row
            TempData["DisplaySchoolName"] = school.SchoolNameEdit;

            // clsContent.GetStaffSubjectClasses(schid);
            GetStaffSubjectToClassFormData(sddc);
            var allStaff = new SelectList(clsContent.GetStaffBySchoolId(sddc).ToList().Select(s => new
            {
                FullName =
                    $"{s.FirstName} {s.LastName}",
                FID = s.ID
            }), "FID", "FullName", staffId);
            //   var allStaff= new SelectList(clsContent.GetStaffBySchoolId(schid), "ID", "FirstName", staffId);
            ViewBag.ClassID = new SelectList(clsContent.GetClass(), "ID", "ClassName");
            var allStaffToSubject = clsContent.GetSubjectByStaffSchoolID(sddc, staffId);

            var allStaffSubjects = new SelectList(allStaffToSubject, "SubjectID", "LongName", subjectId);
            List<SchoolStaffModel> eqp = null;
            ViewBag.Staffs = allStaff;
            ViewBag.SchoolID = sddc;
            ViewBag.StaffSubjects = allStaffSubjects;
            ViewBag.StaffSubjectClassEdit = "active";
            eqp = clsContent.GetSchooStaffInfo(sddc);
            ViewBag.StaffSubject2Classes = eqp;
            TempData["Message"] = CurrentUser.EmailAddress;
            TempData["DisplaySchoolName"] = school.SchoolNameEdit; //GetSubjectByStaffSchoolID

            return View(eqp);
        }

        public ActionResult UpdateRecreational(long sddc, int dd, string user, int record_id = 0)
        {
            var uv = new RecreationalViewModel();
            if (dd != null && user != null)
            {
                if (record_id == 0)
                {
                    // int facid = int.Parse(dd);
                    uv = clsContent.AddRecreational(dd, user, sddc);
                    return
                        Json(new { status = true, message = "Value saved successfully", recordId = uv.ID.ToString() },
                            JsonRequestBehavior.AllowGet);
                }
                else if (record_id != 0)
                {

                    uv = clsContent.UpdateRecreationalValue(user, record_id);

                    return Json(new { status = true, message = "Value updated", recordId = uv.ID.ToString() });

                }
            }

            else
            {
                return Json(new { status = false, message = "Value is required." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, message = "Error occurred." }, JsonRequestBehavior.AllowGet);
        }
    }

}