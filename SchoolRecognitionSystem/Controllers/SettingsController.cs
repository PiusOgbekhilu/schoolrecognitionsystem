﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;
using SchoolRecognitionSystem.Models;

namespace SchoolRecognitionSystem.Controllers
{
    public class SettingsController : Controller
    {
        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();


       

        public ActionResult Index22()
        {
          
            

            return View();
        }





        // GET: Settings
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult CreateCustodian()
        {

            return View();

        }
        //public ActionResult CreateCustodian(cus)
        //{

        //    return View();

        //}
      
        public ActionResult CreateCategory(int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {
            const int pageSize = 10;
            var totalRows = mobjMo.CountCategory();

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var category = mobjMo.GetCategoryPage(page, pageSize, sort, Dir);
            var data = new PagedCustomerModel()
            {
                TotalRows = totalRows,
                PageSize = pageSize,
                Category = category
            };
            return View(data);
        }


        //public JsonResult GetFacility(string item)
        //{
        //    List<string> facilitys = new List<string>();
        //    switch (item)
        //    {
        //        case "1":
        //            facilitys.Add("Equipment Facility");
        //            facilitys.Add("Laboratory Facility");
                    
        //            break;
        //        case "2":
        //            facilitys.Add("Library Facility");
        //            facilitys.Add("Recreational Facility");
        //            facilitys.Add("Classroom Facility");
        //            facilitys.Add("Examination Facility");
        //            facilitys.Add("Assessment Facility");
        //            //add UK states here
        //            break;
               
        //    }
        //    return Json(facilitys);
        //}

        public List<FacilityCategory> LoadFacilitiesByRecognitionType(int categoryid)
        {

            var yy2 = ctx.FacilityCategories.Where(ci => ci.RecognitionID == categoryid);
                  
                 
            return yy2.ToList();

           
        }

        [HttpGet]
        public ActionResult CreateSubjectItems()
        {
            List<SelectListItem> recogs = new List<SelectListItem>();
            vmTypesettingsUser vmModel = new vmTypesettingsUser();

            List<SchoolRecognitionType> rect = ctx.SchoolRecognitionTypes.ToList();
            rect.ForEach(x =>
            {
                recogs.Add(new SelectListItem { Text = x.TypeofRecognition, Value = x.ID.ToString() });
            });
            vmModel.RecNames = recogs;
            return View(vmModel);

            //return View();

        }
        [HttpPost]
        public ActionResult GetFacility(string recid)
        {
            int reid;
            List<SelectListItem> facNames = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(recid))
            {
                reid = Convert.ToInt32(recid);
                List<FacilityCategory> faccate =ctx.FacilityCategories.Where(x => x.RecognitionID == reid).ToList();
                faccate.ForEach(x =>
                {
                    facNames.Add(new SelectListItem { Text = x.TypeOfFacility, Value = x.ID.ToString() });
                });
            }
            return Json(facNames, JsonRequestBehavior.AllowGet);
        }

        public ActionResult activepage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateSubjectItems(vmTypesettingsUser set)
        {
            ViewBag.recognitiontypes = ctx.SchoolRecognitionTypes.Select(srt => new SelectListItem
            {
                Text = srt.TypeofRecognition,
                Value = srt.ID.ToString()
            });


            //ViewBag.PhoneAccessories =
            //    LoadFacilitiesByRecognitionType(set.FacilityTypeID).Select(s => new SelectListItem
            //    {
            //        Text = s.TypeOfFacility,
            //        Value = s.ID.ToString()
            //    }).ToList();



            //var facType = ctx.FacilityCategories.Select(p => new SelectListItem
            //{
            //    Text = p.TypeOfFacility,
            //    Value = p.ID.ToString()
            //});
            //ViewBag.FacilityTypes = facType;

            //var subjectx = ctx.Subjects.Where(ss => ss.HasItem == true).Select(s => new SelectListItem
            //{
            //    Text = s.LongName,
            //    Value = s.ID.ToString()
            //});
            //ViewBag.Subjects = subjectx;
            //if (ModelState.IsValid)
            //{
            //    Facility sett = new Facility();
            //    // sett.ItemDescription = set.ItemDescription;
            //    sett.isActive = set.isActive;

            //    ctx.Facilities.Add(sett);
            //    //ctx.SaveChanges();
            //    SchoolEquipConfig vv = new SchoolEquipConfig();
            //    vv.QuantityRequired = set.QuantityRequired;
            //    vv.SubjectID = set.SubjectID;
            //    ctx.SchoolEquipConfigs.Add(vv);
            //    ctx.SaveChanges();
            //    ModelState.Clear();
            //    TempData["Success"] = "subject is created";
            //}
            return View();

        }

        public ActionResult RenderSelectedItem(int selectedId = 0)
        {
            var model =LoadFacilitiesByRecognitionType(selectedId);
            ViewBag.selectedId = selectedId;
            return PartialView(model);
        }

        public JsonResult SubjectList()
        {
            
            var Subjects = (from s in ctx.Subjects select new { s.ID, s.LongName }).ToList();
           
            return Json(Subjects, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SummaryAsesmentFacility(/*int reccid, int facilityid,*/int page = 1, string sort = "catee", string sortDir = "ASC")
         {


            vmTypesettingsUser mobjMod = new vmTypesettingsUser();
            const int pageSize = 10;
            //var totalRows = mobjMod.CountLab();

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var sumview = mobjMod.GetSummaryPage2(page, pageSize, sort, Dir);
            var yall = from p in sumview

                       select new Facility
                       {
                           ID = p.Facility.ID,
                           ItemDescription = p.Facility.ItemDescription,
                           //SumConfigID = p.ID,

                           FacilityCategoryID = p.Facility.FacilityCategoryID
                       };

            return PartialView("_SummaryAssessmentFacility", yall);

        }


        public ActionResult LaboratoryFacility(/*int reccid, int facilityid,*/int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {


            vmTypesettingsUser mobjMod = new vmTypesettingsUser();
            const int pageSize = 10;
            //var totalRows = mobjMod.CountLab();

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var Labview = mobjMod.GetLabPage(page, pageSize, sort, Dir);



            return PartialView("_LaboratoryFacility", Labview);

        }


       
        vmTypesettingsUser mobjMod = new vmTypesettingsUser();
        public ActionResult EquipmentFacility(/*int reccid, int facilityid*/ int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {
           
            const int pageSize = 10;
            //var totalRows = mobjMod.CountLab();

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var Equipview = mobjMod.GetEquipPage(page, pageSize, sort, Dir);
            //var data = new SchoolEquipView()
            //{
            //    //TotalRows = totalRows,
            //    //PageSize = pageSize,
            //    Equipment = Equipview
            //};


            return PartialView("_EquipmentFacility", Equipview);
        }


        








        public ActionResult ReloadGrid(int page = 1, string sort = "cateeid", string sortDir = "ASC")
        {
            vmTypesettingsUser mobjMod = new vmTypesettingsUser();
            const int pageSize = 10;
            //var totalRows = mobjMod.CountLab();

            bool Dir = sortDir.Equals("desc", StringComparison.CurrentCultureIgnoreCase) ? true : false;

            var Equipview = mobjMod.GetEquipPage(page, pageSize, sort, Dir);
            //var data = new SchoolEquipView()
            //{
            //    //TotalRows = totalRows,
            //    //PageSize = pageSize,
            //    Equipment = Equipview
            //};


            return PartialView("_EquipmentFacility", Equipview);
            

        }

        [HttpGet]
        public ActionResult CreateStaffCategory()
        {

            return View(new SchoolStaffCategory());
        }


        [HttpPost]
        public ActionResult CreateStaffCategory(SchoolStaffCategory ssf)
        {
            SchoolStaffCategory ss = new SchoolStaffCategory();
            ss.Category = ssf.Category;
            ss.isDefault = true;
            ctx.SchoolStaffCategories.Add(ss);
           // ctx.SaveChanges();
            TempData["Success"] = "subject is created";
            return View(ss);
        }

        //ModelServices mobjModel = new ModelServices();
       

        vmTypesettingsUser mobjMo = new vmTypesettingsUser();
        [HttpGet]
        public JsonResult UpdateRecordE(int id, string catee, string Quant, string sub, string equipID)
        {
            bool result = false;
            try
            {
                result = mobjMo.UpdateCategoryE(id, catee, Quant, sub, equipID);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecordCat(string catee)
        {
            bool result = false;
            try
            {
                result = mobjMo.SaveCategoryCate(catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }


        //[HttpGet]
        //public JsonResult SaveRecordE(string catee, string Quant)
        //{
        //    bool result = false;
        //    try
        //    {
        //        result = mobjMo.SaveCategoryE(catee,Quant);

        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    return Json(new { result }, JsonRequestBehavior.AllowGet);
        //}
        [HttpGet]
        public JsonResult UpdateRecordLib(int id, string catee)
        {
            bool result = false;
            try
            {
                result = mobjMo.UpdateCategoryLib(id, catee);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult SaveRecordEquip(string catee,string SU, string Quant,string fac)
        {
            bool result = false;
            try
            {
                result = mobjMo.SaveRecordEquip(catee, Quant, SU,fac);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UpdateRecordSchCate(string id,string catee)
        {
            bool result = false;
            try
            {
                result = mobjMo.updateSchCate(id, catee);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult SaveRecordLib(string catee,string cateid)
        {
            bool result = false;
            try
            {
                result = mobjMo.SaveRecordLib(catee, cateid);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult SaveRecordRecrea(string catee, string cateid)
        {
            bool result = false;
            try
            {
                result = mobjMo.SaveRecordRecrea(catee, cateid);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult UpdateRecordRecre(int id, string catee, string cateid)
        {
            bool result = false;
            try
            {
                result = mobjMo.UpdateRecordRecre(id, catee,cateid);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecordSUM(string catee, string cateid)
        {
            bool result = false;
            try
            {
                result = mobjMo.SaveRecordSUM(catee, cateid);

            }
            catch (Exception ex)
            {

            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteRecordLib(int id)
        {
            bool result = false;
            try
            {
                result = mobjMo.DeleteRecordLib(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult UpdateRecordL(int id, string catee, string sub,string labID)
        {
            bool result = false;
            try
            {
                result = mobjMo.UpdateCategoryL(id, catee, sub, labID);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecordL(string catee, string sub, string facateid)
        {
            bool result = false;
            try
            {
                result = mobjMod.SaveRecordLab(catee, sub, facateid);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteRecordRecrea(int id)
        {
            bool result = false;
            try
            {
                result = mobjMo.DeleteRecordRecrea(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult DeleteRecordEquip(int id,string facid)
        {
            bool result = false;
            try
            {
                result = mobjMo.DeleteRecordEquip(id,facid);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult DeleteRecordL2(int id)
        {
            bool result = false;
            try
            {
                result = mobjMo.DeleteRecordL2(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteRecordCategory(int id)
        {
            bool result = false;
            try
            {
                result =mobjMo.DeleteRecordCateg(id);

            }
            catch (Exception ex)
            {
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
    }
}