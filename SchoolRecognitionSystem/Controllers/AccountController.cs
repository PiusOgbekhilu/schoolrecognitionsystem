﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using SchoolRecognitionSystem.Models;
namespace SchoolRecognitionSystem.Controllers
{
    public class AccountController : Controller
    {
        SchoolSubjectRecognitionContext ctx = new SchoolSubjectRecognitionContext();
        // GET: Account
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
        [Authorize(Roles = "admin")]
        public ActionResult AdminIndex()
        {
            ViewBag.Message = "This can be viewed only by users in Admin role only";
            return View();
        }

   
        // GET: /Account/Register
        [AllowAnonymous]
[HttpGet]
        public ActionResult Register()
        {
            var titles = ctx.Titles.ToList();
            ViewBag.titles = titles.Select(tt => new SelectListItem
            {
                Text = tt.Label,
                Value = tt.ID.ToString()
            });

            var Ranks = ctx.Ranks.ToList();
            ViewBag.Ranks = Ranks.Select(R => new SelectListItem
            {
                Text = R.RankTitle,
                Value = R.ID.ToString()
            });

            var Offices = ctx.Offices.ToList();
            ViewBag.Offices = Offices.Select(of => new SelectListItem
            {
                Text = of.OfficeName,
                Value = of.ID.ToString()
            });

            return View();
        }
        

        //public JsonResult EmailExists(string email)
        //{
        //    var aemail = ctx.Users.Where(u => u.EmailAddress == email).FirstOrDefault();
        //    return Json(!String.Equals(email, aemail.EmailAddress, StringComparison.OrdinalIgnoreCase));
        //}


    }
}
      

        //    //static void EncryptData(String plainText, RijndaelManaged algo)
        //    //{
        //    //    string hold = String.Empty;
        //    //    byte[] plainDataArray = ASCIIEncoding.ASCII.GetBytes(plainText);
        //    //    ICryptoTransform transform = algo.CreateEncryptor();
        //    //    using (var fileStream = new FileStream(hold,FileMode.OpenOrCreate, FileAccess.Write))
        //    //    {
        //    //        using (var cryptoStream = new CryptoStream(fileStream, transform,
        //    //               CryptoStreamMode.Write))
        //    //        {
        //    //            cryptoStream.Write(plainDataArray, 0,plainDataArray.GetLength(0));
        //    //            Console.WriteLine("Encrypted data written to:D:\\CipherText.txt");
        //    //        }
        //    //    }
        //    //    string fi = hold;
        //    //}

        //    //RijndaelManaged symAlgo = new RijndaelManaged();
        //    //Console.WriteLine("Enter data to encrypt.");
        //    //   string dataToEncrypt = Console.ReadLine();
        //    //   EncryptData(dataToEncrypt, symAlgo);



        //}
        // }
    


