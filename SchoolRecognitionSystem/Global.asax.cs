﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using SchoolRecognitionSystem.Models;
using System.Web.Http;
using System.Web.Routing;
using SchoolRecognitionSystem.Infrastructure;

namespace SchoolRecognitionSystem
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

        }
        protected void FormsAuthentication_OnAuthenticate(Object sender, FormsAuthenticationEventArgs e)
        {
            if (FormsAuthentication.CookiesSupported == true)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        //let us take out the username now                
                        string username = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value).Name;
                        string roles = string.Empty;

                        //using (SchoolSubjectRecognitionContext entities = new SchoolSubjectRecognitionContext())
                        //{
                        //    User user = entities.Users.SingleOrDefault(u => u.UserName == username);
                        //    roles = user.UserRoles.FirstOrDefault().Role.Rolename;
                        //   // roles = user.Roles;
                        //}
                        //let us extract the roles from our own custom cookie


                        //Let us set the Pricipal with our user specific details
                        e.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(';'));
                    }
                    catch (Exception)
                    {
                        //somehting went wrong
                    }
                }
            }
        }
    }

    

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SchoolRecognitionAuthorizedAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var ctx = HttpContext.Current;
            // If the browser session or authentication session has expired...
            var httpCookie = ConstantsService.GetCookie(ConstantsService.CookieSchoolRecognitionTokenKey);
            if (httpCookie == null &&
                !filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    // For AJAX requests, we're overriding the returned JSON result with a simple string,
                    // indicating to the calling JavaScript code that a redirect should be performed.
                    filterContext.Result = new JsonResult {Data = "_Logon_"};

                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            {"Controller", "Home"},
                            {"Action", "Index"},
                            {"returnUrl", ctx.Request.Path + "?" + ctx.Request.QueryString}
                        });
                    /*filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            {"Controller", "Home"},
                            {"Action", "AD_Authentication"},
                            {"returnUrl", ctx.Request.Path + "?" + ctx.Request.QueryString}
                        });*/
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }

}
