
        jQuery.noConflict();
        $(document).ready(function () {
            $(function ($) {
                $("#StateID").on("change", function () {
                    var selectedStateId = $(this).val();

                    var options = {
                        url: '/School/RenderSelectLga?selectedId=' + selectedStateId,
                        type: "get"
                    };
                    $.ajax(options).done(function (data) {

                        var target = $("#LgaID");
                        target.children().remove();
                        target.html(data);
                    });
                });

                //  $("#StateId").trigger("change");
            });
        });
