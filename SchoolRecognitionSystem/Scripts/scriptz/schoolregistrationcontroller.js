﻿(function () {
    //'use strict';

    //var app = angular.module('app', []).service("PagerService", PagerService);
    //app.directive('myChange', function () {
    //    return {
    //        link: function link(scope, element) {
    //            element.bind('change', function () {
    //                alert('change on ' + element);
    //            });
    //        }
    //    }
    //});
    //app.filter('startFrom', function () {
    //    return function (input, start) {
    //        if (input) {
    //            start = +start; //parse to int
    //            return input.slice(start);
    //        }
    //        return [];
    //    }
    //});

    //app.controller('schoolregistrationcontroller', schoolregistrationcontroller);
    var app = angular.module('app', ['elif'])
.controller('schoolregistrationcontroller', ['$scope', '$q', '$rootScope', '$http', 'PagerService', schoolregistrationcontroller])
		.service("PagerService", PagerService);

    function schoolregistrationcontroller($scope, $q, $rootScope, $http, PagerService) {
        var vm = this;
        vm.reagents = [];
        vm.allreagents = [];
        vm.allfacility = [];
        vm.subjectid = null;
        vm.schid = 0;
        vm.page = 1;
        vm.ttt;
        vm.items = [];
        vm.schoolsubjects = [];
        vm.totaldigitvalue = 0;
        vm.totalfacilitydigitvalue = 0;
        vm.totaldigitvaluepp = 0;
        vm.pager = {};
        vm.loadeditems = false;
        vm.loadeditemsindex = [];
        //vm.response = [];
        //vm.showresponse = [];
        vm.finished = false;
        vm.itemscount = 0;
        vm.itemSaved = false;

        //functions
        vm.getschoolsubjects = getschoolsubjects;
        vm.sumdigitvalue = sumdigitvalue;
        vm.sumfacilitydigitvalue = sumfacilitydigitvalue;
        vm.getallreagents = getallreagents;
        vm.getallfacility = getallfacility;
        vm.initController = initController;
        vm.setPage = setPage;
        vm.addfacility = addfacility;
        vm.addreagent = addreagent;
        vm.clickk = clickk;

        
        //vm.sumdigitvalue();
        //vm.sumfacilitydigitvalue();

        //initController();

        function sumdigitvalue(item, v) {
           
            vm.totaldigitvalue = 0;
            for (var x = 0; x <= vm.allreagents.length; x++) {
                
                if (vm.allreagents[x] != null) {
                    vm.totaldigitvalue += vm.allreagents[x].DigitValue == null ? 0 : vm.allreagents[x].DigitValue;
                }
            }
            if (item != null && v !== "" && v!=null) {
                //save facility
                
                if (item != null) {
                    //alert(item.EquipmentValue);
                    vm.addreagent(item);
                }
            }
        }

        function sumfacilitydigitvalue(item, v, e, i, uniqueRow) {
            vm.totalfacilitydigitvalue = 0;
            console.log(item);
            console.log(v);
            console.log(e);
            console.log(i);
            console.log(uniqueRow);
            for (var x = 0; x <= vm.allfacility.length; x++) {
                if (vm.allfacility[x] != null) {
                    vm.totalfacilitydigitvalue += vm.allfacility[x].DigitValue == null ? 0 : vm.allfacility[x].DigitValue;
                }
            }
            // console.log(item.EquipmentValue);
            if (item != null && v != "") {
                //save facility
                if (item.EquipmentValue != null) {
                    vm.addfacility(item, e, i);
                }
            }

            //console.log(vm.totalfacilitydigitvalue);
        }

        function clickk(e) {
            console.log(e.target);
        }

        function getallreagents(sid) {
            //alert(sid);
            $http({
                method: 'GET',
                url: '../api/SRApi/GetAllReagentFacilityPerSchool?schid=' + vm.schid + '&subjectid=' + sid
            }).then(function (res) {
                vm.allreagents = res.data;
                if (vm.allreagents.length > 0) {
                    //initController(vm.allreagents);
                    vm.setPage(vm.page, vm.allreagents);
                    vm.sumdigitvalue();
                   // vm.totaldigitvalue = 0;
                    for (var i = 0; i < vm.allreagents.length; i++) {
                        vm.loadeditemsindex[i] = i++;
                        //vm.totaldigitvalue += vm.allreagents[i].DigitValue == null ? 0 : vm.allreagents[i].DigitValue;
                    }
                    vm.itemscount = vm.allreagents.length;

                }
            }, function (error) {
                console.log(error);
                vm.error = "An Error has occured while loading data!";
            });
        }

        function getallfacility(sid) {
            $http({
                method: 'GET',
                url: '../api/SRApi/GetAllEquipmentFacilityPerSchool?schid=' + vm.schid + '&subjectid=' + sid
            }).then(function(res) {
                vm.allfacility = res.data;
                if (vm.allfacility.length > 0) {
                    initController(vm.allfacility);
                    vm.sumfacilitydigitvalue(null,"",null,null);
                    //vm.totalfacilitydigitvalue = 0;
                    for (var i = 0; i <= vm.allfacility.length; i++) {
                        //vm.loadeditemsindex[i] = i++;
                        //vm.totalfacilitydigitvalue += vm.allfacility[i].DigitValue == null ? 0 : vm.allfacility[i].DigitValue;
                        
                    }
                    vm.itemscount = vm.allfacility.length;
                    //vm.allfacility.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    //params.total(vm.allfacility.length);
                }
            }, function(error) {
                console.log(error);
                vm.error = "An Error has occured while loading data!";
            });
          
        }

        function getschoolsubjects(sid) {
            vm.schid = sid;
            $http({
                method: 'GET',
                url: '../api/SRApi/GetSchoolSubjects?schid=' + sid
            }).then(function (res) {
                vm.schoolsubjects = res.data;
                console.log(vm.schoolsubjects);
                //for (var z = 0; z < vm.allfacility.length; z++) {
                //    vm.showresponse[z] = false;
                //    vm.response[z] = null;
                //}
            }, function (error) {
                console.log(error);
                vm.error = "An Error has occured while loading data!";
            });
        }

        function addreagent(item) {
            var success = false;
           
            $.ajax({
                //url: "/Recognition/Schools/SaveEquipment",
                url: '../api/SRApi/SaveReagent?schoolid=' + vm.schid,
                data: item,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var def = data.definition;
                    if (data.status === true) {
                        success = true;
                        var definitionValue = 0;
                        switch (def) {
                            case 'Excellent':
                                definitionValue = 1;
                                break;
                            default:
                                definitionValue = 0;
                        }
                        if (success) {
                            console.log(success);
                            var totalEquipmentItems = vm.itemscount;
                            //console.log(vm.itemscount);
                            var subjectsid = vm.subjectid.ID;
                            //console.log(vm.subjectid);
                            var totalsum = vm.totaldigitvalue;
                            //console.log(vm.totalfacilitydigitvalue);
                            var facilityCategoryId = $('#reagent').attr("data-reagentidd");
                            $.post('/Schools/EquipmentSummary', { totalEquipmentItems: totalEquipmentItems, subjectsid: subjectsid, totalsum: totalsum, facilityCategoryId: facilityCategoryId }, function (response) {
                                if (response.status == true) {
                                    //alert('data saved successfully');

                                } else {
                                    //alert('data not saved correctly');
                                }
                            });
                        }
                    } else {
                    }
                }

            });
        }

        function addfacility(item, e, i) {
            //vm.showresponse[i] = true;
            var success = false;
            $.ajax({
                //url: "/Recognition/Schools/SaveEquipment",
                url: '../api/SRApi/SaveEquipment?schoolid=' + vm.schid,
                data: item,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    var def = data.definition;
                    if (data.status === true) {
                        success = true;
                        $('[data-notifysaved="' + i + '"]').show();
                        var definitionValue = 0;
                        console.log(i);
                        switch (def) {
                            case 'Excellent':
                                definitionValue = 1;
                                break;
                            default:
                                definitionValue = 0;

                        }
                        if (success) {
                            console.log(success);
                            var totalEquipmentItems = vm.itemscount;
                            //console.log(vm.itemscount);
                            var subjectsid = vm.subjectid.ID;
                            //console.log(vm.subjectid);
                            var totalsum = vm.totalfacilitydigitvalue;
                            //console.log(vm.totalfacilitydigitvalue);
                            var facilityCategoryId = $('#equipment').attr("data-equipmentidd");
                            $.post('/Schools/EquipmentSummary', { totalEquipmentItems: totalEquipmentItems, subjectsid: subjectsid, totalsum: totalsum, facilityCategoryId: facilityCategoryId }, function (response) {
                                if (response.status == true) {
                                 //   alert('Data saved successfully');
                                } else {
                                  //  alert('Error saving data.');
                                }
                            });
                        }
                        //vm.response[i] = data; 
                        //vm.showresponse[i] = false;
                        //vm.finished = true;
                        //$timeout(function () {
                        //    vm.showresponse[i] = false;
                        //    vm.finished[i] = false;
                        //}, 1000);
                        //var elem = e;
                        //$('td#definition-' + definitionValue).html(definitionValue);
                        //$(elem).parent().find('.spinner-wrapper').addClass('fa-check-circle text-success').removeClass('fa-spin fa-spinner');
                        //$(elem).parent().find('.message-output').stop().css('opacity', 1).removeClass('text-danger')
                        //    .addClass('text-success').html(data.message).show().fadeOut(10000);

                        //$(elem).attr('data-recordId', data.recordId);
                        //console.log(data);
                        //isSuccess = data;
                        //sumGrades(subjectId);
                    } else {
                        //$(elem).parent().find('.message-output').stop().css('opacity', 1).html(data.message).removeClass('text-success').addClass('text-danger').show();
                        //vm.showresponse[i] = false;

                        //vm.finished[i] = true;
                        //$timeout(function () {
                        //    vm.finished[i] = false;
                        //}, 3000);
                    }
                }

            });
            
           

            //$http({
            //    method: 'POST',
            //    url: '../api/SRApi/SaveEquipment?schoolid=' + vm.schid,
            //    data: item
            //}).then(function (res) {
            //    vm.schoolsubjects = res.data;
            //    console.log(vm.schoolsubjects);
            //    getschoolsubjects();
            //    vm.getallfacility(vm.subjectid.ID);
            //}, function (error) {
            //    console.log(error);
            //    vm.error = "An Error has occured while loading data!";
            //});
        }

        function initController(data) {
            vm.setPage(1, data);
        }

        function setPage(page, data) {
            if (page < 1 || page > vm.pager.totalPages) {
                return;
            }
            // get pager object from service
            //console.log(vm.allreagents.length);
            vm.pager = PagerService.GetPager(data.length, page, 20);
            // get current page of items
            //console.log(vm.allreagents.length);
            vm.items = data.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
            //console.log(vm.items);
        }
    }

    //vm.getschoolsubjects(vm.schid);

    function PagerService() {
        //service definition
        var service = {};
        service.GetPager = GetPager;
        return service;
        function range(start, end) {
            var ans = [];
            for (let i = start; i <= end; i++) {
                ans.push(i);
            }
            return ans;
        }

        // service implementation
        function GetPager(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;
            // default page size is 10
            pageSize = pageSize || 20;
            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);
            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            }
            else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            var pages = range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }
    }

})();





//(function () {
//    //'use strict';

//    var app = angular.module('app', ['elif']).service("PagerService", PagerService);
//    //app.directive('myChange', function () {
//    //    return {
//    //        link: function link(scope, element) {
//    //            element.bind('change', function () {
//    //                alert('change on ' + element);
//    //            });
//    //        }
//    //    }
//    //});
//    //app.filter('startFrom', function () {
//    //    return function (input, start) {
//    //        if (input) {
//    //            start = +start; //parse to int
//    //            return input.slice(start);
//    //        }
//    //        return [];
//    //    }
//    //});
      
//    app.controller('schoolregistrationcontroller', schoolregistrationcontroller);

//    function schoolregistrationcontroller($scope, $timeout, $rootScope, $window, $http, $interval, PagerService)
//    {
//        var vm = this;
//        vm.reagents = [];
//        vm.allreagents = [];
//        vm.allfacility = [];
//        vm.subjectid = null;
//        vm.schid = 0;
//        vm.page = 1;
//        vm.ttt;
//        vm.items = [];
//        vm.schoolsubjects = [];       
//        vm.totaldigitvalue = 0;
//        vm.totalfacilitydigitvalue = 0;
//        vm.totaldigitvaluepp = 0;
//        vm.pager = {};
//        vm.loadeditems = false;
//        vm.loadeditemsindex = [];
//        //vm.response = [];
//        //vm.showresponse = [];
//        vm.finished = false;
//        vm.itemscount = 0;
        
//        //functions
//        vm.getschoolsubjects=getschoolsubjects;
//        vm.sumdigitvalue = sumdigitvalue;
//        vm.sumfacilitydigitvalue = sumfacilitydigitvalue;
//        vm.getallreagents = getallreagents;
//        vm.getallfacility = getallfacility;
//        vm.initController = initController;
//        vm.setPage = setPage;
//        vm.addfacility = addfacility;
//        vm.addreagent = addreagent;
//        vm.clickk = clickk;
        
//        //vm.getschoolsubjects();
//        vm.sumdigitvalue();
       
//        //initController();
        
//        function sumdigitvalue(item, v)
//        {
//            vm.totaldigitvalue = 0;
//            for (var x = 0; x < vm.allreagents.length; x++)
//            {
//                vm.totaldigitvalue += vm.allreagents[x].DigitValue == null ? 0 : vm.allreagents[x].DigitValue;
//            }
//            if (item != null && v != "") {
//                //save facility
//                if (item.EquipmentValue != null) {
//                    vm.addreagent(item);
//                }
//            }
//        }

//        function sumfacilitydigitvalue(item,v,e,i) {
//            vm.totalfacilitydigitvalue = 0;
//            for (var x = 0; x < vm.allfacility.length; x++) {
//                vm.totalfacilitydigitvalue += vm.allfacility[x].DigitValue == null ? 0 : vm.allfacility[x].DigitValue;
//            }
//            //console.log(e.target);
//            if (item!=null && v!="")
//            {
//                //save facility
//                if (item.EquipmentValue != null) {
//                    vm.addfacility(item,e,i);
//                }
//            }
           
//            //console.log(vm.totalfacilitydigitvalue);
//        }

//        function clickk(e)
//        {
//            console.log(e.target);
//        }

//        function getallreagents(sid) {
//            $http({
//                method: 'GET',
//                url: '../api/SRApi/GetAllReagentFacilityPerSchool?schid=' + vm.schid + '&subjectid=' + sid
//            }).then(function (res) {
//                vm.allreagents = res.data;
//                if (vm.allreagents.length > 0) {
//                    //initController(vm.allreagents);
//                    vm.setPage(vm.page, vm.allreagents);
//                    vm.sumdigitvalue();
//                    for (var i = 0; i < vm.allreagents.length; i++)
//                    {
//                        vm.loadeditemsindex[i] = i++;
//                    }
//                    vm.itemscount = vm.allreagents.length;
                    
//                }
//            }, function (error) {
//                console.log(error);
//                vm.error = "An Error has occured while loading data!";
//            });
//        }

//        function getallfacility(sid) {
//            $http({
//                method: 'GET',
//                url: '../api/SRApi/GetAllEquipmentFacilityPerSchool?schid=' + vm.schid + '&subjectid=' + sid
//            }).then(function (res) {
//                vm.allfacility = res.data;
//                if (vm.allfacility.length > 0) {
//                    initController(vm.allfacility);
//                    vm.sumfacilitydigitvalue();
//                    for (var i = 0; i < vm.allfacility.length; i++) {
//                        vm.loadeditemsindex[i] = i++;
//                    }
//                    vm.itemscount = vm.allfacility.length;
//                }
//            }, function (error) {
//                console.log(error);
//                vm.error = "An Error has occured while loading data!";
//            });
//        }
        
//        function getschoolsubjects(sid) {
//            alert(sid);
//            vm.schid = sid;
//            console.log("selected subject " + sid);
//            $http({
//                method: 'GET',
//                url: '../api/SRApi/GetSchoolSubjects?schid=' + sid
//            }).then(function (res) {
//                vm.schoolsubjects = res.data;
//                console.log(vm.schoolsubjects);
//                //for (var z = 0; z < vm.allfacility.length; z++) {
//                //    vm.showresponse[z] = false;
//                //    vm.response[z] = null;
//                //}
//            }, function (error) {
//                console.log(error);
//                vm.error = "An Error has occured while loading data!";
//            });
//        }

//        function addreagent(item)
//        {
//            $.ajax({
//                //url: "/Recognition/Schools/SaveEquipment",
//                url: '~/api/SRApi/SaveReagent?schoolid=' + vm.schid,
//                data: item,
//                type: 'POST',
//                dataType: 'json',
//                success: function (data) {
//                    console.log(data);
//                    var def = data.definition;
//                    if (data.status == true) {

//                        var definitionValue = 0;
//                        switch (def) {
//                            case 'Excellent':
//                                definitionValue = 1;
//                                break;
//                            default:
//                                definitionValue = 0;
//                        }                  
//                    } else {
//                    }
//                }

//            });
//        }

//        function addfacility(item,e,i)
//        {
//            //vm.showresponse[i] = true;
//            $.ajax({
//                //url: "/Recognition/Schools/SaveEquipment",
//                url: '../api/SRApi/SaveEquipment?schoolid=' + vm.schid,
//                data: item,
//                type: 'POST',
//                dataType: 'json',
//                success: function (data) {
//                    //console.log(data);
//                    var def = data.definition;
//                    if (data.status == true) {

//                        var definitionValue = 0;
//                        switch (def) {
//                            case 'Excellent':
//                                definitionValue = 1;
//                                break;
//                            default:
//                                definitionValue = 0;

//                        }
//                        //vm.response[i] = data; 
//                        //vm.showresponse[i] = false;
//                        //vm.finished = true;
//                        //$timeout(function () {
//                        //    vm.showresponse[i] = false;
//                        //    vm.finished[i] = false;
//                        //}, 1000);
//                        //var elem = e;
//                        //$('td#definition-' + definitionValue).html(definitionValue);
//                        //$(elem).parent().find('.spinner-wrapper').addClass('fa-check-circle text-success').removeClass('fa-spin fa-spinner');
//                        //$(elem).parent().find('.message-output').stop().css('opacity', 1).removeClass('text-danger')
//                        //    .addClass('text-success').html(data.message).show().fadeOut(10000);
                       
//                        //$(elem).attr('data-recordId', data.recordId);
//                        //console.log(data);
//                        //isSuccess = data;
//                        //sumGrades(subjectId);
//                    } else {
//                        //$(elem).parent().find('.message-output').stop().css('opacity', 1).html(data.message).removeClass('text-success').addClass('text-danger').show();
//                        //vm.showresponse[i] = false;

//                        //vm.finished[i] = true;
//                        //$timeout(function () {
//                        //    vm.finished[i] = false;
//                        //}, 3000);
//                    }
//                }

//            });


        
//        function initController(data)
//        {
//            vm.setPage(1, data);
//        }

//        function setPage(page, data)
//        {   
//            if (page < 1 || page > vm.pager.totalPages)
//            {
//                return;
//            }
//            // get pager object from service
//            //console.log(vm.allreagents.length);
//            vm.pager = PagerService.GetPager(data.length, page,20);
//            // get current page of items
//            //console.log(vm.allreagents.length);
//            vm.items = data.slice(vm.pager.startIndex, vm.pager.endIndex+1);
//            //console.log(vm.items);
//        }

       

//    }

//    function PagerService()
//    {
//        //service definition
//        var service = {};
//        service.GetPager = GetPager;
//        return service;
//        function range(start, end)
//        {
//            var ans = [];
//            for (let i = start; i <= end; i++) {
//                ans.push(i);
//            }
//            return ans;
//        }

//        // service implementation
//        function GetPager(totalItems, currentPage, pageSize)
//        {
//            // default to first page
//            currentPage = currentPage || 1;
//            // default page size is 10
//            pageSize = pageSize || 20;
//            // calculate total pages
//            var totalPages = Math.ceil(totalItems / pageSize);
//            var startPage, endPage;
//            if (totalPages <= 10) {
//                // less than 10 total pages so show all
//                startPage = 1;
//                endPage = totalPages;
//            }
//            else
//            {
//                // more than 10 total pages so calculate start and end pages
//                if (currentPage <= 6) {
//                    startPage = 1;
//                    endPage = 10;
//                } else if (currentPage + 4 >= totalPages) {
//                    startPage = totalPages - 9;EquipmentSummary
//                    endPage = totalPages;
//                } else {
//                    startPage = currentPage - 5;
//                    endPage = currentPage + 4;
//                }
//            }

//            // calculate start and end item indexes
//            var startIndex = (currentPage - 1) * pageSize;
//            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

//            // create an array of pages to ng-repeat in the pager control
//            var pages = range(startPage, endPage + 1);

//            // return object with all pager properties required by the view
//            return {
//                totalItems: totalItems,
//                currentPage: currentPage,
//                pageSize: pageSize,
//                totalPages: totalPages,
//                startPage: startPage,
//                endPage: endPage,
//                startIndex: startIndex,
//                endIndex: endIndex,
//                pages: pages
//            };
//        }
//    }

//})();